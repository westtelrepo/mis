'use strict';
const uuid = require('node-uuid'),
      util = require('util'),
      assert = require('assert'),
      moment = require('moment-timezone'),
      fs = require('fs');

const gc_stats = require('gc-stats')();

//gc_stats.on('stats', function(stats) {
	//log({'type': 'log/gc/', 'gc-stats': stats});
//});

const wutil = require('./wutil.js');

const instance = uuid.v4();
let instance_num = 0;

const sutc = moment().utc();
// TODO: configure, create directory
const fs_logfile = fs.createWriteStream('/home/westtel/westtel_reports_logs/' + sutc.format('YYYY-MM-DD_HH:mm:ss.SSS') + '_' + instance + '.jlog', { flags: 'w' });

function log(data, special) {
	try {
		if (typeof data === 'string')
			data = {code: data};
		data.instance = {
			iid: instance,
			iid_num: instance_num++
		};
		assert(instance_num < Number.MAX_SAFE_INTEGER - 1000);

		data.id = uuid.v4();
		data.utc = moment().utc().format('YYYY-MM-DD HH:mm:ss.SSS');
		const hrtime = process.hrtime();
		data.hrtime = hrtime[0] + hrtime[1] * 1e-9;

		if (special !== undefined) {
			assert(typeof special === 'object' && special !== null);
			if (special.req) {
				const request = {};
				if (special.req.baseUrl)
					request.baseUrl = special.req.baseUrl;
				if (special.req.ip)
					request.ip = special.req.ip;
				if (special.req.ips)
					request.ips = special.req.ips;
				if (special.req.originalUrl)
					request.originalUrl = special.req.originalUrl;
				if (special.req.params)
					request.params = special.req.params;
				if (special.req.query)
					request.query = special.req.query;
				if (data.request === undefined) data.request = {};
				Object.assign(data.request, request);

				if (special.req.session && special.req.session.user) {
					if (data.user === undefined) data.user = {};
					data.user.uid = special.req.session.user.uid;
				}
			}	
			if (special.res) {
				if (special.res.locals && special.res.locals.requestUUID) {
					if (data.request === undefined)
						data.request = {};
					data.request.rid = special.res.locals.requestUUID;
					data.request.rid_num = special.res.locals.requestUUID_num++;
					assert(data.request.rid_num < Number.MAX_SAFE_INTEGER - 1000);
				}
				if (special.res.locals && special.res.locals.user) {
					if (data.user === undefined) data.user = {};
					data.user.uid = special.res.locals.user.uid;
					data.user['name?'] = special.res.locals.user.username;
					data.user['tz?'] = special.res.locals.user.timezone;
					if (special.res.locals.user.superadmin)
						data.user['superadmin?'] = true;
				}
			}
		}
		fs_logfile.write(JSON.stringify(data) + '\n');
	} catch(e) {
		// all else fails
		console.log('All else failed in logging system: ' + util.inspect(e, {showHidden: true, depth: null, customInspect: false}));
		console.log(e.stack);
	}
}
exports.log = log;

exports.instance_id = function instance_id() {
	return instance;
}

log({'code': 'log/start/'});
