'use strict';
const assert = require('assert');

const bcrypt = require('bcrypt'),
      co = require('co'),
      pg = require('pg');
const uuid = require('node-uuid');

// this file has some nice utilities/wrappers
// a lot turns callbacks into promises (easier to use)

exports.pg_connect = co.wrap(function*(conStr, fn) {
	const obj = yield new Promise(function(resolve, reject) {
		pg.connect(conStr, function(err, client, done) {
			if (err) reject(err);
			else resolve({'client': client, 'done': done});
		});
	});

	try {
		yield fn(obj.client);
	} catch(e) {
		obj.done(e);
		throw e;
	}
	obj.done();
});

exports.pg_connect_ser_ro_def = co.wrap(function*(conStr, fn) {
	yield exports.pg_connect(conStr, co.wrap(function*(client) {
		yield exports.client_query(client, 'BEGIN ISOLATION LEVEL SERIALIZABLE, READ ONLY, DEFERRABLE');
		yield fn(client);
		yield exports.client_query(client, 'COMMIT'); // TODO: this should not be the default, but readonly
	}));
});

exports.client_query = function(client, query, params) {
	return new Promise(function(resolve, reject) {
		client.query(query, params, function(err, result) {
			if (err) reject(err);
			else resolve(result);
		});
	});
};

// later objects have priority, i.e. replace same-named properties of earlier objects
// TODO: can actually be replaced with Object.assign() in many cases,
//       though that overwrites its argument
exports.object_merge = function object_merge() {
	const ret = {}; // chart.js requires ret to have object prototype (i.e. hasOwnProperty)
	for (let i = 0; i < arguments.length; i++) {
		for (const name in arguments[i]) {
			if (Object.prototype.hasOwnProperty.call(arguments[i], name))
				ret[name] = arguments[i][name];
		}
	}
	return ret;
}

exports.wait = function(milli) {
	return new Promise(function(resolve, reject) { setTimeout(resolve, milli); });
}

// NaN val returns NaN
exports.clamp = function(lower, val, upper) {
	assert(typeof lower === 'number');
	assert(typeof upper === 'number');
	assert(!isNaN(lower));
	assert(!isNaN(upper));
	const x = Number(val);
	if (x < lower) return lower;
	else if (x > upper) return upper;
	else return x;
}

exports.bcrypt_compare = function(password, hash) {
	return new Promise(function(resolve, reject) {
		bcrypt.compare(password, hash, function(err, result) {
			if (err) reject(err);
			else resolve(result);
		});
	});
}

exports.bcrypt_hash = function(password, strength) {
	return new Promise(function(resolve, reject) {
		bcrypt.hash(password, strength, function(err, hash) {
			if (err) reject(err);
			else resolve(hash);
		});
	});
}

const zlib = require('zlib');

exports.zlib_deflateRaw = function(buf, options) {
	return new Promise(function(resolve, reject) {
		zlib.deflateRaw(buf, options, function(err, result) {
			if (err) reject(err);
			else resolve(result);
		});
	});
};

exports.zlib_inflateRaw = function(buf, options) {
	return new Promise(function(resolve, reject) {
		zlib.inflateRaw(buf, options, function(err, result) {
			if (err) reject(err);
			else resolve(result);
		});
	});
};
