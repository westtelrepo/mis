'use strict';
const wutil = require('./wutil.js');

const co = require('co'),
      co_express = require('co-express'),
      Handlebars = require('handlebars'),
      urlsafe_base64 = require('urlsafe-base64');
const pg_format = require('pg-format');
const moment = require('moment-timezone');
const log = require('./logger.js').log;

const assert = require('assert'),
      util = require('util');

const client_query = wutil.client_query;

// ------------------------------- CONSTANTS ------------------------------
const noDataStr = '\u2014'; // U+2014 EM DASH

// ------------------------------HELPER FUNCTIONS--------------------------
//
// result is pg.query result
// new_titles is map from result names to pretty result names
// return is like [[{text:""},{text:""}]]
function result_title_map(result, new_titles) {
	return [result.fields.map(x => {
		if (Object.prototype.hasOwnProperty.call(new_titles, x.name))
			return { text: new_titles[x.name] };
		else
			return { text: x.name};
	})];
}

// result is pg.query result
// funcs is a map from result field names to functions that "prettify" results
// return is like [ [{text:"",...}, ...], ... ]
// funcs[i]() returns {text:"",...}, string converted to such
function result_table(result, funcs) {
	if (funcs === undefined)
		funcs = {};
	const names = result.fields.map(x => x.name);

	const ret = [];
	for (let i = 0; i < result.rows.length; i++) {
		const current = [];
		for (let j = 0; j < names.length; j++) {
			if (Object.prototype.hasOwnProperty.call(funcs, names[j])) {
				const text = funcs[names[j]](result.rows[i][names[j]]);
				if (typeof text === 'string')
					current.push({'text': text});
				else if (typeof text === 'object')
					current.push(text);
				else
					assert(false);
			} else {
				current.push({'text': result.rows[i][names[j]]});
			}
		}
		ret.push(current);
	}
	return ret;
}

function table_to_csv(table) {
	return table.map(row => (row.map(x => {
		// TODO: do we need the first replace anymore?
		// TODO: also should probably convert non-ascii to ? or similar
		return String(x.text).replace('<br />', ' ').replace('\u2014', '').replace('\n', ' ');
	}).join(',') + '\r\n')).join('');
}

module.exports = co_express(function*(req, res) {
yield wutil.pg_connect_ser_ro_def(process.env.WT_DATA, co.wrap(function*(client) {
yield wutil.pg_connect_ser_ro_def({}, co.wrap(function*(client_std) {
	const type = req.query.type;
	let psap = req.query.psap;
	// "from" and "to" are inclusive "days"
	let time_from = req.query.from;
	let time_to = req.query.to;
	// 'user_tz' is a special value meaning "the user's timezone"
	const tz = req.query.tz === 'user_tz' ? res.locals.user.timezone : req.query.tz;

	// SHOULD we cache this? (especially uid_has_psap)
	const first_query = yield wutil.client_query(client_std,
		`SELECT uid_has_psap($1, $2) AS is_allowed,
		        TO_CHAR($3::timestamp, 'MM/DD/YYYY') AS first_day,
			TO_CHAR($4::timestamp, 'MM/DD/YYYY') AS last_day,
			(SELECT nice FROM psaps WHERE psap = $2) AS nice`,
		[res.locals.user.uid, psap, time_from, time_to]);

	// TODO: long-term, it is best to have security in layers.
	// PostgreSQL 9.5 introduced row-level securtiy, so we could make it
	// so that this connection can not even access other psaps
	// as a first line of defense against SQL injection.
	// of course you don't let SQL injection happen but things happen,
	// it is best to be prepared
	//
	// one thing we already do is have a readonly transaction
	// which helps with efficiency too (and SQL injection can not edit)
	if (!first_query.rows[0].is_allowed) {
		log({'code':'nauth/psap/','attempted_psap':psap}, {req:req,res:res});
		throw Error(`User '${res.locals.user.username} (${res.locals.user.uid})' is not allowed to access psap '${psap}'`);
	}
	time_from = first_query.rows[0].first_day;
	time_to = first_query.rows[0].last_day;

	// TODO: just use moment.js (and format when necessary)
	let moment_from = moment.tz(time_from, 'MM/DD/YYYY', tz);
	let moment_to = moment.tz(time_to, 'MM/DD/YYYY', tz);
	let adjustment_warning = '';

	// psap fudging for "fake" data
	// YES, this is done after the authorization check
	let use_fake_calltaker = false;
	if (psap === 'CO-999-1') {
		psap = 'MT-120-1';
		use_fake_calltaker = true;
	}
	// pretend psap is constant from now on
	// TODO: actually make it so

	// TODO: eliminate these two constants
	const backend = 'basic_call_info2';
	const b = 1;

	function callTypeParams(type) {
		if (type === '911') {
			return {
				where: `(is_911 = TRUE AND incoming = TRUE AND transfer_type = 'initial')`,
				titlePrefix: '9-1-1 Inbound Calls \u2013 ',
				abanName: 'Abandoned',
			};
		} else if (type === '911t') {
			return {
				where: `(is_911 = TRUE AND incoming = TRUE AND transfer_type != 'initial')`,
				titlePrefix: '9-1-1 Inbound PSAP Transfers \u2013 ',
				abanName: 'Failed Transfer',
			};
		} else if (type === 'admin') {
			return {
				where: `(is_911 = FALSE AND incoming = TRUE AND transfer_type = 'initial')`,
				titlePrefix: 'Admin Inbound Calls \u2013 ',
				abanName: 'Failed to Answer',
			};
		} else if (type === 'admint') {
			return {
				where: `(incoming = TRUE AND is_911 = FALSE AND transfer_type != 'initial')`,
				titlePrefix: 'Inbound Admin PSAP Transfers \u2013 ',
				abanName: 'Failed Transfer',
			};
		} else if (type === 'total') {
			return {
				where: `(incoming = TRUE AND transfer_type = 'initial')`,
				titlePrefix: 'Total Inbound Calls \u2013 ',
				abanName: 'Abandoned',
				specialAban: true,
			};
		} else if (type === 'totalt') {
			return {
				where: `(incoming = TRUE AND transfer_type != 'initial')`,
				titlePrefix: 'Total Inbound PSAP Transfers \u2013 ',
				abanName: 'Failed Transfer',
			};
		} else
			throw new Error(`unknown calltype '${type}'`);
	}

	// TODO: split major_type & minor_type
	if (req.query.minor_type === undefined)
		req.query.minor_type = '';
	const ctp = callTypeParams(req.query.major_type + req.query.minor_type);

	const psap_name = first_query.rows[0].nice;

	let table;
	let table_title;
	let table_footer;
	let graph;
	let title;

	// poor man's inline functions
	const time_within_days = "(utc >= (date_trunc('day', $2::timestamp) AT TIME ZONE $1) AND utc < (date_trunc('day', $3::timestamp) + '1 day'::interval) AT TIME ZONE $1)";
	const psap_where = b === 0 ? 'psap = $4' : '(psap = $4 OR controller = $4)';

	function pg_format_percent(value, total) {
		// value IS NOT ESCAPED (i.e. should be an SQL expression)
		// total IS (i.e. is a constant value, probably gotten from an earlier query)
		return pg_format(`COALESCE(TO_CHAR(100 * (%s)::numeric / NULLIF(%L,0), 'FM990.0%'), %L)`, value, total, noDataStr);
	}

	function fake_calltaker(calltaker) {
		// calltaker not escaped, is SQL expression
		const names = ['madams', 'gfrank', 'tmason', 'asmith'];
		return pg_format(`(CASE (%s) WHEN 'aharbrige' THEN %L WHEN 'MThomas' THEN %L WHEN 'sbruso' THEN %L WHEN 'TRICHARDSON' THEN %L WHEN 'acianflone' THEN %L ELSE %L END)`, calltaker, names[0], names[1], names[2], names[3], names[1], names[2]);
	}

	if (type === 'time') {
		// TODO: this entire "time" section needs refactoring
		const split = req.query.timeSplit;

		const query_middle = `,avg(cnt) AS average,
			max(cnt) AS max,
			sum(cnt) AS total,
			sum(cnt) AS "Percentage",
			sum(tandem) AS "Tandem Transfers",
			sum(ng911) AS "NG911 Transfers",
			sum(blind) AS "Blind Transfers",
			sum(attended) AS "Attended Transfers",
			sum(conference) AS "Conference Transfers",
			sum(transfers) AS "Total Transfers"`;

		const time_titles = {
			'total': 'Total Call Count'
		};

		let summary_total;

		const transform = {
			'average': x => { if (x) return Number(x).toFixed(1); else return noDataStr; },
			'max': x => { if (x) return Number(x).toFixed(); else return noDataStr; },
			'total': x => { if (x) return Number(x).toFixed(); else return noDataStr; },
			'Percentage': x => {
				if (x === noDataStr || x === '100%') return x;
				else if (Number(summary_total) === 0) return noDataStr;
				else if (x) return (100 * Number(x) / summary_total).toFixed(1) + '%';
				else return noDataStr;
			},
			'Tandem Transfers': x => { if (x) return x; else return noDataStr; },
			'NG911 Transfers': x => { if (x) return x; else return noDataStr; },
			'Blind Transfers': x => { if (x) return x; else return noDataStr; },
			'Attended Transfers': x => { if (x) return x; else return noDataStr; },
			'Conference Transfers': x => { if (x) return x; else return noDataStr; },
			'Total Transfers': x => { if (x) return x; else return noDataStr; },
		};

		// TODO: refactor out, probably don't need
		function formatGraphNumber(x) {
			if (x === null) return null;
			const ret = Number(x);
			if (isNaN(ret)) return null;
			return Number(ret.toFixed(1));
		}

		if (split === 'HourOfDay') {
			const query_start = `WITH every_hour AS (
				SELECT h.hour AS hour,
				COUNT(callid) AS cnt,
				COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) AS tandem,
				COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS ng911,
				COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS blind,
				COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS attended,
				COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS conference,
				COALESCE(SUM((transfer_reaction != 'none')::int), 0) AS transfers
				FROM generate_series(date_trunc('day', $2::timestamp),
					date_trunc('day', $3::timestamp) + '23 hours', '1 hour') h(hour)
				LEFT JOIN ${backend}
				ON h.hour = date_trunc('hour', utc AT TIME ZONE $1)
				AND ${psap_where} AND ${ctp.where}
				AND utc >= date_trunc('day', $2::timestamp) AT TIME ZONE $1
				AND utc < (date_trunc('day', $3::timestamp) + '1 day'::interval) AT TIME ZONE $1
				GROUP BY hour ORDER BY hour
			)`;

			const summary = yield client_query(client,
				`${query_start}
				SELECT ${pg_format('%L',noDataStr)} ${query_middle}
				FROM every_hour`,
				[tz, time_from, time_to, psap]
			);
			summary_total = summary.rows[0].total;
			if (Number(summary_total) === 0)
				summary.rows[0]['Percentage'] = noDataStr;

			const result = yield client_query(client,
				`${query_start}
				SELECT (h.hour || ':00') AS hour
				${query_middle}
				FROM generate_series(0,23) h(hour) LEFT JOIN every_hour
				ON h.hour = EXTRACT(HOUR FROM every_hour.hour)
				GROUP BY h.hour
				ORDER BY h.hour`,
				[tz, time_from, time_to, psap]
			);

			table_title = result_title_map(result, wutil.object_merge(time_titles, {
				'hour': 'Hour',
				'average': 'Average Call Count',
				'max': 'Highest Call Count',
				'total': 'Total Call Count'
			}));
			table = result_table(result, transform);
			table_footer = result_table(summary, transform);

			graph = {
				type: 'line',
				datasets: [{
					name:'A',
					data: result.rows.map(x => formatGraphNumber(x.average))
				}],
				labels: result.rows.map(x => x.hour),
				xLabel: 'Hour of Day',
				yLabel: 'Average Call Count',
			};

			title = `${ctp.titlePrefix}Calls by Hour of Day`;
		} else if (split === 'DayOfWeek') {
			const query_with = `WITH every_day AS (
				SELECT d.day AS day,
				COUNT(callid) AS cnt,
				COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) AS tandem,
				COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS ng911,
				COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS blind,
				COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS attended,
				COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS conference,
				COALESCE(SUM((transfer_reaction != 'none')::int), 0) AS transfers
				FROM generate_series(date_trunc('day', $2::timestamp), date_trunc('day', $3::timestamp), '1 day') d(day)
				LEFT JOIN ${backend}
				ON d.day = date_trunc('day', utc AT TIME ZONE $1)
				AND ${psap_where} AND ${ctp.where}
				AND ${time_within_days}
				GROUP BY day ORDER BY day
			)`;
			const summary = yield client_query(client,
				`${query_with}
				SELECT ${pg_format('%L',noDataStr)}
				${query_middle}
				FROM every_day`,
				[tz, time_from, time_to, psap]
			);
			summary_total = summary.rows[0].total;
			if (Number(summary_total) === 0)
				summary.rows[0]['Percentage'] = noDataStr;

			const result = yield client_query(client,
				`${query_with}
				SELECT TO_CHAR('2015-11-01'::timestamp + (d.day || ' days')::interval, 'FMDay') AS day
				${query_middle}
				FROM generate_series(0,6,1) d(day) LEFT JOIN every_day
				ON d.day = EXTRACT(DOW FROM every_day.day)
				GROUP BY d.day ORDER BY d.day`,
				[tz, time_from, time_to, psap]
			);


			table_title = result_title_map(result, wutil.object_merge(time_titles, {
				'day': 'Day',
				'average': 'Average Call Count',
				'max': 'Highest Call Count',
				'total': 'Total Call Count'
			}));

			table = result_table(result, transform);
			table_footer = result_table(summary, transform);

			graph = {
				type: 'line',
				datasets: [{name:'A', data: result.rows.map(x => formatGraphNumber(x.average))}],
				labels: result.rows.map(x => x.day),
				xLabel: 'Day of Week',
				yLabel: 'Average Call Count',
			};
			title = `${ctp.titlePrefix}Calls by Day of Week`;
		} else if (split === 'MonthOfYear') {
			const query_with = `WITH every_month AS (
				SELECT m.month AS month,
				COUNT(callid) AS cnt,
				COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) AS tandem,
				COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS ng911,
				COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS blind,
				COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS attended,
				COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS conference,
				COALESCE(SUM((transfer_reaction != 'none')::int), 0) AS transfers
				FROM generate_series(date_trunc('month', $2::timestamp),
					date_trunc('month', $3::timestamp) , '1 month') m(month)
				LEFT JOIN ${backend}
				ON m.month = date_trunc('month', utc AT TIME ZONE $1)
				AND ${psap_where} AND ${ctp.where}
				AND ${time_within_days}
				GROUP BY month ORDER BY month
			)`;

			const summary = yield client_query(client,
				`${query_with}
				SELECT '\u2014'
				${query_middle}
				FROM every_month`,
				[tz, time_from, time_to, psap]
			);
			summary_total = summary.rows[0].total;
			if (Number(summary_total) === 0)
				summary.rows[0]['Percentage'] = noDataStr;
			const result = yield client_query(client,
				`${query_with}
				SELECT TO_CHAR('2015-12-01'::timestamp + (m.month || ' months')::interval, 'FMMonth') AS "Month"
				${query_middle}
				FROM generate_series(1,12,1) m(month) LEFT JOIN every_month
				ON m.month = EXTRACT(MONTH FROM every_month.month)
				GROUP BY m.month ORDER BY m.month`,
				[tz, time_from, time_to, psap]
			);

			table_title = result_title_map(result, wutil.object_merge(time_titles, {
				'month': 'Month',
				'average': 'Average Call Count',
				'max': 'Highest Call Count'
			}));
			table = result_table(result, transform);
			table_footer = result_table(summary, transform);

			graph = {
				type: 'line',
				datasets: [{name:'A',data:result.rows.map(x => formatGraphNumber(x.average))}],
				labels: result.rows.map(x => x[result.fields[0].name]),
				xLabel: 'Month of Year',
				yLabel: 'Average Call Count'
			};

			title = `${ctp.titlePrefix}Calls by Month of Year`;
		} else if (split === 'week') {
			const query_from = `FROM generate_series(date_trunc('week', $2::timestamp + '1 days'::interval) - '1 days'::interval,
				date_trunc('week', $3::timestamp + '1 days'::interval) - '1 days'::interval, '1 week') w(week)
				LEFT JOIN ${backend}
				ON w.week = date_trunc('week', (utc AT TIME ZONE $1) + '1 day'::interval) - '1 day'::interval
				AND ${psap_where}
				AND ${time_within_days}
				AND ${ctp.where}`;

			const transfer_totals = `COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
				COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
				COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
				COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
				COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
				COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"`;

			const summary = yield client_query(client,
				`SELECT '\u2014',
				COUNT(callid) AS total, '100%' AS "Percentage", ${transfer_totals}
				${query_from}`,
				[tz, time_from, time_to, psap]
			);
			summary_total = summary.rows[0].total;
			if (Number(summary_total) === 0)
				summary.rows[0]['Percentage'] = noDataStr;

			const result = yield client_query(client,
				`SELECT TO_CHAR(w.week, 'FMMM/DD/YYYY') AS week,
				COUNT(callid) AS total, ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage", ${transfer_totals}
				${query_from}
				GROUP BY w.week ORDER BY w.week`,
				[tz, time_from, time_to, psap]
			);

			table_title = result_title_map(result, {
				'week': 'Week Of',
				'total': 'Total Call Count'
			});
			table = result_table(result);
			table_footer = result_table(summary);

			const labels = result.rows.map(x => x[result.fields[0].name]);
			graph = {
				type: 'line',
				datasets: [{name:'A', data: result.rows.map(x => x[result.fields[1].name])}],
				labels: labels,
				xLabel: 'Week',
				yLabel: 'Total Call Count'
			};
			title = `${ctp.titlePrefix}Calls by Week`;
		}
	} else if (type === 'cos' || type === 'cos_type') {
		let last_cos;

		const cosDetail = type === 'cos' ? 'detail' : 'summary';
		if (cosDetail === 'summary')
			last_cos = 'cos_group';
		else
			last_cos = '';

		const where = `WHERE ${ctp.where}
			AND ${time_within_days}
			AND ${psap_where}`;

		const summary = yield client_query(client,
			`SELECT 'All', COUNT(callid) AS "Total Calls", '100%' AS "Percentage"
			FROM ${backend}
			${where}`,
			[tz, time_from, time_to, psap]
		);
		const summary_total = summary.rows[0]['Total Calls'];

		const result = yield client_query(client,
			`SELECT ${last_cos}(COALESCE(${b === 0 ? 'last_cos' : 'cos'}, '${b === 0 ? 'NRF' : 'No ALI Received'}')) AS "Class of Service", COUNT(callid) AS "Call Count", ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage"
			FROM ${backend}
			${where}
			GROUP BY "Class of Service" ORDER BY "Class of Service"`,
			[tz, time_from, time_to, psap]
		);
		for (let i = 0; i < result.rowCount; i++) {
			if (result.rows[i]['Class of Service'] === 'Unknown') {
				let p = result.rows.splice(i, 1);
				result.rows.push(p[0]);
			}
		}
		if (result.rows.length === 0)
			summary.rows[0]['Percentage'] = noDataStr;

		let title_map = {};
		if (cosDetail === 'summary') title_map['Class of Service'] = 'COS Group';
		table_title = result_title_map(result, title_map);
		table = result_table(result);
		table_footer = result_table(summary);

		if (cosDetail === 'summary')
			title = `${ctp.titlePrefix}Calls by Class of Service Summary`;
		else
			title = `${ctp.titlePrefix}Calls by Class of Service`;

		graph = {
			type: 'hbar',
			datasets: [{name:'A', data:result.rows.map(x => x[result.fields[1].name])}],
			labels: result.rows.map(x => x[result.fields[0].name]),
			xLabel: 'Call Count',
			yLabel: cosDetail === 'summary' ? 'COS Group' : 'Class of Service'
		};
		if (type === 'cos_type') graph.type = 'pie';
	} else if (type === 'position') {
		const where = `WHERE ${time_within_days}
			AND ${psap_where}
			AND ${ctp.where}`;

		const summary = yield client_query(client,
			`SELECT 'All', COUNT(callid) AS "Total Calls", '100%' AS "Percentage",
			COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
			COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
			COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
			COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
			COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
			COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"
			FROM ${backend}
			${where}`,
			[tz, time_from, time_to, psap]
		);
		const summary_total = summary.rows[0]['Total Calls'];

		const pos = b === 0 ? 'first_position' : 'pos';
		const result = yield client_query(client,
			`SELECT COALESCE(${b === 0 ? 'first_position' : 'pos'}::text, 'Unknown Position') AS "Position", COUNT(callid) AS "Call Count", ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage",
			COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
			COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
			COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
			COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
			COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
			COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"
			FROM ${backend}
			${where} AND abandoned = FALSE
			GROUP BY ${pos} ORDER BY ${pos}`,
			[tz, time_from, time_to, psap]
		);
		if (result.rows.length === 0)
			summary.rows[0]['Percentage'] = noDataStr;

		const result_aban = yield client_query(client,
			`SELECT ${pg_format('%L', ctp.abanName)}, COUNT(callid) AS "Call Count", ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage",
			COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
			COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
			COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
			COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
			COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
			COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"
			FROM ${backend}
			${where} AND abandoned = TRUE${ctp.specialAban ? ' AND is_911' : ''}`,
			[tz, time_from, time_to, psap]
		);

		let result_fail;
		if (ctp.specialAban) {
			result_fail = yield client_query(client,
				`SELECT 'Failed to Answer', COUNT(callid) AS "Call Count", ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage",
				COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
				COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
				COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
				COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
				COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
				COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"
				FROM ${backend}
				${where} AND abandoned = TRUE AND is_911 = FALSE`,
				[tz, time_from, time_to, psap]
			);
		}

		table_title = result_title_map(result, {});
		table = result_table(result).concat(result_table(result_aban));
		if (result_fail) {
			table = table.concat(result_table(result_fail));
		}
		table_footer = result_table(summary);

		title = `${ctp.titlePrefix}Calls by Position`;

		graph = {
			type: 'bar',
			datasets: [{name:'A',data:table.map(x => x[1].text)}],
			labels: table.map(x => x[0].text),
			xLabel: 'Call Count',
			yLabel: 'Position'
		};
	} else if (type === 'calltaker') {
		const summary = yield client_query(client,
			`SELECT 'All', COUNT(callid) AS "Call Volume", '100%' AS "Percentage",
			COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
			COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
			COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
			COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
			COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
			COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"
			FROM ${backend}
			WHERE ${time_within_days}
			AND ${psap_where} AND ${ctp.where}`,
			[tz, time_from, time_to, psap]
		);
		const summary_total = summary.rows[0]['Call Volume'];

		const expr = `NULLIF(lower(${b === 0 ? 'first_' : ''}calltaker), '')`;

		const result = yield client_query(client,
			pg_format(`SELECT COALESCE(%s, 'Unknown') AS calltaker, COUNT(callid) AS total, ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage",
			COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
			COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
			COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
			COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
			COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
			COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"
			FROM ${backend}
			WHERE ${time_within_days}
			AND ${psap_where}
			AND abandoned = FALSE
			AND ${ctp.where}
			GROUP BY 1 ORDER BY 1`, use_fake_calltaker ? fake_calltaker(expr) : expr),
			[tz, time_from, time_to, psap]
		);
		if (result.rows.length === 0)
			summary.rows[0]['Percentage'] = noDataStr;

		const abandoned = yield client_query(client,
			`SELECT ${pg_format('%L', ctp.abanName)}, COUNT(callid) AS total, ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage",
			COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
			COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
			COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
			COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
			COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
			COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"
			FROM ${backend}
			WHERE ${time_within_days}
			AND ${psap_where}
			AND abandoned = TRUE
			AND ${ctp.where}${ctp.specialAban ? ' AND is_911': ''}`,
			[tz, time_from, time_to, psap]
		);

		let result_fail;
		if (ctp.specialAban) {
			result_fail = yield client_query(client,
				`SELECT 'Failed to Answer', COUNT(callid) AS total, ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage",
				COALESCE(SUM((transfer_reaction = 'tandem')::int), 0) as "Tandem Transfer",
				COALESCE(SUM((transfer_reaction = 'ng911')::int), 0) AS "NG911 Transfers",
				COALESCE(SUM((transfer_reaction = 'blind')::int), 0) AS "Blind Transfers",
				COALESCE(SUM((transfer_reaction = 'attended')::int), 0) AS "Attended Transfers",
				COALESCE(SUM((transfer_reaction = 'conference')::int), 0) AS "Conference Transfers",
				COALESCE(SUM((transfer_reaction != 'none')::int), 0) as "Total Transfers"
				FROM ${backend}
				WHERE ${time_within_days}
				AND ${psap_where}
				AND abandoned = TRUE
				AND ${ctp.where} AND is_911 = FALSE`,
				[tz, time_from, time_to, psap]
			);
		}

		table_title = result_title_map(result, {
			'calltaker': 'Call-Taker\n(Windows Login)',
			'total': 'Total Call Count'
		});
		table = result_table(result).concat(result_table(abandoned));
		if (result_fail) {
			table = table.concat(result_table(result_fail));
		}
		table_footer = result_table(summary);

		title = `${ctp.titlePrefix}Calls by Call-Taker`;

		graph = {
			type: 'hbar',
			datasets: [{name:'A',data:table.map(x => x[1].text)}],
			labels: table.map(x => x[0].text),
			xLabel: 'Total Call Count',
			yLabel: 'Call-Taker (Windows Login)'
		};
	} else if (type === 'aat_time' || type === 'aat_calltaker') {
		// TODO: configure this, it'll be easy
		// for now always have two columns
		const num_seconds_under = 10;
		const red_under = 0.90;
		const num_seconds_under_b = 20;
		const red_under_b = 0.95;

		// some weirdness---we add an abandoned column for aat_time,
		// and an abandoned row for aat_calltaker
		// why? the latter makes counts match other reports
		// and the first is useful
		//
		// TODO: are abandoned calls answered?

		// NOTE: we throw out call_length's less than 0 and greater than 23 hours

		const query_middle = `,COALESCE(TO_CHAR(EXTRACT(EPOCH FROM avg(answer)), 'FM99999999999999990.0'), ${pg_format('%L', noDataStr)}) AS aat,
			SUM((answer < ${pg_format('%L', num_seconds_under + ' seconds')})::int) / SUM((abandoned = FALSE)::int)::numeric AS ltn,
			SUM((answer < ${pg_format('%L', num_seconds_under_b + ' seconds')})::int) / SUM((abandoned = FALSE)::int)::numeric AS ltnb,
			COALESCE(TO_CHAR(EXTRACT(EPOCH FROM max(answer)), 'FM9999999999999999990.0'), ${pg_format('%L', noDataStr)}) AS max_at,
			COALESCE(TO_CHAR(EXTRACT(EPOCH FROM AVG(call_length) FILTER (WHERE call_length > '0 seconds' AND call_length < '23 hours')) * '1 second'::interval, 'HH24:MI:SS'), ${pg_format('%L', noDataStr)}) AS avg_len,
			COUNT(callid) AS total` + (type !== 'aat_calltaker' ?
			`, COALESCE(SUM(${ctp.specialAban ? '(abandoned AND is_911)::int' : 'abandoned::int'}),0) AS "Abandoned"` : '')
			+ (type !== 'aat_calltaker' && ctp.specialAban ? ', COALESCE(SUM((abandoned AND NOT is_911)::int),0) AS "Failed to Answer"' : '');

		const aat_titles = {
			'hour': 'Hour',
			'aat': 'Average Answer Time\n(Seconds)',
			'ltn': `Percentage of Calls Answered\nWithin ${num_seconds_under} Seconds`,
			'ltnb': `Within ${num_seconds_under_b} Seconds`,
			'max_at': 'Longest Answer Time\n(Seconds)',
			'total': 'Total\nCalls',
			'avg_len': 'Average Call Duration',
			'Abandoned': ctp.abanName,
		};

		const query_where = `${time_within_days}
		                     AND ${psap_where}
				     AND ${ctp.where}`;

		function format_percent_red_under(percent) {
			return x => {
				if (x === null) return noDataStr;
				const ret = {
					'text': (100 * Number(x)).toFixed(1) + '%'
				};
				if (Number(x) < percent) ret.warning_style = true;
				return ret;
			};
		}
		const transform = {
			'ltn': format_percent_red_under(red_under),
			'ltnb': format_percent_red_under(red_under_b)
		};

		if (type === 'aat_calltaker') {
			const expr = `NULLIF(lower(${b === 0 ? 'first_' : ''}calltaker), '')`;
			const result = yield client_query(client,
				pg_format(`SELECT COALESCE(%s, 'Unknown') AS calltaker
				${query_middle}
				FROM ${backend}
				WHERE ${query_where} AND abandoned = FALSE
				GROUP BY 1 ORDER BY 1`, use_fake_calltaker ? fake_calltaker(expr) : expr),
				[tz, time_from, time_to, psap]
			);

			const abandoned = yield client_query(client,
				`SELECT ${pg_format('%L', ctp.abanName)} AS a, ${pg_format('%L', noDataStr)} AS b, ${pg_format('%L', noDataStr)} AS c, ${pg_format('%L', noDataStr)} AS d, ${pg_format('%L', noDataStr)} AS e, ${pg_format('%L', noDataStr)} AS f, COUNT(*)
				FROM ${backend}
				WHERE ${query_where} AND abandoned = TRUE${ctp.specialAban ? ' AND is_911': ''}`,
				[tz, time_from, time_to, psap]
			);

			let result_fail;
			if (ctp.specialAban) {
				result_fail = yield client_query(client,
					`SELECT 'Failed to Answer' AS a, ${pg_format('%L', noDataStr)} AS b, ${pg_format('%L', noDataStr)} AS c, ${pg_format('%L', noDataStr)} AS d, ${pg_format('%L', noDataStr)} AS e, ${pg_format('%L', noDataStr)} AS f, COUNT(*)
					FROM ${backend}
					WHERE ${query_where} AND abandoned = TRUE AND is_911 = FALSE`,
					[tz, time_from, time_to, psap]
				);
			}

			const summary = yield client_query(client,
				`SELECT '\u2014'
				${query_middle}
				FROM ${backend}
				WHERE ${query_where}`,
				[tz, time_from, time_to, psap]
			);

			table_title = result_title_map(result, wutil.object_merge(aat_titles, {
				'calltaker': 'Call-Taker\n(Windows Login)'
			}));
			table = result_table(result, transform).concat(result_table(abandoned, transform));
			if (result_fail) {
				table = table.concat(result_table(result_fail));
			}
			table_footer = result_table(summary, transform);
			graph = {
				type: 'hbar',
				datasets: [{name:'A', data: result.rows.map(x => x.aat)}],
				labels: result.rows.map(x => x.calltaker),
				xLabel: 'Average Answer Time (Seconds)',
				yLabel: 'Call-Taker (Windows Login)'
			};

			title = `${ctp.titlePrefix}Answer Time by Call-Taker`;
		} else if (type === 'aat_time') {
			const result = yield client_query(client,
				`SELECT (h.hour || ':00') AS hour
				${query_middle}
				FROM generate_series(0,23) h(hour) LEFT JOIN ${backend}
				ON h.hour = EXTRACT(HOUR FROM (utc AT TIME ZONE $1))
				AND ${query_where}
				GROUP BY h.hour ORDER BY h.hour`,
				[tz, time_from, time_to, psap]
			);

			const summary = yield client_query(client,
				`SELECT '\u2014'
				${query_middle}
				FROM ${backend}
				WHERE ${query_where}`,
				[tz, time_from, time_to, psap]
			);

			table_title = result_title_map(result, aat_titles);

			table = result_table(result, transform);
			table_footer = result_table(summary, transform);
			graph = {
				type: 'aline',
				datasets: [{name:'A',data: result.rows.map(x=>{
					const ret = Number(x[result.fields[1].name]);
					if (isNaN(ret)) return null;
					else return ret;
				})}],
				labels: result.rows.map(x => x[result.fields[0].name]),
				xLabel: 'Hour of Day',
				yLabel: 'Average Answer Time (Seconds)'
			};
			title = `${ctp.titlePrefix}Answer Time by Hour of Day`;
		}
	} else if (type === 'trunk') {
		const summary = yield client_query(client,
			`SELECT 'All',
			COUNT(*) AS cnt, '100%' AS "Percentage"
			FROM ${backend}
			WHERE ${time_within_days}
			AND ${psap_where}
			AND ${ctp.where}`,
			[tz, time_from, time_to, psap]
		);
		const summary_total = summary.rows[0].cnt;

		const result = yield client_query(client,
			`SELECT COALESCE(trunk,0) AS trunk,
			COUNT(*) AS cnt, ${pg_format_percent('COUNT(callid)', summary_total)} AS "Percentage"
			FROM ${backend}
			WHERE ${time_within_days}
			AND ${psap_where}
			AND ${ctp.where}
			GROUP BY trunk ORDER BY trunk`,
			[tz, time_from, time_to, psap]
		);

		if (result.rows.length === 0)
			summary.rows[0]['Percentage'] = noDataStr;

		table_title = result_title_map(result, {
			'trunk': 'Trunk/Line',
			'cnt': 'Call Count'
		});
		table = result_table(result);
		table_footer = result_table(summary);
		title = `${ctp.titlePrefix}Calls by Trunk/Line`;

		graph = {
			type: 'bar',
			datasets: [{name:'A',data: result.rows.map(x=>x.cnt)}],
			labels: result.rows.map(x=>x.trunk),
			xLabel: 'Call Count',
			yLabel: 'Trunk/Line'
		};
	}

	const subtitle = `${moment(time_from, 'MM/DD/YYYY').format('M/D/YYYY')} to ${moment(time_to, 'MM/DD/YYYY').format('M/D/YYYY')}` + adjustment_warning;

	const renderParams = {
		title: title,
		subtitle: subtitle,
		psap_name: psap_name,
		table: table,
		graph_json: JSON.stringify(graph),
	};

	const csv = table_to_csv(table_title.concat(table).concat(table_footer));
	const csv_filename = `WestTel Report for ${psap_name} [${title} ${subtitle}].csv`;

	const csv_file = urlsafe_base64.encode(yield wutil.zlib_deflateRaw(csv));
	renderParams.csv_url = `/data.csv?df=${csv_file}&f=${encodeURIComponent(csv_filename.replace(/\//g,'-').replace(/, /g, '_').replace(/[^-a-zA-Z0-9()\[\].]/g,'_'))}`;

	if (table_title !== undefined)
		renderParams.table_title = table_title;
	if (table_footer)
		renderParams.table_footer = table_footer;

	res.type('text/html; charset=utf-8');
	// 30 second cache in their browser
	res.set({'Cache-Control': 'private, max-age=30'});

	const gotten_in = process.hrtime(res.locals.response_start_time);
	renderParams.debug_info =
`Render time: ${gotten_in[0] + gotten_in[1] * 1e-9}
Timezone: ${tz}
From: ${moment_from.format()}
To: ${moment_to.format()}`;

	res.render('print_report', renderParams);
}));
}));
});

// experimental stuff ahead
module.exports.print_query = co.wrap(function*(report, req, res) {
yield wutil.pg_connect_ser_ro_def({}, co.wrap(function*(client) {
	assert(typeof report.from === 'string');
	assert(typeof report.to === 'string');
	assert(typeof report.tz === 'string');
	assert(typeof report.inbound_psap === 'string' || typeof report.outbound_psap === 'string');

	let cond = pg_format(`utc >= (date_trunc('day', %L::timestamp) AT TIME ZONE %L) AND utc < date_trunc('day', %L::timestamp) + '1 day'::interval) AT TIME ZONE %L`, report.from, report.tz, report.to, report.tz);

	if (report.inbound_psap)
		cond = pg_format(`(%s) AND (caller->>'psap' = %L OR caller->>'controller' = %L)`,
		                 cond, report.inbound_psap, report.inbound_psap);
	if (report.outbound_psap)
		cond = pg_format(`(%s) AND (callee->>'psap' = %L OR callee->>'controller' = %L)`,
		                 cond, report.outbound_psap, report.outbound_psap);

	switch(report.group) {
	case 'HourOfDay': {
		const stats = "avg(cng) AS average,max(cnt) AS max,sum(cnt) AS total";

		if (report.group === 'HourOfDay') {
			const query_format = `SELECT %s, %s
			FROM generate_series(0,23) h(hour) LEFT JOIN (
				SELECT h.hour AS hour,
				COUNT(callid) AS cnt
				FROM generate_series(date_trunc('day', $2::timestamp),
					date_trunc('day',$3::timestamp) + '23 hours', '1 hour') h(hour)
				LEFT JOIN (call_info JOIN call_transfer USING(call_id)) AS calls
				ON h.hour = date_trunc('hour', utc AT TIME ZONE $1)
				AND %s
				GROUP BY hour ORDER BY hour
			) AS every_hour
			ON h.hour = EXTRACT(HOUR FROM every_hour.hour)
			GROUP BY h.hour
			ORDER BY h.hour`;

			const result = yield client_query(client,
				pg_format(query_format, "(h.hour || ':00') AS hour", stats, cond),
				[report.tz, report.from, report.to]);

			table = result_table(result);
		}
	}
	}

	res.send(JSON.stringify(table, null, '\t'));
}));

});
