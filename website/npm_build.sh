#!/bin/bash

set -e
rm -rf static_build/
mkdir -p static_build/js

# -r (--require) adds as if require('') was used
browserify common.js node_modules/westtel-utils/mainLayout.js | uglifyjs bower_components/jquery-1/dist/jquery.js - -c --support-ie8 > static_build/js/common.min.js
# -x (--external) tells it that the module will be provided elsewhere (e.g. by above)
# browserify browser_editor.js | uglifyjs - -c > static_build/js/editor.min.js
# browserify login.js | uglifyjs - -c > static_build/js/login.min.js

browserify report_chooser.js | uglifyjs - -c --support-ie8 > static_build/js/report_chooser.min.js
browserify print_report.js | uglifyjs - -c --support-ie8 > static_build/js/print_report.min.js
