// for now, do we hide with animation or no
var all_has_loaded = false;

function disableFields() {
	$('[data-w-clicksfor]').each(function() {
		var e = $(this);
		var radio = $('#' + e.data('wClicksfor'));
		if (radio.prop('checked'))
			e.removeClass('pretendDisabled');
		else
			e.addClass('pretendDisabled');
	});
}

$(function() {
	var sticky_cookie = '419f70d5-80e7-4157-9f40-3732becd0c32';
	var sticky_options = {path:'',secure:true};
	function set_sticky(name, val) {
		var c = Cookies.getJSON(sticky_cookie);
		if (c === undefined) c = {};
		c[name] = val;
		Cookies.set(sticky_cookie, c, sticky_options);
	}

	var sticky = Cookies.getJSON(sticky_cookie);
	if (sticky === undefined) sticky = {};

	if (sticky.type !== undefined) {
		$('input[type="radio"][name="type"][value="' + sticky.type + '"]').click();
	}

	if (sticky.major_type !== undefined) {
		$('#major_type').val(sticky.major_type);
	}
	
	if (sticky.timeSplit !== undefined)
		$('#select_timeSplit').val(sticky.timeSplit);

	if (sticky.from !== undefined)
		$('#fromdate').val(sticky.from);
	if (sticky.to !== undefined)
		$('#todate').val(sticky.to);

	if (sticky.psap_select !== undefined)
		$('#psap_select').val(sticky.psap_select);

	if ($.ui) {
		$('#fromdate').datepicker({maxDate:0,dateFormat:'m/d/yy'});
		$('#todate').datepicker({maxDate:0,dateFormat:'m/d/yy'});
		$('#fromdate').datepicker('setDate', $('#fromdate').datepicker('getDate'));
		$('#todate').datepicker('setDate', $('#todate').datepicker('getDate'));

		function renderItem(ul, item) {
			var li = $('<li style="padding-left:20px;font-size:1em">').text(item.label.substr(item.optgroup.length + 1));
			return li.appendTo(ul);
		};

		function newRenderItem(ul, item) {
			var val = $('#major_type').val();
			if (item.element.data('divider'))
				return $('<li style="background-color:black;color:black;height:2px;padding:0;margin:2px 2px 2px 2px" class="ui-state-disabled">').appendTo(ul);
			else {
				if (item.element.data('onlyAdmin') && val !== 'admin')
					return $('<li style="display:none">').text(item.label).appendTo(ul);
				else
					return $('<li>').text(item.label).appendTo(ul);
			}
		};
		$.each($('select'), function() {
			$(this).selectmenu({appendTo:$(this).parent()});
			if ($(this).data('wPrefixDisappears'))
				$(this).selectmenu('instance')._renderItem = renderItem;
			else
				$(this).selectmenu('instance')._renderItem = newRenderItem;

		});
	};

	disableFields();
	// setTimeout works around issue in IE6
	$("input[type='radio'][name='type']").change(function() {setTimeout(disableFields, 0);});

	// this is all very weird because of IE6 and 7
	$('[data-w-clicksfor]').each(function() {
		var outer_span = $(this);
		outer_span.children().each(function() {
			var elem = $(this);
			if (elem.is('select')) {
				elem.focusin(function() { 
					$('#' + outer_span.data('wClicksfor')).prop('checked', true);
					disableFields();
				});
			} else {
				elem.click(function() {
					$('#' + outer_span.data('wClicksfor')).prop('checked',true);
					disableFields();
				});
			}
		});
		if ($.ui) {
			outer_span.click(function(event) {
				if ( !$(event.target).is('select') )
					$('#' + outer_span.data('wClicksfor')).click();
			});
		}
	});

	function onMajorTypeChange() {
		var val = $('#major_type').val();

		$('[data-w-calltypes]').each(function() {
			var elem = $(this);
			var types = elem.data('wCalltypes').toString().split(' ');
			if ($.inArray(val, types) > -1)
				elem.show({});
			else {
				elem.hide(all_has_loaded ? {} : undefined);
				elem.find('input[type="radio"][name="type"]').each(function() {
					if ($(this).prop('checked'))
						$('#time_radio').click();
				});
			}
		});

		$('#minor_type').children().each(function() {
			var elem = $(this);
			if (elem.data('onlyAdmin')) {
		       		if (val === 'admin') 
					elem.prop('disabled', false);
				else
					elem.prop('disabled', true);
			}
		});
		if ($.ui)
			$('#minor_type').selectmenu('refresh');
	};

	function checkErrors() {
		function parse(str) {
			if (res = /^(\d{1,2})\/(\d{1,2})\/(\d{4})/.exec(str)) {
				var ret = new Date(Number(res[3]), Number(res[1]) - 1, Number(res[2]));
				if (ret.getFullYear() !== Number(res[3]) || ret.getMonth() !== Number(res[1]) - 1 || ret.getDate() !== Number(res[2]))
					return null;
				else
					return ret;
			} else
				return null;
		}
		var fromdate = parse($('#fromdate').val());
		var todate = parse($('#todate').val());
		var today = new Date();
		var error = $("#daterange_error");
		var submit = $("#submit_button");
		if (fromdate === null) {
			error.text("From date must be a valid date of the format MM/DD/YYYY");
			submit.prop('disabled', true);
		} else if (todate === null) {
			error.text("To date must be a valid date of the format MM/DD/YYYY");
			submit.prop('disabled', true);
		} else if (fromdate > today) {
			error.text('From date must not be in the future.');
			submit.prop('disabled', true);
		} else if (todate > today) {
			error.text('To date must not be in the future.');
			submit.prop('disabled', true);
		} else if (todate < fromdate) {
			error.text('Date range is in the wrong order.');
			submit.prop('disabled', true);
		} else {
			error.text('');
			submit.prop('disabled', false);
			set_sticky('from', $('#fromdate').val());
			set_sticky('to', $('#todate').val());
		}
	}
	
	$('#fromdate').change(checkErrors);
	$('#todate').change(checkErrors);
	$('#fromdate').on('input', checkErrors);
	$('#todate').on('input', checkErrors);

	if ($.ui) {
		$('#major_type').on('selectmenuchange', onMajorTypeChange);
		$('#major_type').trigger('selectmenuchange');
	} else {
		$('#major_type').change(onMajorTypeChange);
		$('#major_type').change();
	}

	(function() {
	 	var set = set_sticky;

		function onMajorTypeChangeSticky() {
			set('major_type', $('#major_type').val());
		}
		if ($.ui)
			$('#major_type').on('selectmenuchange', onMajorTypeChangeSticky);
		else
			$('#major_type').change(onMajorTypeChangeSticky);


		function onTypeChangeSticky() {
			set('type', $('input[type="radio"][name="type"]:checked').val());
		}
		$('input[type="radio"][name="type"]').change(onTypeChangeSticky);
		$('input[type="radio"][name="type"]').click(onTypeChangeSticky);

		function onTimeSplitChangeSticky() {
			set('timeSplit', $('#select_timeSplit').val());
		}
		if ($.ui)
			$('#select_timeSplit').on('selectmenuchange', onTimeSplitChangeSticky);
		else
			$('#select_timeSplit').change(onTimeSplitChangeSticky);

		$('#psap_select').on('change selectmenuchange', function() {
			set_sticky('psap_select', $(this).val());
		});
	}());

	all_has_loaded = true;
});
