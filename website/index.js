'use strict';
require('systemd');
const pg = require('pg');
pg.defaults.poolSize = 10;

const log = require('./logger.js').log;

const assert = require('assert');
const bcrypt = require('bcrypt'),
      body_parser = require('body-parser'),
      co = require('co'),
      co_express = require('co-express'),
      cookie_parser = require('cookie-parser'),
      express = require('express'),
      express_session = require('express-session'),
      helmet = require('helmet'),
      cluster = require('cluster'),
      uuid = require('node-uuid');
const toobusy = require('toobusy-js');
const urlsafe_base64 = require('urlsafe-base64');
const lru_cache = require('lru-cache');
const Handlebars = require('handlebars');
const moment = require('moment-timezone');
const westtel_sessions = require('westtel-utils/sessions');
const db = require('pg-promise')()({});

const handlebars = require('express-handlebars').create({
	handlebars: Handlebars,
	defaultLayout: 'main',
	helpers: {
		// TODO: should probably put helpers in another file,
		// this could become bigger.
		// Or we could use something less opinionated than handlebars.
		ifeq: function(a, b, options) {
			if (a === b)
				return options.fn(this);
			else
				return options.inverse(this);
		},
		if_odd: function(a, options) {
			if (a % 2 === 1)
				return options.fn(this);
			else
				return options.inverse(this);
		}
	}
});

const app = express();

// CSRF protection middleware
const csurf = require('csurf')({
	cookie: {
		secure: true,
		httpOnly: true
	}
});

app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');

app.enable('trust proxy'); // trust nginx

// TODO: better app closing behavior. Also better behavior in nginx
// let app_is_closing = false;
// app.use(function(req, res, next) {
// 	if (app_is_closing) {
// 		res.set({'Connection':'close'});
// 		res.status(502);
// 		res.send('Server is restarting, please retry in a minute...');
// 	} else
// 		next();
// });


function do_csrf(req, res, next) {
	csurf(req, res, function() {
		res.locals.csrfToken = req.csrfToken();
		next();
	});
}

const check_auth = westtel_sessions.check_auth({});
const check_auth_super = westtel_sessions.check_auth({superadmin:true});

app.set('port', process.env.PORT || 3000);

(function toobusy_log() {
	setTimeout(toobusy_log, 5000);
	if (toobusy()) console.log(`[${new Date(Date.now()).toUTCString()}] Too Busy!`);
})();

// debugging tool
app.get('/ping', function(req, res) {
	res.send('hello');
});

// also debugging tool
app.get('/ping_wait', co_express(function*(req, res) {
	const milli = Number.isNaN(Number(req.query.t)) ? Math.random() * 1000 : Number(req.query.t);
	if (req.query.b && toobusy()) res.sendStatus(418); // I'm a teapot
	else {
		yield wutil.wait(milli);
		res.send(`waited ${milli}ms`);
	}
}));

/* Known res.locals: {
 * 	user: {
 * 		uid: uuid,
 * 		timezone: string,
 * 		superadmin: boolean,
 * 		username: string
 * 	},
 * 	requestUUID: uuid,
 * 	requestUUID_num: number,
 * 	debug: boolean,
 * 	response_start_time: hrtime,
 * 	csrfToken: string
 * }
 */

app.use(co_express(function*(req, res, next) {
	res.locals.response_start_time = process.hrtime();
	res.locals.requestUUID = uuid.v4();
	res.locals.requestUUID_num = 0;
	if (req.query.debug === '1')
		res.locals.debug = true;
	next();
}));

// some standard security stuff. TODO: Need to investigate more
// i.e., some stuff we can shut down even more
app.use(helmet({
	frameguard:{action:'deny'},
	contentSecurityPolicy: {
		directives: {
			baseUri: ["'self'"],
			defaultSrc: ["'none'"],
			// google charts uses eval. There's nothing we can do about that
			scriptSrc: ["'self'", "'unsafe-eval'", 'https://www.gstatic.com'],
			styleSrc: ["'self'", "'unsafe-inline'", 'https://fonts.googleapis.com', 'https://www.gstatic.com'],
			imgSrc: ["'self'", 'data:'],
			connectSrc: ["'none'"],
			fontSrc: ['https://fonts.gstatic.com'],
			objectSrc: ["'none'"],
			// plugin-types is not needed since object-src is 'none'. Also 'none' is
			// not a valid value for plugin-types (since it is redundant).
			// pluginTypes: ["'none'"],
			mediaSrc: ["'none'"],
			reportUri: ['/dev/null'],
			childSrc: ["'none'"],
			// we need to be able to redirect to accounts.westtel.com when they are logged out
			formAction: ["https://*.westtel.com"],
			frameAncestors: ["'none'"],
		},
		reportOnly: false
	},
	hsts: {
		maxAge: 15768000000,
		force: true
	},
	referrerPolicy: {
		policy: 'no-referrer'
	}
}));

app.use('/dev/null', function(req, res) { res.send(''); });
app.use('/css/mainLayout.css', express.static('node_modules/westtel-utils/mainLayout.css', {maxAge:'1 hour'}));
app.use('/js/mainLayout.js', express.static('node_modules/westtel-utils/mainLayout.js', {maxAge:'1 hour'}));
app.use(express.static('static_build', {maxAge:'1 hour'}));
app.use('/static', express.static('static', {maxAge:'1 hour'}));

westtel_sessions.install_sessions(app, {db});
if (!app.locals.mainLayout) app.locals.mainLayout = {};
app.locals.mainLayout.title = 'WestTel PSAP Reports';
app.locals.mainLayout.headHTML = [
`<script type="text/javascript" src="/js/common.min.js"></script>`,
`<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>`,
`<!--[if gte IE 8]><!-->
<script src="/static/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/static/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
<!--<![endif]-->
<link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/static/westtel.css" />`,
`<link rel="stylesheet" href="/css/mainLayout.css" />`,
];

/* known session object:
 * session: {
 * 	user: {
 * 		uid: uuid
 * 	}
 * }
 */

// parse POST requests
app.use(body_parser.urlencoded({extended:false}));

const wutil = require('./wutil.js');
const pg_connect = wutil.pg_connect;
const client_query = wutil.client_query;

// TODO: move to wutil, is a utility function
function save_session(session) {
	return new Promise(function(resolve, reject) {
		session.save(function(err) {
			if (err) reject(err);
			else resolve();
		});
	});
}
function destroy_session(session) {
	return new Promise(function(resolve, reject) {
		session.destroy(function(err) {
			if (err) reject(err);
			else resolve();
		});
	});
}

// utility function
app.get('/bcrypt', check_auth_super, co_express(function*(req, res) {
	res.render('bcrypt');
}));

app.post('/bcrypt', check_auth_super, co_express(function*(req, res) {
	// TODO: 10 is a magic number, please replace it
	const hash = yield wutil.bcrypt_hash(req.body.password, 10);
	res.render('bcrypt', {'bcrypt': hash, 'password': req.body.password});
}));

app.get('/', check_auth, co_express(function*(req, res) {
	yield pg_connect({}, function*(client) {
		// TODO: at some point cache this (maybe)
		let result;
		if (res.locals.user.superadmin)
			result = yield client_query(client, 'SELECT psap, nice FROM psaps ORDER BY psap');
		else
			result = yield client_query(client, `SELECT user_psap.psap AS psap, nice
			                            FROM psaps JOIN user_psap
			                            ON psaps.psap = user_psap.psap
			                            AND uid = $1 ORDER BY psap`,
			                            [res.locals.user.uid]
			);

		// we calculate "yesterday" and "the beginning of the year" in
		// the user's timezone
		const now = moment().tz(res.locals.user.timezone);
		const options = {
			psap: result.rows,
			fromdate_def: now.clone().startOf('year').format('M/D/YYYY'),
			todate_def: now.clone().subtract(1, 'day').startOf('day').format('M/D/YYYY')
		};
		res.render('report_chooser', options);
	});
}));

const print_query = require('./print_query.js');
app.get('/print_query', check_auth, co_express(function*(req, res) {
	if (req.query.b === 'call_info') {
		assert(false);
		// TODO: call_info is unfinished and unused
		let report = {
			group: 'HourOfDay',
			inbound_psap: 'OH-004-1',
			from:'2016-03-1',
			to:'2016-04-01',
			tz:'America/Denver'
		};
		yield print_query.print_query(report, req, res);
	} else {
		yield print_query(req, res);
	}
})); 

// yes, you could probably replace this with a data URL in most (but not all) cases
// the compression is to make the URL smaller, which is good because URLs
// might have size limits.
app.get('/data.csv', check_auth, co_express(function*(req, res) {
	const filename = req.query.f || 'WestTel.csv';
	res.set({
		// cache is 1 week. Potentially cache could be forever (because response is
		// determined entirely by parameters and should never change)
		'Cache-Control': 'public, max-age=604800',
		'Expires': new Date(Date.now() + 604800000).toUTCString()
	});
	res.type('text/csv');
	res.attachment(filename);
	let file;
	if (req.query.d)
		file = req.query.d;
	else
		file = (yield wutil.zlib_inflateRaw(urlsafe_base64.decode(req.query.df))).toString('utf8');
	res.send(file);
}));

app.use(function(req, res) {
	res.sendStatus(404); // TODO: more informative page
});

app.use(function(err, req, res, next) {
	console.log(`Error: ${JSON.stringify(err,null,'\t')}`);
	if (res.headersSent) {
		next(err);
		return;
	} else if (err.code === 'EBADCSRFTOKEN') {
		if (req.session.user !== undefined)
			log('csrf_failure', {req:req,res:res});
		res.redirect('/');
	}
	console.error(err.stack);
	res.sendStatus(500); // TODO: better error page
});

const http = require('http');
const server = http.createServer(app).listen(app.get('port'));

console.log(`Listening on port ${app.get('port')}`);

process.on('SIGTERM', function() {
	console.log('received SIGTERM');
	server.close(function() {
		console.log('closing');
		process.exit();
	});
});

