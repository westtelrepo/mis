google.charts.load('44', {'packages':['corechart']});

$(function() {
	var wdata = JSON.parse($('#graph_json').text());
	var width = $('#mainGraphDiv').width();
	var height = 0.66 * width;
	// slightly modified
	var google_charts_default_colors = ['#0057A5','#DC3912','#FF9900','#109618','#990099','#3B3EAC','#0099C6','#DD4477','#66AA00','#B82E2E','#316395','#994499','#22AA99','#AAAA11','#6633CC','#E67300','#8B0707','#329262','#5574A6','#3B3EAC'];
	if (wdata.type === 'pie') {
		height = width;
	}
	if (wdata.type === 'hbar') {
		var new_height = wdata.labels.length * 50;
		if (new_height > height) height = new_height;
	}
	$('#mainGraphDiv').width(width);
	$('#mainGraphDiv').height(height);

google.charts.setOnLoadCallback(function() {
	// copied from westtel.css
	var fontList = 'Droid Sans,HelveticaNeue,Helvetica Neue,Helvetica,Arial,sans-serif';
	var axisFontSize = 20;
	var littleFontSize = 15;

	var data = new google.visualization.DataTable();
	if (wdata.type === 'hbar' || wdata.type === 'bar') {
		data.addColumn('string', wdata.yLabel);
		data.addColumn('number', wdata.xLabel);	
		//data.addColumn({type:'string',role:'style'});
	} else {
		data.addColumn('string', wdata.xLabel);
		data.addColumn('number', wdata.yLabel);	
	}

	if (wdata.type === 'barline')
		data.addColumn('number', wdata.yLabel);

	var non_null = 0;
	for (var i = 0; i < wdata.labels.length; i++) {
		if (wdata.datasets[0].data[i] !== null)
			non_null++;
	}

	for (var i = 0; i < wdata.labels.length; i++) {
		var to_add = wdata.datasets[0].data[i];
		if (to_add !== null) to_add = Number(to_add);
		//else if (wdata.type === 'line') to_add = 0;
		// TODO: replace with option to replace nulls w/ 0
		var to_add_row = [String(wdata.labels[i]), to_add];
		if (wdata.type === 'barline')
			to_add_row.push(to_add);
		//if (wdata.type === 'hbar' || wdata.type === 'bar')
			//to_add.push(google_charts_default_colors[i % google_charts_default_colors.length]);
		data.addRow(to_add_row);
	}
	var options = {
		colors: google_charts_default_colors,
		backgroundColor: 'transparent',
		width: width,
		height: height,
		fontName: fontList,
		chartArea: {
			width:width - 200,
			left:150,
			top:50,
			height:height - 170
		},
		vAxis: {
			minValue: 0,
			viewWindow: {min: 0},
			title: (wdata.type === 'bar' ? wdata.xLabel : wdata.yLabel),
			titleTextStyle: {
				fontSize: axisFontSize,
				italic: false
			},
			textStyle: {
				fontSize: littleFontSize
			},
			maxAlternation: 1
		},
		hAxis: {
			viewWindow: {min: 0},
			title: (wdata.type === 'bar' ? wdata.yLabel : wdata.xLabel),
			titleTextStyle: {
				fontSize: axisFontSize,
				italic: false
			},
			textStyle: {
				fontSize: littleFontSize
			},
			maxAlternation: 1
		},
		legend: { position: 'none' },
		interpolateNulls: true,
		trendlines: {0:{}}
	};

	var mainGraph = document.getElementById('mainGraphDiv');
	var chart;
	switch(wdata.type) {
	case 'barline':
		chart = new google.visualization.ComboChart(mainGraph);
		options.seriesType = 'bars';
		options.series = {1:{type:'line'}};
		options.lineWidth = 6;
		options.curveType = 'function';
		chart.draw(data, options);
		break;
	case 'line':
		if (non_null === 1)
			options.pointSize = 20;
		else
			options.pointSize = 4;
		chart = new google.visualization.AreaChart(mainGraph);
		chart.draw(data, options);
		break;
	case 'aline':
		options.lineWidth = 6;
		chart = new google.visualization.LineChart(mainGraph);
		chart.draw(data, options);
		break;
	case 'hbar':
		options.hAxis.minValue = 0;
		chart = new google.visualization.BarChart(mainGraph);
		chart.draw(data, options);
		break;
	case 'bar':
		chart = new google.visualization.ColumnChart(mainGraph);
		chart.draw(data, options);
		break;
	case 'pie':
		options.chartArea.width = '85%';
		options.chartArea.height = '85%';
		options.chartArea.top = '10%';
		options.chartArea.left = '10%';
		options.legend = {position: 'top'};
		chart = new google.visualization.PieChart(mainGraph);
		chart.draw(data, options);
		break;
	}
});
});
