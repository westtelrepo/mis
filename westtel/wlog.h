#ifndef WESTTEL_WLOG
#define WESTTEL_WLOG

#include <cassert>
#include <iostream>

namespace westtel {

// NOT THREAD SAFE
// NOT INTENDED TO BE THREAD SAFE
// DOESN'T NEED TO BE THREAD SAFE
class wlog_writer {
private:
	std::ofstream out_;
public:
	wlog_writer(const std::string& filename)
	: out_{filename, std::ios_base::trunc | std::ios_base::binary}
	{ }

	void set_checksum(...);

	void write(const std::string& str) {
		out_ << "n " << std::to_string(str.size()) << " " << str << "\n";
	}
	// p257 3 abc\n
	// writes first a character (checksum type) and then checksum of
	// length and text (including newline)
	// length is decimal
	// checksum depends on type (probably decimal, maybe hex)

	void sync(); // call fdatasync essentially (flushes)

	bool needs_sync(); // has data been written and sync() not been called?

};

// also not threadsafe
class wlog_reader {
private:
	std::ifstream in_;
public:
	wlog_reader(const std::string& filename)
	: in_{filename, std::ios_base::binary} { }
	// throws away bad data (obviously, hopefully)
};

#endif
