#ifndef WESTTEL_JSON
#define WESTTEL_JSON

// TODO: C++98 support

#include "utf8.h"
#include "util.h"
#include <cassert>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <string>
#include <vector>

namespace westtel {

enum class JSONType {
	Uninit,
	Null,
	Boolean,
	String,
	Number,
	Object,
	Array
};

// WISHLIST: infinite-precision numerics
// WISHLIST: use union (only in C++11)
class JSON {
	friend void swap(JSON&, JSON&) noexcept;
private:
	JSONType type_ = JSONType::Null;
	bool boolean_ = false;
	int64_t number_ = 0; // WISHLIST: replace with infinite-numeric type
	std::string string_;
	std::map<std::string, std::unique_ptr<JSON>> object_;
	std::vector<std::unique_ptr<JSON>> array_;
public:
	JSON(): type_(JSONType::Uninit) { }
	JSON(JSONType t): type_(t) { }

	JSON(const std::string& s): type_{JSONType::String}, string_{s} { }
	JSON(const char* s): JSON{std::string{s}} { }
	JSON(int64_t i): type_{JSONType::Number}, number_{i} { }
	JSON(int32_t i): type_(JSONType::Number), number_(i) { }
	JSON(std::nullptr_t): type_{JSONType::Null} { }
	JSON(bool b): type_{JSONType::Boolean}, boolean_{b} { }

	JSON(JSON&& j) noexcept
	: type_{JSONType::Null}, boolean_{false}, number_{0} {
		swap(*this, j);
	}

	JSON(const JSON& j)
	: type_{j.type_}, boolean_{j.boolean_}, number_{j.number_},
	string_{j.string_}
	{
		for (auto& i: j.object_)
			object_.emplace(i.first,
				std::unique_ptr<JSON>{new JSON{*i.second}});
		for (auto& i: j.array_)
			array_.emplace_back(new JSON{*i});
	}

	JSON& operator=(JSON j) noexcept {
		swap(j, *this);
		return *this;
	}

	bool is_null() const noexcept { return type_ == JSONType::Null; }
	bool is_boolean() const noexcept { return type_ == JSONType::Boolean; }
	bool is_string() const noexcept { return type_ == JSONType::String; }
	bool is_number() const noexcept { return type_ == JSONType::Number; }
	bool is_object() const noexcept { return type_ == JSONType::Object; }
	bool is_array() const noexcept { return type_ == JSONType::Array; }

	bool has_value() const noexcept { return type_ != JSONType::Uninit; }

	// now has unicode support. However, escapes everything non-printable-ascii
	// TODO: version that doesn't escape
	// TODO: pretty-print
	std::string to_string() const {
		switch(type_) {
		case JSONType::Uninit:
			throw std::runtime_error{"Uninit JSON has no representation"};
		case JSONType::Null:
			return "null";
		case JSONType::Boolean:
			return boolean_ ? "true" : "false";
		case JSONType::String: {
			std::string ret = "\"";
			std::vector<uint16_t> utf16;
			// WISHLIST: instead of using back_inserter, we could
			//   make our own iterator that instead of copying,
			//   executes the loop
			utf8::utf8to16(string_.begin(), string_.end(), std::back_inserter(utf16));
			for (uint16_t c: utf16) {
				if (c == '"') ret += "\\\"";
				else if (c == '\\') ret += "\\\\";
				else if (c == '\b') ret += "\\b";
				else if (c == '\f') ret += "\\f";
				else if (c == '\n') ret += "\\n";
				else if (c == '\r') ret += "\\r";
				else if (c == '\t') ret += "\\t";
				else if (c < 32 || c >= 127) {
					char str[7];
					std::snprintf(str, sizeof str, "\\u%04X",
						static_cast<int>(c));
					ret += str;
				} else
					ret += static_cast<char>(c);
			}
			ret += "\"";
			return ret;
		} case JSONType::Number:
			return std::to_string(number_);
		case JSONType::Object: {
			auto i = object_.begin();
			std::string ret = "{";
			if (i != object_.end()) {
				ret += JSON{i->first}.to_string() + ":"
				     + i->second->to_string();
				++i;
			}
			for (; i != object_.end(); ++i)
				ret += "," + JSON{i->first}.to_string() + ":"
				     + i->second->to_string();
			return ret + "}";
		} case JSONType::Array: {
			auto j = array_.begin();
			std::string ret = "[";
			if (j != array_.end()) {
				ret += (*j)->to_string();
				++j;
			}
			for(; j != array_.end(); ++j)
				ret += "," + (*j)->to_string();
			return ret + "]";
		}
		};
		assert(false);
	}

	JSON& operator[](const std::string& s) {
		if (!has_value())
			type_ = JSONType::Object;
		if (type_ != JSONType::Object)
			throw std::domain_error{"JSON::operator[std::string] "
				"can only be called when *this is an Object"};
		auto i = object_.find(s);
		if (i != object_.end()) return *i->second;
		else {
			std::unique_ptr<JSON> ptr{new JSON};
			JSON* ret = ptr.get();
			object_.emplace(s, std::move(ptr));
			return *ret;
		}
	}

	JSON& operator[](size_t i) {
		assert(type_ == JSONType::Array);
		return *array_[i];
	}

	JSONType type() const { return type_; }

	bool has(const std::string& s) {
		assert(type_ == JSONType::Object);
		return object_.find(s) != object_.end();
	}

	void push_back(const JSON& j) {
		assert(type_ == JSONType::Array);
		array_.push_back(std::unique_ptr<JSON>{new JSON{j}});
	}

	size_t array_size() {
		assert(type_ == JSONType::Array);
		return array_.size();
	}

	std::string string() const {
		assert(type_ == JSONType::String);
		return string_;
	}

	int64_t num() const {
		assert(type_ == JSONType::Number);
		return number_;
	}

	bool boolean() const {
		assert(type_ == JSONType::Boolean);
		return boolean_;
	}

	const decltype(object_)& object() const {
		assert(type_ == JSONType::Object);
		return object_;
	}

	const decltype(array_)& array() const {
		assert(type_ == JSONType::Array);
		return array_;
	}

	// WISHLIST: parse floating point
	static JSON parse(const std::string& s) {
		std::vector<uint16_t> utf16;
		utf8::utf8to16(s.begin(), s.end(), std::back_inserter(utf16));
		auto b = utf16.begin();
		JSON ret = parse(b, utf16.end());
		while (b != utf16.end() && isspace(*b))
			++b;
		if (b != utf16.end())
			throw std::runtime_error{"Unexpected post JSON"};
		return ret;
	}
private:
	// requires utf-16
	template<class Iter>
	static JSON parse(Iter& b, const Iter& end) {
		auto eat = [&]() { // eats whitespace
			while (b != end && isspace(*b))
				++b;
		};
		eat();
		if (b == end) throw std::runtime_error{"JSON parse found eof expected token"};

		if (*b == '-' || isdigit(*b)) {
			bool negative = false;
			if (*b == '-') {
				++b;
				negative = true;
			}

			std::string num;
			while (b != end && isdigit(*b))
				num.push_back(static_cast<char>(*b++));
			if (negative)
				return JSON{-westtel::string_to_int<int64_t>(num)};
			else
				return JSON{westtel::string_to_int<int64_t>(num)};
		} else if (*b == '"') {
			std::vector<uint16_t> ret;
			++b;
			while(b != end && *b != '"') {
				if (*b == '\\') {
					++b;
					if (*b == '\\') {
						++b;
						ret.push_back('\\');
					} else if (*b == 'b') {
						++b;
						ret.push_back('\b');
					} else if (*b == 'f') {
						++b;
						ret.push_back('\f');
					} else if (*b == 'n') {
						++b;
						ret.push_back('\n');
					} else if (*b == 't') {
						++b;
						ret.push_back('\t');
					} else if (*b == 'u') {
						++b;
						std::string hex;
						while (hex.size() < 4 && b != end && isxdigit(*b))
							hex.push_back(static_cast<char>(*b++));
						ret.push_back(westtel::string_to_int<uint16_t>(hex, 16));
					} else
						throw std::runtime_error{"Bad escape character " + std::string{static_cast<char>(*b)}};
				} else
					ret.push_back(*b++);
			}
			if (*b != '"')
				throw std::runtime_error{"expected end of string, got eof"};
			++b;
			std::string utf8;
			utf8::utf16to8(ret.begin(), ret.end(), std::back_inserter(utf8));
			return utf8;
		} else if (*b == 't') {
			++b;
			if (b == end || *b++ != 'r')
				throw std::runtime_error{"Expected true constant"};
			if (b == end || *b++ != 'u')
				throw std::runtime_error{"Expected true constant"};
			if (b == end || *b++ != 'e')
				throw std::runtime_error{"Expected true constant"};
			return true;
		} else if (*b == 'f') {
			++b;
			if (b != end && *b++ == 'a' && b != end && *b++ == 'l'
			    && b != end && *b++ == 's' && b != end && *b++ == 'e')
				return false;
			else
				throw std::runtime_error{"Expected false constant"};
		} else if (*b == 'n') {
			++b;
			if (b != end && *b++ == 'u' && b != end && *b++ == 'l'
			    && b != end && *b++ == 'l')
				return nullptr;
			else
				throw std::runtime_error{"Expected null constant"};
		} else if (*b == '{') {
			++b;
			JSON ret{JSONType::Object};
			while (true) {
				JSON key = parse(b, end);
				if (key.type() != JSONType::String)
					throw std::runtime_error{"Expected string"};
				eat();
				if (b == end || *b != ':')
					throw std::runtime_error{"Expected ':'"};
				++b;
				JSON val = parse(b, end);
				ret[key.string()] = val;
				eat();
				if (b == end)
					throw std::runtime_error{"Expected continued object got eof"};
				if (*b == ',') {
					++b;
					continue;
				}
				if (*b == '}') {
					++b;
					break;
				}
			}
			return ret;
		} else if (*b == '[') {
			++b;
			JSON ret{JSONType::Array};
			while (true) {
				JSON val = parse(b, end);
				ret.push_back(val);
				eat();
				if (b == end)
					throw std::runtime_error{"Expected continued array got eof"};
				if (*b == ',') {
					++b;
					continue;
				}
				if (*b == ']') {
					++b;
					break;
				}
			}
			return ret;
		} else
			throw std::runtime_error{
			 	"Expected token in JSON, found " + std::string{static_cast<char>(*b)}};
	}
};

static_assert(noexcept(std::declval<JSON&>() = std::declval<JSON&&>()),
	"JSON move assignment is not noexcept");
static_assert(!noexcept(std::declval<JSON&>() = std::declval<const JSON&>()),
	"JSON copy assignment must not be noexcept");

inline void swap(JSON& j1, JSON& j2) noexcept {
	using std::swap;
	swap(j1.type_, j2.type_);
	swap(j1.boolean_, j2.boolean_);
	swap(j1.string_, j2.string_);
	swap(j1.number_, j2.number_);
	swap(j1.object_, j2.object_);
	swap(j1.array_, j2.array_);
}

inline std::ostream& operator<<(std::ostream& out, const JSON& j) {
	out << j.to_string();
	return out;
}

} // namespace westtel

#endif
