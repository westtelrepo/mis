#include "json.h"
#include <iostream>

using namespace westtel;

int main() {
	assert(JSON::parse("-123").num() == -123);
	assert(JSON::parse("123").num() == 123);

	bool threw = false;
	try {
		JSON::parse("123 {");
	} catch(...) {
		threw = true;
	}
	assert(threw);

	JSON hello = JSON::parse(R"(   "hello"      )");
	assert(hello.type() == JSONType::String);
	assert(hello.string() == "hello");
	assert(JSON::parse("  \"hello\"  ").type() == JSONType::String);
	assert(JSON::parse("  \"hello\"  ").string() == "hello");
	assert(JSON::parse(R"(    "hello \uE000"   )").string() == u8"hello \uE000");

	assert(JSON::parse("true").boolean());
	assert(JSON::parse("false").boolean() == false);

	threw = false;
	try {
		JSON::parse("tru");
	} catch(...) {
		threw = true;
	}
	assert(threw);

	assert(JSON::parse("null").type() == JSONType::Null);

	{
		JSON obj = JSON::parse(R"({   "hello":"val\uE000","what" : "foo"} )");
		assert(obj.type() == JSONType::Object);
		assert(obj.object().size() == 2);
		assert(obj.has("hello"));
		assert(obj["hello"].type() == JSONType::String);
		assert(obj["hello"].string() == u8"val\uE000");
		assert(obj.has("what"));
		assert(obj["what"].type() == JSONType::String);
		assert(obj["what"].string() == "foo");
	}

	{
		JSON obj = JSON::parse(R"(   {  "he\uD835\uDC9Eo":  123 , "fifty": 57})");
		assert(obj.type() == JSONType::Object);
		assert(obj.object().size() == 2);
		assert(obj.has(u8"he\U0001D49Eo"));
		assert(obj[u8"he\U0001D49Eo"].type() == JSONType::Number);
		assert(obj[u8"he\U0001D49Eo"].num() == 123);
		assert(obj["fifty"].num() == 57);
	}

	{
		JSON array = JSON::parse(R"( [ "hello", 123 ,{"obj":"val"}])");
		assert(array.type() == JSONType::Array);
		assert(array[0].string() == "hello");
		assert(array[1].num() == 123);
		assert(array[2]["obj"].string() == "val");
	}

	{
		JSON j = JSON::parse(R"({"hello":[1]})");
		assert(j["hello"].type() == JSONType::Array);
		assert(j["hello"].array_size() == 1);
	}

	std::cout << "DONE TESTING" << std::endl;
}
