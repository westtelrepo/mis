CREATE TABLE oldlog (
	id UUID PRIMARY KEY,
	controller TEXT NOT NULL,
	utc TIMESTAMPTZ NOT NULL,
	code TEXT NOT NULL,
	callid TEXT,
	trunk INT,
	pos INT,
	ani TEXT,
	pani TEXT,
	thread TEXT,
	message TEXT NOT NULL,
	CHECK(uuid_generate_v5('cfb7b16f-c134-4af5-8cec-bbb1a62c3881',
		controller || ' | '
		|| TO_CHAR(utc, 'YYYY-MM-DD HH:DD:SS.US') || ' | '
		|| code || ' | '
		|| COALESCE(callid, '') || ' | '
		|| COALESCE(trunk::text, '') || ' | '
		|| COALESCE(pos::text, '') || ' | '
		|| COALESCE(ani, '') || ' | '
		|| COALESCE(pani, '') || ' | '
		|| COALESCE(thread, '') || ' | '
		|| message)
		= id)
);
CREATE INDEX oldlog_controller_idx ON oldlog(controller);
CREATE INDEX oldlog_callid_idx ON oldlog(callid);
CREATE INDEX oldlog_code_idx ON oldlog(code);

CREATE TABLE oldlog_files_processed (
	filename TEXT PRIMARY KEY,
	controller TEXT NOT NULL,
	mtime TIMESTAMPTZ NOT NULL,
	size BIGINT NOT NULL
);

CREATE TABLE dialplan (
	controller TEXT NOT NULL,
	num TEXT NOT NULL,
	plan JSONB NOT NULL,
	PRIMARY KEY(controller, num)
);
-- plan is {"psap":"", "positions": [1,...]}

CREATE TABLE newlog (
	id UUID PRIMARY KEY,
	controller TEXT NOT NULL,
	utc TIMESTAMPTZ NOT NULL,
	code TEXT NOT NULL,
	orig JSONB NOT NULL,
	data JSONB, -- NULL means not processed.. not normally NULL
	call_id UUID, -- is a duplicate of orig->'call'->>'callid' and data->'call'->>'callid' because PostgreSQL doesn't collect (useful) statistics on JSONB columns
	CONSTRAINT orig_forbidden_keys CHECK(NOT orig ?| array['id','controller','utc','code']),
	CONSTRAINT data_forbidden_keys CHECK(data IS NULL OR NOT data ?| array['id','controller','utc','code']),
	-- CONSTRAINT data_no_old_processing CHECK(data IS NULL OR NOT data ? 'old_processing'),
	CHECK( jsonb_typeof(orig) = 'object' ),
	CHECK( data IS NULL OR jsonb_typeof(data) = 'object' ),
	CONSTRAINT data_callid CHECK((call_id IS NOT DISTINCT FROM (data->'call'->>'callid')::uuid OR (data->'call'->>'callid') IS NULL) AND call_id IS NOT DISTINCT FROM (orig->'call'->>'callid')::uuid)
);
--TODO: some of these indexes need to be gotten rid of
CREATE INDEX newlog_null_data ON newlog(controller, utc) WHERE data IS NULL; -- most of the time should be empty
CREATE INDEX newlog_order ON newlog(controller, utc);
CREATE INDEX newlog_login_positions ON newlog(controller, (data->'user'->>'position'), utc) WHERE code = 'login/login/';
CREATE INDEX newlog_data_gin ON newlog USING gin(data);
CREATE INDEX newlog_ali_gin ON newlog USING gin((data->'ali')) WHERE data ? 'ali';
CREATE INDEX newlog_call_gin ON newlog USING gin((data->'call')) WHERE data ? 'call';
CREATE INDEX newlog_user_gin ON newlog USING gin((data->'user')) WHERE data ? 'user';
CREATE INDEX newlog_orig_gin ON newlog USING gin(orig);
CREATE INDEX newlog_orig_callid ON newlog(( (orig->'call'->>'callid')::uuid )) WHERE orig ? 'call' AND orig->'call' ? 'callid';
CREATE INDEX newlog_data_callid ON newlog(( (data->'call'->>'callid')::uuid )) WHERE data ? 'call' AND data->'call' ? 'callid';
CREATE INDEX newlog_callid ON newlog(call_id) WHERE call_id IS NOT NULL;
CREATE INDEX newlog_controller_callid_new ON newlog(controller, call_id) WHERE call_id IS NOT NULL;

CREATE INDEX newlog_controller_callid ON newlog(controller, ((data->'call'->>'callid')::uuid)) WHERE data ? 'call' AND data->'call' ? 'callid';

-- this function allows assignment to data or orig's ->'call'->>'callid' to succeed and assign call_id
CREATE OR REPLACE FUNCTION newlog_trigger() RETURNS trigger AS
$$
BEGIN
	IF NEW.data IS NOT NULL AND NEW.data ? 'call' AND NEW.data->'call' ? 'callid' THEN
		NEW.call_id := NEW.data->'call'->>'callid';
	ELSIF NEW.orig IS NOT NULL AND NEW.orig ? 'call' AND NEW.orig->'call' ? 'callid' THEN
		NEW.call_id := NEW.orig->'call'->>'callid';
	END IF;
	RETURN NEW;
END
$$ LANGUAGE plpgsql;

CREATE TRIGGER newlog_trigger
	BEFORE INSERT OR UPDATE OF orig, data, call_id ON newlog
	FOR EACH ROW
	EXECUTE PROCEDURE newlog_trigger();

CREATE TABLE controller_positions (
	controller TEXT NOT NULL,
	pos INT NOT NULL,
	data JSONB NOT NULL,
	PRIMARY KEY(controller, pos)
);
-- data is {"psap":""}

CREATE TABLE controller_trunks (
	controller TEXT NOT NULL,
	trunk INT NOT NULL,
	data JSONB NOT NULL,
	PRIMARY KEY(controller, trunk)
);
-- data is {"psap":"", "call_type":"911"|"admin"}

CREATE TABLE basic_call_info2 (
	id BIGSERIAL PRIMARY KEY,
	callid TEXT NOT NULL,
	controller TEXT NOT NULL,
	psap TEXT,
	utc TIMESTAMPTZ NOT NULL,
	answer INTERVAL,
	abandoned BOOLEAN NOT NULL,
	pos INT,
	calltaker TEXT,
	is_911 BOOLEAN NOT NULL,
	cos TEXT,
	incoming BOOLEAN NOT NULL,
	trunk INT,
	internal_transfer BOOLEAN NOT NULL,
	data JSONB NOT NULL DEFAULT '{}' -- mostly for errors and such, other properties we don't search on
);
-- i don't think data is used for anything, but has ani, pani, and tid in it (maybe others?) for now

CREATE INDEX basic_call_info2_callid ON basic_call_info2(callid text_pattern_ops);
CREATE INDEX basic_call_info2_data_gin ON basic_call_info2 USING gin(data);
CREATE INDEX basic_call_info2_controller_utc ON basic_call_info2(controller, utc);
CREATE INDEX basic_call_info2_psap_utc ON basic_call_info2(psap, utc);
CREATE INDEX basic_call_info2_psap_utc_brin ON basic_call_info2 USING brin(psap, utc);
CREATE INDEX basic_call_info2_controller_utc_brin ON basic_call_info2 USING brin(controller, utc);

-- the following are some idea type stuff. In development

CREATE TABLE call_info (
	call_id UUID PRIMARY KEY,
	utc_range TSTZRANGE NOT NULL,	
	is_911 BOOLEAN NOT NULL,
	cdata JSONB NOT NULL DEFAULT '{}' -- other random things
);

CREATE INDEX call_info_cdata_gin ON call_info USING gin(cdata);

-- has data on "transfers"
CREATE TABLE call_transfer (
	transfer_id UUID PRIMARY KEY,
	call_id UUID NOT NULL REFERENCES call_info(call_id),
	type TEXT NOT NULL, -- is this the movement that starts a call
	abandoned BOOLEAN NOT NULL,
	utc TIMESTAMPTZ NOT NULL,
	answer_time INTERVAL, -- difference between dial & connect
	caller JSONB, -- if null is a barge
	callee JSONB, -- if null it failed/cancelled/abandoned/etc
	tdata JSONB NOT NULL DEFAULT '{}',
	--CHECK(caller IS NOT NULL OR callee IS NOT NULL),
	CHECK(caller IS NULL OR jsonb_typeof(caller) = 'object'),
	CHECK(callee IS NULL OR jsonb_typeof(callee) = 'object'),
	CHECK(jsonb_typeof(tdata) = 'object')
);

CREATE INDEX ON call_transfer(call_id);
CREATE INDEX call_transfer_tdata_gin ON call_transfer USING gin(tdata);
