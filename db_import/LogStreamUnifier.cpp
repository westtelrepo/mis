#include "LogStreamUnifier.h"

struct LogStreamUnifier::impl {
    impl(std::map<std::string, LogStream>&& logs_) {
        for (auto& elem: logs_) {
            files.emplace_back(std::move(elem.second));
        }

        std::sort(files.begin(), files.end(), [] (LogStream& a, LogStream& b) {
            auto left = a.peek();
            auto right = b.peek();

            // null is equal to null
            if (!left && !right) {
                return false;
            }

            // null is less than anything
            if (!left) {
                return true;
            }

            // anything is greater than null
            if (!right) {
                return false;
            }

            return a.peek()->utc() < b.peek()->utc();
        });
    }

    std::vector<LogStream> files;
    std::vector<LogStream> cur_files;
};

LogStreamUnifier::LogStreamUnifier(std::map<std::string, LogStream>&& logs)
: impl_{new impl{std::move(logs)}, [] (impl* i) { delete i; }} {

}

std::vector<OldLog> LogStreamUnifier::process() {
    auto& files = impl_->files;
    auto& cur_files = impl_->cur_files;
    do {
        if (!files.empty() || !cur_files.empty()) {
            if (cur_files.empty()) {
                assert(files.size() > 0);
                cur_files.emplace_back(std::move(files.front()));
                files.erase(files.begin());
                //std::cout << "Start " << e.filename << std::endl;
            }
            assert(!cur_files.empty());

            std::vector<OldLog> logentries;
            for (auto i = cur_files.begin(); i != cur_files.end();) {
                boost::optional<OldLog> e = i->next();
                if (e) {
                    logentries.push_back(*e);
                    ++i;
                }
                else {
                    //std::cout << "Stop " << i->first << std::endl;
                    i = cur_files.erase(i);
                }
            }
            if (logentries.empty())
                continue;

            for (size_t i = 1; i < logentries.size(); ++i)
                if (!logentries[0].issamelog(logentries[i])) {
                    std::cerr << "Logs differ: " << logentries[0]
                            << std::endl << logentries[i]
                        << std::endl;
                    throw std::runtime_error{"abort"};
                }

            for (auto i = files.begin(); i != files.end();) {
                boost::optional<OldLog> peek = i->peek();
                assert(peek);
                assert(logentries.size() > 0);
                if (peek->issamelog(logentries[0])) {
                    i->next();
                    //std::cout << "added " << i->first << std::endl;
                    cur_files.emplace_back(std::move(*i));
                    i = files.erase(i);
                // because the files array is in order, once peek->utc() has become greater
                // it will never find an equal log
                } else if (peek->utc() > logentries[0].utc()) {
                    break;
                } else
                    ++i;
            }

            return logentries;
        } else {
            return std::vector<OldLog>{};
        }
    } while(true);
}
