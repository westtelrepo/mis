#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("no ali database can be bid") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 17:02:43.247 UTC | Mon 2018/01/01 17:02:43 UTC | 316 | 333333333333333333 | T04 | P00 |            | p9110000000 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 17:02:43.260 UTC | Mon 2018/01/01 17:02:43 UTC | 225 | 333333333333333333 | T04 | P00 |            | p9110000000 | ALI     | - | [WARNING] No ALI Database can be Bid for RequestKey=9110000000
WT-001-1 | 2018/01/01 17:02:44.589 UTC | Mon 2018/01/01 17:02:44 UTC | 306 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 17:02:51.683 UTC | Mon 2018/01/01 17:02:51 UTC | 310 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | On Hold
WT-001-1 | 2018/01/01 17:02:55.272 UTC | Mon 2018/01/01 17:02:55 UTC | 311 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | Off Hold
WT-001-1 | 2018/01/01 17:02:55.279 UTC | Mon 2018/01/01 17:02:55 UTC | 342 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 17:02:55.310 UTC | Mon 2018/01/01 17:02:55 UTC | 309 | 333333333333333333 | T04 | P03 |            | p9110000000 | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 17:03:07.411 UTC | Mon 2018/01/01 17:03:07 UTC | 310 | 333333333333333333 | T04 | P03 |            | p9110000000 | ANI 01  | > | On Hold
WT-001-1 | 2018/01/01 17:03:14.334 UTC | Mon 2018/01/01 17:03:14 UTC | 311 | 333333333333333333 | T04 | P03 |            | p9110000000 | ANI 01  | > | Off Hold
WT-001-1 | 2018/01/01 17:03:14.348 UTC | Mon 2018/01/01 17:03:14 UTC | 342 | 333333333333333333 | T04 | P03 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 17:03:14.386 UTC | Mon 2018/01/01 17:03:14 UTC | 309 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 17:03:23.775 UTC | Mon 2018/01/01 17:03:23 UTC | 310 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | On Hold
WT-001-1 | 2018/01/01 17:03:28.445 UTC | Mon 2018/01/01 17:03:28 UTC | 311 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | Off Hold
WT-001-1 | 2018/01/01 17:03:28.451 UTC | Mon 2018/01/01 17:03:28 UTC | 342 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 17:03:28.504 UTC | Mon 2018/01/01 17:03:28 UTC | 309 | 333333333333333333 | T04 | P01 |            | p9110000000 | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 17:03:35.012 UTC | Mon 2018/01/01 17:03:35 UTC | 365 | 333333333333333333 | T04 | P01 |            | p9110000000 | ANI     | < | Conference Transfer from GUI; (Destination=1111111111)
WT-001-1 | 2018/01/01 17:03:37.207 UTC | Mon 2018/01/01 17:03:37 UTC | 395 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | < | Conference Transfer from GUI Connected; (Destination=1111111111)
WT-001-1 | 2018/01/01 17:03:37.259 UTC | Mon 2018/01/01 17:03:37 UTC | 309 | 333333333333333333 | T04 | P00 |            | p9110000000 | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 17:03:58.690 UTC | Mon 2018/01/01 17:03:58 UTC | 309 | 333333333333333333 | T04 | P03 |            | p9110000000 | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 17:04:14.007 UTC | Mon 2018/01/01 17:04:14 UTC | 342 | 333333333333333333 | T04 | P02 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 17:04:23.753 UTC | Mon 2018/01/01 17:04:23 UTC | 342 | 333333333333333333 | T04 | P01 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 17:04:24.751 UTC | Mon 2018/01/01 17:04:24 UTC | 342 | 333333333333333333 | T04 | P03 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 17:04:24.764 UTC | Mon 2018/01/01 17:04:24 UTC | 307 | 333333333333333333 | T04 | P00 |            | p9110000000 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-01-01 17:02:43.247");
    REQUIRE(record.answer_utc == "2018-01-01 17:02:44.589"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 4);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 17:04:24.764"s);
}

TEST_CASE("271 Auto Request w/ TransactionID") {
    auto val = parse(
R"(WT-001-1 | 2017/06/06 06:58:22.269 UTC | Tue 2017/06/06 06:58:22 UTC | 316 | 333333333333333333 | T02 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2017/06/06 06:58:22.310 UTC | Tue 2017/06/06 06:58:22 UTC | 222 | 333333333333333333 | T02 | P00 |            | p2222222222 | ALI 01A | < | Request Sent Index=2222222222 (to DB=04 PP=02; with TransactionID=01)
WT-001-1 | 2017/06/06 06:58:22.310 UTC | Tue 2017/06/06 06:58:22 UTC | 222 | 333333333333333333 | T02 | P00 |            | p2222222222 | ALI 01B | < | Request Sent Index=2222222222 (to DB=04 PP=02; with TransactionID=01)
WT-001-1 | 2017/06/06 06:58:24.933 UTC | Tue 2017/06/06 06:58:24 UTC | 306 | 333333333333333333 | T02 | P02 |            | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2017/06/06 06:58:27.012 UTC | Tue 2017/06/06 06:58:27 UTC | 402 | 333333333333333333 | T02 | P02 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:58:27.012 UTC | Tue 2017/06/06 06:58:27 UTC | 402 | 333333333333333333 | T02 | P02 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:58:47.003 UTC | Tue 2017/06/06 06:58:47 UTC | 271 | 333333333333333333 | T02 | P02 | 1111111111 | p2222222222 | ALI 01A | < | Auto Request Sent Index=2222222222 (to DB=05 PP=97; with TransactionID=01)
WT-001-1 | 2017/06/06 06:58:47.003 UTC | Tue 2017/06/06 06:58:47 UTC | 271 | 333333333333333333 | T02 | P02 | 1111111111 | p2222222222 | ALI 01B | < | Auto Request Sent Index=2222222222 (to DB=05 PP=97; with TransactionID=01)
WT-001-1 | 2017/06/06 06:58:49.421 UTC | Tue 2017/06/06 06:58:49 UTC | 342 | 333333333333333333 | T02 | P02 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2017/06/06 06:58:49.424 UTC | Tue 2017/06/06 06:58:49 UTC | 307 | 333333333333333333 | T02 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2017-06-06 06:58:22.269");
    REQUIRE(record.answer_utc == "2017-06-06 06:58:24.933"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 2);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2017-06-06 06:58:49.424"s);
}

TEST_CASE("270 Rebid Request Sent w/ TransactionID") {
    auto val = parse(
R"(WT-001-1 | 2017/06/06 06:16:54.527 UTC | Tue 2017/06/06 06:16:54 UTC | 316 | 333333333333333333 | T01 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2017/06/06 06:16:54.582 UTC | Tue 2017/06/06 06:16:54 UTC | 222 | 333333333333333333 | T01 | P00 |            | p2222222222 | ALI 01A | < | Request Sent Index=2222222222 (to DB=44 PP=01; with TransactionID=01)
WT-001-1 | 2017/06/06 06:16:54.582 UTC | Tue 2017/06/06 06:16:54 UTC | 222 | 333333333333333333 | T01 | P00 |            | p2222222222 | ALI 01B | < | Request Sent Index=2222222222 (to DB=44 PP=01; with TransactionID=01)
WT-001-1 | 2017/06/06 06:16:59.635 UTC | Tue 2017/06/06 06:16:59 UTC | 306 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2017/06/06 06:16:59.735 UTC | Tue 2017/06/06 06:16:59 UTC | 402 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:16:59.735 UTC | Tue 2017/06/06 06:16:59 UTC | 402 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:17:18.668 UTC | Tue 2017/06/06 06:17:18 UTC | 271 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | ALI 01A | < | Auto Request Sent Index=2222222222 (to DB=45 PP=97; with TransactionID=01)
WT-001-1 | 2017/06/06 06:17:18.668 UTC | Tue 2017/06/06 06:17:18 UTC | 271 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | ALI 01B | < | Auto Request Sent Index=2222222222 (to DB=45 PP=97; with TransactionID=01)
WT-001-1 | 2017/06/06 06:17:23.212 UTC | Tue 2017/06/06 06:17:23 UTC | 402 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:17:23.212 UTC | Tue 2017/06/06 06:17:23 UTC | 402 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:17:42.514 UTC | Tue 2017/06/06 06:17:42 UTC | 270 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | ALI 01A | < | Rebid Request Sent Index=2222222222 (to DB=46 PP=97; with TransactionID=01)
WT-001-1 | 2017/06/06 06:17:42.514 UTC | Tue 2017/06/06 06:17:42 UTC | 270 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | ALI 01B | < | Rebid Request Sent Index=2222222222 (to DB=46 PP=97; with TransactionID=01)
WT-001-1 | 2017/06/06 06:17:47.039 UTC | Tue 2017/06/06 06:17:47 UTC | 402 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:17:47.039 UTC | Tue 2017/06/06 06:17:47 UTC | 402 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:18:07.080 UTC | Tue 2017/06/06 06:18:07 UTC | 271 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | ALI 01A | < | Auto Request Sent Index=2222222222 (to DB=47 PP=97; with TransactionID=01)
WT-001-1 | 2017/06/06 06:18:07.080 UTC | Tue 2017/06/06 06:18:07 UTC | 271 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | ALI 01B | < | Auto Request Sent Index=2222222222 (to DB=47 PP=97; with TransactionID=01)
WT-001-1 | 2017/06/06 06:18:11.613 UTC | Tue 2017/06/06 06:18:11 UTC | 402 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:18:11.613 UTC | Tue 2017/06/06 06:18:11 UTC | 402 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2017/06/06 06:18:12.662 UTC | Tue 2017/06/06 06:18:12 UTC | 342 | 333333333333333333 | T01 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave exten: 2222222222
WT-001-1 | 2017/06/06 06:18:12.666 UTC | Tue 2017/06/06 06:18:12 UTC | 307 | 333333333333333333 | T01 | P01 | 1111111111 | p2222222222 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2017-06-06 06:16:54.527");
    REQUIRE(record.answer_utc == "2017-06-06 06:16:59.635"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 1);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2017-06-06 06:18:12.666"s);
}

TEST_CASE("209 ACK Recevied") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 26:21:57.318 UTC | Wed 2019/01/01 21:21:57 UTC | 316 | 777777777777777777 | T25 | P00 |            | p3333333333 | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 26:21:57.353 UTC | Wed 2019/01/01 21:21:57 UTC | 222 | 777777777777777777 | T25 | P00 |            | p3333333333 | ALI 01S | < | Request Sent Index=01 Trunk=25 (to ALI Service=01 )
WT-001-1 | 2019/01/01 26:21:57.402 UTC | Wed 2019/01/01 21:21:57 UTC | 209 | 777777777777777777 | T25 | P00 |            | p3333333333 | ALI 01S | > | ACK Received Index=01
WT-001-1 | 2019/01/01 26:22:00.943 UTC | Wed 2019/01/01 21:22:00 UTC | 306 | 777777777777777777 | T25 | P01 |            | p3333333333 | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 26:22:00.970 UTC | Wed 2019/01/01 21:22:00 UTC | 391 | 777777777777777777 | T25 | P00 |            | p3333333333 | ANI 01  | > | TDD MODE ON; Channel: f5f646c6-402b-11e9-a3ea-2be70af03246
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 | 777777777777777777 | T25 | P01 |            | p3333333333 | ALI 01S | > | Initial Record received (Data Follows)
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | <STX>201<CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | 333-333-3333 01/01 01:11:00 99 <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | CAUTION:  WIRELESS CALL        <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | VERIZON                    WRLS<CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   |     1119                       <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   |    ROAD RD WW - W SECTOR       <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | AAAAAAAAAA             ESN:0000<CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | CPF:VZW        MTN:111-111-1111<CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | Verify PD                      <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | Verify FD                      <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | Verify EMS                     <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | LAT:            LON:           <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | ELV:       COF:         COP:   <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 26:22:01.398 UTC | Wed 2019/01/01 21:22:01 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2019/01/01 26:22:01.456 UTC | Wed 2019/01/01 21:22:01 UTC | 402 | 777777777777777777 | T25 | P01 |            | p3333333333 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 26:24:30.058 UTC | Wed 2019/01/01 21:24:30 UTC | 342 | 777777777777777777 | T25 | P00 |            | p3333333333 | ANI 01  | > | Conference Leave exten: 911
WT-001-1 | 2019/01/01 26:24:30.061 UTC | Wed 2019/01/01 21:24:30 UTC | 307 | 777777777777777777 | T25 | P01 |            | p3333333333 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "777777777777777777");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 26:21:57.318");
        REQUIRE(rec.answer_utc == "2019-01-01 26:22:00.943"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 1);
        REQUIRE(!rec.calltaker);
        REQUIRE(rec.is_911);
        REQUIRE(rec.cos == "WRLS"s);
        REQUIRE(rec.trunk == 25);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 26:24:30.061"s);
    }
}

TEST_CASE("270 Rebid Request to ALI Service") {
    auto val = parse(
R"(WT-001-1 | 2019/02/02 02:25:00.280 UTC | Wed 2019/02/02 02:25:00 UTC | 316 | 888888888888888888 | T26 | P00 |            | p3333333333 | ANI 01  | > | Ringing
WT-001-1 | 2019/02/02 02:25:00.300 UTC | Wed 2019/02/02 02:25:00 UTC | 222 | 888888888888888888 | T26 | P00 |            | p3333333333 | ALI 01S | < | Request Sent Index=02 Trunk=26 (to ALI Service=01 )
WT-001-1 | 2019/02/02 02:25:00.344 UTC | Wed 2019/02/02 02:25:00 UTC | 209 | 888888888888888888 | T26 | P00 |            | p3333333333 | ALI 01S | > | ACK Received Index=02
WT-001-1 | 2019/02/02 02:25:01.873 UTC | Wed 2019/02/02 02:25:01 UTC | 306 | 888888888888888888 | T26 | P02 |            | p3333333333 | ANI 01  | > | Connect
WT-001-1 | 2019/02/02 02:25:01.926 UTC | Wed 2019/02/02 02:25:01 UTC | 391 | 888888888888888888 | T26 | P00 |            | p3333333333 | ANI 01  | > | TDD MODE ON; Channel: 630475ee-402c-11e9-a428-2be70af03246
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 | 888888888888888888 | T26 | P02 |            | p3333333333 | ALI 01S | > | Initial Record received (Data Follows)
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | <STX>202<CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | 910-644-6898 03/06 07:25:03 41 <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | CAUTION:  WIRELESS CALL        <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | AT&T MOBILITY              WRLS<CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   |      504                       <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   |    FISHERMAN RD - SE SECTOR    <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | CARROLLTON             ESN:0166<CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | CPF:ATTMO      MTN:330-511-4162<CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | Verify PD                      <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | Verify FD                      <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | Verify EMS                     <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | LAT:            LON:           <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | ELV:       COF:         COP:   <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/02/02 02:25:04.341 UTC | Wed 2019/02/02 02:25:04 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2019/02/02 02:25:04.438 UTC | Wed 2019/02/02 02:25:04 UTC | 402 | 888888888888888888 | T26 | P02 |            | p3333333333 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/02/02 02:26:18.461 UTC | Wed 2019/02/02 02:26:18 UTC | 270 | 888888888888888888 | T26 | P02 |            | p3333333333 | ALI 01S | < | Rebid Request Sent Index=03 Trunk=97 (to ALI Service=01 )
WT-001-1 | 2019/02/02 02:26:18.504 UTC | Wed 2019/02/02 02:26:18 UTC | 209 | 888888888888888888 | T26 | P02 |            | p3333333333 | ALI 01S | > | ACK Received Index=03
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 | 888888888888888888 | T26 | P02 |            | p3333333333 | ALI 01S | > | Rebid Record received (Data Follows)
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | <STX>203<CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | 910-644-6898 03/06 07:26:25 42 <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | CAUTION:  WIRELESS CALL        <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | AT&T MOBILITY              WPH2<CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   |      504                       <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   |    FISHERMAN RD - SE SECTOR    <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | CARROLLTON             ESN:0166<CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | CPF:ATTMO      MTN:330-511-4162<CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | Verify PD                      <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | Verify FD                      <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | Verify EMS                     <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | LAT:            LON:           <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | ELV:       COF:         COP:   <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/02/02 02:26:26.475 UTC | Wed 2019/02/02 02:26:26 UTC | 266 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2019/02/02 02:26:26.556 UTC | Wed 2019/02/02 02:26:26 UTC | 402 | 888888888888888888 | T26 | P02 |            | p3333333333 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/02/02 02:27:29.916 UTC | Wed 2019/02/02 02:27:29 UTC | 342 | 888888888888888888 | T26 | P02 |            | p3333333333 | ANI 01  | > | Conference Leave
WT-001-1 | 2019/02/02 02:27:29.939 UTC | Wed 2019/02/02 02:27:29 UTC | 307 | 888888888888888888 | T26 | P00 |            | p3333333333 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "888888888888888888");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-02-02 02:25:00.280");
        REQUIRE(rec.answer_utc == "2019-02-02 02:25:01.873"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(rec.is_911);
        REQUIRE(rec.cos == "WPH2"s);
        REQUIRE(rec.trunk == 26);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-02-02 02:27:29.939"s);
    }
}

TEST_CASE("270 Auto Request to ALI Service") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 01:10:40.996 UTC | Wed 2019/01/01 01:10:40 EST | 316 | 888888888888888888 | T32 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 01:10:41.028 UTC | Wed 2019/01/01 01:10:41 EST | 222 | 888888888888888888 | T32 | P00 |            | p2222222222 | ALI 01S | < | Request Sent Index=01 Trunk=32 (to ALI Service=01 )
WT-001-1 | 2019/01/01 01:10:41.080 UTC | Wed 2019/01/01 01:10:41 EST | 209 | 888888888888888888 | T32 | P00 |            | p2222222222 | ALI 01S | > | ACK Received Index=01
WT-001-1 | 2019/01/01 01:10:42.419 UTC | Wed 2019/01/01 01:10:42 EST | 306 | 888888888888888888 | T32 | P02 |            | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 01:10:42.474 UTC | Wed 2019/01/01 01:10:42 EST | 391 | 888888888888888888 | T32 | P00 |            | p2222222222 | ANI 01  | > | TDD MODE ON; Channel: 26625aca-403b-11e9-aabe-2be70af03246
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 | 888888888888888888 | T32 | P02 | 3333333333 | p2222222222 | ALI 01S | > | Initial Record received (Data Follows)
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | <STX>201<CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | 111-111-1111 01/01 01:11:11  1 <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | CAUTION:  WIRELESS CALL        <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | VERIZON                    WRLS<CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   |     1111                       <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   |    ROAD RD WW - W SECTOR       <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | AAAAAAAAAA             ESN:0000<CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | CPF:VZW        MTN:330-511-5172<CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | Verify PD                      <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | Verify FD                      <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | Verify EMS                     <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | LAT:            LON:           <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | ELV:       COF:         COP:   <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 01:10:45.075 UTC | Wed 2019/01/01 01:10:45 EST | 226 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2019/01/01 01:10:45.145 UTC | Wed 2019/01/01 01:10:45 EST | 402 | 888888888888888888 | T32 | P02 | 3333333333 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 01:11:15.154 UTC | Wed 2019/01/01 01:11:15 EST | 271 | 888888888888888888 | T32 | P02 | 3333333333 | p2222222222 | ALI 01S | < | Auto Request Sent Index=02 Trunk=97 (to ALI Service=01 )
WT-001-1 | 2019/01/01 01:11:15.164 UTC | Wed 2019/01/01 01:11:15 EST | 209 | 888888888888888888 | T32 | P02 | 3333333333 | p2222222222 | ALI 01S | > | ACK Received Index=02
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 | 888888888888888888 | T32 | P02 | 3333333333 | p2222222222 | ALI 01S | > | Auto Rebid Record received (Data Follows)
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | <STX>202<CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | 111-111-1111 01/01 01:11:22  2 <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | CAUTION:  WIRELESS CALL        <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | VERIZON                    WRLS<CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   |     1111                       <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   |    ROAD RD WW - W SECTOR       <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | AAAAAAAAAA             ESN:0000<CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | CPF:VZW        MTN:333-333-3333<CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | Verify PD                      <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | Verify FD                      <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | Verify EMS                     <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | LAT:            LON:           <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | ELV:       COF:         COP:   <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 01:11:23.133 UTC | Wed 2019/01/01 01:11:23 EST | 268 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2019/01/01 01:11:23.168 UTC | Wed 2019/01/01 01:11:23 EST | 402 | 888888888888888888 | T32 | P02 | 3333333333 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 01:11:30.991 UTC | Wed 2019/01/01 01:11:30 EST | 342 | 888888888888888888 | T32 | P02 | 3333333333 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:11:31.011 UTC | Wed 2019/01/01 01:11:31 EST | 307 | 888888888888888888 | T32 | P00 | 3333333333 | p2222222222 | ANI 01  | > | Disconnect
)");
    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "888888888888888888");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 01:10:40.996");
        REQUIRE(rec.answer_utc == "2019-01-01 01:10:42.419"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(rec.is_911);
        REQUIRE(rec.cos == "WRLS"s);
        REQUIRE(rec.trunk == 32);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 01:11:31.011"s);
    }
}
