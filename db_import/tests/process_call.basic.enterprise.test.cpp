#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("process_call enterprise transfer") {
    parse_config config;
    config.controller_positions["WT-001-1"][2] = "WT-001-A";

    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:56:06.418 UTC | Mon 2018/01/01 01:56:06 UTC | 316 | 333333333333333333 | T03 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:56:06.467 UTC | Mon 2018/01/01 01:56:06 UTC | 222 | 333333333333333333 | T03 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=35; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:56:06.467 UTC | Mon 2018/01/01 01:56:06 UTC | 222 | 333333333333333333 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=35; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 | 333333333333333333 | T03 | P00 |            | p1111111111 | ALI 01B | > | Initial Record received (Data Follows)
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | <STX>235<CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | 111-111-1111  01:56:06 11111111<CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | VERIZON WIRELESS               <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | 1111                       WPH2<CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | STATE ROUTE 111                <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   |                     LEC VZW  <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | CANAAN  TWP                  OH<CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | CELL111-1 NW SECTOR           <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   |                       ESN  1111<CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | P#111-111-1111 ALT#111-111-1111<CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | X-011.111111 Y+011.111111 AA11 <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | UNC00000   A       BBBB    <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2018/01/01 01:56:07.297 UTC | Mon 2018/01/01 01:56:07 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2018/01/01 01:56:08.264 UTC | Mon 2018/01/01 01:56:08 UTC | 306 | 333333333333333333 | T03 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 01:56:14.943 UTC | Mon 2018/01/01 01:56:14 UTC | 270 | 333333333333333333 | T03 | P02 | 1111111111 | p1111111111 | ALI 01A | < | Rebid Request Sent with RequestKey=1111111111; Index=36; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:56:14.943 UTC | Mon 2018/01/01 01:56:14 UTC | 270 | 333333333333333333 | T03 | P02 | 1111111111 | p1111111111 | ALI 01B | < | Rebid Request Sent with RequestKey=1111111111; Index=36; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 | 333333333333333333 | T03 | P02 | 1111111111 | p1111111111 | ALI 01B | > | Rebid Record received (Data Follows)
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | <STX>236<CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | 111-111-1111  01:56:18 11111111<CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | VERIZON WIRELESS               <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   |                            WPH2<CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | TMP RD 111                     <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   |                     LEC VZW  <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | MT. GILEAD                   OH<CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | CELL 111-1 S SECTOR           <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   |                       ESN  1111<CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | P#111-111-1111 ALT#111-111-1111<CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | X-011.111111 Y+011.111111 AA00 <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | UNC00000   C       WWWW    <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2018/01/01 01:56:19.199 UTC | Mon 2018/01/01 01:56:19 UTC | 266 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2018/01/01 01:56:54.531 UTC | Mon 2018/01/01 01:56:54 UTC | 365 | 333333333333333333 | T03 | P02 | 1111111111 | p1111111111 | ANI     | < | Tandem Transfer from GUI; (Destination=*11)
WT-001-1 | 2018/01/01 01:57:16.171 UTC | Mon 2018/01/01 20:57:16 UTC | 342 | 333333333333333333 | T03 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:57:16.192 UTC | Mon 2018/01/01 20:57:16 UTC | 307 | 333333333333333333 | T03 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-A"s);
    REQUIRE(record.utc == "2018-01-01 01:56:06.418");
    REQUIRE(record.answer_utc == "2018-01-01 01:56:08.264"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos == "WPH2"s);
    REQUIRE(record.trunk == 3);
    REQUIRE(record.transfer_reaction == transfer_type::tandem);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 01:57:16.192"s);
}

TEST_CASE("911 only by trunk") {
    parse_config config;
    config.controller_trunks["WT-001-1"][1].call_type = call_type::is_911;
    config.controller_trunks["WT-001-1"][1].psap = "WT-001-1";

    auto val = parse(
R"(WT-001-1 | 2015/01/01 01:56:14.845 UTC | Sat 2015/01/01 01:56:14 UTC | 316 | 111111111111111111 | T01 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2015/01/01 01:56:31.491 UTC | Sat 2015/01/01 01:56:31 UTC | 306 | 111111111111111111 | T01 | P02 |            | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2015/01/01 01:57:38.232 UTC | Sat 2015/01/01 01:57:38 UTC | 342 | 111111111111111111 | T01 | P02 |            | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2015/01/01 01:57:38.274 UTC | Sat 2015/01/01 01:57:38 UTC | 307 | 111111111111111111 | T01 | P00 |            | p2222222222 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2015-01-01 01:56:14.845");
    REQUIRE(record.answer_utc.value() == "2015-01-01 01:56:31.491");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos.value() == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 1);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2015-01-01 01:57:38.274"s);
}

TEST_CASE("enterprise identify abandoned call psap by trunk") {
    auto config = parse_config::parse(R"(
- controller: WT-004-1
  psaps:
    - id: WT-004-A
      trunks: [1, 2, 3]
    - id: WT-004-B
      trunks: [4]
    )");

    auto val = parse(
R"(WT-004-1 | 2018/04/15 10:57:32.379 UTC | Sat 2018/04/15 10:57:32 UTC | 316 | 666666666666666666 | T04 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-004-1 | 2018/04/15 10:57:32.389 UTC | Sat 2018/04/15 10:57:32 UTC | 222 | 666666666666666666 | T04 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=17; Trunk=04 (to DB=01 PP=01)
WT-004-1 | 2018/04/15 10:57:32.390 UTC | Sat 2018/04/15 10:57:32 UTC | 222 | 666666666666666666 | T04 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=17; Trunk=04 (to DB=01 PP=01)
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 | 666666666666666666 | T04 | P00 | 2222222222 | p1111111111 | ALI 01B | > | Initial Record received (Data Follows)
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | <STX>282<CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | 222-222-2222  22:22:22 00000000<CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | VERIZON WIRELESS               <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | 000                        WPH1<CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | AA 00                          <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   |                     LEC VZW  <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | WWWWWWW                      AA<CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | CELL 000-0 A SECTOR           <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   |                       ESN  0000<CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | P#222-222-2222 ALT#222-222-2222<CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | X-011.111111 Y+011.111111 AA000<CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | WWW00000   A       WWWW    <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-004-1 | 2018/04/15 10:57:36.087 UTC | Sat 2018/04/15 10:57:36 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-004-1 | 2018/04/15 10:57:53.577 UTC | Sat 2018/04/15 10:57:53 UTC | 312 | 666666666666666666 | T04 | P00 | 2222222222 | p1111111111 | ANI 01  | > | Abandoned
)", config);

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "666666666666666666");
    REQUIRE(record.controller == "WT-004-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-004-B"s);
    REQUIRE(record.utc == "2018-04-15 10:57:32.379");
    REQUIRE(!record.answer_utc);
    REQUIRE(record.abandoned == true);
    REQUIRE(!record.pos);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "WPH1"s);
    REQUIRE(record.trunk == 4);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}

TEST_CASE("395 Outbound Call to Position Connected between different PSAPs") {
    auto config = parse_config::parse(
R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [1]
    - id: WT-001-B
      positions: [7]
)");

    auto val = parse(
R"(WT-001-1 | 2016/01/01 01:01:13.886 UTC | Wed 2016/01/01 01:01:13 UTC | 372 | 111111111111111111 |     | P01 |            |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=1001)
WT-001-1 | 2016/01/01 01:01:17.566 UTC | Wed 2016/01/01 01:01:17 UTC | 395 | 111111111111111111 |     | P07 |            |             | ANI 01  | > | Outbound Call to Position Connected; (Destination=1001)
WT-001-1 | 2016/01/01 01:01:30.999 UTC | Wed 2016/01/01 01:01:30 UTC | 342 | 111111111111111111 |     | P07 |            |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/01/01 01:01:31.020 UTC | Wed 2016/01/01 01:01:31 UTC | 307 | 111111111111111111 |     | P01 |            |             | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);

    auto rec1 = val.at(0);
    auto rec2 = val.at(1);

    REQUIRE(rec1.callid == "111111111111111111");
    REQUIRE(rec1.controller == "WT-001-1");
    REQUIRE(rec1.subid == 0);
    REQUIRE(rec1.psap == "WT-001-A"s);
    REQUIRE(rec1.utc == "2016-01-01 01:01:13.886");
    REQUIRE(rec1.answer_utc == "2016-01-01 01:01:17.566"s);
    REQUIRE(rec1.abandoned == false);
    REQUIRE(rec1.pos == 1);
    REQUIRE(!rec1.calltaker);
    REQUIRE(rec1.is_911 == false);
    REQUIRE(!rec1.cos);
    REQUIRE(!rec1.trunk);
    REQUIRE(rec1.transfer_reaction == transfer_type::none);
    REQUIRE(rec1.incoming == false);
    REQUIRE(rec1.t_type == transfer_type::initial);
    REQUIRE(!rec1.disconnect_utc);

    REQUIRE(rec2.callid == "111111111111111111");
    REQUIRE(rec2.controller == "WT-001-1");
    REQUIRE(rec2.subid == 1);
    REQUIRE(rec2.psap == "WT-001-B"s);
    REQUIRE(rec2.utc == "2016-01-01 01:01:13.886");
    REQUIRE(rec2.answer_utc == "2016-01-01 01:01:17.566"s);
    REQUIRE(rec2.abandoned == false);
    REQUIRE(rec2.pos == 7);
    REQUIRE(!rec2.calltaker);
    REQUIRE(rec2.is_911 == false);
    REQUIRE(!rec2.cos);
    REQUIRE(!rec2.trunk);
    REQUIRE(rec2.transfer_reaction == transfer_type::none);
    REQUIRE(rec2.incoming == true);
    REQUIRE(rec2.t_type == transfer_type::initial);
    REQUIRE(rec2.disconnect_utc == "2016-01-01 01:01:31.020"s);
}

TEST_CASE("372 -> 306 Outbound Call w Calltaker and Dialplan unknown position") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [4]
    - id: WT-001-B
      positions: [42, 43]
  dialplan:
    - num: 1200
      psap: WT-001-B
)");

    auto val = parse(
R"(WT-001-1 | 2016/01/01 12:00:00.000 UTC | Tue 2016/01/01 12:00:00 UTC | 709 |                    |     | P04 |            |             | WRK     | - | [INFO] User BobTheBuilder - Workstation 04 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2017/01/01 01:08:27.855 UTC | Tue 2017/01/01 01:08:27 UTC | 372 | 111111111111111111 | T34 | P04 |            |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=1200)
WT-001-1 | 2017/01/01 01:08:39.677 UTC | Tue 2017/01/01 01:08:39 UTC | 306 | 111111111111111111 | T34 | P04 |            |             | ANI 01  | > | Connect
WT-001-1 | 2017/01/01 01:08:48.048 UTC | Tue 2017/01/01 01:08:48 UTC | 342 | 111111111111111111 | T34 | P00 |            |             | ANI 01  | > | Conference Leave exten: 1200
WT-001-1 | 2017/01/01 01:08:48.052 UTC | Tue 2017/01/01 01:08:48 UTC | 307 | 111111111111111111 | T34 | P04 |            |             | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);

    {
        auto record = val.at(0);

        REQUIRE(record.callid == "111111111111111111");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 0);
        REQUIRE(record.psap == "WT-001-A"s);
        REQUIRE(record.utc == "2017-01-01 01:08:27.855");
        REQUIRE(record.answer_utc == "2017-01-01 01:08:39.677"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 4);
        REQUIRE(record.calltaker == "BobTheBuilder"s);
        REQUIRE(record.is_911 == false);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 34);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == false);
        REQUIRE(record.t_type == transfer_type::initial);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(1);

        REQUIRE(record.callid == "111111111111111111");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 1);
        REQUIRE(record.psap == "WT-001-B"s);
        REQUIRE(record.utc == "2017-01-01 01:08:27.855");
        REQUIRE(record.answer_utc == "2017-01-01 01:08:39.677"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(!record.pos);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911 == false);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 34);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::initial);
        REQUIRE(record.disconnect_utc == "2017-01-01 01:08:48.052"s);
    }
}

TEST_CASE("372 -> 306 Outbound Call w Calltaker and Dialplan known position") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [4]
    - id: WT-001-B
      positions: [7]
  dialplan:
    - num: 1200
      psap: WT-001-B
)");

    auto val = parse(
R"(WT-001-1 | 2016/01/01 12:00:00.000 UTC | Tue 2016/01/01 12:00:00 UTC | 709 |                    |     | P04 |            |             | WRK     | - | [INFO] User BobTheBuilder - Workstation 04 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2016/01/01 12:00:00.000 UTC | Tue 2016/01/01 12:00:00 UTC | 709 |                    |     | P07 |            |             | WRK     | - | [INFO] User Adam - Workstation 07 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2017/01/01 01:08:27.855 UTC | Tue 2017/01/01 01:08:27 UTC | 372 | 111111111111111111 | T34 | P04 |            |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=1200)
WT-001-1 | 2017/01/01 01:08:39.677 UTC | Tue 2017/01/01 01:08:39 UTC | 306 | 111111111111111111 | T34 | P04 |            |             | ANI 01  | > | Connect
WT-001-1 | 2017/01/01 01:08:48.048 UTC | Tue 2017/01/01 01:08:48 UTC | 342 | 111111111111111111 | T34 | P00 |            |             | ANI 01  | > | Conference Leave exten: 1200
WT-001-1 | 2017/01/01 01:08:48.052 UTC | Tue 2017/01/01 01:08:48 UTC | 307 | 111111111111111111 | T34 | P04 |            |             | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);

    {
        auto record = val.at(0);

        REQUIRE(record.callid == "111111111111111111");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 0);
        REQUIRE(record.psap == "WT-001-A"s);
        REQUIRE(record.utc == "2017-01-01 01:08:27.855");
        REQUIRE(record.answer_utc == "2017-01-01 01:08:39.677"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 4);
        REQUIRE(record.calltaker == "BobTheBuilder"s);
        REQUIRE(record.is_911 == false);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 34);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == false);
        REQUIRE(record.t_type == transfer_type::initial);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(1);

        REQUIRE(record.callid == "111111111111111111");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 1);
        REQUIRE(record.psap == "WT-001-B"s);
        REQUIRE(record.utc == "2017-01-01 01:08:27.855");
        REQUIRE(record.answer_utc == "2017-01-01 01:08:39.677"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 7);
        REQUIRE(record.calltaker == "Adam"s);
        REQUIRE(record.is_911 == false);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 34);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::initial);
        REQUIRE(record.disconnect_utc == "2017-01-01 01:08:48.052"s);
    }
}
