#ifndef WESTTEL_PARSE_H
#define WESTTEL_PARSE_H

#include "../LogStream.h"
#include "../log_converter.h"
#include "../process_call.h"

inline std::vector<CallRecord> parse(const std::string& contents, parse_config config = parse_config{}) {
    LogStream reader{std::unique_ptr<std::istringstream>{new std::istringstream(contents)}, "file.txt"};

    std::vector<OldLog> olds;
    while (auto line = reader.next()) {
        olds.push_back(line.value());
    }

    log_converter convert;

    std::vector<Json::Value> ret;
    for (auto& line: olds) {
        auto c = convert.convert(line);
        if (c.isMember("call") && c["call"].isMember("callid")) {
            ret.push_back(c);
        }
    }

    return process_call(ret, config);
}

#endif
