#include "../log_converter.h"
#include "../LogStream.h"

#include <catch.hpp>

static std::vector<Json::Value> parse(const std::string& contents) {
    LogStream reader{std::unique_ptr<std::istringstream>{new std::istringstream(contents)}, "file.txt"};

    std::vector<OldLog> olds;
    while (auto line = reader.next()) {
        olds.push_back(line.value());
    }

    log_converter convert;

    std::vector<Json::Value> ret;
    for (auto& line: olds) {
        ret.push_back(convert.convert(line));
    }

    return ret;
}

TEST_CASE("parse", "log_converter") {
    auto val = parse("");
}

TEST_CASE("parse usernames", "log_converter") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 12:00:00.000 UTC | Tue 2018/01/01 12:00:00 UTC | 709 |                    |     | P02 |            |             | WRK     | - | [INFO] User BobTheBuilder - Workstation 02 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2018/01/01 12:00:05.000 UTC | Tue 2018/01/01 12:00:05 UTC | 306 | 111111111111111111 | T01 | P02 |            |             | ANI 01  | > | Connect
)");

    REQUIRE(val.at(1)["user"]["name"] == "BobTheBuilder");
}

TEST_CASE("unparse usernames", "log_converter") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 12:00:00.000 UTC | Tue 2018/01/01 12:00:00 UTC | 709 |                    |     | P02 |            |             | WRK     | - | [INFO] User BobTheBuilder - Workstation 02 At IP: 10.10.10.10 Now On Line
WY-062-1 | 2018/01/01 12:00:04.000 UTC | Tue 2018/01/01 12:00:04 UTC | 717 |                    |     | P02 |            |             | WRK     | - | [INFO] User BobTheBuilder - Workstation 02 At IP: 10.10.10.10 Now Off Line
WT-001-1 | 2018/01/01 12:00:05.000 UTC | Tue 2018/01/01 12:00:05 UTC | 306 | 111111111111111111 | T01 | P02 |            |             | ANI 01  | > | Connect
)");

    REQUIRE(val.at(2)["user"].isMember("name") == false);
}

TEST_CASE("proper position") {
    auto val = parse("WT-001-1 | 2018/01/01 01:02:00.000 UTC | Tue 2018/01/01 01:02:00 UTC | 306 | 111111111111111111 | T03 | P07 |            | p9999999999 | ANI 01  | > | Connect\n");
    REQUIRE(val.at(0)["user"]["position"].asInt() == 7);
}

TEST_CASE("ali NRF 226") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:00:00.000 UTC | Mon 2018/01/01 01:00:00 UTC | 226 | 111111111111111111 | T01 | P00 |            | p0000000000 | ALI 01A | > | Initial Record received (Data Follows)
WT-001-1 | 2018/01/01 01:00:00.000 UTC | Mon 2018/01/01 01:00:00 UTC | 226 |                    |     |     |            |             |         |   | <STX>912<CR>
WT-001-1 | 2018/01/01 01:00:00.000 UTC | Mon 2018/01/01 01:00:00 UTC | 226 |                    |     |     |            |             |         |   | 0009110013 NO RECORD FOUND<ETX>
)");

    REQUIRE(val.size() == 1);
    REQUIRE(val.at(0)["ali"]["cos"].asString() == "NRF");
    REQUIRE(val.at(0)["call"]["is_911"].asBool() == true);
}
