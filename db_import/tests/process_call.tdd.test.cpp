#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("tdd says and conversation record") {
    auto val = parse(
R"(WT-004-1 | 2014/01/01 01:39:44.489 UTC | Thu 2014/01/01 01:39:44 UTC | 316 | 777777777777777777 | T02 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-004-1 | 2014/01/01 01:39:44.512 UTC | Thu 2014/01/01 01:39:44 UTC | 222 | 777777777777777777 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=02 (to DB=01 PP=01)
WT-004-1 | 2014/01/01 01:39:44.512 UTC | Thu 2014/01/01 01:39:44 UTC | 222 | 777777777777777777 | T02 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=02 (to DB=01 PP=01)
WT-004-1 | 2014/01/01 01:39:45.716 UTC | Thu 2014/01/01 01:39:45 UTC | 306 | 777777777777777777 | T02 | P02 |            | p1111111111 | ANI 01  | > | Connect
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 | 777777777777777777 | T02 | P02 | 1111111111 | p1111111111 | ALI 01A | > | Initial Record received (Data Follows)
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | <STX>214<CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 BUSN 11/11 11:11<CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | wwww wwwwww wwwwwwww wwww   <CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   |        000      P#111-111-1111<CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | w  w ww                  <CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   |                     <CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   |                       000 00000<CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | ww wwwwwwwwwww                 <CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=CTL  <CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | PSAP=wwww--wwwwwwwwwww<CR>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | SPD<CR><LF>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | SFD      <CR><LF>
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   | SPFLD EMS
WT-004-1 | 2014/01/01 01:39:47.757 UTC | Thu 2014/01/01 01:39:47 UTC | 226 |                    |     |     |            |             |         |   |        <ETX>
WT-004-1 | 2014/01/01 01:39:57.197 UTC | Thu 2014/01/01 01:39:57 UTC | 382 | 777777777777777777 | T02 | P02 | 1111111111 | p1111111111 | ANI 01  | > | [TDD] Dispatcher 02 says: 911 what is your emergency q ga
WT-004-1 | 2014/01/01 01:40:26.424 UTC | Thu 2014/01/01 01:40:26 UTC | 382 | 777777777777777777 | T02 | P02 | 1111111111 | p1111111111 | ANI 01  | > | [TDD] Dispatcher 02 says: bnbnghfghfgh
WT-004-1 | 2014/01/01 01:40:54.753 UTC | Thu 2014/01/01 01:40:54 UTC | 342 | 777777777777777777 | T02 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-004-1 | 2014/01/01 01:40:54.774 UTC | Thu 2014/01/01 01:40:54 UTC | 307 | 777777777777777777 | T02 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
WT-004-1 | 2014/01/01 01:40:54.801 UTC | Thu 2014/01/01 01:40:54 UTC | 361 | 777777777777777777 | T02 | P02 | 1111111111 | p1111111111 | ANI 01  | > | TDD Conversation Record (Data Follows)
WT-004-1 | 2014/01/01 01:40:54.801 UTC | Thu 2014/01/01 01:40:54 UTC | 361 |                    |     |     |            |             |         |   |     20:39:57 (P2)911 what is your emergency q ga
WT-004-1 | 2014/01/01 01:40:54.801 UTC | Thu 2014/01/01 01:40:54 UTC | 361 |                    |     |     |            |             |         |   |
WT-004-1 | 2014/01/01 01:40:54.801 UTC | Thu 2014/01/01 01:40:54 UTC | 361 |                    |     |     |            |             |         |   |     20:40:26 (P2)bnbnghfghfgh
WT-004-1 | 2014/01/01 01:40:54.801 UTC | Thu 2014/01/01 01:40:54 UTC | 361 |                    |     |     |            |             |         |   |
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "777777777777777777");
    REQUIRE(record.controller == "WT-004-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-004-1"s);
    REQUIRE(record.utc == "2014-01-01 01:39:44.489");
    REQUIRE(record.answer_utc == "2014-01-01 01:39:45.716"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "BUSN"s);
    REQUIRE(record.trunk == 2);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2014-01-01 01:40:54.774"s);
}

TEST_CASE("tdd mode on with channel") {
    auto val = parse(
R"(WT-004-1 | 2016/01/01 01:05:01.676 UTC | Tue 2016/01/01 01:05:01 UTC | 316 | 222222222222222222 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-004-1 | 2016/01/01 01:05:01.703 UTC | Tue 2016/01/01 01:05:01 UTC | 222 | 222222222222222222 | T01 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=00; Trunk=01 (to DB=01 PP=01)
WT-004-1 | 2016/01/01 01:05:01.703 UTC | Tue 2016/01/01 01:05:01 UTC | 222 | 222222222222222222 | T01 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=00; Trunk=01 (to DB=01 PP=01)
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 | 222222222222222222 | T01 | P00 | 2222222222 | p1111111111 | ALI 01B | > | Initial Record received (Data Follows)
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | <STX>206<CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 WPH1 11/11 11:11<CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | wwwwww WIRELESS 111-111-1111<CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   |        000       P#111-111-1111<CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   |    w wwwwwwww ww - w wwww<CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | ww                  <CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | 2222222222            000 00000<CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | ww wwwww                       <CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=wwwww<CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | +11.111111  -111.111111   0000 <CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | PSAP=wwww--wwww WIRELESS<CR>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-004-1 | 2016/01/01 01:05:08.142 UTC | Tue 2016/01/01 01:05:08 UTC | 226 |                    |     |     |            |             |         |   | VERIFY EMS                     <ETX>
WT-004-1 | 2016/01/01 01:05:09.657 UTC | Tue 2016/01/01 01:05:09 UTC | 306 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ANI 01  | > | Connect
WT-004-1 | 2016/01/01 01:05:09.773 UTC | Tue 2016/01/01 01:05:09 UTC | 391 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ANI 01  | > | TDD MODE ON; Channel: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa
WT-004-1 | 2016/01/01 01:06:01.059 UTC | Tue 2016/01/01 01:06:01 UTC | 270 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ALI 01A | < | Rebid Request Sent with RequestKey=18929513; Index=07; Trunk=00 (to DB=01 PP=01)
WT-004-1 | 2016/01/01 01:06:01.059 UTC | Tue 2016/01/01 01:06:01 UTC | 270 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ALI 01B | < | Rebid Request Sent with RequestKey=18929513; Index=07; Trunk=00 (to DB=01 PP=01)
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ALI 01B | > | Rebid Record received (Data Follows)
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | <STX>207<CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | (000) 000-0000 WPH2 11/11 11:11<CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | wwwwww WIRELESS             <CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   |                  P#111-111-1111<CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   |    WIRELESS CALL         <CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   |                     <CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | 2222222222            000 00000<CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | ww wwww wwwwww ww              <CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   |                   TEL=wwwww<CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | +11.111111  -111.111111    000 <CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | PSAP=wwww--wwww WIRELESS<CR>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-004-1 | 2016/01/01 01:06:08.015 UTC | Tue 2016/01/01 01:06:08 UTC | 266 |                    |     |     |            |             |         |   | VERIFY EMS                     <ETX>
WT-004-1 | 2016/01/01 01:06:36.969 UTC | Tue 2016/01/01 01:06:36 UTC | 270 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ALI 01A | < | Rebid Request Sent with RequestKey=00000000; Index=00; Trunk=00 (to DB=01 PP=01)
WT-004-1 | 2016/01/01 01:06:36.969 UTC | Tue 2016/01/01 01:06:36 UTC | 270 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ALI 01B | < | Rebid Request Sent with RequestKey=00000000; Index=00; Trunk=00 (to DB=01 PP=01)
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ALI 01B | > | Rebid Record received (Data Follows)
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | <STX>208<CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | (111) 111-1111 WPH2 11/11 11:11<CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | wwwwww WIRELESS             <CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   |                  P#111-111-1111<CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   |    WIRELESS CALL         <CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   |                     <CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | 2222222222            000 00000<CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | ww wwww wwwwww ww              <CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   |                   TEL=wwwww<CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | +11.111111  -111.111111     00 <CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | PSAP=wwww--wwww WIRELESS<CR>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-004-1 | 2016/01/01 01:06:43.909 UTC | Tue 2016/01/01 01:06:43 UTC | 266 |                    |     |     |            |             |         |   | VERIFY EMS                     <ETX>
WT-004-1 | 2016/01/01 01:07:08.107 UTC | Tue 2016/01/01 01:07:08 UTC | 342 | 222222222222222222 | T01 | P02 | 2222222222 | p1111111111 | ANI 01  | > | Conference Leave
WT-004-1 | 2016/01/01 01:07:08.130 UTC | Tue 2016/01/01 01:07:08 UTC | 307 | 222222222222222222 | T01 | P00 | 2222222222 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-004-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-004-1"s);
    REQUIRE(record.utc == "2016-01-01 01:05:01.676");
    REQUIRE(record.answer_utc == "2016-01-01 01:05:09.657"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "WPH2"s);
    REQUIRE(record.trunk == 1);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2016-01-01 01:07:08.130"s);
}

TEST_CASE("tdd mode on without channel") {
    auto val = parse(
R"(WT-004-1 | 2011/01/01 01:18:26.358 UTC | Wed 2011/01/01 01:18:26 UTC | 316 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-004-1 | 2011/01/01 01:18:30.126 UTC | Wed 2011/01/01 01:18:30 UTC | 306 | 222222222222222222 | T09 | P03 | 1111111111 |             | ANI 01  | > | Connect
WT-004-1 | 2011/01/01 01:18:30.350 UTC | Wed 2011/01/01 01:18:30 UTC | 391 | 222222222222222222 | T09 | P03 | 1111111111 |             | ANI 01  | > | TDD MODE ON
WT-004-1 | 2011/01/01 01:18:36.925 UTC | Wed 2011/01/01 01:18:36 UTC | 309 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | > | Conference Join
WT-004-1 | 2011/01/01 01:18:37.188 UTC | Wed 2011/01/01 01:18:37 UTC | 342 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-004-1 | 2011/01/01 01:19:37.246 UTC | Wed 2011/01/01 01:19:37 UTC | 342 | 222222222222222222 | T09 | P03 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-004-1 | 2011/01/01 01:19:38.830 UTC | Wed 2011/01/01 01:19:38 UTC | 307 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-004-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-004-1"s);
    REQUIRE(record.utc == "2011-01-01 01:18:26.358");
    REQUIRE(record.answer_utc == "2011-01-01 01:18:30.126"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 3);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == false);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 9);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2011-01-01 01:19:38.830"s);
}

TEST_CASE("tdd character received") {
    auto val = parse(
R"(WT-001-1 | 2017/01/01 13:53:22.973 UTC | Mon 2017/01/01 17:53:22 MDT | 316 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2017/01/01 13:53:25.167 UTC | Mon 2017/01/01 17:53:25 MDT | 306 | 222222222222222222 | T09 | P04 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2017/01/01 13:53:30.258 UTC | Mon 2017/01/01 17:53:30 MDT | 309 | 222222222222222222 | T09 | P03 | 1111111111 |             | ANI 01  | > | Conference Join
WT-001-1 | 2017/01/01 13:53:30.327 UTC | Mon 2017/01/01 17:53:30 MDT | 391 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | TDD MODE ON; Channel: aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa
WT-001-1 | 2017/01/01 13:55:06.831 UTC | Mon 2017/01/01 17:55:06 MDT | 310 | 222222222222222222 | T09 | P04 | 1111111111 |             | ANI 01  | > | On Hold
WT-001-1 | 2017/01/01 13:55:10.956 UTC | Mon 2017/01/01 17:55:10 MDT | 342 | 222222222222222222 | T09 | P04 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2017/01/01 13:55:10.995 UTC | Mon 2017/01/01 17:55:10 MDT | 392 | 222222222222222222 | T09 | P04 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=3333333333)
WT-001-1 | 2017/01/01 13:55:10.997 UTC | Mon 2017/01/01 17:55:10 MDT | 342 | 222222222222222222 | T09 | P03 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2017/01/01 13:55:12.222 UTC | Mon 2017/01/01 17:55:12 MDT | 370 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Connected; (Destination=3333333333)
WT-001-1 | 2017/01/01 14:00:37.192 UTC | Mon 2017/01/01 18:00:37 MDT | 351 | 222222222222222222 | T09 | P04 | 1111111111 |             | ANI 01  | > | [TDD] TDD Character "V" Received
WT-001-1 | 2017/01/01 14:00:41.234 UTC | Mon 2017/01/01 18:00:41 MDT | 352 | 222222222222222222 | T09 | P04 | 1111111111 |             | ANI 01  | > | [TDD] TDD Caller Says: "V"
WT-001-1 | 2017/01/01 14:01:26.681 UTC | Mon 2017/01/01 18:01:26 MDT | 342 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | Conference Leave exten: 93333333333
WT-001-1 | 2017/01/01 14:01:26.705 UTC | Mon 2017/01/01 18:01:26 MDT | 307 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | Disconnect
WT-001-1 | 2017/01/01 14:01:26.733 UTC | Mon 2017/01/01 18:01:26 MDT | 361 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | TDD Conversation Record (Data Follows)
WT-001-1 | 2017/01/01 14:01:26.733 UTC | Mon 2017/01/01 18:01:26 MDT | 361 |                    |     |     |            |             |         |   |     08:00:41 (Caller)
WT-001-1 | 2017/01/01 14:01:26.733 UTC | Mon 2017/01/01 18:01:26 MDT | 361 |                    |     |     |            |             |         |   |     V
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2017-01-01 13:53:22.973");
    REQUIRE(record.answer_utc == "2017-01-01 13:53:25.167"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 4);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == false);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 9);
    REQUIRE(record.transfer_reaction == transfer_type::blind);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2017-01-01 14:01:26.705"s);
}
