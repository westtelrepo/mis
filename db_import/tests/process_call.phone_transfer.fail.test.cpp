#include "parse.h"

#include <boost/optional/optional_io.hpp>

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("process_call inbound failed attended") {
    auto val = parse(
R"(WT-001-1 | 2018/02/01 01:21:45.069 UTC | Wed 2018/02/01 01:21:45 UTC | 316 | 777777777777777777 |     | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=anonymous@anonymous.invalid)
WT-001-1 | 2018/02/01 01:21:47.364 UTC | Wed 2018/02/01 01:21:47 UTC | 306 | 777777777777777777 |     | P04 | VoIP CALL  |             | ANI 01  | > | Connect (From=anonymous@anonymous.invalid)
WT-001-1 | 2018/02/01 01:22:04.329 UTC | Wed 2018/02/01 01:22:04 UTC | 310 | 777777777777777777 |     | P04 | VoIP CALL  |             | ANI 01  | > | On Hold
WT-001-1 | 2018/02/01 01:22:10.181 UTC | Wed 2018/02/01 01:22:10 UTC | 373 | 777777777777777777 |     | P04 | VoIP CALL  |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=33333333333)
WT-001-1 | 2018/02/01 01:22:16.895 UTC | Wed 2018/02/01 01:22:16 UTC | 342 | 777777777777777777 |     | P04 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/02/01 01:22:16.935 UTC | Wed 2018/02/01 01:22:16 UTC | 366 | 777777777777777777 |     | P04 | VoIP CALL  |             | ANI 01  | > | Attended Transfer from Phone Cancelled; (Destination=33333333333)
WT-001-1 | 2018/02/01 01:29:03.876 UTC | Wed 2018/02/01 01:29:03 UTC | 307 | 777777777777777777 |     | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "777777777777777777");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-02-01 01:21:45.069");
    REQUIRE(record.answer_utc == "2018-02-01 01:21:47.364"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 4);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == false);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-02-01 01:29:03.876"s);
}

TEST_CASE("process_call cancelled attended transfer from phone") {
    auto val = parse(
R"(WT-001-1 | 2018/02/02 01:11:47.490 UTC | Tue 2018/02/02 03:41:47 UTC | 316 | 111111111111111111 |     | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=anonymous@anonymous.invalid)
WT-001-1 | 2018/02/02 01:11:49.511 UTC | Tue 2018/02/02 03:41:49 UTC | 306 | 111111111111111111 |     | P07 | VoIP CALL  |             | ANI 01  | > | Connect (From=anonymous@anonymous.invalid)
WT-001-1 | 2018/02/02 01:12:20.636 UTC | Tue 2018/02/02 03:42:20 UTC | 310 | 111111111111111111 |     | P07 | VoIP CALL  |             | ANI 01  | > | On Hold
WT-001-1 | 2018/02/02 01:12:25.827 UTC | Tue 2018/02/02 03:42:25 UTC | 373 | 111111111111111111 |     | P07 | VoIP CALL  |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=11111111111)
WT-001-1 | 2018/02/02 01:12:32.194 UTC | Tue 2018/02/02 03:42:32 UTC | 342 | 111111111111111111 |     | P07 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/02/02 01:12:32.234 UTC | Tue 2018/02/02 03:42:32 UTC | 366 | 111111111111111111 |     | P07 | VoIP CALL  |             | ANI 01  | > | Attended Transfer from Phone Cancelled; (Destination=11111111111)
WT-001-1 | 2018/02/02 01:19:02.099 UTC | Tue 2018/02/02 03:49:02 UTC | 307 | 111111111111111111 |     | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-02-02 01:11:47.490");
    REQUIRE(record.answer_utc.value() == "2018-02-02 01:11:49.511");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos.value() == 7);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-02-02 01:19:02.099"s);
}

TEST_CASE("process_call attended transfer from phone failed with reason") {
    auto val = parse(
R"(WT-042-1 | 2018/03/12 15:06:48.681 UTC | Mon 2018/03/12 15:06:48 UTC | 316 | 333333333333333337 | T12 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-042-1 | 2018/03/12 15:06:48.704 UTC | Mon 2018/03/12 15:06:48 UTC | 222 | 333333333333333337 | T12 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=11; Trunk=12 (to DB=01 PP=01)
WT-042-1 | 2018/03/12 15:06:48.704 UTC | Mon 2018/03/12 15:06:48 UTC | 222 | 333333333333333337 | T12 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=11; Trunk=12 (to DB=01 PP=01)
WT-042-1 | 2018/03/12 15:06:51.346 UTC | Mon 2018/03/12 15:06:51 UTC | 306 | 333333333333333337 | T12 | P02 |            | p1111111111 | ANI 01  | > | Connect
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 | 333333333333333337 | T12 | P02 |            | p1111111111 | ALI 01B | > | Initial Record received (Data Follows)
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | <STX>233<CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | 111-111-1111  11:11:11 11111111<CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | SPRINT PCS WIRELESS            <CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | 111            N           WPH1<CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | 11TH ST                        <CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   |                     WWW WWWWW<CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | AAAAAAAAA                    AA<CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | CELL 1111-1 W SECTOR          <CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   |                       ESN  0000<CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | P#111-111-1111 ALT#111-111-1111<CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | X-111.111111 Y+111.111111 AA111<CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | WWW00000   W       WWWW    <CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | AAAAAAAAA COUNTY LAW A         <CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | AAAAAAAAA COUNTY FIRE          <CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | AAAAAAAAA COUNTY EMS           <CR>
WT-042-1 | 2018/03/12 15:06:52.500 UTC | Mon 2018/03/12 15:06:52 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-042-1 | 2018/03/12 15:07:12.608 UTC | Mon 2018/03/12 15:07:12 UTC | 271 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ALI 01A | < | Auto Request Sent with RequestKey=1111111111; Index=12; Trunk=97 (to DB=01 PP=01)
WT-042-1 | 2018/03/12 15:07:12.608 UTC | Mon 2018/03/12 15:07:12 UTC | 271 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ALI 01B | < | Auto Request Sent with RequestKey=1111111111; Index=12; Trunk=97 (to DB=01 PP=01)
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ALI 01B | > | Auto Rebid Record received (Data Follows)
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | <STX>235<CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | 111-111-1111  11:11:12 11111118<CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | SPRINT PCS WIRELESS            <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | 111            N           WPH2<CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | 11TH ST                        <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   |                     WWW WWWWW<CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | AAAAAAAAA                    AA<CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | CELL 1111-1 W SECTOR          <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   |                       ESN  0000<CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | P#111-111-1111 ALT#111-111-1111<CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | X-111.111111 Y+111.111111 AA00 <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | WWW00000   W       WWWW    <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | AAAAAAAAA COUNTY LAW A         <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | AAAAAAAAA COUNTY FIRE          <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | AAAAAAAAA COUNTY EMS           <CR>
WT-042-1 | 2018/03/12 15:07:16.962 UTC | Mon 2018/03/12 15:07:16 UTC | 268 |                    |     |     |            |             |         |   | <ETX>
WT-042-1 | 2018/03/12 15:08:18.754 UTC | Mon 2018/03/12 15:08:18 UTC | 309 | 333333333333333337 | T12 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Conference Join
WT-042-1 | 2018/03/12 15:09:01.224 UTC | Mon 2018/03/12 15:09:01 UTC | 310 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | On Hold
WT-042-1 | 2018/03/12 15:09:02.229 UTC | Mon 2018/03/12 15:09:02 UTC | 373 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=2222222222)
WT-042-1 | 2018/03/12 15:09:02.229 UTC | Mon 2018/03/12 15:09:02 UTC | 369 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=2222222222); Reason=CALL_REJECTED
WT-042-1 | 2018/03/12 15:11:38.923 UTC | Mon 2018/03/12 15:11:38 UTC | 311 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Off Hold
WT-042-1 | 2018/03/12 15:11:52.712 UTC | Mon 2018/03/12 15:11:52 UTC | 310 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | On Hold
WT-042-1 | 2018/03/12 15:11:57.021 UTC | Mon 2018/03/12 15:11:57 UTC | 373 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=3333333333)
WT-042-1 | 2018/03/12 15:11:57.022 UTC | Mon 2018/03/12 15:11:57 UTC | 369 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=3333333333); Reason=CALL_REJECTED
WT-042-1 | 2018/03/12 15:12:21.736 UTC | Mon 2018/03/12 15:12:21 UTC | 310 | 333333333333333337 | T12 | P01 | 1111111111 | p1111111111 | ANI 01  | > | On Hold
WT-042-1 | 2018/03/12 15:12:25.379 UTC | Mon 2018/03/12 15:12:25 UTC | 373 | 333333333333333337 | T12 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=3333333333)
WT-042-1 | 2018/03/12 15:12:25.379 UTC | Mon 2018/03/12 15:12:25 UTC | 369 | 333333333333333337 | T12 | P01 | 1111111111 | p1111111111 | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=3333333333); Reason=CALL_REJECTED
WT-042-1 | 2018/03/12 15:12:40.649 UTC | Mon 2018/03/12 15:12:40 UTC | 373 | 333333333333333337 | T12 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=3333333333)
WT-042-1 | 2018/03/12 15:12:40.649 UTC | Mon 2018/03/12 15:12:40 UTC | 369 | 333333333333333337 | T12 | P01 | 1111111111 | p1111111111 | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=3333333333); Reason=CALL_REJECTED
WT-042-1 | 2018/03/12 15:13:00.047 UTC | Mon 2018/03/12 15:13:00 UTC | 373 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=3333333333)
WT-042-1 | 2018/03/12 15:13:00.047 UTC | Mon 2018/03/12 15:13:00 UTC | 369 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=3333333333); Reason=CALL_REJECTED
WT-042-1 | 2018/03/12 15:13:09.273 UTC | Mon 2018/03/12 15:13:09 UTC | 311 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Off Hold
WT-042-1 | 2018/03/12 15:13:13.355 UTC | Mon 2018/03/12 15:13:13 UTC | 310 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | On Hold
WT-042-1 | 2018/03/12 15:13:28.075 UTC | Mon 2018/03/12 15:13:28 UTC | 311 | 333333333333333337 | T12 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Off Hold
WT-042-1 | 2018/03/12 15:13:31.840 UTC | Mon 2018/03/12 15:13:31 UTC | 342 | 333333333333333337 | T12 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-042-1 | 2018/03/12 15:13:33.462 UTC | Mon 2018/03/12 15:13:33 UTC | 311 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Off Hold
WT-042-1 | 2018/03/12 15:13:34.445 UTC | Mon 2018/03/12 15:13:34 UTC | 310 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | On Hold
WT-042-1 | 2018/03/12 15:13:49.634 UTC | Mon 2018/03/12 15:13:49 UTC | 311 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Off Hold
WT-042-1 | 2018/03/12 15:13:51.357 UTC | Mon 2018/03/12 15:13:51 UTC | 342 | 333333333333333337 | T12 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-042-1 | 2018/03/12 15:13:51.398 UTC | Mon 2018/03/12 15:13:51 UTC | 307 | 333333333333333337 | T12 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333337");
    REQUIRE(record.controller == "WT-042-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-042-1");
    REQUIRE(record.utc == "2018-03-12 15:06:48.681");
    REQUIRE(record.answer_utc.value() == "2018-03-12 15:06:51.346");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos.value() == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos == "WPH2"s);
    REQUIRE(record.trunk == 12);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-03-12 15:13:51.398"s);
}

TEST_CASE("failed blind with spurious connect") {
    auto val = parse(
R"(WT-001-1 | 2018/02/02 00:17:55.584 UTC | Mon 2018/02/02 00:17:55 UTC | 316 | 222222222222222222 | T17 | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2018/02/02 00:18:04.478 UTC | Mon 2018/02/02 00:18:04 UTC | 306 | 222222222222222222 | T17 | P03 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2018/02/02 00:18:30.984 UTC | Mon 2018/02/02 00:18:30 UTC | 310 | 222222222222222222 | T17 | P03 | 1111111111 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/02/02 00:18:37.483 UTC | Mon 2018/02/02 00:18:37 UTC | 306 | 222222222222222222 | T17 | P02 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2018/02/02 00:18:37.489 UTC | Mon 2018/02/02 00:18:37 UTC | 342 | 222222222222222222 | T17 | P03 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/02/02 00:18:54.780 UTC | Mon 2018/02/02 00:18:54 UTC | 342 | 222222222222222222 | T17 | P02 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/02/02 00:18:55.342 UTC | Mon 2018/02/02 00:18:55 UTC | 392 | 222222222222222222 | T17 | P02 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=12222222222)
WT-001-1 | 2018/02/02 00:18:55.885 UTC | Mon 2018/02/02 00:18:55 UTC | 369 | 222222222222222222 | T17 | P02 | 1111111111 |             | ANI 01  | < | Blind Transfer from Phone Failed; (Destination=12222222222); Reason=UNALLOCATED_NUMBER
WT-001-1 | 2018/02/02 00:18:59.612 UTC | Mon 2018/02/02 00:18:59 UTC | 395 | 222222222222222222 | T17 | P02 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Connected; (Destination=12222222222)
WT-001-1 | 2018/02/02 00:19:18.167 UTC | Mon 2018/02/02 00:19:18 UTC | 342 | 222222222222222222 | T17 | P02 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/02/02 00:19:18.189 UTC | Mon 2018/02/02 00:19:18 UTC | 307 | 222222222222222222 | T17 | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-02-02 00:17:55.584");
    REQUIRE(record.answer_utc == "2018-02-02 00:18:04.478"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 3);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-02-02 00:19:18.189"s);
}

TEST_CASE("failed blind from phone") {
    auto val = parse(
R"(WT-001-1 | 2012/01/01 10:56:25.073 UTC | Sun 2012/01/01 10:56:25 MDT | 316 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2012/01/01 10:56:29.586 UTC | Sun 2012/01/01 10:56:29 MDT | 306 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2012/01/01 10:56:34.707 UTC | Sun 2012/01/01 10:56:34 MDT | 309 | 222222222222222222 | T09 | P03 | 1111111111 |             | ANI 01  | > | Conference Join
WT-001-1 | 2012/01/01 10:57:09.863 UTC | Sun 2012/01/01 10:57:09 MDT | 309 | 222222222222222222 | T09 | P02 | 1111111111 |             | ANI 01  | > | Conference Join
WT-001-1 | 2012/01/01 10:57:24.508 UTC | Sun 2012/01/01 10:57:24 MDT | 342 | 222222222222222222 | T09 | P03 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2012/01/01 10:59:10.223 UTC | Sun 2012/01/01 10:59:10 MDT | 342 | 222222222222222222 | T09 | P02 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2012/01/01 11:03:08.394 UTC | Sun 2012/01/01 11:03:08 MDT | 310 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | > | On Hold
WT-001-1 | 2012/01/01 11:03:11.968 UTC | Sun 2012/01/01 11:03:11 MDT | 342 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2012/01/01 11:03:11.969 UTC | Sun 2012/01/01 11:03:11 MDT | 392 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=2222222222)
WT-001-1 | 2012/01/01 11:03:12.288 UTC | Sun 2012/01/01 11:03:12 MDT | 369 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | < | Blind Transfer from Phone Failed; (Destination=2222222222); Reason=UNALLOCATED_NUMBER
WT-001-1 | 2012/01/01 11:03:20.232 UTC | Sun 2012/01/01 11:03:20 MDT | 370 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | > | Reconnect Ringback
WT-001-1 | 2012/01/01 11:03:57.323 UTC | Sun 2012/01/01 11:03:57 MDT | 342 | 222222222222222222 | T09 | P01 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2012/01/01 11:03:57.344 UTC | Sun 2012/01/01 11:03:57 MDT | 307 | 222222222222222222 | T09 | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2012-01-01 10:56:25.073");
    REQUIRE(record.answer_utc == "2012-01-01 10:56:29.586"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 9);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2012-01-01 11:03:57.344"s);
}

TEST_CASE("failed attended transfer to PSAP by dialplan") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [4]
    - id: WT-001-B
      positions: [5]
    - id: WT-001-C
      positions: [1]
  dialplan:
    - num: 1000
      psap: WT-001-C
)");

    auto val = parse(
R"(WT-001-1 | 2018/02/02 02:57:22.955 UTC | Mon 2018/02/02 02:57:22 UTC | 316 | 222222222222222222 | T09 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2018/02/02 02:57:22.998 UTC | Mon 2018/02/02 02:57:22 UTC | 222 | 222222222222222222 | T09 | P00 |            | p2222222222 | ALI 01A | < | Request Sent with RequestKey=2222222222; Index=29; Trunk=09 (to DB=01 PP=01)
WT-001-1 | 2018/02/02 02:57:22.999 UTC | Mon 2018/02/02 02:57:22 UTC | 222 | 222222222222222222 | T09 | P00 |            | p2222222222 | ALI 01B | < | Request Sent with RequestKey=2222222222; Index=29; Trunk=09 (to DB=01 PP=01)
WT-001-1 | 2018/02/02 02:57:56.904 UTC | Mon 2018/02/02 02:57:56 UTC | 271 | 222222222222222222 | T09 | P00 | 1111111111 | p2222222222 | ALI 01A | < | Auto Request Sent with RequestKey=2222222222; Index=31; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/02/02 02:57:56.905 UTC | Mon 2018/02/02 02:57:56 UTC | 271 | 222222222222222222 | T09 | P00 | 1111111111 | p2222222222 | ALI 01B | < | Auto Request Sent with RequestKey=2222222222; Index=31; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/02/02 02:58:11.779 UTC | Mon 2018/02/02 02:58:11 UTC | 306 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2018/02/02 02:58:11.877 UTC | Mon 2018/02/02 02:58:11 UTC | 402 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2018/02/02 02:58:11.877 UTC | Mon 2018/02/02 02:58:11 UTC | 402 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2018/02/02 02:58:47.483 UTC | Mon 2018/02/02 02:58:47 UTC | 365 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/02/02 02:59:05.860 UTC | Mon 2018/02/02 02:59:05 UTC | 365 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/02/02 02:59:21.139 UTC | Mon 2018/02/02 02:59:21 UTC | 365 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/02/02 02:59:26.548 UTC | Mon 2018/02/02 02:59:26 UTC | 365 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@diff.example.com)
WT-001-1 | 2018/02/02 02:59:28.603 UTC | Mon 2018/02/02 02:59:28 UTC | 378 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@diff.example.com)
WT-001-1 | 2018/02/02 02:59:28.650 UTC | Mon 2018/02/02 02:59:28 UTC | 402 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2018/02/02 02:59:28.650 UTC | Mon 2018/02/02 02:59:28 UTC | 402 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2018/02/02 02:59:48.533 UTC | Mon 2018/02/02 02:59:48 UTC | 365 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/05/28 03:00:00.709 UTC | Mon 2018/05/28 03:00:00 UTC | 365 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/05/28 03:00:11.897 UTC | Mon 2018/05/28 03:00:11 UTC | 342 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/05/28 03:01:19.392 UTC | Mon 2018/05/28 03:01:19 UTC | 310 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | On Hold
WT-001-1 | 2018/05/28 03:01:41.570 UTC | Mon 2018/05/28 03:01:41 UTC | 311 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Off Hold
WT-001-1 | 2018/05/28 03:01:43.762 UTC | Mon 2018/05/28 03:01:43 UTC | 310 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | On Hold
WT-001-1 | 2018/05/28 03:01:51.287 UTC | Mon 2018/05/28 03:01:51 UTC | 373 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1000)
WT-001-1 | 2018/05/28 03:02:23.299 UTC | Mon 2018/05/28 03:02:23 UTC | 369 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=1000); Reason=RECOVERY_ON_TIMER_EXPIRE
WT-001-1 | 2018/05/28 03:04:56.128 UTC | Mon 2018/05/28 03:04:56 UTC | 311 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Off Hold
WT-001-1 | 2018/05/28 03:05:10.134 UTC | Mon 2018/05/28 03:05:10 UTC | 365 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/05/28 03:05:11.816 UTC | Mon 2018/05/28 03:05:11 UTC | 402 | 222222222222222222 | T09 | P01 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2018/05/28 03:05:11.816 UTC | Mon 2018/05/28 03:05:11 UTC | 402 | 222222222222222222 | T09 | P01 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2018/05/28 03:05:11.832 UTC | Mon 2018/05/28 03:05:11 UTC | 378 | 222222222222222222 | T09 | P01 | 1111111111 | p2222222222 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@example.com)
WT-001-1 | 2018/05/28 03:05:21.038 UTC | Mon 2018/05/28 03:05:21 UTC | 342 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/05/28 03:07:29.835 UTC | Mon 2018/05/28 03:07:29 UTC | 342 | 222222222222222222 | T09 | P01 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/05/28 03:07:29.918 UTC | Mon 2018/05/28 03:07:29 UTC | 307 | 222222222222222222 | T09 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 4);

    {
        auto record = val.at(0);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 0);
        REQUIRE(record.psap == "WT-001-A"s);
        REQUIRE(record.utc == "2018-02-02 02:57:22.955");
        REQUIRE(record.answer_utc == "2018-02-02 02:58:11.779"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 4);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::ng911);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::initial);
        REQUIRE(record.disconnect_utc == "2018-05-28 03:07:29.918"s);
    }

    {
        auto record = val.at(1);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 1);
        REQUIRE(record.psap == "WT-001-B"s);
        REQUIRE(record.utc == "2018-02-02 02:59:26.548");
        REQUIRE(record.answer_utc == "2018-02-02 02:59:28.603"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 5);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::ng911);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(2);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 2);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-05-28 03:01:51.287");
        REQUIRE(!record.answer_utc);
        REQUIRE(record.abandoned == true);
        REQUIRE(!record.pos);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::attended);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(3);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 3);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-05-28 03:05:10.134");
        REQUIRE(record.answer_utc == "2018-05-28 03:05:11.832"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 1);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }
}

TEST_CASE("failed blind transfer from phone with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 01:30:59.352 UTC | Wed 2019/01/01 01:30:59 EST | 316 | 888888888888888888 | T50 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 01:31:01.695 UTC | Wed 2019/01/01 01:31:01 EST | 306 | 888888888888888888 | T50 | P01 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 01:31:01.723 UTC | Wed 2019/01/01 01:31:01 EST | 391 | 888888888888888888 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 5eb4e144-4046-11e9-b0de-2be70af03246
WT-001-1 | 2019/01/01 01:32:40.333 UTC | Wed 2019/01/01 01:32:40 EST | 342 | 888888888888888888 | T50 | P01 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:32:40.852 UTC | Wed 2019/01/01 01:32:40 EST | 392 | 888888888888888888 | T50 | P01 | 3333333333 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=4444444444) ID=5eb4e144-4046-11e9-b0de-2be70a
WT-001-1 | 2019/01/01 01:32:40.852 UTC | Wed 2019/01/01 01:32:40 EST | 392 |                    |     |     |            |             |         |   | f03246
WT-001-1 | 2019/01/01 01:32:41.413 UTC | Wed 2019/01/01 01:32:41 EST | 369 | 888888888888888888 | T50 | P01 | 3333333333 |             | ANI 01  | < | Blind Transfer from Phone Failed; (Destination=4444444444); Reason=UNALLOCATED_NUMBER ID=5eb
WT-001-1 | 2019/01/01 01:32:41.413 UTC | Wed 2019/01/01 01:32:41 EST | 369 |                    |     |     |            |             |         |   | 4e144-4046-11e9-b0de-2be70af03246
WT-001-1 | 2019/01/01 01:33:08.031 UTC | Wed 2019/01/01 01:33:08 EST | 370 | 888888888888888888 | T50 | P03 | 3333333333 |             | ANI 01  | > | Reconnect Ringback
WT-001-1 | 2019/01/01 01:33:50.524 UTC | Wed 2019/01/01 01:33:50 EST | 342 | 888888888888888888 | T50 | P03 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:33:50.528 UTC | Wed 2019/01/01 01:33:50 EST | 307 | 888888888888888888 | T50 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "888888888888888888");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 01:30:59.352");
        REQUIRE(rec.answer_utc == "2019-01-01 01:31:01.695"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 1);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 01:33:50.528"s);
    }
}

TEST_CASE("366 attended cancelled from phone") {
    auto val = parse(
R"(WT-001-1 | 2019/01/06 06:18:36.868 UTC | Thu 2019/01/06 06:18:36 UTC | 316 | 555555555555555555 | T50 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2019/01/06 06:18:39.915 UTC | Thu 2019/01/06 06:18:39 UTC | 306 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2019/01/06 06:18:39.939 UTC | Thu 2019/01/06 06:18:39 UTC | 391 | 555555555555555555 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 1788f314-61d4-11e9-b701-11467cfa2825
WT-001-1 | 2019/01/06 06:18:47.616 UTC | Thu 2019/01/06 06:18:47 UTC | 310 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/06 06:18:52.008 UTC | Thu 2019/01/06 06:18:52 UTC | 373 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1219) ID=209d2966-61d4-11e9-b744-11467cfa2
WT-001-1 | 2019/01/06 06:18:52.008 UTC | Thu 2019/01/06 06:18:52 UTC | 373 |                    |     |     |            |             |         |   | 825
WT-001-1 | 2019/01/06 06:18:55.477 UTC | Thu 2019/01/06 06:18:55 UTC | 366 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Cancelled; (Destination=1219) ID=209d2966-61d4-11e9-b744-11467c
WT-001-1 | 2019/01/06 06:18:55.477 UTC | Thu 2019/01/06 06:18:55 UTC | 366 |                    |     |     |            |             |         |   | fa2825
WT-001-1 | 2019/01/06 06:18:55.835 UTC | Thu 2019/01/06 06:18:55 UTC | 311 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/06 06:18:57.137 UTC | Thu 2019/01/06 06:18:57 UTC | 310 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/06 06:19:00.548 UTC | Thu 2019/01/06 06:19:00 UTC | 373 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1212) ID=25b5b5da-61d4-11e9-b772-11467cfa2
WT-001-1 | 2019/01/06 06:19:00.548 UTC | Thu 2019/01/06 06:19:00 UTC | 373 |                    |     |     |            |             |         |   | 825
WT-001-1 | 2019/01/06 06:19:01.548 UTC | Thu 2019/01/06 06:19:01 UTC | 311 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/06 06:19:01.553 UTC | Thu 2019/01/06 06:19:01 UTC | 342 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/06 06:19:20.596 UTC | Thu 2019/01/06 06:19:20 UTC | 370 | 555555555555555555 | T50 | P00 | 3333333333 |             | ANI 01  | > | Unattended Transfer from Phone Connected; (Destination=1212) ID=25b5b5da-61d4-11e9-b772-1146
WT-001-1 | 2019/01/06 06:19:20.596 UTC | Thu 2019/01/06 06:19:20 UTC | 370 |                    |     |     |            |             |         |   | 7cfa2825
WT-001-1 | 2019/01/06 06:19:20.618 UTC | Thu 2019/01/06 06:19:20 UTC | 391 | 555555555555555555 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 25ba2976-61d4-11e9-b79b-11467cfa2825
WT-001-1 | 2019/01/06 06:20:18.181 UTC | Thu 2019/01/06 06:20:18 UTC | 351 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | [TDD] TDD Character "O" Received
WT-001-1 | 2019/01/06 06:20:22.227 UTC | Thu 2019/01/06 06:20:22 UTC | 352 | 555555555555555555 | T50 | P01 | 3333333333 |             | ANI 01  | > | [TDD] TDD Caller Says: "O"
WT-001-1 | 2019/01/06 06:20:30.282 UTC | Thu 2019/01/06 06:20:30 UTC | 342 | 555555555555555555 | T50 | P00 | 3333333333 |             | ANI 01  | > | Conference Leave exten: 4444444444
WT-001-1 | 2019/01/06 06:20:30.287 UTC | Thu 2019/01/06 06:20:30 UTC | 307 | 555555555555555555 | T50 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
WT-001-1 | 2019/01/06 06:20:30.308 UTC | Thu 2019/01/06 06:20:30 UTC | 361 | 555555555555555555 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD Conversation Record (Data Follows)
WT-001-1 | 2019/01/06 06:20:30.308 UTC | Thu 2019/01/06 06:20:30 UTC | 361 |                    |     |     |            |             |         |   |     08:20:22 (Caller)
WT-001-1 | 2019/01/06 06:20:30.308 UTC | Thu 2019/01/06 06:20:30 UTC | 361 |                    |     |     |            |             |         |   |     O
WT-001-1 | 2019/01/06 06:20:30.308 UTC | Thu 2019/01/06 06:20:30 UTC | 361 |                    |     |     |            |             |         |   |
WT-001-1 | 2019/01/06 06:20:30.308 UTC | Thu 2019/01/06 06:20:30 UTC | 361 |                    |     |     |            |             |         |   |
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "555555555555555555");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-06 06:18:36.868");
        REQUIRE(rec.answer_utc == "2019-01-06 06:18:39.915"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 1);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::attended);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-06 06:20:30.287"s);
    }
}

TEST_CASE("366 Attended Transfer Failed with Useless ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 16:59:05.730 UTC | Sat 2019/01/01 16:59:05 UTC | 316 | 555555555555555555 | T39 | P00 |            | p7777777777 | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 16:59:05.782 UTC | Sat 2019/01/01 16:59:05 UTC | 222 | 555555555555555555 | T39 | P00 |            | p7777777777 | ALI 01S | < | Request Sent Index=62 Trunk=39 (to ALI Service=01 )
WT-001-1 | 2019/01/01 16:59:05.836 UTC | Sat 2019/01/01 16:59:05 UTC | 209 | 555555555555555555 | T39 | P00 |            | p7777777777 | ALI 01S | > | ACK Received Index=62
WT-001-1 | 2019/01/01 16:59:07.634 UTC | Sat 2019/01/01 16:59:07 UTC | 306 | 555555555555555555 | T39 | P01 |            | p7777777777 | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 16:59:07.652 UTC | Sat 2019/01/01 16:59:07 UTC | 391 | 555555555555555555 | T39 | P00 |            | p7777777777 | ANI 01  | > | TDD MODE ON; Channel: ec9238f2-6e8d-11e9-bbe1-a982122e8751
WT-001-1 | 2019/01/01 16:59:11.007 UTC | Sat 2019/01/01 16:59:11 UTC | 402 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 16:59:41.000 UTC | Sat 2019/01/01 16:59:41 UTC | 271 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | ALI 01S | < | Auto Request Sent Index=63 Trunk=97 (to ALI Service=01 )
WT-001-1 | 2019/01/01 16:59:41.005 UTC | Sat 2019/01/01 16:59:41 UTC | 209 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | ALI 01S | > | ACK Received Index=63
WT-001-1 | 2019/01/01 16:59:46.128 UTC | Sat 2019/01/01 16:59:46 UTC | 402 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 17:00:18.853 UTC | Sat 2019/01/01 17:00:18 UTC | 365 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@casp.ng911.state.oh.us) ID=c516efdb-e6b4-4
WT-001-1 | 2019/01/01 17:00:18.853 UTC | Sat 2019/01/01 17:00:18 UTC | 365 |                    |     |     |            |             |         |   | 4e0-bef0-ca21bd904038
WT-001-1 | 2019/01/01 17:00:29.546 UTC | Sat 2019/01/01 17:00:29 UTC | 365 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@casp.ng911.state.oh.us) ID=27115376-3587-4
WT-001-1 | 2019/01/01 17:00:29.546 UTC | Sat 2019/01/01 17:00:29 UTC | 365 |                    |     |     |            |             |         |   | f31-bb05-cc07b99905d2
WT-001-1 | 2019/01/01 17:00:30.548 UTC | Sat 2019/01/01 17:00:30 UTC | 378 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@casp.ng911.state.oh.us) ID=27115
WT-001-1 | 2019/01/01 17:00:30.548 UTC | Sat 2019/01/01 17:00:30 UTC | 378 |                    |     |     |            |             |         |   | 376-3587-4f31-bb05-cc07b99905d2
WT-001-1 | 2019/01/01 17:00:30.568 UTC | Sat 2019/01/01 17:00:30 UTC | 391 | 555555555555555555 | T39 | P00 | 4444444444 | p7777777777 | ANI 01  | > | TDD MODE ON; Channel: 18a32c08-6e8e-11e9-bc55-a982122e8751
WT-001-1 | 2019/01/01 17:04:49.433 UTC | Sat 2019/01/01 17:04:49 UTC | 366 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | ANI 01  | > | Attended Transfer from Phone Cancelled; (Destination=911) ID=
WT-001-1 | 2019/01/01 17:04:49.448 UTC | Sat 2019/01/01 17:04:49 UTC | 342 | 555555555555555555 | T39 | P01 | 4444444444 | p7777777777 | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 17:04:52.343 UTC | Sat 2019/01/01 17:04:52 UTC | 342 | 555555555555555555 | T39 | P00 | 4444444444 | p7777777777 | ANI 01  | > | Conference Leave exten: 7777777777
WT-001-1 | 2019/01/01 17:04:52.347 UTC | Sat 2019/01/01 17:04:52 UTC | 307 | 555555555555555555 | T39 | P00 | 4444444444 | p7777777777 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "555555555555555555");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 16:59:05.730");
        REQUIRE(rec.answer_utc == "2019-01-01 16:59:07.634"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 1);
        REQUIRE(!rec.calltaker);
        REQUIRE(rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 39);
        REQUIRE(rec.transfer_reaction == transfer_type::ng911);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 17:04:52.347"s);
    }
}

TEST_CASE("369 Attended Transfer from Phone Failed with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 01:34:36.151 UTC | Mon 2019/01/01 01:34:36 UTC | 316 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 01:34:38.236 UTC | Mon 2019/01/01 01:34:38 UTC | 306 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 01:34:38.289 UTC | Mon 2019/01/01 01:34:38 UTC | 391 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: d973cd24-7583-11e9-b090-11467cfa2825
WT-001-1 | 2019/01/01 01:34:53.003 UTC | Mon 2019/01/01 01:34:53 UTC | 310 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/01 01:35:02.280 UTC | Mon 2019/01/01 01:35:02 UTC | 373 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1427) ID=e90a2fda-7583-11e9-b0d4-11467cfa2
WT-001-1 | 2019/01/01 01:35:02.346 UTC | Mon 2019/01/01 01:35:02 UTC | 369 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=1427); Reason=UNALLOCATED_NUMBER ID=e90a2f
WT-001-1 | 2019/01/01 01:35:10.614 UTC | Mon 2019/01/01 01:35:10 UTC | 311 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/01 01:35:11.475 UTC | Mon 2019/01/01 01:35:11 UTC | 310 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/01 01:35:18.349 UTC | Mon 2019/01/01 01:35:18 UTC | 373 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1247) ID=f29dff36-7583-11e9-b10b-11467cfa2
WT-001-1 | 2019/01/01 01:35:19.251 UTC | Mon 2019/01/01 01:35:19 UTC | 311 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/01 01:35:19.256 UTC | Mon 2019/01/01 01:35:19 UTC | 342 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:35:38.408 UTC | Mon 2019/01/01 01:35:38 UTC | 370 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Unattended Transfer from Phone Connected; (Destination=1247) ID=f29dff36-7583-11e9-b10b-1146
WT-001-1 | 2019/01/01 01:35:38.435 UTC | Mon 2019/01/01 01:35:38 UTC | 391 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: f2a2257a-7583-11e9-b135-11467cfa2825
WT-001-1 | 2019/01/01 01:35:45.515 UTC | Mon 2019/01/01 01:35:45 UTC | 342 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Conference Leave exten: 4444444444
WT-001-1 | 2019/01/01 01:35:45.519 UTC | Mon 2019/01/01 01:35:45 UTC | 307 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "333333333333333333");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 01:34:36.151");
        REQUIRE(rec.answer_utc == "2019-01-01 01:34:38.236"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::attended);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 01:35:45.519"s);
    }
}
