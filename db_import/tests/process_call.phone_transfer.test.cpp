#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("process_call attended transfer from phone") {
    auto val = parse(
R"(WT-001-1 | 2018/03/01 01:49:03.315 UTC | Wed 2018/03/01 01:49:03 UTC | 316 | 555555555555555555 | T17 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2018/03/01 01:49:06.258 UTC | Wed 2018/03/01 01:49:06 UTC | 306 | 555555555555555555 | T17 | P01 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2018/03/01 01:50:11.792 UTC | Wed 2018/03/01 01:50:11 UTC | 310 | 555555555555555555 | T17 | P01 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/03/01 01:50:48.713 UTC | Wed 2018/03/01 01:50:48 UTC | 373 | 555555555555555555 | T17 | P01 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=4444444444)
WT-001-1 | 2018/03/01 01:50:49.854 UTC | Wed 2018/03/01 01:50:49 UTC | 370 | 555555555555555555 | T17 | P00 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Connected; (Destination=4444444444)
WT-001-1 | 2018/03/01 01:50:51.230 UTC | Wed 2018/03/01 01:50:51 UTC | 342 | 555555555555555555 | T17 | P00 | 3333333333 |             | ANI 01  | > | Conference Leave exten: 4444444444
WT-001-1 | 2018/03/01 01:50:51.570 UTC | Wed 2018/03/01 01:50:51 UTC | 311 | 555555555555555555 | T17 | P01 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2018/03/01 01:50:52.492 UTC | Wed 2018/03/01 01:50:52 UTC | 342 | 555555555555555555 | T17 | P01 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/03/01 01:50:52.496 UTC | Wed 2018/03/01 01:50:52 UTC | 307 | 555555555555555555 | T17 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "555555555555555555");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-03-01 01:49:03.315");
    REQUIRE(record.answer_utc.value() == "2018-03-01 01:49:06.258");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos.value() == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::attended);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-03-01 01:50:52.496"s);
}

TEST_CASE("process_call 395 unattended transfer from phone") {
    auto val = parse(
R"(WT-001-1 | 2018/05/03 11:08:05.130 UTC | Sun 2018/05/03 11:08:05 UTC | 316 | 777777777777777777 | T08 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2018/05/03 11:08:05.188 UTC | Sun 2018/05/03 11:08:05 UTC | 222 | 777777777777777777 | T08 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=80; Trunk=08 (to DB=01 PP=01)
WT-001-1 | 2018/05/03 11:08:05.188 UTC | Sun 2018/05/03 11:08:05 UTC | 222 | 777777777777777777 | T08 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=80; Trunk=08 (to DB=01 PP=01)
WT-001-1 | 2018/05/03 11:08:07.395 UTC | Sun 2018/05/03 11:08:07 UTC | 306 | 777777777777777777 | T08 | P06 |            | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 | 777777777777777777 | T08 | P06 | 9999999999 | p1111111111 | ALI 01B | > | Initial Record received (Data Follows)
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | <STX>280<CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 WPH2 11/11 11:11<CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | Verizon Wireless            <CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   |                  P#111-111-1111<CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   |    AAAAAAAA 0 AAAAA AAAAA<CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   |  0 AAAA AAA AAAAAAAA<CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | CALLBK=(999)999-9999  000 00000<CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | AA AAAAAA                      <CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=VZW  <CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | +11.111111  -111.111111     00 <CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | PSAP=AAAA--WIRELESS CALL<CR>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-001-1 | 2018/05/03 11:08:08.573 UTC | Sun 2018/05/03 11:08:08 UTC | 226 |                    |     |     |            |             |         |   | VERIFY AAA                     <ETX>
WT-001-1 | 2018/05/03 11:08:37.485 UTC | Sun 2018/05/03 11:08:37 UTC | 310 | 777777777777777777 | T08 | P06 | 9999999999 | p1111111111 | ANI 01  | > | On Hold
WT-001-1 | 2018/05/03 11:08:44.279 UTC | Sun 2018/05/03 11:08:44 UTC | 373 | 777777777777777777 | T08 | P06 | 9999999999 | p1111111111 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=88888888888)
WT-001-1 | 2018/05/03 11:08:50.372 UTC | Sun 2018/05/03 11:08:50 UTC | 311 | 777777777777777777 | T08 | P06 | 9999999999 | p1111111111 | ANI 01  | > | Off Hold
WT-001-1 | 2018/05/03 11:08:50.373 UTC | Sun 2018/05/03 11:08:50 UTC | 342 | 777777777777777777 | T08 | P06 | 9999999999 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/05/03 11:09:04.187 UTC | Sun 2018/05/03 11:09:04 UTC | 395 | 777777777777777777 | T08 | P00 | 9999999999 | p1111111111 | ANI 01  | > | Unattended Transfer from Phone Connected; (Destination=88888888888)
WT-001-1 | 2018/05/03 11:12:04.936 UTC | Sun 2018/05/03 11:12:04 UTC | 342 | 777777777777777777 | T08 | P00 | 9999999999 | p1111111111 | ANI 01  | > | Conference Leave exten: 88888888888
WT-001-1 | 2018/05/03 11:12:04.942 UTC | Sun 2018/05/03 11:12:04 UTC | 307 | 777777777777777777 | T08 | P00 | 9999999999 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "777777777777777777");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-05-03 11:08:05.130");
    REQUIRE(record.answer_utc == "2018-05-03 11:08:07.395"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 6);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos == "WPH2"s);
    REQUIRE(record.trunk == 8);
    REQUIRE(record.transfer_reaction == transfer_type::attended);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-05-03 11:12:04.942"s);
}

TEST_CASE("process_call 370 unattended transfer from phone") {
    auto val = parse(
R"(WT-007-1 | 2018/01/07 11:53:06.278 UTC | Thu 2018/01/07 11:53:06 UTC | 316 | 565656565656565656 | T17 | P00 | 4444444444 |             | ANI 01  | > | Ringing
WT-007-1 | 2018/01/07 11:53:12.303 UTC | Thu 2018/01/07 11:53:12 UTC | 306 | 565656565656565656 | T17 | P02 | 4444444444 |             | ANI 01  | > | Connect
WT-007-1 | 2018/01/07 11:54:21.564 UTC | Thu 2018/01/07 11:54:21 UTC | 310 | 565656565656565656 | T17 | P02 | 4444444444 |             | ANI 01  | > | On Hold
WT-007-1 | 2018/01/07 11:54:42.617 UTC | Thu 2018/01/07 11:54:42 UTC | 373 | 565656565656565656 | T17 | P02 | 4444444444 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=5555555555)
WT-007-1 | 2018/01/07 11:54:44.879 UTC | Thu 2018/01/07 11:54:44 UTC | 311 | 565656565656565656 | T17 | P02 | 4444444444 |             | ANI 01  | > | Off Hold
WT-007-1 | 2018/01/07 11:54:44.882 UTC | Thu 2018/01/07 11:54:44 UTC | 342 | 565656565656565656 | T17 | P02 | 4444444444 |             | ANI 01  | > | Conference Leave
WT-007-1 | 2018/01/07 11:54:47.120 UTC | Thu 2018/01/07 11:54:47 UTC | 370 | 565656565656565656 | T17 | P00 | 4444444444 |             | ANI 01  | > | Unattended Transfer from Phone Connected; (Destination=5555555555)
WT-007-1 | 2018/01/07 11:55:04.957 UTC | Thu 2018/01/07 11:55:04 UTC | 342 | 565656565656565656 | T17 | P00 | 4444444444 |             | ANI 01  | > | Conference Leave exten: 5555555555
WT-007-1 | 2018/01/07 11:55:04.961 UTC | Thu 2018/01/07 11:55:04 UTC | 307 | 565656565656565656 | T17 | P00 | 4444444444 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "565656565656565656");
    REQUIRE(record.controller == "WT-007-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-007-1"s);
    REQUIRE(record.utc == "2018-01-07 11:53:06.278");
    REQUIRE(record.answer_utc == "2018-01-07 11:53:12.303"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::attended);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-07 11:55:04.961"s);
}

TEST_CASE("392 Blind -> 370 Unattended") {
    auto val = parse(
R"(WT-001-1 | 2018/04/01 01:36:09.801 UTC | Fri 2018/04/01 01:36:09 UTC | 316 | 565656565656565656 | T17 | P00 | 9376933332 |             | ANI 01  | > | Ringing
WT-001-1 | 2018/04/01 01:36:11.522 UTC | Fri 2018/04/01 01:36:11 UTC | 306 | 565656565656565656 | T17 | P03 | 9376933332 |             | ANI 01  | > | Connect
WT-001-1 | 2018/04/01 01:36:41.180 UTC | Fri 2018/04/01 01:36:41 UTC | 342 | 565656565656565656 | T17 | P03 | 9376933332 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/04/01 01:36:41.181 UTC | Fri 2018/04/01 01:36:41 UTC | 392 | 565656565656565656 | T17 | P03 | 9376933332 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=7000)
WT-001-1 | 2018/04/01 01:39:43.098 UTC | Fri 2018/04/01 01:39:43 UTC | 370 | 565656565656565656 | T17 | P03 | 9376933332 |             | ANI 01  | > | Unattended Transfer from Phone Connected; (Destination=7000)
WT-001-1 | 2018/04/01 01:40:09.725 UTC | Fri 2018/04/01 01:40:09 UTC | 342 | 565656565656565656 | T17 | P03 | 9376933332 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/04/01 01:40:09.729 UTC | Fri 2018/04/01 01:40:09 UTC | 307 | 565656565656565656 | T17 | P00 | 9376933332 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "565656565656565656");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-04-01 01:36:09.801");
    REQUIRE(record.answer_utc == "2018-04-01 01:36:11.522"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 3);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::blind);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-04-01 01:40:09.729"s);
}

TEST_CASE("370 blind transfer from phone connected") {
    auto val = parse(
R"(WT-003-1 | 2018/01/14 18:59:11.338 UTC | Thu 2018/06/14 18:59:11 UTC | 316 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-003-1 | 2018/01/14 18:59:13.903 UTC | Thu 2018/06/14 18:59:13 UTC | 306 | 111111111111111111 | T17 | P01 | 1111111111 |             | ANI 01  | > | Connect
WT-003-1 | 2018/01/14 18:59:39.555 UTC | Thu 2018/06/14 18:59:39 UTC | 342 | 111111111111111111 | T17 | P01 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-003-1 | 2018/01/14 18:59:40.074 UTC | Thu 2018/06/14 18:59:40 UTC | 392 | 111111111111111111 | T17 | P01 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=2222222222)
WT-003-1 | 2018/01/14 19:00:04.256 UTC | Thu 2018/06/14 19:00:04 UTC | 370 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Connected; (Destination=2222222222)
WT-003-1 | 2018/01/14 19:00:58.994 UTC | Thu 2018/06/14 19:00:58 UTC | 342 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Conference Leave exten: 2222222222
WT-003-1 | 2018/01/14 19:00:58.998 UTC | Thu 2018/06/14 19:00:58 UTC | 307 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-003-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-003-1");
    REQUIRE(record.utc == "2018-01-14 18:59:11.338");
    REQUIRE(record.answer_utc.value() == "2018-01-14 18:59:13.903");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos.value() == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::blind);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-14 19:00:58.998"s);
}

TEST_CASE("395 blind transfer from phone connected, intra-PSAP") {
    auto val = parse(
R"(WT-001-1 | 2016/01/01 18:32:17.576 UTC | Mon 2016/01/01 18:32:17 UTC | 316 | 333333333333333333 |     | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2016/01/01 18:32:21.290 UTC | Mon 2016/01/01 18:32:21 UTC | 306 | 333333333333333333 |     | P02 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2016/01/01 18:32:31.166 UTC | Mon 2016/01/01 18:32:31 UTC | 310 | 333333333333333333 |     | P02 | 1111111111 |             | ANI 01  | > | On Hold
WT-001-1 | 2016/01/01 18:32:37.926 UTC | Mon 2016/01/01 18:32:37 UTC | 342 | 333333333333333333 |     | P02 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/01/01 18:32:37.944 UTC | Mon 2016/01/01 18:32:37 UTC | 392 | 333333333333333333 |     | P02 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=2222222222)
WT-001-1 | 2016/01/01 18:32:54.921 UTC | Mon 2016/01/01 18:32:54 UTC | 395 | 333333333333333333 |     | P03 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Connected; (Destination=2222222222)
WT-001-1 | 2016/01/01 18:34:01.228 UTC | Mon 2016/01/01 18:34:01 UTC | 342 | 333333333333333333 |     | P03 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/01/01 18:34:01.249 UTC | Mon 2016/01/01 18:34:01 UTC | 307 | 333333333333333333 |     | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2016-01-01 18:32:17.576");
    REQUIRE(record.answer_utc == "2016-01-01 18:32:21.290"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2016-01-01 18:34:01.249"s);
}

TEST_CASE("395 blind transfer from phone connected, inter-PSAP") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [2]
    - id: WT-001-B
      positions: [3]
)");

    auto val = parse(
R"(WT-001-1 | 2016/01/01 18:32:17.576 UTC | Mon 2016/01/01 18:32:17 UTC | 316 | 333333333333333333 |     | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2016/01/01 18:32:21.290 UTC | Mon 2016/01/01 18:32:21 UTC | 306 | 333333333333333333 |     | P02 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2016/01/01 18:32:31.166 UTC | Mon 2016/01/01 18:32:31 UTC | 310 | 333333333333333333 |     | P02 | 1111111111 |             | ANI 01  | > | On Hold
WT-001-1 | 2016/01/01 18:32:37.926 UTC | Mon 2016/01/01 18:32:37 UTC | 342 | 333333333333333333 |     | P02 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/01/01 18:32:37.944 UTC | Mon 2016/01/01 18:32:37 UTC | 392 | 333333333333333333 |     | P02 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=2222222222)
WT-001-1 | 2016/01/01 18:32:54.921 UTC | Mon 2016/01/01 18:32:54 UTC | 395 | 333333333333333333 |     | P03 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Connected; (Destination=2222222222)
WT-001-1 | 2016/01/01 18:34:01.228 UTC | Mon 2016/01/01 18:34:01 UTC | 342 | 333333333333333333 |     | P03 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/01/01 18:34:01.249 UTC | Mon 2016/01/01 18:34:01 UTC | 307 | 333333333333333333 |     | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-A"s);
    REQUIRE(record.utc == "2016-01-01 18:32:17.576");
    REQUIRE(record.answer_utc == "2016-01-01 18:32:21.290"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::blind);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2016-01-01 18:34:01.249"s);

    auto rec2 = val.at(1);

    REQUIRE(rec2.callid == "333333333333333333");
    REQUIRE(rec2.controller == "WT-001-1");
    REQUIRE(rec2.subid == 1);
    REQUIRE(rec2.psap == "WT-001-B"s);
    REQUIRE(rec2.utc == "2016-01-01 18:32:37.944");
    REQUIRE(rec2.answer_utc == "2016-01-01 18:32:54.921"s);
    REQUIRE(rec2.abandoned == false);
    REQUIRE(rec2.pos == 3);
    REQUIRE(!rec2.calltaker);
    REQUIRE(!rec2.is_911);
    REQUIRE(!rec2.cos);
    REQUIRE(!rec2.trunk);
    REQUIRE(rec2.transfer_reaction == transfer_type::none);
    REQUIRE(rec2.incoming == true);
    REQUIRE(rec2.t_type == transfer_type::blind);
    REQUIRE(!rec2.disconnect_utc);
}

TEST_CASE("395 Attended Transfer from Phone Connected, intra-PSAP") {
    auto val = parse(
R"(WT-001-1 | 2017/03/27 11:26:26.684 UTC | Tue 2017/03/27 11:26:26 UTC | 316 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2017/03/27 11:26:27.775 UTC | Tue 2017/03/27 11:26:27 UTC | 306 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2017/03/27 11:26:32.406 UTC | Tue 2017/03/27 11:26:32 UTC | 310 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | On Hold
WT-001-1 | 2017/03/27 11:26:37.207 UTC | Tue 2017/03/27 11:26:37 UTC | 373 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=6666)
WT-001-1 | 2017/03/27 11:26:40.806 UTC | Tue 2017/03/27 11:26:40 UTC | 395 | 111111111111111111 | T17 | P06 | 1111111111 |             | ANI 01  | > | Attended Transfer from Phone Connected; (Destination=6666)
WT-001-1 | 2017/03/27 11:26:59.721 UTC | Tue 2017/03/27 11:26:59 UTC | 311 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | Off Hold
WT-001-1 | 2017/03/27 11:26:59.727 UTC | Tue 2017/03/27 11:26:59 UTC | 342 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2017/03/27 11:28:09.712 UTC | Tue 2017/03/27 11:28:09 UTC | 342 | 111111111111111111 | T17 | P06 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2017/03/27 11:28:09.734 UTC | Tue 2017/03/27 11:28:09 UTC | 307 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2017-03-27 11:26:26.684");
    REQUIRE(record.answer_utc == "2017-03-27 11:26:27.775"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2017-03-27 11:28:09.734"s);
}

TEST_CASE("395 Attended Transfer from Phone Connected, inter-PSAP") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [6]
    - id: WT-001-B
      positions: [2]
)");

    auto val = parse(
R"(WT-001-1 | 2017/03/27 11:26:26.684 UTC | Tue 2017/03/27 11:26:26 UTC | 316 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2017/03/27 11:26:27.775 UTC | Tue 2017/03/27 11:26:27 UTC | 306 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2017/03/27 11:26:32.406 UTC | Tue 2017/03/27 11:26:32 UTC | 310 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | On Hold
WT-001-1 | 2017/03/27 11:26:37.207 UTC | Tue 2017/03/27 11:26:37 UTC | 373 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=6666)
WT-001-1 | 2017/03/27 11:26:40.806 UTC | Tue 2017/03/27 11:26:40 UTC | 395 | 111111111111111111 | T17 | P06 | 1111111111 |             | ANI 01  | > | Attended Transfer from Phone Connected; (Destination=6666)
WT-001-1 | 2017/03/27 11:26:59.721 UTC | Tue 2017/03/27 11:26:59 UTC | 311 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | Off Hold
WT-001-1 | 2017/03/27 11:26:59.727 UTC | Tue 2017/03/27 11:26:59 UTC | 342 | 111111111111111111 | T17 | P02 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2017/03/27 11:28:09.712 UTC | Tue 2017/03/27 11:28:09 UTC | 342 | 111111111111111111 | T17 | P06 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2017/03/27 11:28:09.734 UTC | Tue 2017/03/27 11:28:09 UTC | 307 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-B"s);
    REQUIRE(record.utc == "2017-03-27 11:26:26.684");
    REQUIRE(record.answer_utc == "2017-03-27 11:26:27.775"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::attended);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2017-03-27 11:28:09.734"s);

    auto rec2 = val.at(1);

    REQUIRE(rec2.callid == "111111111111111111");
    REQUIRE(rec2.controller == "WT-001-1");
    REQUIRE(rec2.subid == 1);
    REQUIRE(rec2.psap == "WT-001-A"s);
    REQUIRE(rec2.utc == "2017-03-27 11:26:37.207");
    REQUIRE(rec2.answer_utc == "2017-03-27 11:26:40.806"s);
    REQUIRE(rec2.abandoned == false);
    REQUIRE(rec2.pos == 6);
    REQUIRE(!rec2.calltaker);
    REQUIRE(!rec2.is_911);
    REQUIRE(!rec2.cos);
    REQUIRE(rec2.trunk == 17);
    REQUIRE(rec2.transfer_reaction == transfer_type::none);
    REQUIRE(rec2.incoming == true);
    REQUIRE(rec2.t_type == transfer_type::attended);
    REQUIRE(!rec2.disconnect_utc);
}

TEST_CASE("370 Outbound Blind Transfer from Phone Connected") {
    auto val = parse(
R"(WT-001-1 | 2011/12/04 01:32:46.295 UTC | Tue 2011/12/04 01:32:46 UTC | 316 | 333333333333333333 |     | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=1010@10.10.10.10)
WT-001-1 | 2011/12/04 01:32:52.604 UTC | Tue 2011/12/04 01:32:52 UTC | 306 | 333333333333333333 |     | P02 | VoIP CALL  |             | ANI 01  | > | Connect (From=1010@10.10.10.10)
WT-001-1 | 2011/12/04 01:33:08.575 UTC | Tue 2011/12/04 01:33:08 UTC | 310 | 333333333333333333 |     | P02 | VoIP CALL  |             | ANI 01  | > | On Hold
WT-001-1 | 2011/12/04 01:33:14.973 UTC | Tue 2011/12/04 01:33:14 UTC | 373 | 333333333333333333 |     | P02 | VoIP CALL  |             | ANI 01  | > | Transfer from Phone Dialed; (Destination=4444)
WT-001-1 | 2011/12/04 01:33:18.656 UTC | Tue 2011/12/04 01:33:18 UTC | 342 | 333333333333333333 |     | P02 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2011/12/04 01:33:20.302 UTC | Tue 2011/12/04 01:33:20 UTC | 370 | 333333333333333333 |     | P00 | VoIP CALL  |             | ANI 01  | > | Outbound Blind Transfer from Phone Connected; (Destination=)
WT-001-1 | 2011/12/04 01:37:15.279 UTC | Tue 2011/12/04 01:37:15 UTC | 342 | 333333333333333333 |     | P00 | VoIP CALL  |             | ANI 01  | > | Conference Leave exten: )" R"(
WT-001-1 | 2011/12/04 01:37:15.279 UTC | Tue 2011/12/04 01:37:15 UTC | 307 | 333333333333333333 |     | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2011-12-04 01:32:46.295");
    REQUIRE(record.answer_utc == "2011-12-04 01:32:52.604"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::unknown); // NOTE: we mark this as unknown because it's weird and doesn't exist anymore
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2011-12-04 01:37:15.279"s);
}

TEST_CASE("Failed transfer then attended transfer intra-PSAP") {
    auto val = parse(
R"(WT-001-1 | 2011/01/01 01:37:20.929 UTC | Wed 2011/01/01 01:37:20 UTC | 316 | 333333333333333333 | T17 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2011/01/01 01:37:23.857 UTC | Wed 2011/01/01 01:37:23 UTC | 306 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2011/01/01 01:37:37.419 UTC | Wed 2011/01/01 01:37:37 UTC | 310 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2011/01/01 01:37:40.841 UTC | Wed 2011/01/01 01:37:40 UTC | 373 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=2222)
WT-001-1 | 2011/01/01 01:37:51.496 UTC | Wed 2011/01/01 01:37:51 UTC | 366 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Transfer from Phone Cancelled; (Destination=2222)
WT-001-1 | 2011/01/01 01:37:51.636 UTC | Wed 2011/01/01 01:37:51 UTC | 311 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2011/01/01 01:37:59.657 UTC | Wed 2011/01/01 01:37:59 UTC | 310 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2011/01/01 01:38:03.200 UTC | Wed 2011/01/01 01:38:03 UTC | 373 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1001)
WT-001-1 | 2011/01/01 01:38:08.272 UTC | Wed 2011/01/01 01:38:08 UTC | 370 | 333333333333333333 | T17 | P01 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Connected; (Destination=1001)
WT-001-1 | 2011/01/01 01:38:19.119 UTC | Wed 2011/01/01 01:38:19 UTC | 342 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 01:38:49.649 UTC | Wed 2011/01/01 01:38:49 UTC | 342 | 333333333333333333 | T17 | P01 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 01:38:49.649 UTC | Wed 2011/01/01 01:38:49 UTC | 307 | 333333333333333333 | T17 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2011-01-01 01:37:20.929");
    REQUIRE(record.answer_utc == "2011-01-01 01:37:23.857"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2011-01-01 01:38:49.649"s);
}

TEST_CASE("Failed transfer then attended transfer inter-PSAP") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [2]
    - id: WT-001-B
      positions: [1]
)");

    auto val = parse(
R"(WT-001-1 | 2011/01/01 01:37:20.929 UTC | Wed 2011/01/01 01:37:20 UTC | 316 | 333333333333333333 | T17 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2011/01/01 01:37:23.857 UTC | Wed 2011/01/01 01:37:23 UTC | 306 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2011/01/01 01:37:37.419 UTC | Wed 2011/01/01 01:37:37 UTC | 310 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2011/01/01 01:37:40.841 UTC | Wed 2011/01/01 01:37:40 UTC | 373 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=2222)
WT-001-1 | 2011/01/01 01:37:51.496 UTC | Wed 2011/01/01 01:37:51 UTC | 366 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Transfer from Phone Cancelled; (Destination=2222)
WT-001-1 | 2011/01/01 01:37:51.636 UTC | Wed 2011/01/01 01:37:51 UTC | 311 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2011/01/01 01:37:59.657 UTC | Wed 2011/01/01 01:37:59 UTC | 310 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2011/01/01 01:38:03.200 UTC | Wed 2011/01/01 01:38:03 UTC | 373 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1001)
WT-001-1 | 2011/01/01 01:38:08.272 UTC | Wed 2011/01/01 01:38:08 UTC | 370 | 333333333333333333 | T17 | P01 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Connected; (Destination=1001)
WT-001-1 | 2011/01/01 01:38:19.119 UTC | Wed 2011/01/01 01:38:19 UTC | 342 | 333333333333333333 | T17 | P02 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 01:38:49.649 UTC | Wed 2011/01/01 01:38:49 UTC | 342 | 333333333333333333 | T17 | P01 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 01:38:49.649 UTC | Wed 2011/01/01 01:38:49 UTC | 307 | 333333333333333333 | T17 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-A"s);
    REQUIRE(record.utc == "2011-01-01 01:37:20.929");
    REQUIRE(record.answer_utc == "2011-01-01 01:37:23.857"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::attended);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2011-01-01 01:38:49.649"s);

    auto rec2 = val.at(1);

    REQUIRE(rec2.callid == "333333333333333333");
    REQUIRE(rec2.controller == "WT-001-1");
    REQUIRE(rec2.subid == 1);
    REQUIRE(rec2.psap == "WT-001-B"s);
    REQUIRE(rec2.utc == "2011-01-01 01:38:03.200");
    REQUIRE(rec2.answer_utc == "2011-01-01 01:38:08.272"s);
    REQUIRE(rec2.abandoned == false);
    REQUIRE(rec2.pos == 1);
    REQUIRE(!rec2.calltaker);
    REQUIRE(!rec2.is_911);
    REQUIRE(!rec2.cos);
    REQUIRE(rec2.trunk == 17);
    REQUIRE(rec2.transfer_reaction == transfer_type::none);
    REQUIRE(rec2.incoming == true);
    REQUIRE(rec2.t_type == transfer_type::attended);
    REQUIRE(!rec2.disconnect_utc);
}

TEST_CASE("Attended Transfer with ID") {
    auto val = parse(
R"(WT-001-1 | 2018/01/04 05:13:02.423 UTC | Wed 2018/01/04 05:13:02 UTC | 372 | 555555555555555555 | T50 | P02 | 8888888888 |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=8888888888)
WT-001-1 | 2018/01/04 05:13:12.760 UTC | Wed 2018/01/04 05:13:12 UTC | 306 | 555555555555555555 | T50 | P02 | 8888888888 |             | ANI 01  | > | Connect
WT-001-1 | 2018/01/04 05:13:12.794 UTC | Wed 2018/01/04 05:13:12 UTC | 391 | 555555555555555555 | T50 | P00 | 8888888888 |             | ANI 01  | > | TDD MODE ON; Channel: 345b78fc-4002-11e9-a097-2be70af03246
WT-001-1 | 2018/01/04 05:14:07.309 UTC | Wed 2018/01/04 05:14:07 UTC | 310 | 555555555555555555 | T50 | P02 | 8888888888 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/01/04 05:14:08.320 UTC | Wed 2018/01/04 05:14:08 UTC | 373 | 555555555555555555 | T50 | P02 | 8888888888 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=12222222222) ID=5ba018b4-4002-11e9-a09f-2b
WT-001-1 | 2018/01/04 05:14:08.320 UTC | Wed 2018/01/04 05:14:08 UTC | 373 |                    |     |     |            |             |         |   | e70af03246
WT-001-1 | 2018/01/04 05:14:20.691 UTC | Wed 2018/01/04 05:14:20 UTC | 370 | 555555555555555555 | T50 | P00 | 8888888888 |             | ANI 01  | > | Attended Transfer from Phone Connected; (Destination=12222222222) ID=5ba018b4-4002-11e9-a09f
WT-001-1 | 2018/01/04 05:14:20.691 UTC | Wed 2018/01/04 05:14:20 UTC | 370 |                    |     |     |            |             |         |   | -2be70af03246
WT-001-1 | 2018/01/04 05:14:20.693 UTC | Wed 2018/01/04 05:14:20 UTC | 391 | 555555555555555555 | T50 | P00 | 8888888888 |             | ANI 01  | > | TDD MODE ON; Channel: 5ba3b8f2-4002-11e9-a0c8-2be70af03246
WT-001-1 | 2018/01/04 05:14:27.429 UTC | Wed 2018/01/04 05:14:27 UTC | 342 | 555555555555555555 | T50 | P00 | 8888888888 |             | ANI 01  | > | Conference Leave exten: 53304476523
WT-001-1 | 2018/01/04 05:14:27.709 UTC | Wed 2018/01/04 05:14:27 UTC | 311 | 555555555555555555 | T50 | P02 | 8888888888 |             | ANI 01  | > | Off Hold
WT-001-1 | 2018/01/04 05:15:23.396 UTC | Wed 2018/01/04 05:15:23 UTC | 342 | 555555555555555555 | T50 | P00 | 8888888888 |             | ANI 01  | > | Conference Leave exten: 8888888888
WT-001-1 | 2018/01/04 05:15:23.414 UTC | Wed 2018/01/04 05:15:23 UTC | 307 | 555555555555555555 | T50 | P02 | 8888888888 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "555555555555555555");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2018-01-04 05:13:02.423");
        REQUIRE(rec.answer_utc == "2018-01-04 05:13:12.760"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == false);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(!rec.disconnect_utc);
    }
}

TEST_CASE("370 Unattended Transfer with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 01:24:56.844 UTC | Tue 2019/01/01 01:24:56 UTC | 316 | 222222222222222222 | T50 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 01:24:58.782 UTC | Tue 2019/01/01 01:24:58 UTC | 306 | 222222222222222222 | T50 | P01 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 01:24:58.828 UTC | Tue 2019/01/01 01:24:58 UTC | 391 | 222222222222222222 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 7c54188c-5ac2-11e9-9ce0-11467cfa2825
WT-001-1 | 2019/01/01 01:25:04.841 UTC | Tue 2019/01/01 01:25:04 UTC | 310 | 222222222222222222 | T50 | P01 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/01 01:25:08.089 UTC | Tue 2019/01/01 01:25:08 UTC | 373 | 222222222222222222 | T50 | P01 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1111) ID=83102d64-5ac2-11e9-9d22-11467cfa2
WT-001-1 | 2019/01/01 01:25:08.089 UTC | Tue 2019/01/01 01:25:08 UTC | 373 |                    |     |     |            |             |         |   | 825
WT-001-1 | 2019/01/01 01:25:10.437 UTC | Tue 2019/01/01 01:25:10 UTC | 311 | 222222222222222222 | T50 | P01 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/01 01:25:10.441 UTC | Tue 2019/01/01 01:25:10 UTC | 342 | 222222222222222222 | T50 | P01 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:25:10.712 UTC | Tue 2019/01/01 01:25:10 UTC | 370 | 222222222222222222 | T50 | P00 | 3333333333 |             | ANI 01  | > | Unattended Transfer from Phone Connected; (Destination=1111) ID=83102d64-5ac2-11e9-9d22-1146
WT-001-1 | 2019/01/01 01:25:10.712 UTC | Tue 2019/01/01 01:25:10 UTC | 370 |                    |     |     |            |             |         |   | 7cfa2825
WT-001-1 | 2019/01/01 01:25:10.725 UTC | Tue 2019/01/01 01:25:10 UTC | 391 | 222222222222222222 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 83142ec8-5ac2-11e9-9d4b-11467cfa2825
WT-001-1 | 2019/01/01 01:25:22.655 UTC | Tue 2019/01/01 01:25:22 UTC | 342 | 222222222222222222 | T50 | P00 | 3333333333 |             | ANI 01  | > | Conference Leave exten: 2222222222
WT-001-1 | 2019/01/01 01:25:22.660 UTC | Tue 2019/01/01 01:25:22 UTC | 307 | 222222222222222222 | T50 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "222222222222222222");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 01:24:56.844");
        REQUIRE(rec.answer_utc == "2019-01-01 01:24:58.782"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 1);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::attended);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 01:25:22.660"s);
    }
}

TEST_CASE("blind transfer with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 01:17:50.344 UTC | Wed 2019/01/01 01:17:50 UTC | 316 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 01:17:51.950 UTC | Wed 2019/01/01 01:17:51 UTC | 306 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 01:17:52.009 UTC | Wed 2019/01/01 01:17:52 UTC | 391 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 54e012ca-5bc5-11e9-9472-11467cfa2825
WT-001-1 | 2019/01/01 01:18:02.570 UTC | Wed 2019/01/01 01:18:02 UTC | 310 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/01 01:18:30.231 UTC | Wed 2019/01/01 01:18:30 UTC | 311 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/01 01:18:32.515 UTC | Wed 2019/01/01 01:18:32 UTC | 310 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/01 01:18:38.346 UTC | Wed 2019/01/01 01:18:38 UTC | 311 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/01 01:18:38.349 UTC | Wed 2019/01/01 01:18:38 UTC | 342 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:18:38.866 UTC | Wed 2019/01/01 01:18:38 UTC | 392 | 333333333333333333 | T50 | P02 | 3333333333 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=2222) ID=54e012ca-5bc5-11e9-9472-11467cfa2825
WT-001-1 | 2019/01/01 01:18:44.911 UTC | Wed 2019/01/01 01:18:44 UTC | 370 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Blind Transfer from Phone Connected; (Destination=2222) ID=54e012ca-5bc5-11e9-9472-11467cfa2
WT-001-1 | 2019/01/01 01:18:44.911 UTC | Wed 2019/01/01 01:18:44 UTC | 370 |                    |     |     |            |             |         |   | 825
WT-001-1 | 2019/01/01 01:18:44.940 UTC | Wed 2019/01/01 01:18:44 UTC | 391 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 71de22e0-5bc5-11e9-94c2-11467cfa2825
WT-001-1 | 2019/01/01 01:18:51.875 UTC | Wed 2019/01/01 01:18:51 UTC | 342 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Conference Leave exten: 2222
WT-001-1 | 2019/01/01 01:18:51.880 UTC | Wed 2019/01/01 01:18:51 UTC | 307 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "333333333333333333");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 01:17:50.344");
        REQUIRE(rec.answer_utc == "2019-01-01 01:17:51.950"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::blind);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 01:18:51.880"s);
    }
}

TEST_CASE("395 blind transfer with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 01:43:03.241 UTC | Thu 2019/01/01 01:43:03 UTC | 316 | 444444444444444444 | T50 | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=anonymous@anonymous.invalid)
WT-001-1 | 2019/01/01 01:43:09.449 UTC | Thu 2019/01/01 01:43:09 UTC | 306 | 444444444444444444 | T50 | P02 | VoIP CALL  |             | ANI 01  | > | Connect (From=anonymous@anonymous.invalid)
WT-001-1 | 2019/01/01 01:43:09.465 UTC | Thu 2019/01/01 01:43:09 UTC | 391 | 444444444444444444 | T50 | P00 | VoIP CALL  |             | ANI 01  | > | TDD MODE ON; Channel: ba709a98-5c5f-11e9-ba1a-11467cfa2825
WT-001-1 | 2019/01/01 01:44:04.864 UTC | Thu 2019/01/01 01:44:04 UTC | 342 | 444444444444444444 | T50 | P02 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:44:05.383 UTC | Thu 2019/01/01 01:44:05 UTC | 392 | 444444444444444444 | T50 | P02 | VoIP CALL  |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=4000) ID=ba709a98-5c5f-11e9-ba1a-11467cfa2825
WT-001-1 | 2019/01/01 01:45:39.409 UTC | Thu 2019/01/01 01:45:39 UTC | 395 | 444444444444444444 | T50 | P01 | VoIP CALL  |             | ANI 01  | > | Blind Transfer from Phone Connected; (Destination=4000) ID=ba709a98-5c5f-11e9-ba1a-11467cfa2
WT-001-1 | 2019/01/01 01:45:39.409 UTC | Thu 2019/01/01 01:45:39 UTC | 395 |                    |     |     |            |             |         |   | 825
WT-001-1 | 2019/01/01 01:46:02.964 UTC | Thu 2019/01/01 01:46:02 UTC | 342 | 444444444444444444 | T50 | P01 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:46:02.968 UTC | Thu 2019/01/01 01:46:02 UTC | 307 | 444444444444444444 | T50 | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "444444444444444444");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 01:43:03.241");
        REQUIRE(rec.answer_utc == "2019-01-01 01:43:09.449"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 01:46:02.968"s);
    }
}

TEST_CASE("395 unattended transfer with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 01:09:15.597 UTC | Tue 2019/01/01 01:09:15 EDT | 372 | 444444444444444444 | T50 | P01 |            |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=1248)
WT-001-1 | 2019/01/01 01:09:18.224 UTC | Tue 2019/01/01 01:09:18 EDT | 306 | 444444444444444444 | T50 | P01 |            |             | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 01:09:18.260 UTC | Tue 2019/01/01 01:09:18 EDT | 391 | 444444444444444444 | T50 | P00 |            |             | ANI 01  | > | TDD MODE ON; Channel: 3b5c60ae-65b8-11e9-9e9d-11467cfa2825
WT-001-1 | 2019/01/01 01:09:39.315 UTC | Tue 2019/01/01 01:09:39 EDT | 310 | 444444444444444444 | T50 | P01 |            |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/01 01:09:58.631 UTC | Tue 2019/01/01 01:09:58 EDT | 311 | 444444444444444444 | T50 | P01 |            |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/01 01:10:00.975 UTC | Tue 2019/01/01 01:10:00 EDT | 310 | 444444444444444444 | T50 | P01 |            |             | ANI 01  | > | On Hold
WT-001-1 | 2019/01/01 01:10:07.845 UTC | Tue 2019/01/01 01:10:07 EDT | 373 | 444444444444444444 | T50 | P01 |            |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1294) ID=5a7e099c-65b8-11e9-9ea6-11467cfa2
WT-001-1 | 2019/01/01 01:10:07.845 UTC | Tue 2019/01/01 01:10:07 EDT | 373 |                    |     |     |            |             |         |   | 825
WT-001-1 | 2019/01/01 01:10:09.449 UTC | Tue 2019/01/01 01:10:09 EDT | 311 | 444444444444444444 | T50 | P01 |            |             | ANI 01  | > | Off Hold
WT-001-1 | 2019/01/01 01:10:09.455 UTC | Tue 2019/01/01 01:10:09 EDT | 342 | 444444444444444444 | T50 | P01 |            |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:10:12.714 UTC | Tue 2019/01/01 01:10:12 EDT | 395 | 444444444444444444 | T50 | P04 |            |             | ANI 01  | > | Unattended Transfer from Phone Connected; (Destination=1294) ID=5a7e099c-65b8-11e9-9ea6-1146
WT-001-1 | 2019/01/01 01:10:12.714 UTC | Tue 2019/01/01 01:10:12 EDT | 395 |                    |     |     |            |             |         |   | 7cfa2825
WT-001-1 | 2019/01/01 01:10:59.757 UTC | Tue 2019/01/01 01:10:59 EDT | 342 | 444444444444444444 | T50 | P04 |            |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:10:59.762 UTC | Tue 2019/01/01 01:10:59 EDT | 307 | 444444444444444444 | T50 | P00 |            |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "444444444444444444");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 01:09:15.597");
        REQUIRE(rec.answer_utc == "2019-01-01 01:09:18.224"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 1);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == false);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(!rec.disconnect_utc);
    }
}
