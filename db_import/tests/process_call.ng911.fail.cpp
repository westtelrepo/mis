#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("failed ng911 calls by dialplan") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [4]
    - id: WT-001-B
      positions: [5]
    - id: WT-001-C
      positions: [1]
  dialplan:
    - num: 1000
      psap: WT-001-C
    - num: sip:911@example.com
      psap: WT-001-C
    - num: sip:911@diff.example.com
      psap: WT-001-B
)");

    auto val = parse(
R"(WT-001-1 | 2018/02/02 02:57:22.955 UTC | Mon 2018/02/02 02:57:22 UTC | 316 | 222222222222222222 | T09 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2018/02/02 02:57:22.998 UTC | Mon 2018/02/02 02:57:22 UTC | 222 | 222222222222222222 | T09 | P00 |            | p2222222222 | ALI 01A | < | Request Sent with RequestKey=2222222222; Index=29; Trunk=09 (to DB=01 PP=01)
WT-001-1 | 2018/02/02 02:57:22.999 UTC | Mon 2018/02/02 02:57:22 UTC | 222 | 222222222222222222 | T09 | P00 |            | p2222222222 | ALI 01B | < | Request Sent with RequestKey=2222222222; Index=29; Trunk=09 (to DB=01 PP=01)
WT-001-1 | 2018/02/02 02:57:56.904 UTC | Mon 2018/02/02 02:57:56 UTC | 271 | 222222222222222222 | T09 | P00 | 1111111111 | p2222222222 | ALI 01A | < | Auto Request Sent with RequestKey=2222222222; Index=31; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/02/02 02:57:56.905 UTC | Mon 2018/02/02 02:57:56 UTC | 271 | 222222222222222222 | T09 | P00 | 1111111111 | p2222222222 | ALI 01B | < | Auto Request Sent with RequestKey=2222222222; Index=31; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/02/02 02:58:11.779 UTC | Mon 2018/02/02 02:58:11 UTC | 306 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2018/02/02 02:58:11.877 UTC | Mon 2018/02/02 02:58:11 UTC | 402 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2018/02/02 02:58:11.877 UTC | Mon 2018/02/02 02:58:11 UTC | 402 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2018/02/02 02:58:47.483 UTC | Mon 2018/02/02 02:58:47 UTC | 365 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/02/02 02:59:05.860 UTC | Mon 2018/02/02 02:59:05 UTC | 365 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/02/02 02:59:21.139 UTC | Mon 2018/02/02 02:59:21 UTC | 365 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/02/02 02:59:26.548 UTC | Mon 2018/02/02 02:59:26 UTC | 365 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@diff.example.com)
WT-001-1 | 2018/02/02 02:59:28.603 UTC | Mon 2018/02/02 02:59:28 UTC | 378 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@diff.example.com)
WT-001-1 | 2018/02/02 02:59:28.650 UTC | Mon 2018/02/02 02:59:28 UTC | 402 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2018/02/02 02:59:28.650 UTC | Mon 2018/02/02 02:59:28 UTC | 402 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2018/02/02 02:59:48.533 UTC | Mon 2018/02/02 02:59:48 UTC | 365 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/05/28 03:00:00.709 UTC | Mon 2018/05/28 03:00:00 UTC | 365 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/05/28 03:00:11.897 UTC | Mon 2018/05/28 03:00:11 UTC | 342 | 222222222222222222 | T09 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/05/28 03:01:19.392 UTC | Mon 2018/05/28 03:01:19 UTC | 310 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | On Hold
WT-001-1 | 2018/05/28 03:01:41.570 UTC | Mon 2018/05/28 03:01:41 UTC | 311 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Off Hold
WT-001-1 | 2018/05/28 03:01:43.762 UTC | Mon 2018/05/28 03:01:43 UTC | 310 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | On Hold
WT-001-1 | 2018/05/28 03:01:51.287 UTC | Mon 2018/05/28 03:01:51 UTC | 373 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=1000)
WT-001-1 | 2018/05/28 03:02:23.299 UTC | Mon 2018/05/28 03:02:23 UTC | 369 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=1000); Reason=RECOVERY_ON_TIMER_EXPIRE
WT-001-1 | 2018/05/28 03:04:56.128 UTC | Mon 2018/05/28 03:04:56 UTC | 311 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Off Hold
WT-001-1 | 2018/05/28 03:05:10.134 UTC | Mon 2018/05/28 03:05:10 UTC | 365 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example.com)
WT-001-1 | 2018/05/28 03:05:11.816 UTC | Mon 2018/05/28 03:05:11 UTC | 402 | 222222222222222222 | T09 | P01 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2018/05/28 03:05:11.816 UTC | Mon 2018/05/28 03:05:11 UTC | 402 | 222222222222222222 | T09 | P01 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2018/05/28 03:05:11.832 UTC | Mon 2018/05/28 03:05:11 UTC | 378 | 222222222222222222 | T09 | P01 | 1111111111 | p2222222222 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@example.com)
WT-001-1 | 2018/05/28 03:05:21.038 UTC | Mon 2018/05/28 03:05:21 UTC | 342 | 222222222222222222 | T09 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/05/28 03:07:29.835 UTC | Mon 2018/05/28 03:07:29 UTC | 342 | 222222222222222222 | T09 | P01 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/05/28 03:07:29.918 UTC | Mon 2018/05/28 03:07:29 UTC | 307 | 222222222222222222 | T09 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 9);

    {
        auto record = val.at(0);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 0);
        REQUIRE(record.psap == "WT-001-A"s);
        REQUIRE(record.utc == "2018-02-02 02:57:22.955");
        REQUIRE(record.answer_utc == "2018-02-02 02:58:11.779"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 4);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::ng911);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::initial);
        REQUIRE(record.disconnect_utc == "2018-05-28 03:07:29.918"s);
    }

    {
        auto record = val.at(1);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 1);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-02-02 02:58:47.483");
        REQUIRE(!record.answer_utc);
        REQUIRE(record.abandoned ==true);
        REQUIRE(!record.pos);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(2);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 2);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-02-02 02:59:05.860");
        REQUIRE(!record.answer_utc);
        REQUIRE(record.abandoned == true);
        REQUIRE(!record.pos);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(3);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 3);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-02-02 02:59:21.139");
        REQUIRE(!record.answer_utc);
        REQUIRE(record.abandoned == true);
        REQUIRE(!record.pos);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(4);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 4);
        REQUIRE(record.psap == "WT-001-B"s);
        REQUIRE(record.utc == "2018-02-02 02:59:26.548");
        REQUIRE(record.answer_utc == "2018-02-02 02:59:28.603"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 5);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::ng911);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(5);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 5);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-02-02 02:59:48.533");
        REQUIRE(!record.answer_utc);
        REQUIRE(record.abandoned == true);
        REQUIRE(!record.pos);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(6);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 6);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-05-28 03:00:00.709");
        REQUIRE(!record.answer_utc);
        REQUIRE(record.abandoned == true);
        REQUIRE(!record.pos);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(7);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 7);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-05-28 03:01:51.287");
        REQUIRE(!record.answer_utc);
        REQUIRE(record.abandoned == true);
        REQUIRE(!record.pos);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::attended);
        REQUIRE(!record.disconnect_utc);
    }

    {
        auto record = val.at(8);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 8);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2018-05-28 03:05:10.134");
        REQUIRE(record.answer_utc == "2018-05-28 03:05:11.832"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 1);
        REQUIRE(!record.calltaker);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 9);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }
}
