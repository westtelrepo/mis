#include <catch.hpp>

#include "../ali.h"

TEST_CASE("ali_constructor") {
    ali_parser p;
}

TEST_CASE("ali_basic") {
    ali_parser p;

    const char* one =
        "\u0002111\r"
        "(111) 111-1111 WPH2 11/11 11:11\r"
        "Verizon WRLS 111-111-1111   \r"
        "      1111       P#111-111-1111\r"
        "   AAAAAA AAA AAAA - AAAA\r"
        " AAAAAA             \r"
        "CALLBK=(111)111-1111  111 11111\r"
        "AA AAAAAAAAAA                  \r"
        "                  TEL=VZW  \r"
        "+11.111111  -111.111111    111 \r"
        "+11.111111  -111.111111    111 \r"
        "VERIFY\r\n"
        "VERIFY\r\n"
        "VERIFY     \u0003";

    REQUIRE(p.parse_cos(one) == "WPH2");

    const char* nrf_flat10 = "\u0002111\r1111111111 NO RECORD FOUND\u0003";
    REQUIRE(p.parse_cos(nrf_flat10) == "NRF");

    const char* nrf_flat13 = "\u00021111111111111 NO RECORD FOUND\u0003";
    REQUIRE(p.parse_cos(nrf_flat13) == "NRF");

    const char* nrf =
        "\u00021111111111111 RECORD NOT FOUND\n"
        "                               \r"
        "\u007F\u007F\u0003";
    REQUIRE(p.parse_cos(nrf) == "NRF");

    REQUIRE(p.parse_cos("\u0002111\r1111111111 NO RESPONSE FROM CELL STEER\u0003") == "NRF");

}
