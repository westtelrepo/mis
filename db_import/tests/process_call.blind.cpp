#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("failed blind from GUI 365 -> 383") {
    auto val = parse(
R"(WT-001-1 | 2012/04/23 01:15:35.535 UTC | Thu 2012/04/23 01:15:35 UTC | 316 | 111111111111111111 |     | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=2222222222@10.10.10.10)
WT-001-1 | 2012/04/23 01:15:42.521 UTC | Thu 2012/04/23 01:15:42 UTC | 306 | 111111111111111111 |     | P02 | VoIP CALL  |             | ANI 01  | > | Connect (From=2222222222@10.10.10.10)
WT-001-1 | 2012/04/23 01:16:35.827 UTC | Thu 2012/04/23 01:16:35 UTC | 310 | 111111111111111111 |     | P02 | VoIP CALL  |             | ANI 01  | > | On Hold
WT-001-1 | 2012/04/23 01:16:50.245 UTC | Thu 2012/04/23 01:16:50 UTC | 311 | 111111111111111111 |     | P02 | VoIP CALL  |             | ANI 01  | > | Off Hold
WT-001-1 | 2012/04/23 01:17:03.791 UTC | Thu 2012/04/23 01:17:03 UTC | 365 | 111111111111111111 |     | P02 | VoIP CALL  |             | ANI     | < | Blind Transfer from GUI; (Destination=301)
WT-001-1 | 2012/04/23 01:17:03.852 UTC | Thu 2012/04/23 01:17:03 UTC | 342 | 111111111111111111 |     | P02 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2012/04/23 01:17:07.838 UTC | Thu 2012/04/23 01:17:07 UTC | 383 | 111111111111111111 |     | P02 | VoIP CALL  |             | ANI 01  | > | Blind Transfer From GUI Failed; (Destination=301); Reason=UNALLOCATED_NUMBER
WT-001-1 | 2012/04/23 01:17:13.265 UTC | Thu 2012/04/23 01:17:13 UTC | 370 | 111111111111111111 |     | P02 | VoIP CALL  |             | ANI 01  | > | Reconnect Ringback
WT-001-1 | 2012/04/23 01:18:20.944 UTC | Thu 2012/04/23 01:18:20 UTC | 342 | 111111111111111111 |     | P02 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2012/04/23 01:18:20.964 UTC | Thu 2012/04/23 01:18:20 UTC | 307 | 111111111111111111 |     | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2012-04-23 01:15:35.535");
    REQUIRE(record.answer_utc == "2012-04-23 01:15:42.521"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2012-04-23 01:18:20.964"s);
}
