#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("process_call outbound simple") {
    auto val = parse(
R"(WT-001-1 | 2016/01/01 01:13:55.808 UTC | Sun 2016/01/01 01:13:55 UTC | 372 | 111111111111111111 | T17 | P01 | 1111111111 |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=1111111111)
WT-001-1 | 2016/01/01 01:14:11.509 UTC | Sun 2016/01/01 01:14:11 UTC | 306 | 111111111111111111 | T17 | P01 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2016/01/01 01:14:32.168 UTC | Sun 2016/01/01 01:14:32 UTC | 342 | 111111111111111111 | T17 | P00 | 1111111111 |             | ANI 01  | > | Conference Leave exten: 1111111111
WT-001-1 | 2016/01/01 01:14:32.172 UTC | Sun 2016/01/01 01:14:32 UTC | 307 | 111111111111111111 | T17 | P01 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2016-01-01 01:13:55.808");
    REQUIRE(record.answer_utc == "2016-01-01 01:14:11.509"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == false);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}

TEST_CASE("process_call outbound from phone with outbound from phone connected") {
    auto val = parse(
R"(WT-001-1 | 2015/01/03 12:12:01.014 UTC | Fri 2015/01/03 12:12:01 UTC | 372 | 111111111111111111 |     | P03 |            |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=111111111111)
WT-001-1 | 2015/01/03 12:12:04.574 UTC | Fri 2015/01/03 12:12:04 UTC | 370 | 111111111111111111 |     | P03 |            |             | ANI 01  | > | Outbound Call from Phone Connected; (Destination=111111111111)
WT-001-1 | 2015/01/03 12:12:11.012 UTC | Fri 2015/01/03 12:12:11 UTC | 342 | 111111111111111111 |     | P01 |            |             | ANI 01  | > | Conference Leave
WT-001-1 | 2015/01/03 12:12:11.016 UTC | Fri 2015/01/03 12:12:11 UTC | 307 | 111111111111111111 |     | P03 |            |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2015-01-03 12:12:01.014");
    REQUIRE(record.answer_utc == "2015-01-03 12:12:04.574"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 3);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == false);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}

TEST_CASE("395 Outbound Call to Position Connected within same PSAP") {
    auto val = parse(
R"(WT-001-1 | 2016/01/01 01:01:13.886 UTC | Wed 2016/01/01 01:01:13 UTC | 372 | 111111111111111111 |     | P01 |            |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=1001)
WT-001-1 | 2016/01/01 01:01:17.566 UTC | Wed 2016/01/01 01:01:17 UTC | 395 | 111111111111111111 |     | P07 |            |             | ANI 01  | > | Outbound Call to Position Connected; (Destination=1001)
WT-001-1 | 2016/01/01 01:01:30.999 UTC | Wed 2016/01/01 01:01:30 UTC | 342 | 111111111111111111 |     | P07 |            |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/01/01 01:01:31.020 UTC | Wed 2016/01/01 01:01:31 UTC | 307 | 111111111111111111 |     | P01 |            |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 0);
}

TEST_CASE("368 warning ignore") {
    auto val = parse(
R"(WT-001-1 | 2014/01/01 01:55:31.230 UTC | Fri 2014/01/01 01:55:31 UTC | 372 | 333333333333333333 |     | P06 |            |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=33333333)
WT-001-1 | 2014/01/01 01:55:34.178 UTC | Fri 2014/01/01 01:55:34 UTC | 306 | 333333333333333333 |     | P06 |            |             | ANI 01  | > | Connect
WT-001-1 | 2014/01/01 01:55:44.009 UTC | Fri 2014/01/01 01:55:44 UTC | 368 | 333333333333333333 |     | P06 |            |             | ANI 01  | > | [WARNING] Transfer Cancelled, ext: 33333333 is currently connected
WT-001-1 | 2014/01/01 01:55:49.986 UTC | Fri 2014/01/01 01:55:49 UTC | 342 | 333333333333333333 |     | P00 |            |             | ANI 01  | > | Conference Leave exten: 33333333
WT-001-1 | 2014/01/01 01:55:50.007 UTC | Fri 2014/01/01 01:55:50 UTC | 307 | 333333333333333333 |     | P06 |            |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2014-01-01 01:55:31.230");
    REQUIRE(record.answer_utc == "2014-01-01 01:55:34.178"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 6);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == false);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}

TEST_CASE("external outbound abandon") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:48:43.314 UTC | Sat 2018/01/01 01:48:43 UTC | 372 | 333333333333333333 |     | P08 | 1111111111 |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=91111111111)
WT-001-1 | 2018/01/01 01:48:46.647 UTC | Sat 2018/01/01 01:48:46 UTC | 312 | 333333333333333333 |     | P08 | 1111111111 |             | ANI 01  | > | Abandoned
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-01-01 01:48:43.314");
    REQUIRE(!record.answer_utc);
    REQUIRE(record.abandoned == true);
    REQUIRE(record.pos == 8);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == false);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}

TEST_CASE("external outbound abandon enterprise") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [8]
)");

    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:48:43.314 UTC | Sat 2018/01/01 01:48:43 UTC | 372 | 333333333333333333 |     | P08 | 1111111111 |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=91111111111)
WT-001-1 | 2018/01/01 01:48:46.647 UTC | Sat 2018/01/01 01:48:46 UTC | 312 | 333333333333333333 |     | P08 | 1111111111 |             | ANI 01  | > | Abandoned
)", config);

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-A"s);
    REQUIRE(record.utc == "2018-01-01 01:48:43.314");
    REQUIRE(!record.answer_utc);
    REQUIRE(record.abandoned == true);
    REQUIRE(record.pos == 8);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == false);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}
