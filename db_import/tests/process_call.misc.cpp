#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("ALI unmatched record and warnings ignored") {
    auto val = parse(
R"(WT-001-1 | 2014/01/01 03:13:24.863 UTC | Thu 2014/01/01 03:13:24 UTC | 316 | 444444444444444444 | T04 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2014/01/01 03:13:24.874 UTC | Thu 2014/01/01 03:13:24 UTC | 222 | 444444444444444444 | T04 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=22222222; Index=08; Trunk=04 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 03:13:24.875 UTC | Thu 2014/01/01 03:13:24 UTC | 222 | 444444444444444444 | T04 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=22222222; Index=08; Trunk=04 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 03:13:26.814 UTC | Thu 2014/01/01 03:13:26 UTC | 306 | 444444444444444444 | T04 | P02 |            | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2014/01/01 03:13:30.921 UTC | Thu 2014/01/01 03:13:30 UTC | 220 | 444444444444444444 | T04 | P02 |            | p1111111111 | ALI 01A | - | [WARNING] Record Timeout
WT-001-1 | 2014/01/01 03:13:30.921 UTC | Thu 2014/01/01 03:13:30 UTC | 220 | 444444444444444444 | T04 | P02 |            | p1111111111 | ALI 01B | - | [WARNING] Record Timeout
WT-001-1 | 2014/01/01 03:13:30.921 UTC | Thu 2014/01/01 03:13:30 UTC | 231 | 444444444444444444 | T04 | P02 |            | p1111111111 | ALI 01A | < | Request Resent with RequestKey=22222222; Index=08; Trunk=04 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 03:13:30.921 UTC | Thu 2014/01/01 03:13:30 UTC | 231 | 444444444444444444 | T04 | P02 |            | p1111111111 | ALI 01B | < | Request Resent with RequestKey=22222222; Index=08; Trunk=04 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 | 444444444444444444 | T04 | P02 |            | p1111111111 | ALI 01B | > | Initial Record received (Data Follows)
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | <STX>208<CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 WPH1 11/11 17:11<CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | wwwwww WIRELESS 111-111-1111<CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   |      00000       P#111-111-1111<CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   |    wwww ww - w wwwwww    <CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | 0000000000            000 00000<CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | ww wwwwwwwwwww                 <CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=NECCI<CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | +11.111111  -111.111111   0000 <CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | PSAP=wwww--BACA WIRELESS<CR>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-001-1 | 2014/01/01 03:13:31.297 UTC | Thu 2014/01/01 03:13:31 UTC | 226 |                    |     |     |            |             |         |   | VERIFY EMS                     <ETX>
WT-001-1 | 2014/01/01 03:13:31.304 UTC | Thu 2014/01/01 03:13:31 UTC | 246 |                    |     |     |            |             | ALI 01B | > | [WARNING] Stray ACK Received
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             | ALI 01B | > | [WARNING] UnMatched Record received, TransactionId=08 (Data Follows)
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | <STX>208<CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | (111) 111-1111 WPH1 11/11 17:11<CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | wwwwww WIRELESS 111-111-1111<CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   |      11111       P#111-111-1111<CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   |    wwww ww - w wwwwww    <CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | 3333333333            000 00000<CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | ww wwwwwwwwwww                 <CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   |                   TEL=NECCI<CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | +11.111111  -111.111111   0000 <CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | PSAP=BCSO--BACA WIRELESS<CR>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-001-1 | 2014/01/01 03:13:34.205 UTC | Thu 2014/01/01 03:13:34 UTC | 232 |                    |     |     |            |             |         |   | VERIFY EMS                     <ETX>
WT-001-1 | 2014/01/01 03:14:21.086 UTC | Thu 2014/01/01 03:14:21 UTC | 342 | 444444444444444444 | T04 | P02 | 3333333333 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2014/01/01 03:14:21.107 UTC | Thu 2014/01/01 03:14:21 UTC | 307 | 444444444444444444 | T04 | P00 | 3333333333 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "444444444444444444");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2014-01-01 03:13:24.863");
    REQUIRE(record.answer_utc == "2014-01-01 03:13:26.814"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "WPH1"s);
    REQUIRE(record.trunk == 4);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2014-01-01 03:14:21.107"s);
}

TEST_CASE("733 Abandoned Call Taken Control by Workstation") {
    auto val = parse(
R"(WT-001-1 | 2014/01/01 01:24:36.866 UTC | Thu 2014/01/01 01:24:36 UTC | 316 | 444444444444444444 | T01 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2014/01/01 01:24:36.899 UTC | Thu 2014/01/01 01:24:36 UTC | 222 | 444444444444444444 | T01 | P00 |            | p2222222222 | ALI 01A | < | Request Sent with RequestKey=33333333; Index=14; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 01:24:36.899 UTC | Thu 2014/01/01 01:24:36 UTC | 222 | 444444444444444444 | T01 | P00 |            | p2222222222 | ALI 01B | < | Request Sent with RequestKey=33333333; Index=14; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 01:24:40.853 UTC | Thu 2014/01/01 01:24:40 UTC | 312 | 444444444444444444 | T01 | P00 |            | p2222222222 | ANI 01  | > | Abandoned
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 | 444444444444444444 | T01 | P00 |            | p2222222222 | ALI 01B | > | Initial Record received (Data Follows)
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | <STX>214<CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 WPH1 11/11 17:11<CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | wwwwww WIRELESS 111-111-1111<CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   |      00000       P#111-111-1111<CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   |    MAIN ST - N SECTOR    <CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | 0000000000            000 00000<CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | ww wwwwwwwwwww                 <CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=NECCI<CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | +11.111111  -111.111111   1111 <CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | PSAP=wwww-BACA WIRELESS<CR>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-001-1 | 2014/01/01 01:24:42.703 UTC | Thu 2014/01/01 01:24:42 UTC | 226 |                    |     |     |            |             |         |   | VERIFY EMS                     <ETX>
WT-001-1 | 2014/01/01 01:28:56.019 UTC | Thu 2014/01/01 01:28:56 UTC | 733 | 444444444444444444 | T01 | P01 |            | p2222222222 | WRK     | > | Abandoned Call Taken Control by Workstation
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "444444444444444444");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2014-01-01 01:24:36.866");
    REQUIRE(!record.answer_utc);
    REQUIRE(record.abandoned == true);
    REQUIRE(!record.pos);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "WPH1"s);
    REQUIRE(record.trunk == 1);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}

TEST_CASE("261 warning") {
    auto val = parse(
R"(WT-001-1 | 2015/01/01 01:07:03.577 UTC | Fri 2015/01/01 01:07:03 UTC | 316 | 444444444444444444 | T01 | P00 |            | p9110000000 | ANI 01  | > | Ringing
WT-001-1 | 2015/01/01 01:07:03.622 UTC | Fri 2015/01/01 01:07:03 UTC | 261 | 444444444444444444 | T01 | P00 |            | p9110000000 | ALI     | - | [WARNING] Area Code 911 not found in NPD Table, System Will Bid Dummy Key
WT-001-1 | 2015/01/01 01:07:03.622 UTC | Fri 2015/01/01 01:07:03 UTC | 222 | 444444444444444444 | T01 | P00 |            | p9110000000 | ALI 01A | < | Request Sent with RequestKey=01000000; Index=01; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2015/01/01 01:07:03.622 UTC | Fri 2015/01/01 01:07:03 UTC | 222 | 444444444444444444 | T01 | P00 |            | p9110000000 | ALI 01B | < | Request Sent with RequestKey=01000000; Index=01; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2015/01/01 01:07:04.254 UTC | Fri 2015/01/01 01:07:04 UTC | 226 | 444444444444444444 | T01 | P00 |            | p9110000000 | ALI 01B | > | Initial Record received (Data Follows)
WT-001-1 | 2015/01/01 01:07:04.254 UTC | Fri 2015/01/01 01:07:04 UTC | 226 |                    |     |     |            |             |         |   | <STX>9019110000000 RECORD NOT FOUND<LF>
WT-001-1 | 2015/01/01 01:07:04.254 UTC | Fri 2015/01/01 01:07:04 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2015/01/01 01:07:04.254 UTC | Fri 2015/01/01 01:07:04 UTC | 226 |                    |     |     |            |             |         |   | <127><127><ETX>
WT-001-1 | 2015/01/01 01:07:11.004 UTC | Fri 2015/01/01 01:07:11 UTC | 306 | 444444444444444444 | T01 | P01 |            | p9110000000 | ANI 01  | > | Connect
WT-001-1 | 2015/01/01 01:07:24.518 UTC | Fri 2015/01/01 01:07:24 UTC | 342 | 444444444444444444 | T01 | P01 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2015/01/01 01:07:24.523 UTC | Fri 2015/01/01 01:07:24 UTC | 307 | 444444444444444444 | T01 | P00 |            | p9110000000 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "444444444444444444");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2015-01-01 01:07:03.577");
    REQUIRE(record.answer_utc == "2015-01-01 01:07:11.004"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "NRF"s);
    REQUIRE(record.trunk == 1);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2015-01-01 01:07:24.523"s);
}

TEST_CASE("147 Warning") {
    auto val = parse(
R"(WT-001-1 | 2018/06/30 10:31:25.827 UTC | Sat 2018/06/30 10:31:25 UTC | 316 | 444444444444444444 | T02 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2018/06/30 10:31:25.857 UTC | Sat 2018/06/30 10:31:25 UTC | 222 | 444444444444444444 | T02 | P00 |            | p2222222222 | ALI 01A | < | Request Sent with RequestKey=33333333; Index=78; Trunk=02 (to DB=01 PP=01)
WT-001-1 | 2018/06/30 10:31:25.857 UTC | Sat 2018/06/30 10:31:25 UTC | 222 | 444444444444444444 | T02 | P00 |            | p2222222222 | ALI 01B | < | Request Sent with RequestKey=33333333; Index=78; Trunk=02 (to DB=01 PP=01)
WT-001-1 | 2018/06/30 10:31:29.989 UTC | Sat 2018/06/30 10:31:29 UTC | 306 | 444444444444444444 | T02 | P01 |            | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 | 444444444444444444 | T02 | P01 | 1111111111 | p2222222222 | ALI 01A | > | Initial Record received (Data Follows)
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | <STX>278<CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 WPH2 01/11 17:11<CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | wwwwww WIRELESS 111-111-1111<CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   |      00000       P#000-000-0000<CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   |    ww 11.1  - w wwwwww   <CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | 1111111111            000 00000<CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | ww wwwwwwwwwww                 <CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=NECCI<CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | +11.111111  -111.111111     00 <CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | PSAP=wwww--wwww WIRELESS<CR>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-001-1 | 2018/06/30 10:31:31.903 UTC | Sat 2018/06/30 10:31:31 UTC | 226 |                    |     |     |            |             |         |   | VERIFY EMS                     <ETX>
WT-001-1 | 2018/06/30 10:31:53.510 UTC | Sat 2018/06/30 10:31:53 UTC | 270 | 444444444444444444 | T02 | P01 | 1111111111 | p2222222222 | ALI 01A | < | Rebid Request Sent with RequestKey=33333333; Index=79; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/06/30 10:31:53.510 UTC | Sat 2018/06/30 10:31:53 UTC | 270 | 444444444444444444 | T02 | P01 | 1111111111 | p2222222222 | ALI 01B | < | Rebid Request Sent with RequestKey=33333333; Index=79; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 | 444444444444444444 | T02 | P01 | 1111111111 | p2222222222 | ALI 01A | > | Rebid Record received (Data Follows)
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | <STX>279<CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | (111) 111-1111 WPH2 01/11 17:11<CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | wwwwww WIRELESS             <CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   |                  P#111-111-1111<CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   |    WIRELESS CALL         <CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | 1111111111            000 00000<CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | ww wwww wwwwww ww              <CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   |                   TEL=NECCI<CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | +11.111111  -111.111111     11 <CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | PSAP=wwww--wwww WIRELESS<CR>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-001-1 | 2018/06/30 10:32:00.329 UTC | Sat 2018/06/30 10:32:00 UTC | 266 |                    |     |     |            |             |         |   | VERIFY EMS                     <ETX>
WT-001-1 | 2018/06/30 10:32:40.263 UTC | Sat 2018/06/30 10:32:40 UTC | 342 | 444444444444444444 | T02 | P01 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/06/30 10:32:40.268 UTC | Sat 2018/06/30 10:32:40 UTC | 307 | 444444444444444444 | T02 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Disconnect
WT-001-1 | 2018/07/08 04:54:29.643 UTC | Sat 2018/07/07 22:54:29 UTC | 147 | 444444444444444444 |     | P00 |            |             | KRN     | - | [WARNING] Call-ID not Found; Forced Disconnect Failed
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "444444444444444444");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-06-30 10:31:25.827");
    REQUIRE(record.answer_utc == "2018-06-30 10:31:29.989"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "WPH2"s);
    REQUIRE(record.trunk == 2);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-06-30 10:32:40.268"s);
}

TEST_CASE("some cad warnings: 413, 430, 428") {
    auto val = parse(
R"(WT-001-1 | 2011/01/01 01:44:33.658 UTC | Tue 2011/01/01 01:44:33 UTC | 316 | 444444444444444444 | T03 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2011/01/01 01:44:33.701 UTC | Tue 2011/01/01 01:44:33 UTC | 222 | 444444444444444444 | T03 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=00000000; Index=01; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2011/01/01 01:44:35.358 UTC | Tue 2011/01/01 01:44:35 UTC | 226 | 444444444444444444 | T03 | P00 |            | p1111111111 | ALI 01A | > | Record received (Data Follows)
WT-001-1 | 2011/01/01 01:44:35.358 UTC | Tue 2011/01/01 01:44:35 UTC | 226 |                    |     |     |            |             |         |   | <STX>9011111111111 RECORD NOT FOUND<LF>
WT-001-1 | 2011/01/01 01:44:35.358 UTC | Tue 2011/01/01 01:44:35 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2011/01/01 01:44:35.358 UTC | Tue 2011/01/01 01:44:35 UTC | 226 |                    |     |     |            |             |         |   | <127><127><ETX>
WT-001-1 | 2011/01/01 01:44:36.220 UTC | Tue 2011/01/01 01:44:36 UTC | 306 | 444444444444444444 | T03 | P01 |            | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2011/01/01 01:44:36.251 UTC | Tue 2011/01/01 01:44:36 UTC | 402 | 444444444444444444 | T03 | P01 |            | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2011/01/01 01:44:37.271 UTC | Tue 2011/01/01 01:44:37 UTC | 413 | 444444444444444444 | T03 | P01 |            | p1111111111 | CAD 01  | - | [WARNING] ALI Record ACK Timeout
WT-001-1 | 2011/01/01 01:44:37.271 UTC | Tue 2011/01/01 01:44:37 UTC | 430 | 444444444444444444 | T03 | P01 |            | p1111111111 | CAD 01  | < | [WARNING] ALI Record retransmitted
WT-001-1 | 2011/01/01 01:44:38.291 UTC | Tue 2011/01/01 01:44:38 UTC | 413 | 444444444444444444 | T03 | P01 |            | p1111111111 | CAD 01  | - | [WARNING] ALI Record ACK Timeout
WT-001-1 | 2011/01/01 01:44:38.291 UTC | Tue 2011/01/01 01:44:38 UTC | 428 | 444444444444444444 | T03 | P01 |            | p1111111111 | CAD 01  | - | [WARNING] ALI Record Lost
WT-001-1 | 2011/01/01 01:44:46.445 UTC | Tue 2011/01/01 01:44:46 UTC | 342 | 444444444444444444 | T03 | P01 |            | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 01:44:46.446 UTC | Tue 2011/01/01 01:44:46 UTC | 307 | 444444444444444444 | T03 | P00 |            | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "444444444444444444");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2011-01-01 01:44:33.658");
    REQUIRE(record.answer_utc == "2011-01-01 01:44:36.220"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "NRF"s);
    REQUIRE(record.trunk == 3);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2011-01-01 01:44:46.446"s);
}

TEST_CASE("146 Warning") {
    auto val = parse(
R"(WT-001-1 | 2011/05/11 17:01:54.858 UTC | Wed 2011/05/11 17:01:54 UTC | 316 | 444444444444444444 | T01 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2011/05/11 17:01:54.862 UTC | Wed 2011/05/11 17:01:54 UTC | 222 | 444444444444444444 | T01 | P00 |            | p2222222222 | ALI 01A | < | Request Sent with RequestKey=00000000; Index=46; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2011/05/11 17:01:54.862 UTC | Wed 2011/05/11 17:01:54 UTC | 222 | 444444444444444444 | T01 | P00 |            | p2222222222 | ALI 01B | < | Request Sent with RequestKey=00000000; Index=46; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 | 444444444444444444 | T01 | P00 | 1111111111 | p2222222222 | ALI 01A | > | Record received (Data Follows)
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | <STX>246<CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 VOIP 05/11 17:01<CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | wwwww,www                   <CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   |        000       P#111-111-1111<CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   |    COLORADO 103          <CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   |                       000 00000<CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | ww wwwww wwwwwww               <CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=wwwww<CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | +11.111111  -111.111111      0 <CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | PSAP=CLEAR CREEK VOIP<CR>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | <CR><LF>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | VOIP 911 CALL<CR><LF>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | VERIFY CALLERS LOCATION<CR><LF>
WT-001-1 | 2011/05/11 17:01:58.363 UTC | Wed 2011/05/11 17:01:58 UTC | 226 |                    |     |     |            |             |         |   | VERIFYEMS     <ETX>
WT-001-1 | 2011/05/11 17:02:00.296 UTC | Wed 2011/05/11 17:02:00 UTC | 306 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2011/05/11 17:02:00.369 UTC | Wed 2011/05/11 17:02:00 UTC | 402 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2011/05/11 17:02:00.738 UTC | Wed 2011/05/11 17:02:00 UTC | 404 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | CAD 01  | > | ACK Received
WT-001-1 | 2011/05/11 17:02:06.716 UTC | Wed 2011/05/11 17:02:06 UTC | 309 | 444444444444444444 | T01 | P02 | 1111111111 | p2222222222 | ANI 01  | > | Conference Join
WT-001-1 | 2011/05/11 17:02:14.779 UTC | Wed 2011/05/11 17:02:14 UTC | 309 | 444444444444444444 | T01 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Join
WT-001-1 | 2011/05/11 17:02:44.510 UTC | Wed 2011/05/11 17:02:44 UTC | 342 | 444444444444444444 | T01 | P02 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/05/11 17:02:58.404 UTC | Wed 2011/05/11 17:02:58 UTC | 222 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | ALI 01A | < | Request Sent with RequestKey=00000000; Index=47; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2011/05/11 17:02:58.404 UTC | Wed 2011/05/11 17:02:58 UTC | 222 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | ALI 01B | < | Request Sent with RequestKey=00000000; Index=47; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2011/05/11 17:02:58.420 UTC | Wed 2011/05/11 17:02:58 UTC | 342 | 444444444444444444 | T01 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | ALI 01A | > | Record received (Data Follows)
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | <STX>247<CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 VOIP 05/11 17:02<CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | wwwww,www                   <CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   |        000       P#111-111-1111<CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   |    COLORADO 103          <CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   |                       000 00000<CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | ww wwwww wwwwwww               <CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=wwwww<CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | +11.111111  -111.111111      0 <CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | PSAP=CLEAR CREEK VOIP<CR>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | <CR><LF>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | VOIP 911 CALL<CR><LF>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | VERIFY CALLERS LOCATION<CR><LF>
WT-001-1 | 2011/05/11 17:03:01.878 UTC | Wed 2011/05/11 17:03:01 UTC | 226 |                    |     |     |            |             |         |   | VERIFYEMS     <ETX>
WT-001-1 | 2011/05/11 17:03:01.924 UTC | Wed 2011/05/11 17:03:01 UTC | 402 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2011/05/11 17:03:02.294 UTC | Wed 2011/05/11 17:03:02 UTC | 404 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | CAD 01  | > | ACK Received
WT-001-1 | 2011/05/11 17:03:13.233 UTC | Wed 2011/05/11 17:03:13 UTC | 309 | 444444444444444444 | T01 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Join
WT-001-1 | 2011/05/11 17:03:16.513 UTC | Wed 2011/05/11 17:03:16 UTC | 342 | 444444444444444444 | T01 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/05/11 17:03:17.960 UTC | Wed 2011/05/11 17:03:17 UTC | 310 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | ANI 01  | > | On Hold
WT-001-1 | 2011/05/11 17:03:22.562 UTC | Wed 2011/05/11 17:03:22 UTC | 373 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=3333333333)
WT-001-1 | 2011/05/11 17:03:34.423 UTC | Wed 2011/05/11 17:03:34 UTC | 370 | 444444444444444444 | T01 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Attended Transfer from Phone Connected; (Destination=3333333333)
WT-001-1 | 2011/05/11 17:03:45.382 UTC | Wed 2011/05/11 17:03:45 UTC | 342 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/05/11 17:13:33.975 UTC | Wed 2011/05/11 17:13:33 UTC | 342 | 444444444444444444 | T01 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave exten: 3333333333
WT-001-1 | 2011/05/11 20:25:53.827 UTC | Wed 2011/05/11 20:25:53 UTC | 146 | 444444444444444444 | T01 | P01 | 1111111111 | p2222222222 | KRN     | - | [WARNING] Forced Disconnect Sent
WT-001-1 | 2011/05/11 20:25:53.827 UTC | Wed 2011/05/11 20:25:53 UTC | 307 | 444444444444444444 | T01 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "444444444444444444");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2011-05-11 17:01:54.858");
    REQUIRE(record.answer_utc == "2011-05-11 17:02:00.296"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "VOIP"s);
    REQUIRE(record.trunk == 1);
    REQUIRE(record.transfer_reaction == transfer_type::attended);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc); // because of forced disconnect warning
}

TEST_CASE("408 Warning") {
    auto val = parse(
R"(WT-001-1 | 2011/01/01 00:57:37.914 UTC | Sat 2011/01/01 00:57:37 UTC | 316 | 444444444444444444 | T01 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2011/01/01 00:57:37.930 UTC | Sat 2011/01/01 00:57:37 UTC | 222 | 444444444444444444 | T01 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=00000000; Index=29; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2011/01/01 00:57:37.930 UTC | Sat 2011/01/01 00:57:37 UTC | 222 | 444444444444444444 | T01 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=00000000; Index=29; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2011/01/01 00:57:41.597 UTC | Sat 2011/01/01 00:57:41 UTC | 306 | 444444444444444444 | T01 | P01 |            | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2011/01/01 00:57:44.467 UTC | Sat 2011/01/01 00:57:44 UTC | 402 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2011/01/01 00:57:44.837 UTC | Sat 2011/01/01 00:57:44 UTC | 404 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | CAD 01  | > | ACK Received
WT-001-1 | 2011/01/01 00:58:23.518 UTC | Sat 2011/01/01 00:58:23 UTC | 309 | 444444444444444444 | T01 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Conference Join
WT-001-1 | 2011/01/01 00:58:44.482 UTC | Sat 2011/01/01 00:58:44 UTC | 222 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | ALI 01A | < | Request Sent with RequestKey=00000000; Index=30; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2011/01/01 00:58:44.482 UTC | Sat 2011/01/01 00:58:44 UTC | 222 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | ALI 01B | < | Request Sent with RequestKey=00000000; Index=30; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2011/01/01 00:58:46.053 UTC | Sat 2011/01/01 00:58:46 UTC | 342 | 444444444444444444 | T01 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 00:58:51.425 UTC | Sat 2011/01/01 00:58:51 UTC | 402 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2011/01/01 00:58:52.292 UTC | Sat 2011/01/01 00:58:52 UTC | 365 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | ANI 01  | < | Transfer from GUI; (Destination=*3032394501)
WT-001-1 | 2011/01/01 00:58:52.445 UTC | Sat 2011/01/01 00:58:52 UTC | 413 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | CAD 01  | - | [WARNING] ALI Record ACK Timeout
WT-001-1 | 2011/01/01 00:58:52.445 UTC | Sat 2011/01/01 00:58:52 UTC | 430 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | CAD 01  | < | [WARNING] ALI Record retransmitted
WT-001-1 | 2011/01/01 00:58:53.465 UTC | Sat 2011/01/01 00:58:53 UTC | 413 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | CAD 01  | - | [WARNING] ALI Record ACK Timeout
WT-001-1 | 2011/01/01 00:58:53.465 UTC | Sat 2011/01/01 00:58:53 UTC | 428 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | CAD 01  | - | [WARNING] ALI Record Lost
WT-001-1 | 2011/01/01 00:58:53.466 UTC | Sat 2011/01/01 00:58:53 UTC | 408 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | CAD 01  | - | [WARNING] Begin Reduced Heartbeat Interval; Interval=10 Seconds (Ping Successful)
WT-001-1 | 2011/01/01 00:58:55.900 UTC | Sat 2011/01/01 00:58:55 UTC | 342 | 444444444444444444 | T01 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 00:58:55.992 UTC | Sat 2011/01/01 00:58:55 UTC | 307 | 444444444444444444 | T01 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "444444444444444444");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2011-01-01 00:57:37.914");
    REQUIRE(record.answer_utc == "2011-01-01 00:57:41.597"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 1);
    REQUIRE(record.transfer_reaction == transfer_type::unknown);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2011-01-01 00:58:55.992"s);
}

TEST_CASE("only disconnect") {
    auto val = parse(
R"(WT-001-1 | 2016/01/01 01:15:26.898 UTC | Wed 2016/01/01 01:15:26 UTC | 342 | 444444444444444444 | T02 | P01 | 2222222222 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2016/01/01 01:15:26.915 UTC | Wed 2016/01/01 01:15:26 UTC | 307 | 444444444444444444 | T02 | P00 | 2222222222 | p2222222222 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 0);
}
