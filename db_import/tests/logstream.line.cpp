#include "../LogStream.h"

#include <catch.hpp>

TEST_CASE("line that requires fallback") {
    LogStream::Line line{"WT-001-1 | 2012/01/01 01:01:01.001 UTC | Mon 0000/00/00 00:00:00 UTC | 719 |                    |     |     |            |             | WRK 02 | - | [WARNING] UDP Error (Code=22, MSG=[22] Invalid argument.)"};

    REQUIRE(line.psap == "WT-001-1");
    REQUIRE(line.utc == "2012/01/01 01:01:01.001");
    REQUIRE(line.code == 719);
    REQUIRE(line.callid == "");
    REQUIRE(line.position == "");
    REQUIRE(line.trunk == "");
    REQUIRE(line.ani == "");
    REQUIRE(line.pani == "");
    REQUIRE(line.thread == "WRK 02");
    REQUIRE(line.direction == "-");
    REQUIRE(line.message == "[WARNING] UDP Error (Code=22, MSG=[22] Invalid argument.)");
}

TEST_CASE("line") {
    LogStream::Line line{"WT-001-1 | 2018/01/01 01:00:01.316 UTC | Fri 2018/01/01 01:00:01 UTC | 269 | 222222222222222222 | T98 | P01 | 4444444444 | p4444444444 | ALI 01B | < | Manual Request Sent with RequestKey=4444444444; Index=0; Trunk=0 (to DB=01 PP=01)"};

    REQUIRE(line.psap == "WT-001-1");
    REQUIRE(line.utc == "2018/01/01 01:00:01.316");
    REQUIRE(line.code == 269);
    REQUIRE(line.callid == "222222222222222222");
    REQUIRE(line.position == "P01");
    REQUIRE(line.trunk == "T98");
    REQUIRE(line.ani == "4444444444");
    REQUIRE(line.pani == "p4444444444");
    REQUIRE(line.thread == "ALI 01B");
    REQUIRE(line.direction == "<");
    REQUIRE(line.message == "Manual Request Sent with RequestKey=4444444444; Index=0; Trunk=0 (to DB=01 PP=01)");
}
