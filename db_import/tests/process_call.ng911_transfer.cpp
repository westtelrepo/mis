#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("378 ng911 SIP transfer") {
    auto config = parse_config::parse(
R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [2]
    - id: WT-001-B
      positions: [5]
)");

    auto val = parse(
R"(WT-001-1 | 2014/01/01 01:14:13.387 UTC | Thu 2014/01/01 01:14:13 UTC | 316 | 666666666666666666 | T09 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2014/01/01 01:14:13.409 UTC | Thu 2014/01/01 01:14:13 UTC | 222 | 666666666666666666 | T09 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=02; Trunk=09 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 01:14:13.409 UTC | Thu 2014/01/01 01:14:13 UTC | 222 | 666666666666666666 | T09 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=02; Trunk=09 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 01:14:18.514 UTC | Thu 2014/01/01 01:14:18 UTC | 306 | 666666666666666666 | T09 | P02 | 2222222222 | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2014/01/01 01:14:18.520 UTC | Thu 2014/01/01 01:14:18 UTC | 402 | 666666666666666666 | T09 | P02 | 2222222222 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2014/01/01 01:14:18.520 UTC | Thu 2014/01/01 01:14:18 UTC | 402 | 666666666666666666 | T09 | P02 | 2222222222 | p1111111111 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2014/01/01 01:14:49.363 UTC | Thu 2014/01/01 01:14:49 UTC | 365 | 666666666666666666 | T09 | P02 | 2222222222 | p1111111111 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@psap.example.com)
WT-001-1 | 2014/01/01 01:14:53.038 UTC | Thu 2014/01/01 01:14:53 UTC | 309 | 666666666666666666 | T09 | P05 | 2222222222 | p1111111111 | ANI 01  | > | Conference Join
WT-001-1 | 2014/01/01 01:14:53.067 UTC | Thu 2014/01/01 01:14:53 UTC | 378 | 666666666666666666 | T09 | P05 | 2222222222 | p1111111111 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@psap.example.com)
WT-001-1 | 2014/01/01 01:14:53.120 UTC | Thu 2014/01/01 01:14:53 UTC | 402 | 666666666666666666 | T09 | P05 | 2222222222 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2014/01/01 01:14:53.120 UTC | Thu 2014/01/01 01:14:53 UTC | 402 | 666666666666666666 | T09 | P05 | 2222222222 | p1111111111 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2014/01/01 01:15:16.969 UTC | Thu 2014/01/01 01:15:16 UTC | 342 | 666666666666666666 | T09 | P02 | 2222222222 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2014/01/01 01:16:06.234 UTC | Thu 2014/01/01 01:16:06 UTC | 342 | 666666666666666666 | T09 | P05 | 2222222222 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2014/01/01 01:16:06.280 UTC | Thu 2014/01/01 01:16:06 UTC | 307 | 666666666666666666 | T09 | P00 | 2222222222 | p1111111111 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);
    auto record = val.at(0);

    REQUIRE(record.callid == "666666666666666666");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-A"s);
    REQUIRE(record.utc == "2014-01-01 01:14:13.387");
    REQUIRE(record.answer_utc == "2014-01-01 01:14:18.514"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 9);
    REQUIRE(record.transfer_reaction == transfer_type::ng911);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2014-01-01 01:16:06.280"s);

    auto rec2 = val.at(1);

    REQUIRE(rec2.callid == "666666666666666666");
    REQUIRE(rec2.controller == "WT-001-1");
    REQUIRE(rec2.subid == 1);
    REQUIRE(rec2.psap == "WT-001-B"s);
    REQUIRE(rec2.utc == "2014-01-01 01:14:49.363");
    REQUIRE(rec2.answer_utc == "2014-01-01 01:14:53.067"s);
    REQUIRE(rec2.abandoned == false);
    REQUIRE(rec2.pos == 5);
    REQUIRE(!rec2.calltaker);
    REQUIRE(rec2.is_911);
    REQUIRE(!rec2.cos);
    REQUIRE(rec2.trunk == 9);
    REQUIRE(rec2.transfer_reaction == transfer_type::none);
    REQUIRE(rec2.incoming == true);
    REQUIRE(rec2.t_type == transfer_type::ng911);
    REQUIRE(!rec2.disconnect_utc);
}
