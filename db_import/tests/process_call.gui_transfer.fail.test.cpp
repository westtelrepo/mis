#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("blind transfer from GUI fail also double connect") {
    auto val = parse(
R"(WT-004-1 | 2018/01/01 01:63:02.006 UTC | Mon 2018/01/01 01:63:02 UTC | 316 | 444444444444444444 | T17 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-004-1 | 2018/01/01 01:63:07.380 UTC | Mon 2018/01/01 01:63:07 UTC | 306 | 444444444444444444 | T17 | P10 | 3333333333 |             | ANI 01  | > | Connect
WT-004-1 | 2018/01/01 01:63:10.923 UTC | Mon 2018/01/01 01:63:10 UTC | 306 | 444444444444444444 | T17 | P05 | 3333333333 |             | ANI 01  | > | Connect
WT-004-1 | 2018/01/01 01:63:10.983 UTC | Mon 2018/01/01 01:63:10 UTC | 309 | 444444444444444444 | T17 | P05 | 3333333333 |             | ANI 01  | > | Conference Join
WT-004-1 | 2018/01/01 01:63:28.723 UTC | Mon 2018/01/01 01:63:28 UTC | 342 | 444444444444444444 | T17 | P05 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-004-1 | 2018/01/01 01:64:35.339 UTC | Mon 2018/01/01 01:64:35 UTC | 365 | 444444444444444444 | T17 | P10 | 3333333333 |             | ANI     | < | Blind Transfer from GUI; (Destination=3333)
WT-004-1 | 2018/01/01 01:64:35.705 UTC | Mon 2018/01/01 01:64:35 UTC | 342 | 444444444444444444 | T17 | P10 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-004-1 | 2018/01/01 01:64:35.727 UTC | Mon 2018/01/01 01:64:35 UTC | 342 | 444444444444444444 | T17 | P00 | 3333333333 |             | ANI 01  | > | Conference Leave exten: 3333333333
WT-004-1 | 2018/01/01 01:64:51.553 UTC | Mon 2018/01/01 01:64:51 UTC | 367 | 444444444444444444 | T17 | P10 | 3333333333 |             | ANI 01  | < | Blind Transfer from GUI Failed; (Destination=3333)
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "444444444444444444");
    REQUIRE(record.controller == "WT-004-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-004-1");
    REQUIRE(record.utc == "2018-01-01 01:63:02.006");
    REQUIRE(record.answer_utc == "2018-01-01 01:63:07.380"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 10);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == false);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}

TEST_CASE("failed conference transfer from GUI also double connect") {
    auto val = parse(
R"(WT-027-1 | 2018/01/01 01:39:50.765 UTC | Sun 2018/01/01 01:39:50 UTC | 316 | 777777777777777777 | T17 | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=3333333333@invalid.unknown.domain)
WT-027-1 | 2018/01/01 01:39:53.362 UTC | Sun 2018/01/01 01:39:53 UTC | 306 | 777777777777777777 | T17 | P05 | VoIP CALL  |             | ANI 01  | > | Connect (From=3333333333@invalid.unknown.domain)
WT-027-1 | 2018/01/01 01:40:01.149 UTC | Sun 2018/01/01 01:40:01 UTC | 306 | 777777777777777777 | T17 | P07 | VoIP CALL  |             | ANI 01  | > | Connect (From=3333333333@invalid.unknown.domain)
WT-027-1 | 2018/01/01 01:40:33.675 UTC | Sun 2018/01/01 01:40:33 UTC | 342 | 777777777777777777 | T17 | P07 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-027-1 | 2018/01/01 01:43:33.827 UTC | Sun 2018/01/01 01:43:33 UTC | 365 | 777777777777777777 | T17 | P05 | VoIP CALL  |             | ANI     | < | Conference Transfer from GUI; (Destination=5555555555)
WT-027-1 | 2018/01/01 01:43:37.885 UTC | Sun 2018/01/01 01:43:37 UTC | 367 | 777777777777777777 | T17 | P05 | VoIP CALL  |             | ANI 01  | < | Conference Transfer from GUI Cancelled; (Destination=5555555555)
WT-027-1 | 2018/01/01 01:44:39.846 UTC | Sun 2018/01/01 01:44:39 UTC | 342 | 777777777777777777 | T17 | P05 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-027-1 | 2018/01/01 01:44:39.867 UTC | Sun 2018/01/01 01:44:39 UTC | 307 | 777777777777777777 | T17 | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "777777777777777777");
    REQUIRE(record.controller == "WT-027-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-027-1");
    REQUIRE(record.utc == "2018-01-01 01:39:50.765");
    REQUIRE(record.answer_utc == "2018-01-01 01:39:53.362"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 5);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == false);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 01:44:39.867"s);
}

TEST_CASE("every transfer attempt fails") {
    auto val = parse(
R"(WT-001-1 | 2018/02/01 01:48:22.655 UTC | Tue 2018/02/01 01:48:22 UTC | 316 | 222222222222222222 | T17 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2018/02/01 01:48:25.292 UTC | Tue 2018/02/01 01:48:25 UTC | 306 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2018/02/01 01:49:32.473 UTC | Tue 2018/02/01 01:49:32 UTC | 365 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI     | < | Conference Transfer from GUI; (Destination=18001111111)
WT-001-1 | 2018/02/01 01:49:42.123 UTC | Tue 2018/02/01 01:49:42 UTC | 367 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | < | Transfer from GUI Cancelled; (Destination=18001111111) Reason: NO_USER_RESPONSE
WT-001-1 | 2018/02/01 01:49:53.952 UTC | Tue 2018/02/01 01:49:53 UTC | 365 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI     | < | Conference Transfer from GUI; (Destination=18001111111)
WT-001-1 | 2018/02/01 01:50:04.044 UTC | Tue 2018/02/01 01:50:04 UTC | 367 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | < | Transfer from GUI Cancelled; (Destination=18001111111) Reason: NO_USER_RESPONSE
WT-001-1 | 2018/02/01 01:50:34.334 UTC | Tue 2018/02/01 01:50:34 UTC | 310 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/02/01 01:50:44.274 UTC | Tue 2018/02/01 01:50:44 UTC | 373 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=918001111111)
WT-001-1 | 2018/02/01 01:50:44.780 UTC | Tue 2018/02/01 01:50:44 UTC | 369 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=918001111111); Reason=DESTINATION_OUT_OF_O
WT-001-1 | 2018/02/01 01:50:44.780 UTC | Tue 2018/02/01 01:50:44 UTC | 369 |                    |     |     |            |             |         |   | RDER
WT-001-1 | 2018/02/01 01:50:50.994 UTC | Tue 2018/02/01 01:50:50 UTC | 311 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2018/02/01 01:51:07.848 UTC | Tue 2018/02/01 01:51:07 UTC | 310 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/02/01 01:51:17.223 UTC | Tue 2018/02/01 01:51:17 UTC | 373 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=918001111111)
WT-001-1 | 2018/02/01 01:51:20.365 UTC | Tue 2018/02/01 01:51:20 UTC | 369 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=918001111111); Reason=DESTINATION_OUT_OF_O
WT-001-1 | 2018/02/01 01:51:20.365 UTC | Tue 2018/02/01 01:51:20 UTC | 369 |                    |     |     |            |             |         |   | RDER
WT-001-1 | 2018/02/01 01:52:06.542 UTC | Tue 2018/02/01 01:52:06 UTC | 311 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2018/02/01 01:52:48.298 UTC | Tue 2018/02/01 01:52:48 UTC | 310 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/02/01 01:56:21.610 UTC | Tue 2018/02/01 01:56:21 UTC | 311 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2018/02/01 01:57:06.861 UTC | Tue 2018/02/01 01:57:06 UTC | 310 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/02/01 01:57:15.734 UTC | Tue 2018/02/01 01:57:15 UTC | 373 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=918001111111)
WT-001-1 | 2018/02/01 01:57:16.080 UTC | Tue 2018/02/01 01:57:16 UTC | 369 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=918001111111); Reason=DESTINATION_OUT_OF_O
WT-001-1 | 2018/02/01 01:57:16.080 UTC | Tue 2018/02/01 01:57:16 UTC | 369 |                    |     |     |            |             |         |   | RDER
WT-001-1 | 2018/02/01 01:57:22.272 UTC | Tue 2018/02/01 01:57:22 UTC | 311 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2018/02/01 01:57:23.599 UTC | Tue 2018/02/01 01:57:23 UTC | 310 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/02/01 01:57:32.411 UTC | Tue 2018/02/01 01:57:32 UTC | 373 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Attended Transfer from Phone Dialed; (Destination=918001111111)
WT-001-1 | 2018/02/01 01:57:39.052 UTC | Tue 2018/02/01 01:57:39 UTC | 369 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | < | Attended Transfer from Phone Failed; (Destination=918001111111); Reason=DESTINATION_OUT_OF_O
WT-001-1 | 2018/02/01 01:57:39.052 UTC | Tue 2018/02/01 01:57:39 UTC | 369 |                    |     |     |            |             |         |   | RDER
WT-001-1 | 2018/02/01 01:57:43.314 UTC | Tue 2018/02/01 01:57:43 UTC | 311 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Off Hold
WT-001-1 | 2018/02/01 01:58:41.007 UTC | Tue 2018/02/01 01:58:41 UTC | 342 | 222222222222222222 | T17 | P09 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/02/01 01:58:41.913 UTC | Tue 2018/02/01 01:58:41 UTC | 307 | 222222222222222222 | T17 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-02-01 01:48:22.655");
    REQUIRE(record.answer_utc == "2018-02-01 01:48:25.292"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 9);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == false);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-02-01 01:58:41.913"s);
}

TEST_CASE("gui transfer cancelled") {
    auto val = parse(
R"(WT-001-1 | 2011/01/01 01:09:40.803 UTC | Tue 2011/01/01 01:09:40 UTC | 316 | 111111111111111111 | T01 | P00 |            | p3333333333 | ANI 01  | > | Ringing
WT-001-1 | 2011/01/01 01:09:40.831 UTC | Tue 2011/01/01 01:09:40 UTC | 222 | 111111111111111111 | T01 | P00 |            | p3333333333 | ALI 01A | < | Request Sent with RequestKey=00000000; Index=00; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2011/01/01 01:09:40.831 UTC | Tue 2011/01/01 01:09:40 UTC | 222 | 111111111111111111 | T01 | P00 |            | p3333333333 | ALI 01B | < | Request Sent with RequestKey=00000000; Index=00; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 | 111111111111111111 | T01 | P00 | 3333333333 | p3333333333 | ALI 01A | > | Record received (Data Follows)
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   | <STX>212<CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   | (333) 333-3333 BUSN 03/33 11:11<CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   | AAAAA AAAAA AAAAAA AAAAAAAAA<CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   |        000       P#333-333-3333<CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   |    0TH ST                <CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   |                       000 00000<CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   | AA AAAAAAAAAA                  <CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=QWSTC<CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   | PSAP=AAAA--AAAAAAAAAA<CR>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   | AAAAAAAAAA AA<CR><LF>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   | AAAA AAAA<CR><LF>
WT-001-1 | 2011/01/01 01:09:44.043 UTC | Tue 2011/01/01 01:09:44 UTC | 226 |                    |     |     |            |             |         |   | AAAA AAA                      <ETX>
WT-001-1 | 2011/01/01 01:09:44.400 UTC | Tue 2011/01/01 01:09:44 UTC | 306 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | > | Connect
WT-001-1 | 2011/01/01 01:09:44.413 UTC | Tue 2011/01/01 01:09:44 UTC | 402 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2011/01/01 01:09:44.783 UTC | Tue 2011/01/01 01:09:44 UTC | 404 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | CAD 01  | > | ACK Received
WT-001-1 | 2011/01/01 01:09:48.101 UTC | Tue 2011/01/01 01:09:48 UTC | 309 | 111111111111111111 | T01 | P02 | 3333333333 | p3333333333 | ANI 01  | > | Conference Join
WT-001-1 | 2011/01/01 01:09:54.450 UTC | Tue 2011/01/01 01:09:54 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=*4444444444)
WT-001-1 | 2011/01/01 01:10:13.168 UTC | Tue 2011/01/01 01:10:13 UTC | 342 | 111111111111111111 | T01 | P02 | 3333333333 | p3333333333 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 01:10:25.559 UTC | Tue 2011/01/01 01:10:25 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=*4444444444)
WT-001-1 | 2011/01/01 01:10:29.893 UTC | Tue 2011/01/01 01:10:29 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=*5555555555)
WT-001-1 | 2011/01/01 01:10:42.949 UTC | Tue 2011/01/01 01:10:42 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=*5555555555)
WT-001-1 | 2011/01/01 01:10:45.498 UTC | Tue 2011/01/01 01:10:45 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=**111)
WT-001-1 | 2011/01/01 01:10:51.719 UTC | Tue 2011/01/01 01:10:51 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=**111)
WT-001-1 | 2011/01/01 01:10:53.912 UTC | Tue 2011/01/01 01:10:53 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=**222)
WT-001-1 | 2011/01/01 01:10:59.215 UTC | Tue 2011/01/01 01:10:59 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=**222)
WT-001-1 | 2011/01/01 01:11:01.510 UTC | Tue 2011/01/01 01:11:01 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=**333)
WT-001-1 | 2011/01/01 01:11:07.203 UTC | Tue 2011/01/01 01:11:07 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=**333)
WT-001-1 | 2011/01/01 01:11:09.850 UTC | Tue 2011/01/01 01:11:09 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=**444)
WT-001-1 | 2011/01/01 01:11:32.447 UTC | Tue 2011/01/01 01:11:32 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=**444)
WT-001-1 | 2011/01/01 01:11:34.895 UTC | Tue 2011/01/01 01:11:34 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=*555)
WT-001-1 | 2011/01/01 01:11:55.855 UTC | Tue 2011/01/01 01:11:55 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=*555)
WT-001-1 | 2011/01/01 01:11:58.761 UTC | Tue 2011/01/01 01:11:58 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=**666)
WT-001-1 | 2011/01/01 01:12:04.934 UTC | Tue 2011/01/01 01:12:04 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=**666)
WT-001-1 | 2011/01/01 01:12:07.433 UTC | Tue 2011/01/01 01:12:07 UTC | 365 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI; (Destination=*6666666666)
WT-001-1 | 2011/01/01 01:12:13.450 UTC | Tue 2011/01/01 01:12:13 UTC | 367 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | < | Transfer from GUI Cancelled; (Destination=*6666666666)
WT-001-1 | 2011/01/01 01:12:18.959 UTC | Tue 2011/01/01 01:12:18 UTC | 342 | 111111111111111111 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | > | Conference Leave
WT-001-1 | 2011/01/01 01:12:21.001 UTC | Tue 2011/01/01 01:12:21 UTC | 307 | 111111111111111111 | T01 | P00 | 3333333333 | p3333333333 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2011-01-01 01:09:40.803");
    REQUIRE(record.answer_utc == "2011-01-01 01:09:44.400"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == true);
    REQUIRE(record.cos == "BUSN"s);
    REQUIRE(record.trunk == 1);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2011-01-01 01:12:21.001"s);
}

TEST_CASE("383 Conference Transfer From GUI Failed") {
    auto val = parse(
R"(WT-001-1 | 2013/12/21 11:55:02.074 UTC | Sun 2013/12/21 14:55:02 UTC | 316 | 777777777777777777 |     | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=4444444444@invalid.unknown.domain)
WT-001-1 | 2013/12/21 11:55:03.863 UTC | Sun 2013/12/21 14:55:03 UTC | 306 | 777777777777777777 |     | P05 | VoIP CALL  |             | ANI 01  | > | Connect (From=4444444444@invalid.unknown.domain)
WT-001-1 | 2013/12/21 11:56:16.416 UTC | Sun 2013/12/21 14:56:16 UTC | 365 | 777777777777777777 |     | P05 | VoIP CALL  |             | ANI     | < | Conference Transfer from GUI; (Destination=11111111)
WT-001-1 | 2013/12/21 12:02:13.761 UTC | Sun 2013/12/21 15:02:13 UTC | 383 | 777777777777777777 |     | P05 | VoIP CALL  |             | ANI 01  | > | Conference Transfer From GUI Failed; (Destination=11111111); Reason=NORMAL_CLEARING
WT-001-1 | 2013/12/21 12:04:06.144 UTC | Sun 2013/12/21 15:04:06 UTC | 342 | 777777777777777777 |     | P05 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2013/12/21 12:04:06.148 UTC | Sun 2013/12/21 15:04:06 UTC | 307 | 777777777777777777 |     | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "777777777777777777");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2013-12-21 11:55:02.074");
    REQUIRE(record.answer_utc == "2013-12-21 11:55:03.863"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 5);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911 == false);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2013-12-21 12:04:06.148"s);
}
