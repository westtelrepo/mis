#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("process_call parse") {
    auto val = parse("");
}

TEST_CASE("process_call basic") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T03 | P00 |            | p9999999999 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:02:00.000 UTC | Tue 2018/01/01 01:02:00 UTC | 306 | 111111111111111111 | T03 | P07 |            | p9999999999 | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 01:03:00.000 UTC | Tue 2018/01/01 01:03:00 UTC | 342 | 111111111111111111 | T03 | P07 | 9999999999 | p9999999999 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:04:00.000 UTC | Tue 2018/01/01 01:04:00 UTC | 307 | 111111111111111111 | T03 | P00 | 9999999999 | p9999999999 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-01-01 01:00:00.000");
    REQUIRE(record.answer_utc.value() == "2018-01-01 01:02:00.000");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos.value() == 7);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk.value() == 3);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 01:04:00.000"s);
}

TEST_CASE("process_call abandon") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 20:00:00.000 UTC | Tue 2018/01/01 20:00:00 UTC | 316 | 111111111111111111 |     | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=1111@example.com)
WT-001-1 | 2018/01/01 20:00:00.000 UTC | Tue 2018/01/01 20:00:00 UTC | 312 | 111111111111111111 |     | P00 | VoIP CALL  |             | ANI 01  | > | Abandoned
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-01-01 20:00:00.000");
    REQUIRE(!record.answer_utc);
    REQUIRE(record.abandoned == true);
    REQUIRE(!record.pos);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}

TEST_CASE("process_call basic with ali") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:00:00.000 UTC | Tue 2018/01/01 01:00:00 UTC | 316 | 111111111111111111 | T03 | P00 |            | p9999999999 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:00:00.000 UTC | Mon 2018/01/01 01:00:00 UTC | 226 | 111111111111111111 | T01 | P00 |            | p9999999999 | ALI 01A | > | Initial Record received (Data Follows)
WT-001-1 | 2018/01/01 01:00:00.000 UTC | Mon 2018/01/01 01:00:00 UTC | 226 |                    |     |     |            |             |         |   | <STX>912<CR>
WT-001-1 | 2018/01/01 01:00:00.000 UTC | Mon 2018/01/01 01:00:00 UTC | 226 |                    |     |     |            |             |         |   | 0009110013 NO RECORD FOUND<ETX>
WT-001-1 | 2018/01/01 01:02:00.000 UTC | Tue 2018/01/01 01:02:00 UTC | 306 | 111111111111111111 | T03 | P07 |            | p9999999999 | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 01:03:00.000 UTC | Tue 2018/01/01 01:03:00 UTC | 342 | 111111111111111111 | T03 | P07 | 9999999999 | p9999999999 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:04:00.000 UTC | Tue 2018/01/01 01:04:00 UTC | 307 | 111111111111111111 | T03 | P00 | 9999999999 | p9999999999 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-01-01 01:00:00.000");
    REQUIRE(record.answer_utc.value() == "2018-01-01 01:02:00.000");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos.value() == 7);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos.value() == "NRF");
    REQUIRE(record.trunk.value() == 3);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 01:04:00.000"s);
}

TEST_CASE("process_call forced disconnect") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:34:14.180 UTC | Mon 2018/01/01 01:34:14 UTC | 316 | 222222222222222222 | T17 | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=7777777777@invalid.unknown.domain)
WT-001-1 | 2018/01/01 01:34:16.454 UTC | Mon 2018/01/01 01:34:16 UTC | 306 | 222222222222222222 | T17 | P10 | VoIP CALL  |             | ANI 01  | > | Connect (From=7777777777@invalid.unknown.domain)
WT-001-1 | 2018/01/01 01:34:20.940 UTC | Mon 2018/01/01 01:34:20 UTC | 306 | 222222222222222222 | T17 | P05 | VoIP CALL  |             | ANI 01  | > | Connect (From=7777777777@invalid.unknown.domain)
WT-001-1 | 2018/01/01 01:34:21.000 UTC | Mon 2018/01/01 01:34:21 UTC | 309 | 222222222222222222 | T17 | P05 | VoIP CALL  |             | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 01:34:31.002 UTC | Mon 2018/01/01 01:34:31 UTC | 342 | 222222222222222222 | T17 | P05 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:34:31.523 UTC | Mon 2018/01/01 01:34:31 UTC | 342 | 222222222222222222 | T17 | P10 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:34:31.528 UTC | Mon 2018/01/01 01:34:31 UTC | 342 | 222222222222222222 | T17 | P00 | VoIP CALL  |             | ANI 01  | > | Conference Leave exten: 7777777777
WT-001-1 | 2018/01/02 01:34:14.288 UTC | Tue 2018/01/02 01:34:14 UTC | 146 | 222222222222222222 | T17 | P05 | VoIP CALL  |             | KRN     | - | [WARNING] Forced Disconnect Sent
WT-001-1 | 2018/01/02 01:34:14.289 UTC | Tue 2018/01/02 01:34:14 UTC | 307 | 222222222222222222 | T17 | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");


    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-01-01 01:34:14.180");
    REQUIRE(record.answer_utc == "2018-01-01 01:34:16.454"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 10);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 17);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(!record.disconnect_utc);
}
