#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("365 process call blind from gui, intra-PSAP") {
    auto val = parse(
R"(WT-002-1 | 2011/01/11 01:00:08.626 UTC | Thu 2011/01/11 01:00:08 UTC | 316 | 111111111111111111 | T03 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-002-1 | 2011/01/11 01:00:08.647 UTC | Thu 2011/01/11 01:00:08 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-002-1 | 2011/01/11 01:00:08.647 UTC | Thu 2011/01/11 01:00:08 UTC | 222 | 111111111111111111 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-002-1 | 2011/01/11 01:00:10.028 UTC | Thu 2011/01/11 01:00:10 UTC | 306 | 111111111111111111 | T03 | P03 |            | p1111111111 | ANI 01  | > | Connect
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 | 111111111111111111 | T03 | P03 |            | p1111111111 | ALI 01A | > | Initial Record received (Data Follows)
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | <STX>219<CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 WRLS 01/11 11:11<CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | VERIZON WIRELESS            <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |       1111       P#111-111-1111<CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |    HORIZON PKY - NW SECTOR
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |               <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |                              CAL
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | LBK=111-111-1111       1111 <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | ST WHATS CITY                  <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=VZW  <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |  +011.11111  -111.1111111%1111<CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | PSAP=    --<CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |  WIRELESS<CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS<CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |    <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-002-1 | 2011/01/11 01:00:12.986 UTC | Thu 2011/01/11 01:00:12 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-002-1 | 2011/01/11 01:00:20.264 UTC | Thu 2011/01/11 01:00:20 UTC | 365 | 111111111111111111 | T03 | P03 | 1111111111 | p1111111111 | ANI     | < | Blind Transfer from GUI; (Destination=9511388)
WT-002-1 | 2011/01/11 01:00:20.390 UTC | Thu 2011/01/11 01:00:20 UTC | 378 | 111111111111111111 | T03 | P03 | 1111111111 | p1111111111 | ANI 01  | < | Blind Transfer from GUI Connected; (Destination=9511388)
WT-002-1 | 2011/01/11 01:00:20.645 UTC | Thu 2011/01/11 01:00:20 UTC | 342 | 111111111111111111 | T03 | P03 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-002-1 | 2011/01/11 01:00:33.033 UTC | Thu 2011/01/11 01:00:33 UTC | 271 | 111111111111111111 | T03 | P03 | 1111111111 | p1111111111 | ALI 01A | < | Auto Request Sent with RequestKey=1111111111; Index=11; Trunk=11 (to DB=01 PP=01)
WT-002-1 | 2011/01/11 01:00:33.033 UTC | Thu 2011/01/11 01:00:33 UTC | 271 | 111111111111111111 | T03 | P03 | 1111111111 | p1111111111 | ALI 01B | < | Auto Request Sent with RequestKey=1111111111; Index=11; Trunk=11 (to DB=01 PP=01)
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 | 111111111111111111 | T03 | P03 | 1111111111 | p1111111111 | ALI 01A | > | Auto Rebid Record received (Data Follows)
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | <STX>220<CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | (111) 111-1111 WRLS 01/11 11:12<CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | VERIZON WIRELESS            <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |       1111       P#111-111-1111<CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |    HORIZON PKY - NW SECTOR
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |               <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |                              CAL
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | LBK=303-229-3816       1749 <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | ST WHATS CITY                  <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |                   TEL=VZW  <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |  +011.11111  -111.1111111%1111<CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | PSAP=    --<CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |  WIRELESS<CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | WIRELESS<CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | WIRELESS
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |    <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-002-1 | 2011/01/11 01:00:37.533 UTC | Thu 2011/01/11 01:00:37 UTC | 268 |                    |     |     |            |             |         |   | <ETX>
WT-002-1 | 2011/01/11 01:00:41.980 UTC | Thu 2011/01/11 01:00:41 UTC | 342 | 111111111111111111 | T03 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave exten: 1111111
WT-002-1 | 2011/01/11 01:00:47.134 UTC | Thu 2011/01/11 01:00:47 UTC | 307 | 111111111111111111 | T03 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-002-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-002-1");
    REQUIRE(record.utc == "2011-01-11 01:00:08.626");
    REQUIRE(record.answer_utc.value() == "2011-01-11 01:00:10.028");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 3);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos.value() == "WRLS");
    REQUIRE(record.trunk.value() == 3);
    REQUIRE(record.transfer_reaction == transfer_type::blind);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2011-01-11 01:00:47.134"s);
}

TEST_CASE("process call tandem from gui") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:02:53.398 UTC | Mon 2018/01/01 01:02:53 UTC | 316 | 111111111111111111 | T02 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:02:53.417 UTC | Mon 2018/01/01 01:02:53 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=02 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:02:53.417 UTC | Mon 2018/01/01 01:02:53 UTC | 222 | 111111111111111111 | T02 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=02 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 | 111111111111111111 | T02 | P00 | 1111111111 | p1111111111 | ALI 01A | > | Initial Record received (Data Follows)
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | <STX>235<CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 WPH1 01/01 00:11<CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | VERIZON WRLS 800-111-1111   <CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   |                  P#111-111-1111<CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   |    WHATS WHATS MTNN 11111<CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | 1 HWY 1/HWY 111- NW <CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | 1111111111            111 00000<CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | CO GOLDEN                      <CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   |                   TEL=VZW  <CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | +11.111111  -111.111111   1111 <CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | PSAP=AAAA--WHATSN WIRELESS<CR>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-001-1 | 2018/01/01 01:02:59.788 UTC | Mon 2018/01/01 01:02:59 UTC | 226 |                    |     |     |            |             |         |   | VERIFY AMB                   <ETX>
WT-001-1 | 2018/01/01 01:03:00.630 UTC | Mon 2018/01/01 01:03:00 UTC | 306 | 111111111111111111 | T02 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 01:03:19.859 UTC | Mon 2018/01/01 01:03:19 UTC | 271 | 111111111111111111 | T02 | P01 | 1111111111 | p1111111111 | ALI 01A | < | Auto Request Sent with RequestKey=05112107; Index=36; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:03:19.859 UTC | Mon 2018/01/01 01:03:19 UTC | 271 | 111111111111111111 | T02 | P01 | 1111111111 | p1111111111 | ALI 01B | < | Auto Request Sent with RequestKey=05112107; Index=36; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 | 111111111111111111 | T02 | P01 | 1111111111 | p1111111111 | ALI 01A | > | Auto Rebid Record received (Data Follows)
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | <STX>236<CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | (111) 111-1111 WPH2 01/01 00:23<CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | VERIZON WRLS 800-111-1111   <CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   |                  P#111-111-1111<CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   |    WHATS WHATS MTNN 11111<CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | 1 HWY 1/HWY 111- NW <CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | 1111111111            111 00000<CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | CO GOLDEN                      <CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   |                   TEL=VZW  <CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | +11.111111  -111.111111    182 <CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | PSAP=AAAA--WHATSN WIRELESS<CR>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | VERIFY PD<CR><LF>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | VERIFY FD<CR><LF>
WT-001-1 | 2018/01/01 01:03:26.734 UTC | Mon 2018/01/01 01:03:26 UTC | 268 |                    |     |     |            |             |         |   | VERIFY AMB                   <ETX>
WT-001-1 | 2018/01/01 01:03:54.907 UTC | Mon 2018/01/01 01:03:54 UTC | 365 | 111111111111111111 | T02 | P01 | 1111111111 | p1111111111 | ANI     | < | Tandem Transfer from GUI; (Destination=2222222222)
WT-001-1 | 2018/01/01 01:03:58.707 UTC | Mon 2018/01/01 01:03:58 UTC | 342 | 111111111111111111 | T02 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:03:58.724 UTC | Mon 2018/01/01 01:03:58 UTC | 307 | 111111111111111111 | T02 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    auto record = val.at(0);

    REQUIRE(record.callid == "111111111111111111");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-01-01 01:02:53.398");
    REQUIRE(record.answer_utc.value() == "2018-01-01 01:03:00.630");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos.value() == "WPH2");
    REQUIRE(record.trunk.value() == 2);
    REQUIRE(record.transfer_reaction == transfer_type::tandem);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 01:03:58.724"s);
}

TEST_CASE("process_call conference transfer from gui") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 11:56:17.426 UTC | Sun 2018/01/01 17:56:17 UTC | 316 | 222222222222222222 | T01 | P00 |            | p3333333333 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 11:56:17.462 UTC | Sun 2018/01/01 17:56:17 UTC | 222 | 222222222222222222 | T01 | P00 |            | p3333333333 | ALI 01A | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 11:56:17.462 UTC | Sun 2018/01/01 17:56:17 UTC | 222 | 222222222222222222 | T01 | P00 |            | p3333333333 | ALI 01B | < | Request Sent with RequestKey=11111111; Index=11; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 11:56:18.935 UTC | Sun 2018/01/01 17:56:18 UTC | 306 | 222222222222222222 | T01 | P01 |            | p3333333333 | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 | 222222222222222222 | T01 | P01 | 3333333333 | p3333333333 | ALI 01A | > | Initial Record received (Data Follows)
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   | <STX>218<CR>
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111   11:11    11/11<CR>
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   |    11111        TWP RD 11 - E SE
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   | CTOR)" "                            " R"(
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   | DUNKIRK           AA 11  WRLS<CR>
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   | VERIZON
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   | <CR>
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   |                   567-511-0005<CR>
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   |                         CP:   <CR>
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   | VERIFY PD              VERIFY FD
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   |               VERIFY EMS
WT-001-1 | 2018/01/01 11:56:21.597 UTC | Sun 2018/01/01 17:56:21 UTC | 226 |                    |     |     |            |             |         |   |       <ETX>
WT-001-1 | 2018/01/01 11:56:34.141 UTC | Sun 2018/01/01 17:56:34 UTC | 270 | 222222222222222222 | T01 | P01 | 3333333333 | p3333333333 | ALI 01A | < | Rebid Request Sent with RequestKey=11111111; Index=11; Trunk=11 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 11:56:34.141 UTC | Sun 2018/01/01 17:56:34 UTC | 270 | 222222222222222222 | T01 | P01 | 3333333333 | p3333333333 | ALI 01B | < | Rebid Request Sent with RequestKey=11111111; Index=11; Trunk=11 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 | 222222222222222222 | T01 | P01 | 3333333333 | p3333333333 | ALI 01A | > | Rebid Record received (Data Follows)
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   | <STX>219<CR>
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   | (111) 111-1111   11:11    11/11<CR>
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   |    11111        TWP RD 11 - E SE
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   | CTOR)" "                            " R"(
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   | DUNKIRK           AA 11  WPH2<CR>
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   | VERIZON
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   | <CR>
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   |                   111-111-1111<CR>
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   |                     <CR>
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   | +11.111111  -111.111111 AA:11.<CR>
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   | VERIFY PD              VERIFY FD
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   |               VERIFY EMS
WT-001-1 | 2018/01/01 11:56:35.170 UTC | Sun 2018/01/01 17:56:35 UTC | 266 |                    |     |     |            |             |         |   |       <ETX>
WT-001-1 | 2018/01/01 11:58:59.018 UTC | Sun 2018/01/01 17:58:59 UTC | 365 | 222222222222222222 | T01 | P01 | 3333333333 | p3333333333 | ANI     | < | Conference Transfer from GUI; (Destination=11111111111)
WT-001-1 | 2018/01/01 12:05:23.001 UTC | Sun 2018/01/01 18:05:23 UTC | 342 | 222222222222222222 | T01 | P01 | 3333333333 | p3333333333 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 12:05:23.022 UTC | Sun 2018/01/01 18:05:23 UTC | 307 | 222222222222222222 | T01 | P00 | 3333333333 | p3333333333 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-001-1");
    REQUIRE(record.utc == "2018-01-01 11:56:17.426");
    REQUIRE(record.answer_utc.value() == "2018-01-01 11:56:18.935");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos.value() == "WPH2");
    REQUIRE(record.trunk.value() == 1);
    REQUIRE(record.transfer_reaction == transfer_type::conference);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 12:05:23.022"s);
}

TEST_CASE("process_call ng911 transfer from gui, intra-PSAP") {
    auto val = parse(
R"(WT-004-1 | 2018/01/01 00:16:03.979 UTC | Thu 2018/01/01 00:16:03 UTC | 316 | 333333333333333333 | T07 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-004-1 | 2018/01/01 00:16:04.001 UTC | Thu 2018/01/01 00:16:04 UTC | 222 | 333333333333333333 | T07 | P00 |            | p1111111111 | ALI 02A | < | Request Sent with RequestKey=1111111111; Index=11; Trunk=07 (to DB=01 PP=01)
WT-004-1 | 2018/01/01 00:16:04.001 UTC | Thu 2018/01/01 00:16:04 UTC | 222 | 333333333333333333 | T07 | P00 |            | p1111111111 | ALI 02B | < | Request Sent with RequestKey=1111111111; Index=11; Trunk=07 (to DB=01 PP=01)
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 | 333333333333333333 | T07 | P00 | 1111111111 | p1111111111 | ALI 02A | > | Initial Record received (Data Follows)
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | <STX>256<CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 RESD 01/11 11:11<CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | ROAD, WHATSSS A             <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |      111     <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |    WWWWWWW RD                  <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | WWWWWWWWW TWP                  <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | AA              P#(111)111-1111<CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | CO=FTR           ESN=0000<CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | WCSO               <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | WHATSS FIRE        <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | WHATSS EMS
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |          <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |                                <ETX>
WT-004-1 | 2018/01/01 00:16:10.249 UTC | Thu 2018/01/01 00:16:10 UTC | 306 | 333333333333333333 | T07 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Connect
WT-004-1 | 2018/01/01 00:16:31.390 UTC | Thu 2018/01/01 00:16:31 UTC | 365 | 333333333333333333 | T07 | P02 | 1111111111 | p1111111111 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@fire.ng911.example)
WT-004-1 | 2018/01/01 00:16:42.442 UTC | Thu 2018/01/01 00:16:42 UTC | 395 | 333333333333333333 | T07 | P07 | 1111111111 | p1111111111 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@fire.ng911.example)
WT-004-1 | 2018/01/01 00:16:55.100 UTC | Thu 2018/01/01 00:16:55 UTC | 342 | 333333333333333333 | T07 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-004-1 | 2018/01/01 00:17:22.773 UTC | Thu 2018/01/01 00:17:22 UTC | 342 | 333333333333333333 | T07 | P07 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-004-1 | 2018/01/01 00:17:22.793 UTC | Thu 2018/01/01 00:17:22 UTC | 307 | 333333333333333333 | T07 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-004-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap.value() == "WT-004-1");
    REQUIRE(record.utc == "2018-01-01 00:16:03.979");
    REQUIRE(record.answer_utc.value() == "2018-01-01 00:16:10.249");
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos.value() == "RESD");
    REQUIRE(record.trunk.value() == 7);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 00:17:22.793"s);
}

TEST_CASE("process_call ng911 transfer from gui inter-PSAP") {
    auto config = parse_config::parse(R"(
- controller: WT-004-1
  psaps:
    - id: WT-004-A
      positions: [2]
    - id: WT-004-B
      positions: [7]
)");

    auto val = parse(
R"(WT-004-1 | 2018/01/01 00:16:03.979 UTC | Thu 2018/01/01 00:16:03 UTC | 316 | 333333333333333333 | T07 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-004-1 | 2018/01/01 00:16:04.001 UTC | Thu 2018/01/01 00:16:04 UTC | 222 | 333333333333333333 | T07 | P00 |            | p1111111111 | ALI 02A | < | Request Sent with RequestKey=1111111111; Index=11; Trunk=07 (to DB=01 PP=01)
WT-004-1 | 2018/01/01 00:16:04.001 UTC | Thu 2018/01/01 00:16:04 UTC | 222 | 333333333333333333 | T07 | P00 |            | p1111111111 | ALI 02B | < | Request Sent with RequestKey=1111111111; Index=11; Trunk=07 (to DB=01 PP=01)
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 | 333333333333333333 | T07 | P00 | 1111111111 | p1111111111 | ALI 02A | > | Initial Record received (Data Follows)
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | <STX>256<CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | (111) 111-1111 RESD 01/11 11:11<CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | ROAD, WHATSSS A             <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |      111     <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |    WWWWWWW RD                  <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | WWWWWWWWW TWP                  <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | AA              P#(111)111-1111<CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | CO=FTR           ESN=0000<CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | WCSO               <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | WHATSS FIRE        <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   | WHATSS EMS
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |          <CR>
WT-004-1 | 2018/01/01 00:16:07.748 UTC | Thu 2018/01/01 00:16:07 UTC | 226 |                    |     |     |            |             |         |   |                                <ETX>
WT-004-1 | 2018/01/01 00:16:10.249 UTC | Thu 2018/01/01 00:16:10 UTC | 306 | 333333333333333333 | T07 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Connect
WT-004-1 | 2018/01/01 00:16:31.390 UTC | Thu 2018/01/01 00:16:31 UTC | 365 | 333333333333333333 | T07 | P02 | 1111111111 | p1111111111 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@fire.ng911.example)
WT-004-1 | 2018/01/01 00:16:42.442 UTC | Thu 2018/01/01 00:16:42 UTC | 395 | 333333333333333333 | T07 | P07 | 1111111111 | p1111111111 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@fire.ng911.example)
WT-004-1 | 2018/01/01 00:16:55.100 UTC | Thu 2018/01/01 00:16:55 UTC | 342 | 333333333333333333 | T07 | P02 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-004-1 | 2018/01/01 00:17:22.773 UTC | Thu 2018/01/01 00:17:22 UTC | 342 | 333333333333333333 | T07 | P07 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-004-1 | 2018/01/01 00:17:22.793 UTC | Thu 2018/01/01 00:17:22 UTC | 307 | 333333333333333333 | T07 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);

    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-004-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-004-A"s);
    REQUIRE(record.utc == "2018-01-01 00:16:03.979");
    REQUIRE(record.answer_utc == "2018-01-01 00:16:10.249"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos == "RESD"s);
    REQUIRE(record.trunk == 7);
    REQUIRE(record.transfer_reaction == transfer_type::ng911);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 00:17:22.793"s);

    auto rec2 = val.at(1);

    REQUIRE(rec2.callid == "333333333333333333");
    REQUIRE(rec2.controller == "WT-004-1");
    REQUIRE(rec2.subid == 1);
    REQUIRE(rec2.psap == "WT-004-B"s);
    REQUIRE(rec2.utc == "2018-01-01 00:16:31.390");
    REQUIRE(rec2.answer_utc == "2018-01-01 00:16:42.442"s);
    REQUIRE(rec2.abandoned == false);
    REQUIRE(rec2.pos == 7);
    REQUIRE(!rec2.calltaker);
    REQUIRE(rec2.is_911);
    REQUIRE(rec2.cos == "RESD"s);
    REQUIRE(rec2.trunk == 7);
    REQUIRE(rec2.transfer_reaction == transfer_type::none);
    REQUIRE(rec2.incoming == true);
    REQUIRE(rec2.t_type == transfer_type::ng911);
    REQUIRE(!rec2.disconnect_utc);
}

TEST_CASE("308 Transfer from GUI Connected") {
    auto val = parse(
R"(WT-001-1 | 2015/01/01 11:47:40.525 UTC | Fri 2015/01/01 11:47:40 UTC | 316 | 333333333333333333 | T05 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2015/01/01 11:47:40.533 UTC | Fri 2015/01/01 11:47:40 UTC | 222 | 333333333333333333 | T05 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=00; Trunk=05 (to DB=01 PP=01)
WT-001-1 | 2015/01/01 11:47:40.533 UTC | Fri 2015/01/01 11:47:40 UTC | 222 | 333333333333333333 | T05 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=00; Trunk=05 (to DB=01 PP=01)
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 | 333333333333333333 | T05 | P00 | 9999999999 | p1111111111 | ALI 01B | > | Record received (Data Follows)
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | <STX>269<CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | 222-222-2222  22:22:22 00000000<CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | VERIZON WIRELESS               <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | 00000                      WPH1<CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | wwwww www 000                  <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   |                     www www  <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | wwwwwwww                     ww<CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | wwww 000-0 ww wwwwww          <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   |                       ESN  0000<CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | P#111-111-1111 ALT#111-111-1111<CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | X-011.111111 Y+011.111111 ww000<CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | UNC00000   w       wwww    <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2015/01/01 11:47:44.444 UTC | Fri 2015/01/01 11:47:44 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2015/01/01 11:47:47.167 UTC | Fri 2015/01/01 11:47:47 UTC | 306 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2015/01/01 11:47:47.260 UTC | Fri 2015/01/01 11:47:47 UTC | 402 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2015/01/01 11:47:47.424 UTC | Fri 2015/01/01 11:47:47 UTC | 404 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | CAD 01  | > | ACK Received
WT-001-1 | 2015/01/01 11:48:44.462 UTC | Fri 2015/01/01 11:48:44 UTC | 222 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=00; Trunk=00 (to DB=01 PP=01)
WT-001-1 | 2015/01/01 11:48:44.463 UTC | Fri 2015/01/01 11:48:44 UTC | 222 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=00; Trunk=00 (to DB=01 PP=01)
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | ALI 01B | > | Record received (Data Follows)
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | <STX>271<CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | 111-111-1111  11:11:11 00000000<CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | VERIZON WIRELESS               <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | 000                        WPH1<CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | wwwwwwww                       <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   |           ww        LEC VZW  <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | wwwwwwww                     ww<CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | wwww 111-1 ww wwwwww          <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   |                       ESN  0000<CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | P#111-111-1111 ALT#111-111-1111<CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | X-011.111111 Y+011.111111 ww000<CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | UNC00000   w       wwww    <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | WIRELESS 911 CALLER            <CR>
WT-001-1 | 2015/01/01 11:48:48.953 UTC | Fri 2015/01/01 11:48:48 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2015/01/01 11:48:49.004 UTC | Fri 2015/01/01 11:48:49 UTC | 402 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2015/01/01 11:48:49.226 UTC | Fri 2015/01/01 11:48:49 UTC | 404 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | CAD 01  | > | ACK Received
WT-001-1 | 2015/01/01 11:49:51.319 UTC | Fri 2015/01/01 11:49:51 UTC | 365 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | ANI 01  | < | Transfer from GUI; (Destination=3333333333)
WT-001-1 | 2015/01/01 11:49:59.931 UTC | Fri 2015/01/01 11:49:59 UTC | 308 | 333333333333333333 | T05 | P00 | 9999999999 | p1111111111 | ANI 01  | > | Transfer from GUI Connected; (Destination=3333333333)
WT-001-1 | 2015/01/01 11:49:59.939 UTC | Fri 2015/01/01 11:49:59 UTC | 402 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2015/01/01 11:50:00.026 UTC | Fri 2015/01/01 11:50:00 UTC | 404 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | CAD 01  | > | ACK Received
WT-001-1 | 2015/01/01 11:50:25.567 UTC | Fri 2015/01/01 11:50:25 UTC | 342 | 333333333333333333 | T05 | P02 | 9999999999 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2015/01/01 11:51:19.553 UTC | Fri 2015/01/01 11:51:19 UTC | 342 | 333333333333333333 | T05 | P00 | 9999999999 | p1111111111 | ANI 01  | > | Conference Leave exten: 333-333-3333
WT-001-1 | 2015/01/01 11:51:19.640 UTC | Fri 2015/01/01 11:51:19 UTC | 307 | 333333333333333333 | T05 | P00 | 9999999999 | p1111111111 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "333333333333333333");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2015-01-01 11:47:40.525");
    REQUIRE(record.answer_utc == "2015-01-01 11:47:47.167"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 2);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(record.cos == "WPH1"s);
    REQUIRE(record.trunk == 5);
    REQUIRE(record.transfer_reaction == transfer_type::unknown);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2015-01-01 11:51:19.640"s);
}

TEST_CASE("381 INFO, also conference transfer intra-PSAP") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:22:48.893 UTC | Mon 2018/01/01 01:22:48 UTC | 316 | 888888888888888888 | T04 | P00 |            | p9110000000 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:22:48.916 UTC | Mon 2018/01/01 01:22:48 UTC | 225 | 888888888888888888 | T04 | P00 |            | p9110000000 | ALI     | - | [WARNING] No ALI Database can be Bid for RequestKey=9110000000
WT-001-1 | 2018/01/01 01:22:50.251 UTC | Mon 2018/01/01 01:22:50 UTC | 306 | 888888888888888888 | T04 | P01 |            | p9110000000 | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 01:23:01.237 UTC | Mon 2018/01/01 01:23:01 UTC | 365 | 888888888888888888 | T04 | P01 |            | p9110000000 | ANI     | < | Conference Transfer from GUI; (Destination=6222)
WT-001-1 | 2018/01/01 01:23:03.254 UTC | Mon 2018/01/01 01:23:03 UTC | 395 | 888888888888888888 | T04 | P02 |            | p9110000000 | ANI 01  | < | Conference Transfer from GUI Connected; (Destination=6222)
WT-001-1 | 2018/01/01 01:23:03.310 UTC | Mon 2018/01/01 01:23:03 UTC | 309 | 888888888888888888 | T04 | P00 |            | p9110000000 | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 01:23:14.701 UTC | Mon 2018/01/01 01:23:14 UTC | 381 | 888888888888888888 | T04 | P01 |            | p9110000000 | ANI 01  | - | [INFO] Cannot Blind Transfer From a Conference!
WT-001-1 | 2018/01/01 01:23:19.731 UTC | Mon 2018/01/01 01:23:19 UTC | 342 | 888888888888888888 | T04 | P02 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:23:20.370 UTC | Mon 2018/01/01 01:23:20 UTC | 342 | 888888888888888888 | T04 | P01 |            | p9110000000 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:23:20.384 UTC | Mon 2018/01/01 01:23:20 UTC | 307 | 888888888888888888 | T04 | P00 |            | p9110000000 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "888888888888888888");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-01-01 01:22:48.893");
    REQUIRE(record.answer_utc == "2018-01-01 01:22:50.251"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 4);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 01:23:20.384"s);
}

TEST_CASE("blind transfer from gui 365 -> 378") {
    auto val = parse(
R"(WT-001-1 | 2018/08/01 01:01:23.155 UTC | Mon 2018/08/01 01:01:23 UTC | 316 | 222222222222222222 | T33 | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=+11111111111@10.10.10.10)
WT-001-1 | 2018/08/01 01:01:29.793 UTC | Mon 2018/08/01 01:01:29 UTC | 306 | 222222222222222222 | T33 | P01 | VoIP CALL  |             | ANI 01  | > | Connect (From=+11111111111@10.10.10.10)
WT-001-1 | 2018/08/01 01:02:54.578 UTC | Mon 2018/08/01 01:02:54 UTC | 365 | 222222222222222222 | T33 | P01 | VoIP CALL  |             | ANI     | < | Blind Transfer from GUI; (Destination=3333333333)
WT-001-1 | 2018/08/01 01:02:55.225 UTC | Mon 2018/08/01 01:02:55 UTC | 342 | 222222222222222222 | T33 | P01 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/08/01 01:02:56.700 UTC | Mon 2018/08/01 01:02:56 UTC | 378 | 222222222222222222 | T33 | P01 | VoIP CALL  |             | ANI 01  | < | Blind Transfer from GUI Connected; (Destination=3333333333)
WT-001-1 | 2018/08/01 01:17:59.674 UTC | Mon 2018/08/01 01:17:59 UTC | 342 | 222222222222222222 | T33 | P00 | VoIP CALL  |             | ANI 01  | > | Conference Leave exten: 2222222222
WT-001-1 | 2018/08/01 01:17:59.698 UTC | Mon 2018/08/01 01:17:59 UTC | 307 | 222222222222222222 | T33 | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-08-01 01:01:23.155");
    REQUIRE(record.answer_utc == "2018-08-01 01:01:29.793"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 33);
    REQUIRE(record.transfer_reaction == transfer_type::blind);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-08-01 01:17:59.698"s);
}

TEST_CASE("blind transfer from GUI 365 -> 395, intra-PSAP") {
    auto val = parse(
R"(WT-001-1 | 2016/12/01 01:46:38.716 UTC | Mon 2016/12/01 01:46:38 UTC | 316 | 555555555555555555 |     | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2016/12/01 01:46:40.848 UTC | Mon 2016/12/01 01:46:40 UTC | 306 | 555555555555555555 |     | P01 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2016/12/01 01:46:54.997 UTC | Mon 2016/12/01 01:46:54 UTC | 365 | 555555555555555555 |     | P01 | 1111111111 |             | ANI     | < | Blind Transfer from GUI; (Destination=221)
WT-001-1 | 2016/12/01 01:46:55.382 UTC | Mon 2016/12/01 01:46:55 UTC | 342 | 555555555555555555 |     | P01 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/12/01 01:46:58.679 UTC | Mon 2016/12/01 01:46:58 UTC | 395 | 555555555555555555 |     | P03 | 1111111111 |             | ANI 01  | < | Blind Transfer from GUI Connected; (Destination=221)
WT-001-1 | 2016/12/01 01:47:04.574 UTC | Mon 2016/12/01 01:47:04 UTC | 342 | 555555555555555555 |     | P03 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/12/01 01:47:09.360 UTC | Mon 2016/12/01 01:47:09 UTC | 307 | 555555555555555555 |     | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "555555555555555555");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2016-12-01 01:46:38.716");
    REQUIRE(record.answer_utc == "2016-12-01 01:46:40.848"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::none);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2016-12-01 01:47:09.360"s);
}

TEST_CASE("blind transfer from GUI 365 -> 395, inter-PSAP") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [1]
    - id: WT-001-B
      positions: [3]
)");

    auto val = parse(
R"(WT-001-1 | 2016/12/01 01:46:38.716 UTC | Mon 2016/12/01 01:46:38 UTC | 316 | 555555555555555555 |     | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2016/12/01 01:46:40.848 UTC | Mon 2016/12/01 01:46:40 UTC | 306 | 555555555555555555 |     | P01 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2016/12/01 01:46:54.997 UTC | Mon 2016/12/01 01:46:54 UTC | 365 | 555555555555555555 |     | P01 | 1111111111 |             | ANI     | < | Blind Transfer from GUI; (Destination=221)
WT-001-1 | 2016/12/01 01:46:55.382 UTC | Mon 2016/12/01 01:46:55 UTC | 342 | 555555555555555555 |     | P01 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/12/01 01:46:58.679 UTC | Mon 2016/12/01 01:46:58 UTC | 395 | 555555555555555555 |     | P03 | 1111111111 |             | ANI 01  | < | Blind Transfer from GUI Connected; (Destination=221)
WT-001-1 | 2016/12/01 01:47:04.574 UTC | Mon 2016/12/01 01:47:04 UTC | 342 | 555555555555555555 |     | P03 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2016/12/01 01:47:09.360 UTC | Mon 2016/12/01 01:47:09 UTC | 307 | 555555555555555555 |     | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);
    auto record = val.at(0);

    REQUIRE(record.callid == "555555555555555555");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-A"s);
    REQUIRE(record.utc == "2016-12-01 01:46:38.716");
    REQUIRE(record.answer_utc == "2016-12-01 01:46:40.848"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::blind);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2016-12-01 01:47:09.360"s);

    auto rec2 = val.at(1);

    REQUIRE(rec2.callid == "555555555555555555");
    REQUIRE(rec2.controller == "WT-001-1");
    REQUIRE(rec2.subid == 1);
    REQUIRE(rec2.psap == "WT-001-B"s);
    REQUIRE(rec2.utc == "2016-12-01 01:46:54.997");
    REQUIRE(rec2.answer_utc == "2016-12-01 01:46:58.679"s);
    REQUIRE(rec2.abandoned == false);
    REQUIRE(rec2.pos == 3);
    REQUIRE(!rec2.calltaker);
    REQUIRE(!rec2.is_911);
    REQUIRE(!rec2.cos);
    REQUIRE(!rec2.trunk);
    REQUIRE(rec2.transfer_reaction == transfer_type::none);
    REQUIRE(rec2.incoming == true);
    REQUIRE(rec2.t_type == transfer_type::blind);
    REQUIRE(!rec2.disconnect_utc);
}

TEST_CASE("365 indigital transfer") {
    auto val = parse(
R"(WT-001-1 | 2016/03/03 03:58:28.391 UTC | Sun 2016/03/03 03:58:28 UTC | 316 | 222222222222222222 | T05 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2016/03/03 03:58:28.414 UTC | Sun 2016/03/03 03:58:28 UTC | 222 | 222222222222222222 | T05 | P00 |            | p2222222222 | ALI 01A | < | Request Sent with RequestKey=2222222222; Index=01; Trunk=05 (to DB=01 PP=01)
WT-001-1 | 2016/03/03 03:58:28.414 UTC | Sun 2016/03/03 03:58:28 UTC | 222 | 222222222222222222 | T05 | P00 |            | p2222222222 | ALI 01B | < | Request Sent with RequestKey=2222222222; Index=01; Trunk=05 (to DB=01 PP=01)
WT-001-1 | 2016/03/03 03:58:38.266 UTC | Sun 2016/03/03 03:58:38 UTC | 306 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2016/03/03 03:58:38.338 UTC | Sun 2016/03/03 03:58:38 UTC | 402 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2016/03/03 03:58:38.338 UTC | Sun 2016/03/03 03:58:38 UTC | 402 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2016/03/03 03:59:29.792 UTC | Sun 2016/03/03 03:59:29 UTC | 271 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | ALI 01A | < | Auto Request Sent with RequestKey=2222222222; Index=02; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2016/03/03 03:59:29.792 UTC | Sun 2016/03/03 03:59:29 UTC | 271 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | ALI 01B | < | Auto Request Sent with RequestKey=2222222222; Index=02; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2016/03/03 03:59:31.180 UTC | Sun 2016/03/03 03:59:31 UTC | 402 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2016/03/03 03:59:31.180 UTC | Sun 2016/03/03 03:59:31 UTC | 402 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2016/03/03 03:59:38.257 UTC | Sun 2016/03/03 03:59:38 UTC | 365 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | ANI     | < | INdigital Transfer from GUI; (Destination=*0#0000#)
WT-001-1 | 2016/05/22 09:00:13.559 UTC | Sun 2016/05/22 05:00:13 UTC | 342 | 222222222222222222 | T05 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2016/05/22 09:00:13.580 UTC | Sun 2016/05/22 05:00:13 UTC | 307 | 222222222222222222 | T05 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2016-03-03 03:58:28.391");
    REQUIRE(record.answer_utc == "2016-03-03 03:58:38.266"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 4);
    REQUIRE(!record.calltaker);
    REQUIRE(record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 5);
    REQUIRE(record.transfer_reaction == transfer_type::indigital);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2016-05-22 09:00:13.580"s);
}

TEST_CASE("inter-PSAP inbound then conference then conference") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [3]
    - id: WT-001-B
      positions: [1]
    - id: WT-001-C
      positions: [5]
)");

    auto val = parse(
R"(WT-001-1 | 2010/01/01 12:00:00.000 UTC | Tue 2018/01/01 12:00:00 UTC | 709 |                    |     | P03 |            |             | WRK     | - | [INFO] User BobTheBuilder - Workstation 03 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2010/01/01 12:00:00.000 UTC | Tue 2018/01/01 12:00:00 UTC | 709 |                    |     | P01 |            |             | WRK     | - | [INFO] User TomTheDestroyer - Workstation 01 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2010/01/01 12:00:00.000 UTC | Tue 2018/01/01 12:00:00 UTC | 709 |                    |     | P05 |            |             | WRK     | - | [INFO] User LaurenceTheUgly - Workstation 05 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2014/01/01 01:34:29.263 UTC | Wed 2014/01/01 01:34:29 UTC | 316 | 222222222222222222 | T03 | P00 |            | p1111111111 | ANI 01  | > | Ringing
WT-001-1 | 2014/01/01 01:34:29.288 UTC | Wed 2014/01/01 01:34:29 UTC | 222 | 222222222222222222 | T03 | P00 |            | p1111111111 | ALI 01A | < | Request Sent with RequestKey=1111111111; Index=04; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 01:34:29.288 UTC | Wed 2014/01/01 01:34:29 UTC | 222 | 222222222222222222 | T03 | P00 |            | p1111111111 | ALI 01B | < | Request Sent with RequestKey=1111111111; Index=04; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2014/01/01 01:34:30.792 UTC | Wed 2014/01/01 01:34:30 UTC | 306 | 222222222222222222 | T03 | P03 | 1111111111 | p1111111111 | ANI 01  | > | Connect
WT-001-1 | 2014/01/01 01:34:30.874 UTC | Wed 2014/01/01 01:34:30 UTC | 402 | 222222222222222222 | T03 | P03 | 1111111111 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2014/01/01 01:34:42.608 UTC | Wed 2014/01/01 01:34:42 UTC | 365 | 222222222222222222 | T03 | P03 | 1111111111 | p1111111111 | ANI     | < | Conference Transfer from GUI; (Destination=911)
WT-001-1 | 2014/01/01 01:34:45.691 UTC | Wed 2014/01/01 01:34:45 UTC | 309 | 222222222222222222 | T03 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Conference Join
WT-001-1 | 2014/01/01 01:34:45.722 UTC | Wed 2014/01/01 01:34:45 UTC | 378 | 222222222222222222 | T03 | P01 | 1111111111 | p1111111111 | ANI 01  | < | Conference Transfer from GUI Connected; (Destination=911)
WT-001-1 | 2014/01/01 01:34:45.742 UTC | Wed 2014/01/01 01:34:45 UTC | 402 | 222222222222222222 | T03 | P01 | 1111111111 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2014/01/01 01:35:21.465 UTC | Wed 2014/01/01 01:35:21 UTC | 365 | 222222222222222222 | T03 | P01 | 1111111111 | p1111111111 | ANI     | < | Conference Transfer from GUI; (Destination=911)
WT-001-1 | 2014/01/01 01:35:33.492 UTC | Wed 2014/01/01 01:35:33 UTC | 309 | 222222222222222222 | T03 | P05 | 1111111111 | p1111111111 | ANI 01  | > | Conference Join
WT-001-1 | 2014/01/01 01:35:33.522 UTC | Wed 2014/01/01 01:35:33 UTC | 378 | 222222222222222222 | T03 | P05 | 1111111111 | p1111111111 | ANI 01  | < | Conference Transfer from GUI Connected; (Destination=911)
WT-001-1 | 2014/01/01 01:35:33.549 UTC | Wed 2014/01/01 01:35:33 UTC | 402 | 222222222222222222 | T03 | P05 | 1111111111 | p1111111111 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2014/01/01 01:35:48.373 UTC | Wed 2014/01/01 01:35:48 UTC | 342 | 222222222222222222 | T03 | P05 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2014/01/01 01:35:48.922 UTC | Wed 2014/01/01 01:35:48 UTC | 342 | 222222222222222222 | T03 | P03 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave
WT-001-1 | 2014/01/01 01:35:49.767 UTC | Wed 2014/01/01 01:35:49 UTC | 342 | 222222222222222222 | T03 | P00 | 1111111111 | p1111111111 | ANI 01  | > | Conference Leave exten: 1111111111
WT-001-1 | 2014/01/01 01:35:49.807 UTC | Wed 2014/01/01 01:35:49 UTC | 307 | 222222222222222222 | T03 | P01 | 1111111111 | p1111111111 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 3);

    {
        auto record = val.at(0);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 0);
        REQUIRE(record.psap == "WT-001-A"s);
        REQUIRE(record.utc == "2014-01-01 01:34:29.263");
        REQUIRE(record.answer_utc == "2014-01-01 01:34:30.792"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 3);
        REQUIRE(record.calltaker == "BobTheBuilder"s);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 3);
        REQUIRE(record.transfer_reaction == transfer_type::conference);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::initial);
        REQUIRE(record.disconnect_utc == "2014-01-01 01:35:49.807"s);
    }

    {
        auto record = val.at(1);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 1);
        REQUIRE(record.psap == "WT-001-B"s);
        REQUIRE(record.utc == "2014-01-01 01:34:42.608");
        REQUIRE(record.answer_utc == "2014-01-01 01:34:45.722"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 1);
        REQUIRE(record.calltaker == "TomTheDestroyer"s);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 3);
        REQUIRE(record.transfer_reaction == transfer_type::conference);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::conference);
        REQUIRE(!record.disconnect_utc);
    }

       {
        auto record = val.at(2);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 2);
        REQUIRE(record.psap == "WT-001-C"s);
        REQUIRE(record.utc == "2014-01-01 01:35:21.465");
        REQUIRE(record.answer_utc == "2014-01-01 01:35:33.522"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 5);
        REQUIRE(record.calltaker == "LaurenceTheUgly"s);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 3);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::conference);
        REQUIRE(!record.disconnect_utc);
    }
}

TEST_CASE("NG911 SIP transfer then NG911 SIP transfer self") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [4]
    - id: WT-001-B
      positions: [5]
)");

    auto val = parse(
R"(WT-001-1 | 2010/01/01 12:00:00.000 UTC | Tue 2018/01/01 12:00:00 UTC | 709 |                    |     | P04 |            |             | WRK     | - | [INFO] User BobTheBuilder - Workstation 04 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2010/01/01 12:00:00.000 UTC | Tue 2018/01/01 12:00:00 UTC | 709 |                    |     | P05 |            |             | WRK     | - | [INFO] User TomTheDestroyer - Workstation 05 At IP: 10.10.10.10 Now On Line
WT-001-1 | 2018/01/01 01:11:43.556 UTC | Sun 2018/01/01 01:11:43 UTC | 316 | 222222222222222222 | T03 | P00 |            | p2222222222 | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 01:11:43.615 UTC | Sun 2018/01/01 01:11:43 UTC | 222 | 222222222222222222 | T03 | P00 |            | p2222222222 | ALI 01A | < | Request Sent with RequestKey=2222222222; Index=83; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:11:43.615 UTC | Sun 2018/01/01 01:11:43 UTC | 222 | 222222222222222222 | T03 | P00 |            | p2222222222 | ALI 01B | < | Request Sent with RequestKey=2222222222; Index=83; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2018/01/01 01:11:46.301 UTC | Sun 2018/01/01 01:11:46 UTC | 306 | 222222222222222222 | T03 | P04 |            | p2222222222 | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 01:11:47.297 UTC | Sun 2018/01/01 01:11:47 UTC | 402 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2018/01/01 01:11:47.297 UTC | Sun 2018/01/01 01:11:47 UTC | 402 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2018/01/01 01:12:15.471 UTC | Sun 2018/01/01 01:12:15 UTC | 402 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2018/01/01 01:12:15.472 UTC | Sun 2018/01/01 01:12:15 UTC | 402 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2018/01/01 01:12:37.512 UTC | Sun 2018/01/01 01:12:37 UTC | 365 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example)
WT-001-1 | 2018/01/01 01:12:41.049 UTC | Sun 2018/01/01 01:12:41 UTC | 395 | 222222222222222222 | T03 | P05 | 1111111111 | p2222222222 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@example)
WT-001-1 | 2018/01/01 01:12:41.101 UTC | Sun 2018/01/01 01:12:41 UTC | 309 | 222222222222222222 | T03 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 01:12:43.891 UTC | Sun 2018/01/01 01:12:43 UTC | 365 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@example)
WT-001-1 | 2018/01/01 01:13:03.195 UTC | Sun 2018/01/01 01:13:03 UTC | 342 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:13:16.194 UTC | Sun 2018/01/01 01:13:16 UTC | 395 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@example)
WT-001-1 | 2018/01/01 01:13:26.968 UTC | Sun 2018/01/01 01:13:26 UTC | 342 | 222222222222222222 | T03 | P04 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:13:40.475 UTC | Sun 2018/01/01 01:13:40 UTC | 342 | 222222222222222222 | T03 | P05 | 1111111111 | p2222222222 | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:13:40.489 UTC | Sun 2018/01/01 01:13:40 UTC | 307 | 222222222222222222 | T03 | P00 | 1111111111 | p2222222222 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);

    {
        auto record = val.at(0);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 0);
        REQUIRE(record.psap == "WT-001-A"s);
        REQUIRE(record.utc == "2018-01-01 01:11:43.556");
        REQUIRE(record.answer_utc == "2018-01-01 01:11:46.301"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 4);
        REQUIRE(record.calltaker == "BobTheBuilder"s);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 3);
        REQUIRE(record.transfer_reaction == transfer_type::ng911);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::initial);
        REQUIRE(record.disconnect_utc == "2018-01-01 01:13:40.489"s);
    }

    {
        auto record = val.at(1);

        REQUIRE(record.callid == "222222222222222222");
        REQUIRE(record.controller == "WT-001-1");
        REQUIRE(record.subid == 1);
        REQUIRE(record.psap == "WT-001-B"s);
        REQUIRE(record.utc == "2018-01-01 01:12:37.512");
        REQUIRE(record.answer_utc == "2018-01-01 01:12:41.049"s);
        REQUIRE(record.abandoned == false);
        REQUIRE(record.pos == 5);
        REQUIRE(record.calltaker == "TomTheDestroyer"s);
        REQUIRE(record.is_911);
        REQUIRE(!record.cos);
        REQUIRE(record.trunk == 3);
        REQUIRE(record.transfer_reaction == transfer_type::none);
        REQUIRE(record.incoming == true);
        REQUIRE(record.t_type == transfer_type::ng911);
        REQUIRE(!record.disconnect_utc);
    }
}

TEST_CASE("378 conference from GUI connected external") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 01:42:29.091 UTC | Tue 2018/01/01 01:42:29 UTC | 316 | 222222222222222222 | T33 | P00 | VoIP CALL  |             | ANI 01  | > | Ringing (From=+11111111111@10.10.10.10)
WT-001-1 | 2018/01/01 01:42:36.422 UTC | Tue 2018/01/01 01:42:36 UTC | 306 | 222222222222222222 | T33 | P01 | VoIP CALL  |             | ANI 01  | > | Connect (From=+11111111111@10.10.10.10)
WT-001-1 | 2018/01/01 01:43:30.963 UTC | Tue 2018/01/01 01:43:30 UTC | 365 | 222222222222222222 | T33 | P01 | VoIP CALL  |             | ANI     | < | Conference Transfer from GUI; (Destination=2222222222)
WT-001-1 | 2018/01/01 01:43:32.153 UTC | Tue 2018/01/01 01:43:32 UTC | 378 | 222222222222222222 | T33 | P01 | VoIP CALL  |             | ANI 01  | < | Conference Transfer from GUI Connected; (Destination=2222222222)
WT-001-1 | 2018/01/01 01:43:32.216 UTC | Tue 2018/01/01 01:43:32 UTC | 309 | 222222222222222222 | T33 | P00 | VoIP CALL  |             | ANI 01  | > | Conference Join
WT-001-1 | 2018/01/01 01:44:08.842 UTC | Tue 2018/01/01 01:44:08 UTC | 342 | 222222222222222222 | T33 | P00 | VoIP CALL  |             | ANI 01  | > | Conference Leave exten: 2222222222
WT-001-1 | 2018/01/01 01:44:09.072 UTC | Tue 2018/01/01 01:44:09 UTC | 342 | 222222222222222222 | T33 | P01 | VoIP CALL  |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 01:44:09.080 UTC | Tue 2018/01/01 01:44:09 UTC | 307 | 222222222222222222 | T33 | P00 | VoIP CALL  |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "222222222222222222");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-01-01 01:42:29.091");
    REQUIRE(record.answer_utc == "2018-01-01 01:42:36.422"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 33);
    REQUIRE(record.transfer_reaction == transfer_type::conference);
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 01:44:09.080"s);
}

TEST_CASE("conference from GUI with ID") {
    auto val = parse(
R"(WT-001-1 | 2018/04/01 81:42:39.174 UTC | Wed 2018/04/01 86:42:39 UTC | 316 | 444444444444444444 | T50 | P00 | 3333333333 |             | ANI 01  | > | Ringing
WT-001-1 | 2018/04/01 81:42:40.899 UTC | Wed 2018/04/01 86:42:40 UTC | 306 | 444444444444444444 | T50 | P02 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2018/04/01 81:42:40.947 UTC | Wed 2018/04/01 86:42:40 UTC | 391 | 444444444444444444 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: f1b27192-4004-11e9-a1b3-2be70af03246
WT-001-1 | 2018/04/01 81:43:09.303 UTC | Wed 2018/04/01 86:43:09 UTC | 365 | 444444444444444444 | T50 | P02 | 3333333333 |             | ANI     | < | Conference Transfer from GUI; (Destination=12222222222) ID=2963fa03-7c27-4982-8b78-d43f99bf9
WT-001-1 | 2018/04/01 81:43:09.303 UTC | Wed 2018/04/01 86:43:09 UTC | 365 |                    |     |     |            |             |         |   | 452
WT-001-1 | 2018/04/01 81:43:09.398 UTC | Wed 2018/04/01 86:43:09 UTC | 378 | 444444444444444444 | T50 | P02 | 3333333333 |             | ANI 01  | < | Conference Transfer from GUI Connected; (Destination=12222222222) ID=2963fa03-7c27-4982-8b78
WT-001-1 | 2018/04/01 81:43:09.398 UTC | Wed 2018/04/01 86:43:09 UTC | 378 |                    |     |     |            |             |         |   | -d43f99bf9452
WT-001-1 | 2018/04/01 81:43:09.413 UTC | Wed 2018/04/01 86:43:09 UTC | 309 | 444444444444444444 | T50 | P00 | 3333333333 |             | ANI 01  | > | Conference Join
WT-001-1 | 2018/04/01 81:43:09.425 UTC | Wed 2018/04/01 86:43:09 UTC | 391 | 444444444444444444 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 03c3c2d2-4005-11e9-a1fd-2be70af03246
WT-001-1 | 2018/04/01 81:43:12.976 UTC | Wed 2018/04/01 86:43:12 UTC | 342 | 444444444444444444 | T50 | P00 | 3333333333 |             | ANI 01  | > | Conference Leave exten: 12222222222
WT-001-1 | 2018/04/01 81:43:44.449 UTC | Wed 2018/04/01 86:43:44 UTC | 342 | 444444444444444444 | T50 | P02 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/04/01 81:43:44.469 UTC | Wed 2018/04/01 86:43:44 UTC | 307 | 444444444444444444 | T50 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "444444444444444444");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2018-04-01 81:42:39.174");
        REQUIRE(rec.answer_utc == "2018-04-01 81:42:40.899"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::conference);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2018-04-01 81:43:44.469"s);
    }
}

TEST_CASE("ng911 SIP from GUI with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 10:59:28.831 UTC | Wed 2019/01/01 15:59:28 UTC | 316 | 333333333333333333 | T29 | P00 |            | p3333333333 | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 10:59:28.881 UTC | Wed 2019/01/01 15:59:28 UTC | 222 | 333333333333333333 | T29 | P00 |            | p3333333333 | ALI 01S | < | Request Sent Index=38 Trunk=29 (to ALI Service=01 )
WT-001-1 | 2019/01/01 10:59:28.893 UTC | Wed 2019/01/01 15:59:28 UTC | 209 | 333333333333333333 | T29 | P00 |            | p3333333333 | ALI 01S | > | ACK Received Index=38
WT-001-1 | 2019/01/01 10:59:30.535 UTC | Wed 2019/01/01 15:59:30 UTC | 306 | 333333333333333333 | T29 | P01 |            | p3333333333 | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 10:59:30.564 UTC | Wed 2019/01/01 15:59:30 UTC | 391 | 333333333333333333 | T29 | P00 |            | p3333333333 | ANI 01  | > | TDD MODE ON; Channel: bb0aa710-4052-11e9-b90d-2be70af03246
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | ALI 01S | > | Initial Record received (Data Follows)
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | <STX>238<CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | 333-333-3333 01/01 11:11:11 11 <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | CAUTION:  WIRELESS CALL        <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | VERIZON                    WRLS<CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   |     1111                       <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   |    ROAD RD WW - S SECTOR       <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | AAAAAAAAAA             ESN:0000<CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | CPF:VZW        MTN:333-333-3333<CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | Verify PD                      <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | Verify FD                      <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | Verify EMS                     <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | LAT:            LON:           <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | ELV:       COF:         COP:   <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 10:59:32.889 UTC | Wed 2019/01/01 15:59:32 UTC | 226 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2019/01/01 10:59:32.933 UTC | Wed 2019/01/01 15:59:32 UTC | 402 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 10:59:41.925 UTC | Wed 2019/01/01 15:59:41 UTC | 365 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@psap.invalid) ID=6326dfe1-e2ec
WT-001-1 | 2019/01/01 10:59:41.925 UTC | Wed 2019/01/01 15:59:41 UTC | 365 |                    |     |     |            |             |         |   | -4b87-bc6a-18ceaebd5435
WT-001-1 | 2019/01/01 10:59:44.573 UTC | Wed 2019/01/01 15:59:44 UTC | 378 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@psap.invalid) ID=632
WT-001-1 | 2019/01/01 10:59:44.573 UTC | Wed 2019/01/01 15:59:44 UTC | 378 |                    |     |     |            |             |         |   | 6dfe1-e2ec-4b87-bc6a-18ceaebd5435
WT-001-1 | 2019/01/01 10:59:44.629 UTC | Wed 2019/01/01 15:59:44 UTC | 391 | 333333333333333333 | T29 | P00 | 4444444444 | p3333333333 | ANI 01  | > | TDD MODE ON; Channel: c352bd36-4052-11e9-b953-2be70af03246
WT-001-1 | 2019/01/01 11:00:02.953 UTC | Wed 2019/01/01 16:00:02 UTC | 271 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | ALI 01S | < | Auto Request Sent Index=39 Trunk=97 (to ALI Service=01 )
WT-001-1 | 2019/01/01 11:00:02.974 UTC | Wed 2019/01/01 16:00:02 UTC | 209 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | ALI 01S | > | ACK Received Index=39
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | ALI 01S | > | Auto Rebid Record received (Data Follows)
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | <STX>239<CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | 333-333-3333 01/01 11:11:22 11 <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | CAUTION:  WIRELESS CALL        <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | VERIZON                    WPH2<CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   |     1119                       <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   |    ROAD RD WW - S SECTOR       <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | AAAAAAAAAA             ESN:0000<CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | CPF:VZW        MTN:333-333-3333<CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | Verify PD                      <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | Verify FD                      <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | Verify EMS                     <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | LAT:            LON:           <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | ELV:       COF:         COP:   <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   |                                <CR>
WT-001-1 | 2019/01/01 11:00:04.038 UTC | Wed 2019/01/01 16:00:04 UTC | 268 |                    |     |     |            |             |         |   | <ETX>
WT-001-1 | 2019/01/01 11:00:04.099 UTC | Wed 2019/01/01 16:00:04 UTC | 402 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 11:00:05.168 UTC | Wed 2019/01/01 16:00:05 UTC | 342 | 333333333333333333 | T29 | P00 | 4444444444 | p3333333333 | ANI 01  | > | Conference Leave exten: 911
WT-001-1 | 2019/01/01 11:00:06.597 UTC | Wed 2019/01/01 16:00:06 UTC | 342 | 333333333333333333 | T29 | P01 | 4444444444 | p3333333333 | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 11:00:06.606 UTC | Wed 2019/01/01 16:00:06 UTC | 307 | 333333333333333333 | T29 | P00 | 4444444444 | p3333333333 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "333333333333333333");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 10:59:28.831");
        REQUIRE(rec.answer_utc == "2019-01-01 10:59:30.535"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 1);
        REQUIRE(!rec.calltaker);
        REQUIRE(rec.is_911);
        REQUIRE(rec.cos == "WPH2"s);
        REQUIRE(rec.trunk == 29);
        REQUIRE(rec.transfer_reaction == transfer_type::ng911);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 11:00:06.606"s);
    }
}

TEST_CASE("ng911 SIP from GUI with ID but 395 ID is blank") {
    auto config = parse_config::parse(R"(
- controller: WT-001-1
  psaps:
    - id: WT-001-A
      positions: [2]
    - id: WT-001-B
      positions: [4]
)");
    auto val = parse(
R"(WT-001-1 | 2019/01/03 03:44:55.010 UTC | Thu 2019/01/03 03:44:55 UTC | 316 | 555555555555555555 | T03 | P00 |            | p7777777777 | ANI 01  | > | Ringing
WT-001-1 | 2019/01/03 03:44:55.045 UTC | Thu 2019/01/03 03:44:55 UTC | 222 | 555555555555555555 | T03 | P00 |            | p7777777777 | ALI 01A | < | Request Sent with RequestKey=7777777777; Index=01; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2019/01/03 03:44:55.045 UTC | Thu 2019/01/03 03:44:55 UTC | 222 | 555555555555555555 | T03 | P00 |            | p7777777777 | ALI 01B | < | Request Sent with RequestKey=7777777777; Index=01; Trunk=03 (to DB=01 PP=01)
WT-001-1 | 2019/01/03 03:44:59.937 UTC | Thu 2019/01/03 03:44:59 UTC | 306 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | ANI 01  | > | Connect
WT-001-1 | 2019/01/03 03:44:59.974 UTC | Thu 2019/01/03 03:44:59 UTC | 391 | 555555555555555555 | T03 | P00 | 2222222222 | p7777777777 | ANI 01  | > | TDD MODE ON; Channel: b90ffdd2-51a2-11e9-989c-2b9f1ee5eb71
WT-001-1 | 2019/01/03 03:44:59.989 UTC | Thu 2019/01/03 03:44:59 UTC | 402 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:44:59.989 UTC | Thu 2019/01/03 03:44:59 UTC | 402 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:00.090 UTC | Thu 2019/01/03 03:45:00 UTC | 402 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:00.090 UTC | Thu 2019/01/03 03:45:00 UTC | 402 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:17.815 UTC | Thu 2019/01/03 03:45:17 UTC | 271 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | ALI 01A | < | Auto Request Sent with RequestKey=7777777777; Index=02; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2019/01/03 03:45:17.815 UTC | Thu 2019/01/03 03:45:17 UTC | 271 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | ALI 01B | < | Auto Request Sent with RequestKey=7777777777; Index=02; Trunk=97 (to DB=01 PP=01)
WT-001-1 | 2019/01/03 03:45:18.736 UTC | Thu 2019/01/03 03:45:18 UTC | 402 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:18.736 UTC | Thu 2019/01/03 03:45:18 UTC | 402 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:33.915 UTC | Thu 2019/01/03 03:45:33 UTC | 365 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@mofp.ng911.state.oh.us) ID=b9d8be29-d39c-4
WT-001-1 | 2019/01/03 03:45:33.915 UTC | Thu 2019/01/03 03:45:33 UTC | 365 |                    |     |     |            |             |         |   | 3ad-84d0-34387cb5cd1e
WT-001-1 | 2019/01/03 03:45:37.751 UTC | Thu 2019/01/03 03:45:37 UTC | 395 | 555555555555555555 | T03 | P04 | 2222222222 | p7777777777 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@mofp.ng911.state.oh.us) ID=
WT-001-1 | 2019/01/03 03:45:37.793 UTC | Thu 2019/01/03 03:45:37 UTC | 402 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:37.793 UTC | Thu 2019/01/03 03:45:37 UTC | 402 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | CAD 02  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:37.894 UTC | Thu 2019/01/03 03:45:37 UTC | 402 | 555555555555555555 | T03 | P04 | 2222222222 | p7777777777 | CAD 03  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:37.894 UTC | Thu 2019/01/03 03:45:37 UTC | 402 | 555555555555555555 | T03 | P04 | 2222222222 | p7777777777 | CAD 04  | < | ALI Record Sent
WT-001-1 | 2019/01/03 03:45:45.109 UTC | Thu 2019/01/03 03:45:45 UTC | 342 | 555555555555555555 | T03 | P04 | 2222222222 | p7777777777 | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/03 03:45:45.437 UTC | Thu 2019/01/03 03:45:45 UTC | 342 | 555555555555555555 | T03 | P02 | 2222222222 | p7777777777 | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/03 03:45:45.443 UTC | Thu 2019/01/03 03:45:45 UTC | 307 | 555555555555555555 | T03 | P00 | 2222222222 | p7777777777 | ANI 01  | > | Disconnect
)", config);

    REQUIRE(val.size() == 2);

    {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "555555555555555555");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-A"s);
        REQUIRE(rec.utc == "2019-01-03 03:44:55.010");
        REQUIRE(rec.answer_utc == "2019-01-03 03:44:59.937"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 3);
        REQUIRE(rec.transfer_reaction == transfer_type::ng911);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-03 03:45:45.443"s);
    }

    {
        auto rec = val.at(1);
        REQUIRE(rec.callid == "555555555555555555");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 1);
        REQUIRE(rec.psap == "WT-001-B"s);
        REQUIRE(rec.utc == "2019-01-03 03:45:33.915");
        REQUIRE(rec.answer_utc == "2019-01-03 03:45:37.751"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 4);
        REQUIRE(!rec.calltaker);
        REQUIRE(rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 3);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::ng911);
        REQUIRE(!rec.disconnect_utc);
    }
}

TEST_CASE("Outbound 367 Conference Transfer from GUI fail with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/02/02 02:04:07.393 UTC | Wed 2019/02/02 02:04:07 UTC | 372 | 333333333333333333 | T50 | P01 | 3333333333 |             | ANI 01  | > | Outbound Call from Phone Dialed; (Destination=13333333333)
WT-001-1 | 2019/02/02 02:04:12.104 UTC | Wed 2019/02/02 02:04:12 UTC | 306 | 333333333333333333 | T50 | P01 | 3333333333 |             | ANI 01  | > | Connect
WT-001-1 | 2019/02/02 02:04:12.152 UTC | Wed 2019/02/02 02:04:12 UTC | 391 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 4a90d54c-55d6-11e9-9b14-11467cfa2825
WT-001-1 | 2019/02/02 02:04:18.100 UTC | Wed 2019/02/02 02:04:18 UTC | 365 | 333333333333333333 | T50 | P01 | 3333333333 |             | ANI     | < | Conference Transfer from GUI; (Destination=444444444444) ID=46082aaf-11cf-4fe1-a90b-b4cd7ebc
WT-001-1 | 2019/02/02 02:04:18.100 UTC | Wed 2019/02/02 02:04:18 UTC | 365 |                    |     |     |            |             |         |   | cf93
WT-001-1 | 2019/02/02 02:04:30.046 UTC | Wed 2019/02/02 02:04:30 UTC | 367 | 333333333333333333 | T50 | P01 | 3333333333 |             | ANI 01  | < | Conference Transfer from GUI Cancelled; (Destination=444444444444) ID=46082aaf-11cf-4fe1-a90
WT-001-1 | 2019/02/02 02:04:30.046 UTC | Wed 2019/02/02 02:04:30 UTC | 367 |                    |     |     |            |             |         |   | b-b4cd7ebccf93
WT-001-1 | 2019/02/02 02:04:37.444 UTC | Wed 2019/02/02 02:04:37 UTC | 365 | 333333333333333333 | T50 | P01 | 3333333333 |             | ANI     | < | Conference Transfer from GUI; (Destination=5555555555) ID=8c95e6e5-4c3e-4b76-b7b6-ee964bf523
WT-001-1 | 2019/02/02 02:04:37.444 UTC | Wed 2019/02/02 02:04:37 UTC | 365 |                    |     |     |            |             |         |   | 56
WT-001-1 | 2019/02/02 02:04:37.777 UTC | Wed 2019/02/02 02:04:37 UTC | 378 | 333333333333333333 | T50 | P01 | 3333333333 |             | ANI 01  | < | Conference Transfer from GUI Connected; (Destination=5555555555) ID=8c95e6e5-4c3e-4b76-b7b6-
WT-001-1 | 2019/02/02 02:04:37.777 UTC | Wed 2019/02/02 02:04:37 UTC | 378 |                    |     |     |            |             |         |   | ee964bf52356
WT-001-1 | 2019/02/02 02:04:37.808 UTC | Wed 2019/02/02 02:04:37 UTC | 309 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Conference Join
WT-001-1 | 2019/02/02 02:04:37.808 UTC | Wed 2019/02/02 02:04:37 UTC | 391 | 333333333333333333 | T50 | P01 | 3333333333 |             | ANI 01  | > | TDD MODE ON; Channel: 5c87d8fe-55d6-11e9-9b8b-11467cfa2825
WT-001-1 | 2019/02/02 02:05:15.816 UTC | Wed 2019/02/02 02:05:15 UTC | 342 | 333333333333333333 | T50 | P01 | 3333333333 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2019/02/02 02:05:16.406 UTC | Wed 2019/02/02 02:05:16 UTC | 342 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Conference Leave exten: 5555555555
WT-001-1 | 2019/02/02 02:05:16.409 UTC | Wed 2019/02/02 02:05:16 UTC | 307 | 333333333333333333 | T50 | P00 | 3333333333 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

     {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "333333333333333333");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-02-02 02:04:07.393");
        REQUIRE(rec.answer_utc == "2019-02-02 02:04:12.104"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 1);
        REQUIRE(!rec.calltaker);
        REQUIRE(!rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 50);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == false);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(!rec.disconnect_utc);
    }
}

TEST_CASE("365 NG911 Transfer from GUI with ID") {
    auto val = parse(
R"(WT-001-1 | 2019/01/01 01:08:55.252 UTC | Wed 2019/01/01 01:08:55 UTC | 316 | 444444444444444444 | T01 | P00 |            | p7777777777 | ANI 01  | > | Ringing
WT-001-1 | 2019/01/01 01:08:55.312 UTC | Wed 2019/01/01 01:08:55 UTC | 222 | 444444444444444444 | T01 | P00 |            | p7777777777 | ALI 01A | < | Request Sent with RequestKey=7777777777; Index=04; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2019/01/01 01:08:55.312 UTC | Wed 2019/01/01 01:08:55 UTC | 222 | 444444444444444444 | T01 | P00 |            | p7777777777 | ALI 01B | < | Request Sent with RequestKey=7777777777; Index=04; Trunk=01 (to DB=01 PP=01)
WT-001-1 | 2019/01/01 01:08:57.791 UTC | Wed 2019/01/01 01:08:57 UTC | 306 | 444444444444444444 | T01 | P02 |            | p7777777777 | ANI 01  | > | Connect
WT-001-1 | 2019/01/01 01:08:57.831 UTC | Wed 2019/01/01 01:08:57 UTC | 391 | 444444444444444444 | T01 | P00 |            | p7777777777 | ANI 01  | > | TDD MODE ON; Channel: c1cabff2-8d34-11e9-b789-531651ac06b2
WT-001-1 | 2019/01/01 01:09:02.320 UTC | Wed 2019/01/01 01:09:02 UTC | 402 | 444444444444444444 | T01 | P02 | 7777777777 | p7777777777 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 01:09:03.322 UTC | Wed 2019/01/01 01:09:03 UTC | 413 | 444444444444444444 | T01 | P02 | 7777777777 | p7777777777 | CAD 01  | - | [WARNING] ALI Record ACK Timeout
WT-001-1 | 2019/01/01 01:09:03.322 UTC | Wed 2019/01/01 01:09:03 UTC | 430 | 444444444444444444 | T01 | P02 | 7777777777 | p7777777777 | CAD 01  | < | [WARNING] ALI Record retransmitted
WT-001-1 | 2019/01/01 01:09:03.377 UTC | Wed 2019/01/01 01:09:03 UTC | 365 | 444444444444444444 | T01 | P02 | 7777777777 | p7777777777 | ANI     | < | NG911 SIP Transfer from GUI; (Destination=sip:911@pcso.psap.oh.us) ID=4c1eabc3-87c0-422d-ab0
WT-001-1 | 2019/01/01 01:09:03.377 UTC | Wed 2019/01/01 01:09:03 UTC | 365 |                    |     |     |            |             |         |   | 8-79dcb6002a8a
WT-001-1 | 2019/01/01 01:09:04.324 UTC | Wed 2019/01/01 01:09:04 UTC | 413 | 444444444444444444 | T01 | P02 | 7777777777 | p7777777777 | CAD 01  | - | [WARNING] ALI Record ACK Timeout
WT-001-1 | 2019/01/01 01:09:04.324 UTC | Wed 2019/01/01 01:09:04 UTC | 428 | 444444444444444444 | T01 | P02 | 7777777777 | p7777777777 | CAD 01  | - | [WARNING] ALI Record Lost
WT-001-1 | 2019/01/01 01:09:09.615 UTC | Wed 2019/01/01 01:09:09 UTC | 395 | 444444444444444444 | T01 | P04 | 7777777777 | p7777777777 | ANI 01  | < | NG911 SIP Transfer from GUI Connected; (Destination=sip:911@pcso.psap.oh.us) ID=4c1eabc3-87c
WT-001-1 | 2019/01/01 01:09:09.615 UTC | Wed 2019/01/01 01:09:09 UTC | 395 |                    |     |     |            |             |         |   | 0-422d-ab08-79dcb6002a8a
WT-001-1 | 2019/01/01 01:09:09.632 UTC | Wed 2019/01/01 01:09:09 UTC | 402 | 444444444444444444 | T01 | P04 | 7777777777 | p7777777777 | CAD 01  | < | ALI Record Sent
WT-001-1 | 2019/01/01 01:09:10.634 UTC | Wed 2019/01/01 01:09:10 UTC | 413 | 444444444444444444 | T01 | P04 | 7777777777 | p7777777777 | CAD 01  | - | [WARNING] ALI Record ACK Timeout
WT-001-1 | 2019/01/01 01:09:10.634 UTC | Wed 2019/01/01 01:09:10 UTC | 430 | 444444444444444444 | T01 | P04 | 7777777777 | p7777777777 | CAD 01  | < | [WARNING] ALI Record retransmitted
WT-001-1 | 2019/01/01 01:09:11.636 UTC | Wed 2019/01/01 01:09:11 UTC | 413 | 444444444444444444 | T01 | P04 | 7777777777 | p7777777777 | CAD 01  | - | [WARNING] ALI Record ACK Timeout
WT-001-1 | 2019/01/01 01:09:11.636 UTC | Wed 2019/01/01 01:09:11 UTC | 428 | 444444444444444444 | T01 | P04 | 7777777777 | p7777777777 | CAD 01  | - | [WARNING] ALI Record Lost
WT-001-1 | 2019/01/01 01:09:22.968 UTC | Wed 2019/01/01 01:09:22 UTC | 342 | 444444444444444444 | T01 | P02 | 7777777777 | p7777777777 | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:09:35.885 UTC | Wed 2019/01/01 01:09:35 UTC | 342 | 444444444444444444 | T01 | P04 | 7777777777 | p7777777777 | ANI 01  | > | Conference Leave
WT-001-1 | 2019/01/01 01:09:35.910 UTC | Wed 2019/01/01 01:09:35 UTC | 307 | 444444444444444444 | T01 | P00 | 7777777777 | p7777777777 | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);

     {
        auto rec = val.at(0);
        REQUIRE(rec.callid == "444444444444444444");
        REQUIRE(rec.controller == "WT-001-1");
        REQUIRE(rec.subid == 0);
        REQUIRE(rec.psap == "WT-001-1"s);
        REQUIRE(rec.utc == "2019-01-01 01:08:55.252");
        REQUIRE(rec.answer_utc == "2019-01-01 01:08:57.791"s);
        REQUIRE(rec.abandoned == false);
        REQUIRE(rec.pos == 2);
        REQUIRE(!rec.calltaker);
        REQUIRE(rec.is_911);
        REQUIRE(!rec.cos);
        REQUIRE(rec.trunk == 1);
        REQUIRE(rec.transfer_reaction == transfer_type::none);
        REQUIRE(rec.incoming == true);
        REQUIRE(rec.t_type == transfer_type::initial);
        REQUIRE(rec.disconnect_utc == "2019-01-01 01:09:35.910"s);
    }
}
