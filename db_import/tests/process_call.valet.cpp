#include "parse.h"

#include <catch.hpp>

using namespace std::string_literals;

TEST_CASE("valet") {
    auto val = parse(
R"(WT-003-1 | 2018/04/20 01:47:07.865 UTC | Fri 2018/04/20 01:47:07 MDT | 316 | 343434343434343434 |     | P00 | 6666666666 |             | ANI 01  | > | Ringing
WT-003-1 | 2018/04/20 01:47:10.280 UTC | Fri 2018/04/20 01:47:10 MDT | 306 | 343434343434343434 |     | P01 | 6666666666 |             | ANI 01  | > | Connect
WT-003-1 | 2018/04/20 01:47:22.676 UTC | Fri 2018/04/20 01:47:22 MDT | 342 | 343434343434343434 |     | P01 | 6666666666 |             | ANI 01  | > | Conference Leave
WT-003-1 | 2018/04/20 01:47:22.679 UTC | Fri 2018/04/20 01:47:22 MDT | 392 | 343434343434343434 |     | P01 | 6666666666 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=7000)
WT-003-1 | 2018/04/20 01:47:50.752 UTC | Fri 2018/04/20 01:47:50 MDT | 370 | 343434343434343434 |     | P00 | 6666666666 |             | ANI 01  | > | Valet Park Pickup from lot: 7001
WT-003-1 | 2018/04/20 01:51:25.834 UTC | Fri 2018/04/20 01:51:25 MDT | 342 | 343434343434343434 |     | P00 | 6666666666 |             | ANI 01  | > | Conference Leave exten: 264
WT-003-1 | 2018/04/20 01:51:25.857 UTC | Fri 2018/04/20 01:51:25 MDT | 307 | 343434343434343434 |     | P00 | 6666666666 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "343434343434343434");
    REQUIRE(record.controller == "WT-003-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-003-1"s);
    REQUIRE(record.utc == "2018-04-20 01:47:07.865");
    REQUIRE(record.answer_utc == "2018-04-20 01:47:10.280"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(!record.trunk);
    REQUIRE(record.transfer_reaction == transfer_type::blind); // TODO: decide if this will be the case
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-04-20 01:51:25.857"s);
}

TEST_CASE("call merge") {
    auto val = parse(
R"(WT-001-1 | 2018/01/01 13:48:30.002 UTC | Wed 2018/01/01 13:48:30 UTC | 316 | 343434343434343434 | T10 | P00 | 1111111111 |             | ANI 01  | > | Ringing
WT-001-1 | 2018/01/01 13:48:32.769 UTC | Wed 2018/01/01 13:48:32 UTC | 306 | 343434343434343434 | T10 | P01 | 1111111111 |             | ANI 01  | > | Connect
WT-001-1 | 2018/01/01 13:48:48.884 UTC | Wed 2018/01/01 13:48:48 UTC | 310 | 343434343434343434 | T10 | P01 | 1111111111 |             | ANI 01  | > | On Hold
WT-001-1 | 2018/01/01 13:48:55.526 UTC | Wed 2018/01/01 13:48:55 UTC | 311 | 343434343434343434 | T10 | P01 | 1111111111 |             | ANI 01  | > | Off Hold
WT-001-1 | 2018/01/01 13:48:55.533 UTC | Wed 2018/01/01 13:48:55 UTC | 342 | 343434343434343434 | T10 | P01 | 1111111111 |             | ANI 01  | > | Conference Leave
WT-001-1 | 2018/01/01 13:48:55.536 UTC | Wed 2018/01/01 13:48:55 UTC | 392 | 343434343434343434 | T10 | P01 | 1111111111 |             | ANI 01  | > | Blind Transfer from Phone Dialed; (Destination=7001)
WT-001-1 | 2018/01/01 13:48:55.573 UTC | Wed 2018/01/01 13:48:55 UTC | 396 | 343434343434343434 | T10 | P00 | 1111111111 |             | ANI 01  | > | Call Merged with UUid 444444444444444444
WT-001-1 | 2018/01/01 13:48:55.574 UTC | Wed 2018/01/01 13:48:55 UTC | 307 | 343434343434343434 | T10 | P00 | 1111111111 |             | ANI 01  | > | Disconnect
)");

    REQUIRE(val.size() == 1);
    auto record = val.at(0);

    REQUIRE(record.callid == "343434343434343434");
    REQUIRE(record.controller == "WT-001-1");
    REQUIRE(record.subid == 0);
    REQUIRE(record.psap == "WT-001-1"s);
    REQUIRE(record.utc == "2018-01-01 13:48:30.002");
    REQUIRE(record.answer_utc == "2018-01-01 13:48:32.769"s);
    REQUIRE(record.abandoned == false);
    REQUIRE(record.pos == 1);
    REQUIRE(!record.calltaker);
    REQUIRE(!record.is_911);
    REQUIRE(!record.cos);
    REQUIRE(record.trunk == 10);
    REQUIRE(record.transfer_reaction == transfer_type::blind); // TODO: decide if this will be the case
    REQUIRE(record.incoming == true);
    REQUIRE(record.t_type == transfer_type::initial);
    REQUIRE(record.disconnect_utc == "2018-01-01 13:48:55.574"s);
}
