#ifndef WESTTEL_MIS_DB
#define WESTTEL_MIS_DB

#include "call_record.h"

#include <memory>

class db {
private:
    struct impl;
    std::unique_ptr<impl, void(*)(impl*)> impl_;
public:
    db(std::string controller);
    ~db();

    void add_call(CallRecord call);

    void join_and_commit();
};

#endif
