#ifndef WESTTEL_MIS_LOG_CONVERTER
#define WESTTEL_MIS_LOG_CONVERTER

#include "log_util.h"

#include <json/json.h>
#include <memory>

std::string parse_angle_form(const std::string& str);

class log_converter {
private:
    struct impl;
    std::unique_ptr<impl, void(*)(impl*)> impl_;
public:
    log_converter();

    Json::Value convert(const OldLog& o);

    void throw_exceptions_less();
};

#endif
