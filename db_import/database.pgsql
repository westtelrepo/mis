-- DO NOT RUN this with `psql -f`
-- it compiles inside of db_import and is used with `db_import --init-schema`


-- the entire file is idempotent (i.e. if the schema is already setup correctly it should do nothing)

-- if the database gets screwed up, drop it and make a new one
-- there is no important data here (the important data is in the original log files)

-- db_import already puts this inside a transaction, hence no BEGIN or COMMIT

CREATE TABLE IF NOT EXISTS basic_call_info2 (
    controller TEXT,
    callid TEXT,
    subid INT,
    PRIMARY KEY(controller, callid, subid),
    psap TEXT,
    utc TIMESTAMPTZ NOT NULL,
    answer INTERVAL,
    abandoned BOOLEAN NOT NULL,
    pos INT,
    calltaker TEXT,
    is_911 BOOLEAN NOT NULL,
    cos TEXT,
    incoming BOOLEAN NOT NULL,
    trunk INT,
    internal_transfer BOOLEAN NOT NULL DEFAULT FALSE,
    transfer_reaction TEXT NOT NULL DEFAULT 'none',
    transfer_type TEXT NOT NULL
);

CREATE INDEX IF NOT EXISTS basic_call_info2_callid ON basic_call_info2(callid text_pattern_ops);
CREATE INDEX IF NOT EXISTS basic_call_info2_controller_utc ON basic_call_info2(controller, utc);
CREATE INDEX IF NOT EXISTS basic_call_info2_psap_utc ON basic_call_info2(psap, utc);

CREATE TABLE IF NOT EXISTS classofservice (
    cos TEXT PRIMARY KEY,
    grp TEXT
);

INSERT INTO classofservice VALUES ('WPH1', 'Wireless'),
    ('MOBL', 'Wireless'),
    ('WRLS', 'Wireless'),
    ('BSNX', 'Wireline'),
    ('BUS', 'Wireline'),
    ('BUSN', 'Wireline'),
    ('BUSX', 'Wireline'),
    ('CN', 'Wireline'),
    ('CNTX', 'Wireline'),
    ('COIN', 'Wireline'),
    ('CTX', 'Wireline'),
    ('RES', 'Wireline'),
    ('RESD', 'Wireline'),
    ('RESX', 'Wireline'),
    ('PAY$', 'Wireline'),
    ('VOIP', 'VoIP'),
    ('NRF', 'Unknown'),
    ('RSDX', 'Wireline'),
    ('PXB', 'Wireline'),
    ('PBXB', 'Wireline'),
    ('PBXR', 'Wireline'),
    ('PBXb', 'Wireline'),
    ('WPH2', 'Wireless')
    ON CONFLICT DO NOTHING;

CREATE OR REPLACE FUNCTION public.cos_group(cos_ text)
RETURNS text
LANGUAGE plpgsql
STABLE STRICT
AS $function$
DECLARE
    ret TEXT;
BEGIN
    SELECT grp INTO ret FROM classofservice WHERE cos = cos_;
    IF FOUND THEN
        RETURN ret;
    ELSE
        RETURN cos_;
    END IF;
END;
$function$;

-- call length update

ALTER TABLE basic_call_info2 ADD COLUMN call_length INTERVAL;
