#ifndef WESTTEL_LOGSTREAM_HEADER
#define WESTTEL_LOGSTREAM_HEADER

#include "log_util.h"
#include <boost/optional.hpp>
#include <istream>
#include <memory>

class LogStream {
public: // for testing
	struct Line {
		std::string psap;
		std::string utc;
		int32_t code; // TODO: should probably become a string
		std::string callid;
		std::string trunk;
		std::string position;
		std::string ani;
		std::string pani;
		std::string thread;
		std::string direction;
		std::string message;

		Line(const std::string& line);
	};
private:

	struct impl;
	std::unique_ptr<impl, void(*)(impl*)> impl_;

	boost::optional<std::string> read_line();
	boost::optional<std::string> peek_line();
public:
	LogStream(std::unique_ptr<std::istream> in, std::string filename);

	void set_preserve_newlines();

	// return nullptr on EOF
	boost::optional<OldLog> peek();
	boost::optional<OldLog> next();

	LogStream& operator=(LogStream&& other) = default;
	LogStream(LogStream&& other) = default;
};

#endif
