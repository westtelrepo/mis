#ifndef WESTTEL_MIS_CALL_RECORD
#define WESTTEL_MIS_CALL_RECORD

#include <boost/optional.hpp>
#include <string>

enum class transfer_type {
    none,
    initial,
    tandem,
    ng911,
    blind,
    attended,
    conference,
    indigital,
    unknown,
};

inline std::string to_string(transfer_type type) {
    switch (type) {
        case transfer_type::none:
            return "none";
        case transfer_type::tandem:
            return "tandem";
        case transfer_type::ng911:
            return "ng911";
        case transfer_type::blind:
            return "blind";
        case transfer_type::attended:
            return "attended";
        case transfer_type::conference:
            return "conference";
        case transfer_type::indigital:
            return "indigital";
        case transfer_type::unknown:
            return "unknown";
        case transfer_type::initial:
            return "initial";
    }
}

inline transfer_type transfer_type_from_string(const std::string& str) {
    if (str == "blind") {
        return transfer_type::blind;
    } else if (str == "tandem") {
        return transfer_type::tandem;
    } else if (str == "conference") {
        return transfer_type::conference;
    } else if (str == "ng911") {
        return transfer_type::ng911;
    } else if (str == "attended") {
        return transfer_type::attended;
    } else if (str == "indigital") {
        return transfer_type::indigital;
    } else if (str == "initial") {
        return transfer_type::initial;
    } else {
        return transfer_type::unknown;
    }
}

inline std::ostream& operator<<(std::ostream& o, transfer_type type) {
    o << to_string(type);
    return o;
}

struct CallRecord {
    // (callid, controller, subid) is PRIMARY KEY
    std::string callid;
    std::string controller;
    int32_t subid;
    boost::optional<std::string> psap;
    std::string utc;
    boost::optional<std::string> answer_utc;
    bool abandoned;
    boost::optional<int32_t> pos;
    boost::optional<std::string> calltaker;
    bool is_911;
    boost::optional<std::string> cos;
    boost::optional<int32_t> trunk;

    // the type of the first transfer of "call"
    transfer_type transfer_reaction = transfer_type::none;
    bool incoming = true;

    // the type of the "transfer" itself
    transfer_type t_type = transfer_type::initial;

    // the utc of the disconnect, used for average call time
    boost::optional<std::string> disconnect_utc;
};

#endif
