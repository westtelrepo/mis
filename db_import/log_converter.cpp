#include "log_converter.h"

#include "ali.h"

#include <regex>

struct log_converter::impl {
    std::regex r146_warning{R"(\[WARNING\] (.*))"};
    std::regex r147_warning{R"(\[WARNING\] (.*))"};
    std::regex r204_lis_deref{R"(HTTP De-reference sent to : (\S+))"};
    std::regex r204_lost{R"(HTTP LoST Service by Location Request (.+) sent to : (\S+))"};
    std::regex r205_http_location_request{R"(HTTP LocationRequest sent to : (\S+))"};
    std::regex r209{R"(ACK Received Index=(.*))"};
    std::regex r220_warning{R"(\[WARNING\] (.*))"};
    std::regex r222_1{R"(Request Sent with RequestKey=(.*); Index=(.*); Trunk=(.*) \(to DB=(.*) PP=(.*)\))"};
    std::regex r222_2{R"(Request Sent Index=(.*) \(to DB=(.*) PP=(.*); with TransactionID=(.*)\))"};
    std::regex r222_3{R"(Request Sent Index=(.*) Trunk=(.*) \(to ALI Service=(.*) \))"};
    std::regex r225{R"(\[WARNING\] No ALI Database can be Bid for RequestKey=(.*))"};
    std::regex r226{R"((?:Initial )?Record received \(Data Follows\)(.*))"};
    std::regex r231_resent{R"(Request Resent with RequestKey=(.*); Index=(.*); Trunk=(.*) \(to DB=(.*) PP=(.*)\))"};
    std::regex r232_unmatched{R"(\[WARNING\] UnMatched Record received(?:, TransactionId=(.*))? \(Data Follows\)(.*))"};
    std::regex r246_warning{R"(\[WARNING\] (.*))"};
    std::regex r261_warning{R"(\[WARNING\] (.*))"};
    std::regex r266{R"(Rebid Record received \(Data Follows\)(.*))"};
    std::regex r267{R"(Manual Bid Record received \(Data Follows\)(.*))"};
    std::regex r268{R"(Auto Rebid Record received \(Data Follows\)(.*))"};
    std::regex r269{R"(Manual Request Sent with RequestKey=(.*); Index=(.*); Trunk=(.*) \(to DB=(.*) PP=(.*)\))"};
    std::regex r269_ali{R"(Manual Request Sent Index=(.*) Trunk=(.*) \(to ALI Service=(.*) \))"};
    std::regex r270{R"(Rebid Request Sent with RequestKey=(.*); Index=(.*); Trunk=(.*) \(to DB=(.*) PP=(.*)\))"};
    std::regex r270_tid{R"(Rebid Request Sent Index=(.*) \(to DB=(.*) PP=(.*); with TransactionID=(.*)\))"};
    std::regex r270_ali{R"(Rebid Request Sent Index=(.*) Trunk=(.*) \(to ALI Service=(.*) \))"};
    std::regex r271{R"(Auto Request Sent with RequestKey=(.*); Index=(.*); Trunk=(.*) \(to DB=(.*) PP=(.*)\))"};
    std::regex r271_tid{R"(Auto Request Sent Index=(.*) \(to DB=(.*) PP=(.*); with TransactionID=(.*)\))"};
    std::regex r271_ali{R"(Auto Request Sent Index=(.*) Trunk=(.*) \(to ALI Service=(.*) \))"};
    std::regex r279_lis{R"(NG911 Location Data received \(Data Follows\)(.*))"};

    std::regex r306{R"(Connect \(From=(.*)\))"};
    std::regex r308_1{R"(Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r316{R"(Ringing \(From=(.*)\))"};
    std::regex r341_i3{R"(NENA I3 CallId: (\S+) )"};
    std::regex r341_i3_incidentid{R"(NENA I3 IncidentId: (\S+) )"};
    std::regex r342_1{R"(Conference Leave ?)"};
    std::regex r342_2{R"(Conference Leave exten: (.*))"};

    std::regex r351{R"regex(\[TDD\] TDD Character "(.*)" Received)regex"};
    std::regex r352{R"regex(\[TDD\] TDD Caller Says: "(.*)")regex"};

    std::regex r361{R"(TDD Conversation Record \(Data Follows\)(.*))"};

    std::regex r365_1{R"(Conference Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_2{R"(Attended Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_3{R"(Blind Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_4{R"(Tandem Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_5{R"(NG911 SIP Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_6{R"(Flash Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_7{R"(Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_indigital{R"(INdigital Transfer from GUI; \(Destination=(.*)\))"};
    std::regex r365_conference_id{R"(Conference Transfer from GUI; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r365_ng911_id{R"(NG911 SIP Transfer from GUI; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r365_tandem_id{R"(Tandem Transfer from GUI; \(Destination=(\S+)\) ID=(\S+))"};
    std::regex r365_intrado_id{R"(Intrado Transfer from GUI; \(Destination=(\S+)\) ID=(\S+))"};
    std::regex r365_indigital_id{R"(INdigital Transfer from GUI; \(Destination=(\S+)\) ID=(\S+))"};

    std::regex r366_1{R"(Transfer from Phone Cancelled; \(Destination=(.*)\))"};
    std::regex r366_2{R"(Attended Transfer from Phone Cancelled; \(Destination=(.*)\))"};
    std::regex r366_3{R"(Blind Transfer from Phone Cancelled; \(Destination=(.*)\))"};
    std::regex r366_4{R"(\{INFO\] Transfer Cancelled; (.*))"}; // NOTE: doesn't exist
    std::regex r366_attended_id{R"(Attended Transfer from Phone Cancelled; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r366_attended_useless_id{R"(Attended Transfer from Phone Cancelled; \(Destination=(.*)\) ID=)"};

    std::regex r367_1{R"(Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_2{R"(Transfer from GUI Cancelled; \(Destination=(.*?)\) Reason: (.*))"};
    std::regex r367_3{R"(Attended Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_4{R"(Blind Transfer from GUI (?:Cancelled|Failed); \(Destination=(.*)\))"};
    std::regex r367_6{R"(Tandem Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_7{R"(Conference Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_8{R"(Blind Transfer from GUI Cancelled; \(Destination=(.*)\))"};
    std::regex r367_conference_fail_id{R"(Conference Transfer from GUI Cancelled; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r367_intrado_fail_id{R"(Intrado Transfer from GUI Cancelled; \(Destination=(.*)\) ID=(\S+))"};

    std::regex r368_warning{R"(\[WARNING\] (.*))"};

    std::regex r369_1{R"(Attended Transfer from Phone Failed; \(Destination=(.*)\); Reason=(\S*))"};
    std::regex r369_2{R"(Blind Transfer from Phone Failed; \(Destination=(.*)\); Reason=(\S*))"};
    std::regex r369_blind_id{R"(Blind Transfer from Phone Failed; \(Destination=(.*)\); Reason=(\S*) ID=(\S*))"};
    std::regex r369_attended_id{R"(Attended Transfer from Phone Failed; \(Destination=(.*)\); Reason=(\S*) ID=(\S+))"};

    std::regex r370_1{R"(Outbound Call from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_2{R"(Attended Transfer from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_3{R"(Unattended Transfer from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_4{R"(Blind Transfer from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_5{R"(Outbound Blind Transfer from Phone Connected; \(Destination=(.*)\))"};
    std::regex r370_valet{R"(Valet Park Pickup from lot: (.*))"};
    std::regex r370_attended_id{R"(Attended Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r370_unattended_id{R"(Unattended Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r370_blind_id{R"(Blind Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r370_blind_useless_id{R"(Blind Transfer from Phone Connected; \(Destination=(.*)\) ID=)"};
    std::regex r370_valet_to_lot{R"(Valet Park to lot: (\d+) ID=(\S+))"};
    std::regex r370_attended_useless_id{R"(Attended Transfer from Phone Connected; \(Destination=(\S*)\) ID=)"};

    std::regex r372{R"(Outbound Call from Phone Dialed; \(Destination=(.*)\))"};

    std::regex r373_1{R"(Transfer from Phone Dialed; \(Destination=(.*)\))"};
    std::regex r373_2{R"(\[INFO\] Transfer\/Conf Dialed; (.*))"};
    std::regex r373_attended{R"(Attended Transfer from Phone Dialed; \(Destination=(.*)\))"};
    std::regex r373_attended_id{R"(Attended Transfer from Phone Dialed; \(Destination=(.*)\) ID=(\S+))"};

    std::regex r378_1{R"(Conference Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r378_2{R"(Blind Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r378_3{R"(Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r378_4{R"(NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\))"};
    std::regex r378_conference_id{R"(Conference Transfer from GUI Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r378_conference_useless_id{R"(Conference Transfer from GUI Connected; \(Destination=(.*)\) ID=)"};
    // warning: ID can be blank
    std::regex r378_ng911_id{R"(NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\) ID=(\S*))"};

    std::regex r381_info{R"(\[INFO\] (.*))"};
    std::regex r382{R"(\[TDD\] Dispatcher (.*) says: (.*))"};

    std::regex r383_blind{R"(Blind Transfer From GUI Failed; \(Destination=(.*)\); Reason=(.*))"};
    std::regex r383_conference{R"(Conference Transfer From GUI Failed; \(Destination=(.*)\); Reason=(.*))"};
    std::regex r383_ng911{R"(NG911 SIP Transfer From GUI Failed; \(Destination=(\S*)\); Reason=(\S+) ID=(\S+))"};

    std::regex r391{R"(TDD MODE ON; Channel: (.*))"};

    std::regex r392{R"(Blind Transfer from Phone Dialed; \(Destination=(.*)\))"};
    std::regex r392_id{R"(Blind Transfer from Phone Dialed; \(Destination=(.*)\) ID=(\S+))"};

    std::regex r395_1{R"(Outbound Call to Position Connected; \(Destination=(.*)\))"};
    std::regex r395_ng911_sip_no_id{R"(NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\) ID=)"};
    std::regex r395_ng911_sip_id{R"(NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r395_blind_id{R"(Blind Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r395_unattended_id{R"(Unattended Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r395_attended_id{R"(Attended Transfer from Phone Connected; \(Destination=(.*)\) ID=(\S+))"};
    std::regex r395_conference_id{R"(Conference Transfer from GUI Connected; \(Destination=(.*)\) ID=(\S+))"};

    std::regex r396_merge{R"(Call Merged with UUid (\d+))"};

    std::regex r408_warning{R"(\[WARNING\] (.*))"};
    std::regex r413_warning{R"(\[WARNING\] (.*))"};
    std::regex r428_warning{R"(\[WARNING\] (.*))"};
    std::regex r430_warning{R"(\[WARNING\] (.*))"};

    std::regex r709{R"(\[INFO\] (?:User (.*) - )?Workstation (.*) At IP: (.*) Now On Line)"};
    std::regex r717{R"(\[INFO\] (?:User (.*) - )?Workstation (.*) At IP: (.*) Now Off Line)"};

    ali_parser ali;
    std::map<int, std::string> calltaker;

    bool throw_exceptions_less_ = false;
};

log_converter::log_converter(): impl_{new impl{}, [](impl* i){ delete i; }} {
}

Json::Value log_converter::convert(const OldLog& e) {
    try {
    Json::Value log;
    log["controller"] = e.psap;
    log["utc"] = e.utc();
    log["code"] = "unhandled" + std::to_string(e.code);

    if (e.position.value_or(0) != 0) {
        log["user"]["position"] = e.position.value();
        if (impl_->calltaker.count(e.position.value())) {
            log["user"]["name"] = impl_->calltaker[e.position.value()];
        }
    }

    if (e.callid || e.trunk || e.ani || e.pani) {
        if (e.callid) {
            log["call"]["callid"] = e.callid.value();
        }

        if (e.trunk) {
            log["call"]["trunk"] = e.trunk.value();
        }

        if (e.ani) {
            if (e.ani.value() == "9999999999") {
                log["call"]["is_911"] = true;
            } else if (e.ani.value() == "1111111111") {
                log["call"]["is_911"] = false;
            }

            log["call"]["ani"] = e.ani.value();
        }

        if (e.pani) {
            if (e.pani.value() == "p9999999999" || e.pani.value() == "p0000000000") {
                log["call"]["is_911"] = true;
            }

            log["call"]["pani"] = e.pani.value();
            log["call"]["is_911"] = true;
        }
    }

    auto no_newlines = [] (const std::string& s) {
        std::string ret;
        for (auto c: s) {
            if (c != '\r' && c != '\n')
                ret += c;
        }
        return ret;
    };

    std::smatch m;
    const std::string message_no_new = no_newlines(e.message);


    if (e.code == 146) {
        if (std::regex_match(message_no_new, m, impl_->r146_warning)) {
            if (m[1] == "Forced Disconnect Sent") {
                log["code"] = "call/warning/forced_disconnect/";
            }
        } else {
            throw std::runtime_error{"Unknown 146 message: " + message_no_new};
        }
    } else if (e.code == 147) {
        if (std::regex_match(message_no_new, m, impl_->r147_warning)) {

        } else {
            throw std::runtime_error{"Unknown 147 message: " + message_no_new};
        }
    } else if (e.code == 204) {
        if (std::regex_match(message_no_new, m, impl_->r204_lis_deref)) {
            // ignore
        } else if (std::regex_match(message_no_new, m, impl_->r204_lost)) {
            // ignore
        } else {
            throw std::runtime_error{"Unknown 204 message: " + message_no_new};
        }
    } else if (e.code == 205) {
        if (std::regex_match(message_no_new, m, impl_->r205_http_location_request)) {
            // ignore
            log["call"]["is_911"] = true;
        } else {
            throw std::runtime_error{"Unknown 205 HTTP LocationRequest message: " + message_no_new};
        }
    } else if (e.code == 209) {
        if (std::regex_match(message_no_new, m, impl_->r209)) {
            // ignore
        } else {
            // only throw unknown if it has a callid
            if (e.callid) {
                throw std::runtime_error{"Unknown 209 message: " + message_no_new};
            }
        }
    } else if (e.code == 220) {
        if (std::regex_match(message_no_new, m, impl_->r220_warning)) {

        } else {
            throw std::runtime_error{"Unknown 220 message: " + message_no_new};
        }
    } else if (e.code == 222) {
        log["code"] = "ali/request/";
        log["call"]["is_911"] = true;

        if (std::regex_match(message_no_new, m, impl_->r222_1)) {
        } else if (std::regex_match(message_no_new, m, impl_->r222_2)) {
        } else if (std::regex_match(message_no_new, m, impl_->r222_3)) {
        } else {
            throw std::runtime_error{"Unknown 222 message: " + message_no_new};
        }
    } else if (e.code == 225) {
        log["call"]["is_911"] = true;
        log["code"] = "ali/nobid/";
        if (std::regex_match(message_no_new, m, impl_->r225)) {

        } else {
            throw std::runtime_error{"Unknown 225 message: " + message_no_new};
        }
    } else if (e.code == 226) {
        log["call"]["is_911"] = true;
        log["code"] = "ali/received/";
        if (std::regex_match(message_no_new, m, impl_->r226)) {
            //log["ali"]["raw"] = m[1].str();
            log["ali"]["cos"] = impl_->ali.parse_cos(parse_angle_form(m[1].str()));
        } else {
            throw std::runtime_error{"Unknown 226 message: " + message_no_new};
        }
    } else if (e.code == 231) {
        log["call"]["is_911"] = true;
        if (std::regex_match(message_no_new, m, impl_->r231_resent)) {

        } else {
            throw std::runtime_error{"Unknown 231 ALI resent message: " + message_no_new};
        }
    } else if (e.code == 232) {
        log["call"]["is_911"] = true;
        if (std::regex_match(message_no_new, m, impl_->r232_unmatched)) {
            // TODO: do we do anything with this?
        } else {
            throw std::runtime_error{"Unknown 232 ALI unmatched error: " + message_no_new};
        }
    } else if (e.code == 246) {
        log["call"]["is_911"] = true;
        if (std::regex_match(message_no_new, m, impl_->r246_warning)) {

        } else {
            throw std::runtime_error{"Unknown 246 message: " + message_no_new};
        }
    } else if (e.code == 261) {
        if (std::regex_match(message_no_new, m, impl_->r261_warning)) {

        } else {
            throw std::runtime_error{"Unknown 261 message: " + message_no_new};
        }
    } else if (e.code == 266) {
        log["call"]["is_911"] = true;
        log["code"] = "ali/received/rebid/";
        if (std::regex_match(message_no_new, m, impl_->r266)) {
            //log["ali"]["raw"] = m[1].str();
            log["ali"]["cos"] = impl_->ali.parse_cos(parse_angle_form(m[1].str()));
        } else {
            throw std::runtime_error{"Unknown 266 ALI Rebid message: " + message_no_new};
        }
    } else if (e.code == 267) {
        log["call"]["is_911"] = true;
        log["code"] = "ali/received/manual/";
        if (std::regex_match(message_no_new, m, impl_->r267)) {
            //log["ali"]["raw"] = m[1].str();
            log["ali"]["cos"] = impl_->ali.parse_cos(parse_angle_form(m[1].str()));
        } else {
            throw std::runtime_error{"Unknown 267 ALI manual message: " + message_no_new};
        }
    } else if (e.code == 268) {
        log["call"]["is_911"] = true;
        log["code"] = "ali/received/autorebid/";
        if (std::regex_match(message_no_new, m, impl_->r268)) {
            //log["ali"]["raw"] = m[1].str();
            log["ali"]["cos"] = impl_->ali.parse_cos(parse_angle_form(m[1].str()));
        } else {
            throw std::runtime_error{"Unknown 268 autorebid message: " + message_no_new};
        }
    } else if (e.code == 269) {
        log["code"] = "ali/request/";
        log["call"]["is_911"] = true;
        if (std::regex_match(message_no_new, m, impl_->r269)) {

        } else if (std::regex_match(message_no_new, m, impl_->r269_ali)) {

        } else {
            throw std::runtime_error{"Unknown 269 manual ali bid message: " + message_no_new};
        }
    } else if (e.code == 270) {
        log["code"] = "ali/request/";
        log["call"]["is_911"] = true;
        if (std::regex_match(message_no_new, m, impl_->r270)) {

        } else if (std::regex_match(message_no_new, m, impl_->r270_tid)) {

        } else if (std::regex_match(message_no_new, m, impl_->r270_ali)) {

        } else {
            throw std::runtime_error{"Unknown 270 Rebid Request message: " + message_no_new};
        }
    } else if (e.code == 271) {
        log["code"] = "ali/request/";
        log["call"]["is_911"] = true;
        if (std::regex_match(message_no_new, m, impl_->r271)) {

        } else if (std::regex_match(message_no_new, m, impl_->r271_tid)) {

        } else if (std::regex_match(message_no_new, m, impl_->r271_ali)) {

        } else {
            throw std::runtime_error{"Unknown 271 Auto Request message: " + message_no_new};
        }
    } else if (e.code == 278) {
        log["code"] = "lis/http/received/";
        log["call"]["is_911"] = true;
        // TODO
    } else if (e.code == 279) {
        log["code"] = "lis/ng911/received/";
        log["call"]["is_911"] = true;
        if (std::regex_match(message_no_new, m, impl_->r279_lis)) {
            log["ali"]["cos"] = impl_->ali.parse_cos(parse_angle_form(m[1].str()));
        } else {
            throw std::runtime_error{"Unknown 279 LIS message: " + message_no_new};
        }
    } else if (e.code == 306) {
        log["transfer"]["state"] = "join";
        log["transfer"]["type"] = "initial";

        log["code"] = "call/connect/";
        log["user"]["exten"] = Json::nullValue;
        log["user"]["may_be_wrong_outbound"] = true;

        if (e.message == "Connect") {
        } else if (std::regex_match(message_no_new, m, impl_->r306)) {
            //log["debug"]["connect_from"] = m[1].str();
        } else {
            throw std::runtime_error{"Unknown 306 Connect message: " + message_no_new};
        }
    } else if (e.code == 307) {
        log["user"]["exten"] = Json::nullValue;

        log["code"] = "call/disconnect/";
        if (e.message == "Disconnect") {
        } else {
            throw std::runtime_error{"Unknown 307 Disconnect message: " + message_no_new};
        }
    } else if (e.code == 308) {
        log["transfer"]["state"] = "join";
        if (std::regex_match(message_no_new, m, impl_->r308_1)) {
            log["transfer"]["type"] = "unknown";
            log["transfer"]["destination"] = m[1].str();
        } else {
            throw std::runtime_error{"Unknown 308 message: " + message_no_new};
        }
    } else if (e.code == 309) {
        log["code"] = "call/join/";
        if (e.message == "Conference Join") {
        } else {
            throw std::runtime_error{"Unknown 309 Conference Join message: " + message_no_new};
        }
    } else if (e.code == 310) {
        log["code"] = "call/onhold/";
        if (e.message == "On Hold") {
        } else {
            throw std::runtime_error{"Unknown 310 On Hold message: " + message_no_new};
        }
    } else if (e.code == 311) {
        log["code"] = "call/offhold/";
        if (e.message == "Off Hold") {
        } else {
            throw std::runtime_error{"Unknown 311 Off Hold message: " + message_no_new};
        }
    } else if (e.code == 312) {
        log["transfer"]["state"] = "abandon";
        log["transfer"]["type"] = "initial";

        // destroy user information because position is incorrect and useless
        log["user"] = Json::objectValue;
        log["user"]["controller"] = e.psap;
        log["user"]["psap"] = e.psap; // TODO:
        log["user"]["may_be_wrong_outbound"] = true;

        log["code"] = "call/abandoned/";
        if (e.message == "Abandoned") {
        } else {
            throw std::runtime_error{"Unknown 312 Abandoned message: " + message_no_new};
        }
    } else if (e.code == 316) {
        if (log.isMember("user"))
            throw std::runtime_error{"316 Ringing has position user"};

        if (e.ani)
            log["user"]["exten"] = e.ani.value();
        else if (e.pani)
            log["user"]["exten"] = e.pani.value();
        else
            log["user"]["exten"] = Json::nullValue;

        log["code"] = "call/ringing/";

        log["transfer"]["state"] = "dial";
        log["transfer"]["type"] = "initial";
        if (e.message == "Ringing") {
        } else if (std::regex_match(message_no_new, m, impl_->r316)) {
        } else if (e.message == "Ring Back") {
            log.removeMember("transfer");
            log["code"] = "call/316_ring_back/";
        } else {
            throw std::runtime_error{"Unknown 316 message: " + message_no_new};
        }
    } else if (e.code == 341) {
        log["call"]["is_911"] = true;
        if (std::regex_match(message_no_new, m, impl_->r341_i3)) {
            // ignore
        } else if (std::regex_match(message_no_new, m, impl_->r341_i3_incidentid)) {
            // ignore
        } else {
            throw std::runtime_error{"Unknown 341 NENA I3 message: " + message_no_new};
        }
    } else if (e.code == 342) {
        if (std::regex_match(message_no_new, m, impl_->r342_1)) {
            log["code"] = "call/leave/";
            if (e.position.value_or(0) == 0)
                throw std::runtime_error{"Conference Leave w/o exten with zero or null position"};
        } else if (std::regex_match(message_no_new, m, impl_->r342_2)) {
            log["code"] = "call/leave/";
            if (!log.isMember("user")) {
                log["user"]["exten"] = m[1].str();
            } else {
                throw std::runtime_error{"342 Conference Leave has both exten and position"};
            }
        } else {
            throw std::runtime_error{"Unknown 342 Conference Leave message: " + message_no_new};
        }
    } else if (e.code == 351) {
        // [TDD] TDD Character "*" Received
        if (std::regex_match(message_no_new, m, impl_->r351)) {

        } else {
            throw std::runtime_error{"Unknown 351 message: " + message_no_new};
        }
    } else if (e.code == 352) {
        // [TDD] Caller Says: "*"
        if (std::regex_match(message_no_new, m, impl_->r352)) {

        } else {
            throw std::runtime_error{"Unknown 352 message: " + message_no_new};
        }
    } else if (e.code == 361) {
        if (std::regex_match(message_no_new, m, impl_->r361)) {

        } else {
            throw std::runtime_error{"Unknown 361 message: " + message_no_new};
        }
    } else if (e.code == 365) {
        log["transfer"]["state"] = "dial";
        if (std::regex_match(message_no_new, m, impl_->r365_1)) {
            log["code"] = "call/transfer/conference/";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "conference";
        } else if (std::regex_match(message_no_new, m, impl_->r365_2)) {
            throw std::runtime_error{"Got a 365 Attended Transfer from GUI, which does not exist"};
            //log["code"] = "call/transfer/attended/";
            //log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r365_3)) {
            log["code"] = "call/transfer/blind/";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "blind";
        } else if (std::regex_match(message_no_new, m, impl_->r365_4)) {
            log["code"] = "call/transfer/tandem/";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "tandem";
        } else if (std::regex_match(message_no_new, m, impl_->r365_5)) {
            log["code"] = "call/transfer/sip/";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "ng911";
            log["transfer"]["no_callee_is_abandoned"] = true;
        } else if (std::regex_match(message_no_new, m, impl_->r365_6)) {
            throw std::runtime_error{"Got 365 Flash Transfer from GUI, which does not exist"};
            //log["code"] = "call/transfer/flash/";
            //log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r365_7)) {
            // NOTE: does not exist in 2018
            log["code"] = "call/transfer/transfer/";
            log["transfer"]["type"] = "unknown";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r365_indigital)) {
            // TODO: only one site has these
            log["code"] = "call/transfer/indigital/";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "indigital";
        } else if (std::regex_match(message_no_new, m, impl_->r365_conference_id)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
            log["transfer"]["type"] = "conference";
        } else if (std::regex_match(message_no_new, m, impl_->r365_ng911_id)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
            log["transfer"]["type"] = "ng911";
        } else if (std::regex_match(message_no_new, m, impl_->r365_tandem_id)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
            log["transfer"]["type"] = "tandem";
        } else if (std::regex_match(message_no_new, m, impl_->r365_intrado_id)) {
            log["code"] = "call/transfer/intrado/gui/";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
            log["transfer"]["type"] = "intrado";
        } else if (std::regex_match(message_no_new, m, impl_->r365_indigital_id)) {
            log["code"] = "call/transfer/indigital/";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
            log["transfer"]["type"] = "indigital";
        } else {
            throw std::runtime_error{"Unknown 365 Transfer from GUI message: " + message_no_new};
        }
    } else if (e.code == 366) {
        log["transfer"]["state"] = "abandon";
        if (std::regex_match(message_no_new, m, impl_->r366_1)) {
            // NOTE: this only exists in 2011
            log["transfer"]["type"] = "*"; // TODO: is it really?
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r366_2)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r366_3)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r366_attended_id)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else if (std::regex_match(message_no_new, m, impl_->r366_attended_useless_id)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["_match_with_id"] = true;
        } else {
            throw std::runtime_error{"Unknown 366 Transfer Cancelled message: " + message_no_new};
        }
    } else if (e.code == 367) {
        log["transfer"]["state"] = "abandon";
        if (std::regex_match(message_no_new, m, impl_->r367_1)) {
            log["transfer"]["type"] = "unknown";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r367_2)) {
            log["transfer"]["type"] = "*";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
            // TODO: log["transfer"]["Reason"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r367_3)) {
            log["transfer"]["destination"] = m[1].str();
            throw std::runtime_error{"370 Attended Transfer from GUI Cancelled doesn't exist"};
        } else if (std::regex_match(message_no_new, m, impl_->r367_4)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r367_6)) {
            log["transfer"]["destination"] = m[1].str();
            throw std::runtime_error{"367 Tandem Transfer from GUI Cancelled"};
        } else if (std::regex_match(message_no_new, m, impl_->r367_7)) {
            log["transfer"]["type"] = "conference";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r367_8)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "tandem";
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r367_conference_fail_id)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
            log["transfer"]["type"] = "conference";
        } else if (std::regex_match(message_no_new, m, impl_->r367_intrado_fail_id)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
            log["transfer"]["type"] = "intrado";
        } else {
            throw std::runtime_error{"Unknown 367 Transfer GUI Cancelled message: " + message_no_new};
        }
    } else if (e.code == 368) {
        if (std::regex_match(message_no_new, m, impl_->r368_warning)) {

        } else {
            throw std::runtime_error{"Unknown 368 message"};
        }
    } else if (e.code == 369) {
        log["transfer"]["state"] = "abandon";
        if (std::regex_match(message_no_new, m, impl_->r369_1)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r369_2)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r369_blind_id)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[3].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r369_attended_id)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[3].str();
            log["user"].removeMember("position");
        } else {
            throw std::runtime_error{"Unknown 369 message: " + message_no_new};
        }

    } else if (e.code == 370) {
        log["transfer"]["state"] = "join";

        if (std::regex_match(message_no_new, m, impl_->r370_1)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "initial";
            log["code"] = "call/connect/outbound/";
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r370_2)) {
            log["code"] = "call/join/attended/";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "attended";
        } else if (std::regex_match(message_no_new, m, impl_->r370_3)) {
            log["code"] = "call/join/attended/unattended";
            log["call"]["destination"] = m[1].str();
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "*";
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r370_4)) {
            log["code"] = "call/join/blind/";
            log["call"]["destination"] = m[1].str();
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "blind";
        } else if (std::regex_match(message_no_new, m, impl_->r370_5)) {
            log["code"] = "call/join/outbound_blind/";
            log["transfer"]["type"] = "unknown";
            log["transfer"]["destination"] = m[1].str();
            //log["call"]["destination"] = m[1].str();
            //log["transfer"]["destination"] = m[1].str();
            // NOTE: doesn't appear in 2017-2018
        } else if (std::regex_match(message_no_new, m, impl_->r370_valet)) {
            // TODO: we are ignoring this
            log.removeMember("transfer");
        } else if (std::regex_match(message_no_new, m, impl_->r370_valet_to_lot)) {
            // TODO: we are ignoring this
            log.removeMember("transfer");
        } else if (std::regex_match(message_no_new, m, impl_->r370_attended_id)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else if (std::regex_match(message_no_new, m, impl_->r370_unattended_id)) {
            log["transfer"]["type"] = "unattended";
            log["transfer"]["destination"] = m[1].str();
            log["trasnfer"]["id"] = m[2].str();
        } else if (std::regex_match(message_no_new, m, impl_->r370_blind_id)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else if (std::regex_match(message_no_new, m, impl_->r370_blind_useless_id)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r370_attended_useless_id)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
        } else if (e.message == "Reconnect Ringback") {
            log["code"] = "call/error/reconnect_ringback/";
            log.removeMember("transfer");
        } else {
            throw std::runtime_error{"Unknown 370 message: " + message_no_new};
        }
    } else if (e.code == 372) {
        log["transfer"]["state"] = "dial";
        log["transfer"]["type"] = "initial";

        log["code"] = "call/outbound/";
        if (std::regex_match(message_no_new, m, impl_->r372)) {
            log["call"]["destination"] = m[1].str();
            log["transfer"]["destination"] = m[1].str();
        } else {
            throw std::runtime_error{"Unknown 372 message: " + message_no_new};
        }
    } else if (e.code == 373) {
        log["transfer"]["state"] = "dial";

        if (std::regex_match(message_no_new, m, impl_->r373_1)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "unknown";
        } else if (std::regex_match(message_no_new, m, impl_->r373_2)) {
        } else if (std::regex_match(message_no_new, m, impl_->r373_attended)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r373_attended_id)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else {
            throw std::runtime_error{"Unknown 373 message: " + message_no_new};
        }
    } else if (e.code == 378) {
        log["transfer"]["state"] = "join";

        if (std::regex_match(message_no_new, m, impl_->r378_1)) {
            log["code"] = "call/join/conference/";
            log["call"]["destination"] = m[1].str();
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "conference";
            log["user"]["wrong_if_same_position"] = true;
        } else if (std::regex_match(message_no_new, m, impl_->r378_2)) {
            log["code"] = "call/join/blind/";
            log["call"]["destination"] = m[1].str();
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "blind";
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r378_3)) {
            log["code"] = "call/join/transfer/";
            log["call"]["destination"] = m[1].str();
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "*";
        } else if (std::regex_match(message_no_new, m, impl_->r378_4)) {
            log["code"] = "call/join/ng911/";
            log["call"]["destination"] = m[1].str();
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["type"] = "ng911";
        } else if (std::regex_match(message_no_new, m, impl_->r378_conference_id)) {
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
            log["transfer"]["type"] = "conference";
            log["user"]["wrong_if_same_position"] = true;
        } else if (std::regex_match(message_no_new, m, impl_->r378_conference_useless_id)) {
            if (m[1].str() != "") {
                log["transfer"]["destination"] = m[1].str();
            }
            log["transfer"]["type"] = "conference";
            log["user"]["wrong_if_same_position"] = true;
        } else if (std::regex_match(message_no_new, m, impl_->r378_ng911_id)) {
            log["transfer"]["destination"] = m[1].str();
            auto id = m[2].str();
            if (id != "") {
                log["transfer"]["id"] = id;
            }
            log["transfer"]["type"] = "ng911";
            log["user"]["wrong_if_same_position"] = true;
        } else {
            throw std::runtime_error{"Unknown 378 message: " + message_no_new};
        }
    } else if (e.code == 381) {
        if (std::regex_match(message_no_new, m, impl_->r381_info)) {

        } else {
            throw std::runtime_error{"Unknown 381 message: " + message_no_new};
        }
    } else if (e.code == 382) {
        // 382 DISPATCHER SAYS
        if (std::regex_match(message_no_new, m, impl_->r382)) {

        } else {
            throw std::runtime_error{"Unknown 382 message: " + message_no_new};
        }
    } else if (e.code == 383) {
        if (std::regex_match(message_no_new, m, impl_->r383_blind)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["state"] = "abandon";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r383_conference)) {
            log["transfer"]["type"] = "conference";
            log["transfer"]["state"] = "abandon";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else if (std::regex_match(message_no_new, m, impl_->r383_ng911)) {
            log["transfer"]["type"] = "ng911";
            log["transfer"]["state"] = "abandon";
            log["transfer"]["destination"] = m[1].str();
            log["user"].removeMember("position");
        } else {
            throw std::runtime_error{"Unknown 383 message: " + message_no_new};
        }
    } else if (e.code == 391) {
        // 391 TDD MODE ON
        if (std::regex_match(message_no_new, m, impl_->r391)) {

        } else if (message_no_new == "TDD MODE ON") {

        } else {
            throw std::runtime_error{"Unknown 391 message: " + message_no_new};
        }
    } else if (e.code == 392) {
        log["transfer"]["state"] = "dial";
        log["code"] = "kbnp 392";
        if (std::regex_match(message_no_new, m, impl_->r392)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r392_id)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else {
            throw std::runtime_error{"Unknown 392 message: " + message_no_new};
        }
    } else if (e.code == 395) {
        log["transfer"]["state"] = "join";

        if (std::regex_match(message_no_new, m, impl_->r370_3)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r378_1)) {
            log["transfer"]["type"] = "conference";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r378_4)) {
            log["transfer"]["type"] = "ng911";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r370_4)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r395_1)) {
            log["transfer"]["type"] = "initial";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r370_2)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r378_2)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
        } else if (std::regex_match(message_no_new, m, impl_->r395_ng911_sip_no_id)) {
            log["transfer"]["type"] = "ng911";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["_match_with_id"] = true;
        } else if (std::regex_match(message_no_new, m, impl_->r395_blind_id)) {
            log["transfer"]["type"] = "blind";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else if (std::regex_match(message_no_new, m, impl_->r395_unattended_id)) {
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else if (std::regex_match(message_no_new, m, impl_->r395_ng911_sip_id)) {
            log["code"] = "transfer/join/ng911/gui/";
            log["transfer"]["type"] = "ng911";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else if (std::regex_match(message_no_new, m, impl_->r395_attended_id)) {
            log["code"] = "transfer/join/attended/phone/";
            log["transfer"]["type"] = "attended";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else if (std::regex_match(message_no_new, m, impl_->r395_conference_id)) {
            log["code"] = "transfer/join/conference/gui/";
            log["transfer"]["type"] = "conference";
            log["transfer"]["destination"] = m[1].str();
            log["transfer"]["id"] = m[2].str();
        } else {
            throw std::runtime_error{"Unknown 395 message: " + message_no_new};
        }
    } else if (e.code == 396) {
        if (std::regex_match(message_no_new, m, impl_->r396_merge)) {
            // TODO: figure out call merging, which is... interesting
        } else {
            throw std::runtime_error{"Unknown 396 message: "};
        }
    } else if (e.code == 402) {
        if (message_no_new == "ALI Record Sent") {
            log["call"]["is_911"] = true;
        } else {
            throw std::runtime_error{"Unknown 402 ALI Record Sent message: " + message_no_new};
        }
    } else if (e.code == 404) {
        if (message_no_new == "ACK Received") {

        } else {
            throw std::runtime_error{"Unknown 404 ACK Received message: " + message_no_new};
        }
    } else if (e.code == 408) {
        if (std::regex_match(e.message, m, impl_->r408_warning)) {

        } else {
            throw std::runtime_error{"Unknown CAD 408 message: " + message_no_new};
        }
    } else if (e.code == 413) {
        if (std::regex_match(e.message, m, impl_->r413_warning)) {

        } else {
            throw std::runtime_error{"Unknown CAD 413 message: " + message_no_new};
        }
    } else if (e.code == 428) {
        if (std::regex_match(e.message, m, impl_->r428_warning)) {

        } else {
            throw std::runtime_error{"Unknown CAD 428 message: " + message_no_new};
        }
    } else if (e.code == 430) {
        if (std::regex_match(e.message, m, impl_->r430_warning)) {

        } else {
            throw std::runtime_error{"Unknown CAD 430 message: " + message_no_new};
        }
    } else if (e.code == 709) {
        log["code"] = "login/login/";
        if (std::regex_match(e.message, m, impl_->r709)) {
            log["user"]["name"] = m[1].str();

            if (std::stoi(m[2].str()) != e.position.value())
                throw std::runtime_error{"709 Login Workstation does not match position"};

            impl_->calltaker[e.position.value()] = m[1].str();
        } else {
            throw std::runtime_error{"Unknown 709 message: " + message_no_new};
        }
    } else if (e.code == 717) {
        log["code"] = "login/logout/";
        if (std::regex_match(e.message, m, impl_->r717)) {
            log["user"]["name"] = m[1].str();

            if (std::stoi(m[2].str()) != e.position.value())
                throw std::runtime_error{"717 Logout Workstation does not match position"};

            impl_->calltaker.erase(e.position.value());
        } else {
            throw std::runtime_error{"Unknown 717 message: " + message_no_new};
        }
    } else if (e.code == 733) {
        if (e.message == "Abandoned Call Taken Control by Workstation" || e.message == "Abandoned Call Taken Control by Software") {

        } else {
            throw std::runtime_error{"Unknown 733 message: " + message_no_new};
        }
    } else {
        if (!impl_->throw_exceptions_less_)
            throw std::runtime_error{"unknown log line"};

        if (e.callid && !std::regex_match(message_no_new, m, impl_->r146_warning))
            throw std::runtime_error{"unknown log line with callid"};
    }

    if (log.isMember("user") && !log["user"].isMember("position")) {
        log["user"].removeMember("name");
    }

    for (auto x: impl_->calltaker) {
        log["user_positions"][std::to_string(x.first)] = x.second;
    }

    return log;
    } catch(const std::exception&) {
        std::cerr << "exception with " << e << std::endl;
        throw;
    }
}

void log_converter::throw_exceptions_less() {
    impl_->throw_exceptions_less_ = true;
}

std::string parse_angle_form(const std::string& str) {
    std::string ret;
    size_t i = 0;

    for (; i < str.size(); i++) {
        if (str[i] == '<') {
            std::string code;
            i++;
            while (true) {
                if (i >= str.size()) {
                    ret += '<' + code;
                    goto break_outer_loop;
                } else if (str[i] == '<') {
                    // retry code processing on new '<'
                    i--;
                    ret += '<' + code;
                    goto continue_outer_loop;
                } else if (str[i] == '>') {
                    break;
                }
                code += str[i];
                i++;
            }

            if (code == "STX")
                ret += '\x02';
            else if (code == "ETX")
                ret += '\x03';
            else if (code == "CR")
                ret += '\r';
            else if (code == "LF")
                ret += '\n';
            else {
                int ch;
                try {
                    ch = std::stoi(code);
                } catch(const std::invalid_argument&) {
                    ch = -1;
                } catch(const std::out_of_range&) {
                    ch = -1;
                }
                if (ch == 0 || (ch > 127 && ch <= 255)) {
                    //ret += "<unknown>"; // TODO: fromCharCode(0xE000 + ch);
                    utf8::append(0xE000 + static_cast<uint32_t>(ch), std::back_inserter(ret));
                } else if (ch > 0 && ch <= 127) {
                    ret += static_cast<char>(ch);
                } else {
                    ret += '<' + code + '>';
                }
            }
        } else
            ret += str[i];
        continue_outer_loop: ;
    }
    break_outer_loop: ;
    return ret;
}
