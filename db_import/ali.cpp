#include "ali.h"

#include <boost/algorithm/string.hpp>
#include <json/json.h>
#include <map>
#include <regex>
#include <set>

#include <iostream>

struct ali_parser::impl {
    std::map<std::string, std::string> known_cos;

    std::regex space_before{R"(^\u0002\d+\r(?: {31}|                     \uE0F8          )\r[\d ]{3}-[\d ]{3}-[\d ]{4}  \d{2}:\d{2}:\d{2} \d{8}\r.{31}\r.{27}(.{4})\r)"};
    std::regex nrf_flat13{R"(^\u0002\d{13} NO RECORD FOUND\u0003$)"};
    std::regex nrf_flat10{R"(^\u0002\d+\r\d{10} NO RECORD FOUND\u0003$)"};
    std::regex huron{R"(^\u0002\d+\r\d{3}-\d{3}-\d{4}.{32}(.{4}))"};
    std::regex nrf_hyphen{R"(^\u0002\d+\r\d{3}-\d{3}-\d{4} NO RECORD FOUND\u0003$)"};
    std::regex nrf_flat_lf_cr{R"(^\u0002\d{13} RECORD NOT FOUND\n                               \r(?:\u007F\u007F)?\u0003$)"};
    std::regex erie{R"(^\u0002\d+\r\n?\(\d{3}\) \d{3}-\d{4} (.{4}) \d{2}\/\d{2} \d{2}:\d{2}\r)"};

    std::regex clay{R"(^\u0002\d+\r\(\d{3}\) \d{3}-\d{4}   \d{2}:\d{2}  \d{2}\/\d{2}\r.{49}\r.{30}\r\r(?:CALLBK=\(\d{3}\)\d{3}-\d{4} {10}|.{30})\r.{24}ESN [ \d]{4}\rCO=.{5} PSAP \d{2} POS# \d{2}   (.{4})\r[^]*\u0003$)"};
    std::regex clay_wrls{R"(^\u0002\d+\r\(\d{3}\) \d{3}-\d{4}   \d{2}:\d{2}  \d{2}\/\d{2}\r.{49}\r.{30}\r\r.{30}\r.{24}ESN  {4}\rCO=.{5} PSAP \d{2} POS# \d{2}    {4}\r {20}\r {8}\r {19}\r {6}\rP#\(\d{3}\)\d{3}-\d{4}\r {30}\r[+-]\d{3}\.\d{6} [+-]\d{3}\.\d{6}\u0003$)"};

    std::regex UNION{R"(^\x02\d+\r\r                               \r\d{3}-\d{3}-\d{4}  \d{2}:\d{2}:\d{2} \d{8}\r.{31}\r.{27}(.{4})\r[^]*\x03$)"};
    std::regex nrf_no_response{R"(^\u0002\d+\r\d{10} NO RESPONSE FROM WIRE STEER\u0003$)"};
    std::regex hardin{R"(^\u0002\d+\r\(\d{3}\) \d{3}-\d{4}   \d{2}:\d{2}    \d{2}\/\d{2}\r.{82}.{2} (?:\d{3}|\d{2} ) (.{4})\r)"};
    std::regex putnam{R"(^\u0002\d+\r\(\d{3}\) \d{3}-\d{4} (.{4}) \d{2}\/\d{2} \d{2}:\d{2} .{28}\r)"};
    std::regex monroe{R"(^\u0002\d+ \d{2}\/\d{2}\/\d{4}  \d{2}:\d{2}:\d{2}\r \(\d{3}\) \d{3}-\d{4}  (.{4}))"};
    std::regex nrf_no_response_steer{R"(^\u0002\d+\r\d{10} NO RESPONSE FROM CELL STEER\u0003$)"};
    std::regex auglaize{R"(^\u0002\d+\r.{31}\r\d{3}-\d{3}-\d{4} \d{2}:\d{2}:\d\d? \d{2}\/\d{2}\/\d{4}\r.{31}\r.{27}(.{4})\r)"};
    std::regex carroll{R"(^\u0002\d+\r\d{3}-\d{3}-\d{4} \d{2}/\d{2} \d{2}:\d{2}:\d{2} (?:\d| )\d \r.{31}\r.{27}(.{4})\r)"};
    std::regex indigital{R"(^\u0002\d+\n    CBN \d{3}-\d{3}-\d{4} \d{3}-\d{3}-\d{4} .*\n.*\n.{29} (.{4}) .{40}\n)"};
    std::regex indigital_nospace{R"(^\u0002\d+\nCBN \d{3}-\d{3}-\d{4} \d{3}-\d{3}-\d{4} .*\n.*\n.{5} (.{4}) .{40}\n)"};
    std::regex nrf_lf{R"(^\u0002\d{13} No Record Found\u0003$)"};
    std::regex turner{R"(^\u0002\d+\r\(\d{3}\) \d{3}-\d{4}   \d{2}:\d{2}    \d{2}/\d{2}\r.{90} (.{4})\r)"};

    std::regex clay_ar{R"(^\u0002\d+\r\r.{11} ESN=\d{6} .{8}\r\(\d{3}\) \d{3}-\d{4}   \d{2}:\d{2} \d{2}/\d{2}/\d{2}\r.{13}\r.{31}\r.{20}\r.{31}\r.{31}\r\d{3}-\d{3}-\d{4}        (.{4})\r)"};

    impl() {
        known_cos["WPH1"] = "WPH1";
        known_cos["WPH2"] = "WPH2";
        known_cos["WRLS"] = "WRLS";
        known_cos["MOBL"] = "MOBL";

	    known_cos["RESD"] = "RESD";

	    known_cos["BUSN"] = "BUSN";
        known_cos["BUSX"] = "BUSX";
        known_cos["BSNX"] = "BSNX";

	    known_cos["VOIP"] = "VOIP";
        known_cos["VoIP"] = "VOIP";

	    known_cos["PBXB"] = "PBXB";
	    known_cos["CENT"] = "CENT";
	    known_cos["COIN"] = "COIN";
	    known_cos["PAY$"] = "PAY$";
    	known_cos["PBXR"] = "PBXR";

	    known_cos["RES"] = "RES";
        known_cos["BUS"] = "BUS";
        known_cos["CTX"] = "CTX";
        known_cos["PXB"] = "PXB";

	    known_cos["RESX"] = "RESX";
        known_cos["CN"] = "CN";
        known_cos["CNTX"] = "CNTX";

	    known_cos["PBXb"] = "PBXB"; // TODO: very likely an error, happens very commonly though
	    known_cos["NRF"] = "NRF";
	    known_cos["N/A"] = "Ali Error";
	    known_cos["RSDX"] = "RSDX";
    }
};

ali_parser::ali_parser(): impl_{new impl, [] (impl* i) { delete i; }} {
}

std::string ali_parser::parse_cos(const std::string& raw) const {
    std::set<std::string> matched_formats;

    auto do_cos = [&] (std::string cos) {
        boost::trim(cos);
        if (impl_->known_cos.count(cos)) {
            matched_formats.insert(cos);
        } else {
            // matched_formats.insert("Unknown");
            // throw std::runtime_error{"Unknown COS " + cos};
        }
    };

    std::smatch m;

    if (std::regex_search(raw, m, impl_->space_before)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->nrf_flat13)) {
        do_cos("NRF");
    }

    if (std::regex_search(raw, m, impl_->nrf_flat10)) {
        do_cos("NRF");
    }

    if (std::regex_search(raw, m, impl_->huron)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->nrf_hyphen)) {
        do_cos("NRF");
    }

    if (std::regex_search(raw, m, impl_->nrf_flat_lf_cr)) {
        do_cos("NRF");
    }

    if (std::regex_search(raw, m, impl_->erie)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->clay)) {
        std::smatch m2;
        if (std::regex_search(raw, m2, impl_->clay_wrls)) {
            do_cos("WRLS");
        } else {
            do_cos(m[1].str());
        }
    }

    if (std::regex_search(raw, m, impl_->UNION)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->nrf_no_response)) {
        do_cos("NRF");
    }

    if (std::regex_search(raw, m, impl_->hardin)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->putnam)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->monroe)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->nrf_no_response_steer)) {
        do_cos("NRF");
    }

    if (std::regex_search(raw, m, impl_->auglaize)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->carroll)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->indigital)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->indigital_nospace)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->nrf_lf)) {
        do_cos("NRF");
    }

    if (std::regex_search(raw, m, impl_->turner)) {
        do_cos(m[1].str());
    }

    if (std::regex_search(raw, m, impl_->clay_ar)) {
        do_cos(m[1].str());
    }

    if (matched_formats.size() == 1) {
        return *matched_formats.begin();
    } else {
        Json::Value val{raw};
        // TODO: better information
        // TODO: flag for debug info, since it's constant
        // std::cout << "unknown ali for " << val << std::endl;
        return "Unknown";
    }
}
