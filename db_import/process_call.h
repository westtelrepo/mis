#ifndef WESTTEL_MIS_PROCESS_CALL
#define WESTTEL_MIS_PROCESS_CALL

#include "call_record.h"
#include "log_util.h"
#include "parse_config.h"

#include <json/json.h>
#include <vector>

std::vector<CallRecord> process_call(std::vector<Json::Value> from, parse_config = parse_config{});

#endif
