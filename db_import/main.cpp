#include "LogStreamUnifier.h"
#include "db.h"
#include "process_call.h"
#include "log_converter.h"

#include <boost/program_options.hpp>
#include <fstream>
#include <json/json.h>
#include <list>
#include <pqxx/pqxx>

// database file generated by meson
extern const char database[];
extern const size_t database_len;

// build information generated by meson
extern const char* GIT_HASH;
extern const char* GIT_HASH_SHORT;
extern time_t BUILD_TIME;
extern const char* BUILD_LSB_DESC;
extern bool GIT_DIRTY;

// this follows the semantic versioning spec <https://semver.org/>
static std::string version() {
	std::array<char, 20> date_str;
	if (!strftime(date_str.data(), date_str.size(), "%Y%m%d", gmtime(&BUILD_TIME)))
		assert(false);
	return std::string{"1.1.32+"} + GIT_HASH_SHORT + (GIT_DIRTY ? "-dirty" : "") + "." + date_str.data();
}

int main(int argc, char** argv) {
    std::string database_str{database, database_len};

    std::vector<std::string> input_files;
    std::vector<std::string> input_file_list0;

    std::string controller;
    std::string parse_config_file;

    std::string write_log_utc_file;
    std::string write_call_utc_file;

    boost::program_options::options_description desc{"Options"};
    desc.add_options()
        ("controller", boost::program_options::value<std::string>(&controller), "Controller")
        ("help,h", "Help")
        ("input-file", boost::program_options::value<std::vector<std::string>>(&input_files), "Input File")
        ("input-file-list0", boost::program_options::value<std::vector<std::string>>(&input_file_list0), "Input File List (as from find -print0)")
        ("parse-config", boost::program_options::value<std::string>(&parse_config_file))
        ("init-schema", "Initialize Schema")
        ("import", "Import data")
        ("test", "Quick internal test")
        ("write-log-utc-file", boost::program_options::value<std::string>(&write_log_utc_file), "Write the latest log UTC to a file")
        ("write-call-utc-file", boost::program_options::value<std::string>(&write_call_utc_file), "Write the latest call UTC to a file")
        ("semver", "Print semantic version string")
        ("version,v", "Print version");

    boost::program_options::positional_options_description pos;
    pos.add("input-file", -1);

    boost::program_options::variables_map vm;
    boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).positional(pos).run(), vm);
    boost::program_options::notify(vm);

    if (vm.count("init-schema")) {
        pqxx::connection c;
        pqxx::work w{c};
        // this is a really dumb schema update mechanism, but it avoids conflicts
        // TODO: better schema update mechanism
        // this means that if schema changes we won't update it automatically
        pqxx::result r = w.exec("SELECT relname FROM pg_class WHERE relname = 'basic_call_info2'");
        if (r.size() == 0) {
            w.exec(database_str);
            w.commit();
            std::cout << "changed schema" << std::endl;
        } else {
            std::cout << "schema up to date" << std::endl;
        }
        return 0;
    } else if (vm.count("import")) {
        // drop down below
    } else if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    } else if (vm.count("test")) {
        std::cout << "Succeeded" << std::endl;
        return 0;
    } else if (vm.count("version")) {
		std::cout << "db_import version " << version() << std::endl;
		std::cout << std::endl;
		std::cout << "Build data:" << std::endl;
		std::cout << "LSB Description: " << BUILD_LSB_DESC << std::endl;

		std::cout << "Git hash: " << GIT_HASH;
		if (GIT_DIRTY)
			std::cout << " (dirty)";
		std::cout << std::endl;

		std::array<char, 100> date_str;
		if (strftime(date_str.data(), date_str.size(), "%c %Z(%z)", localtime(&BUILD_TIME)))
			std::cout << "Build date: " << date_str.data() << std::endl;

		// TODO: include version data of dependencies
		return 0;
    } else if (vm.count("semver")) {
        std::cout << version() << std::endl;
        return 0;
    } else {
        std::cerr << "db_import requires argument, try --help for help" << std::endl;
        return 1;
    }

    // read every "line" (null terminated) and add that file to be imported
    for (const std::string& file: input_file_list0) {
        std::ifstream i{file};
        i.exceptions(std::ifstream::badbit);

        std::string file_name;
        while(std::getline(i, file_name, '\0')) {
            input_files.push_back(file_name);
        }
    }

    if (input_files.size() == 0) {
        std::cerr << "Refusing to import 0 files" << std::endl;
        return 1;
    }

    parse_config config;
    if (parse_config_file != "") {
        std::ifstream i{parse_config_file};
        i.exceptions(std::ifstream::badbit | std::ifstream::eofbit | std::ifstream::failbit);
        std::stringstream buffer;
        buffer << i.rdbuf();
        config = parse_config::parse(buffer.str());
    }

    std::cout << "Starting " << controller << std::endl;

    std::map<std::string, LogStream> files;
    std::map<std::string, std::string> orig_name;
    for (auto file: input_files) {
        std::string f = westtel::util::str_basename(file);
        LogStream reader{std::unique_ptr<std::ifstream>{
            new std::ifstream(file)}, f};
        reader.set_preserve_newlines();
        files.emplace(f, std::move(reader));
        orig_name.emplace(f, file);
    }

    LogStreamUnifier unif{std::move(files)};

    db db_connection{controller};

    log_converter converter;
    converter.throw_exceptions_less();

    std::string greatest_log_utc = "1900-01-01";
    std::string greatest_call_utc = "1900-01-01";

    auto process_list = [&] (auto list) {
        for (auto& pair: list) {
            std::vector<CallRecord> calls;
            try {
                calls = process_call(pair.second, config);
            } catch(...) {
                std::cout << "exception parsing callid: " << pair.first << std::endl;
                throw;
            }
            for (auto& call: calls) {
                db_connection.add_call(call);
                greatest_call_utc = std::max(greatest_call_utc, call.utc);
            }
        }
    };

    std::list<std::map<std::string, std::vector<Json::Value>>> log_list;
    log_list.push_back(std::map<std::string, std::vector<Json::Value>>{});
    while (true) {
        auto log_vector = unif.process();
        if (!log_vector.size())
            break;

        for (auto& log_iter: log_vector) {
            greatest_log_utc = std::max(greatest_log_utc, log_iter.utc());

            Json::Value log = converter.convert(log_iter);
            assert(log["controller"] == controller);
            if (log.isMember("call") && log["call"].isMember("callid")) {
                for (auto list = log_list.begin(); list != log_list.end(); ++list) {
                    if (list->count(log["call"]["callid"].asString()) || list == --log_list.end()) {
                        (*list)[log["call"]["callid"].asString()].push_back(log);
                        break;
                    }
                }
            }
        }

        if (log_list.back().size() > 1000) {
            log_list.push_back(std::map<std::string, std::vector<Json::Value>>{});
        }

        if (log_list.size() > 3) {
            auto list = log_list.front();
            log_list.pop_front();

            process_list(list);
        }
    }

    for (auto& list: log_list) {
        process_list(list);
    }

    db_connection.join_and_commit();

    if (write_call_utc_file != "") {
        std::ofstream f{write_call_utc_file};
        f << greatest_call_utc << std::endl;
    }

    if (write_log_utc_file != "") {
        std::ofstream f{write_log_utc_file};
        f << greatest_log_utc << std::endl;
    }
}
