#include "process_call.h"

std::vector<CallRecord> process_call(std::vector<Json::Value> logs, const parse_config config) {
    if (logs.size() == 0)
        return std::vector<CallRecord>{};

    std::string controller = logs[0]["controller"].asString();

    for (auto& log: logs) {
        if (log.isMember("call") && log["call"].isMember("trunk")) {
            if (auto trunk = config.trunk(controller, log["call"]["trunk"].asInt())) {
                if (trunk->call_type == call_type::is_911) {
                    log["call"]["is_911"] = true;
                }
                // only set PSAP by trunk if it's the initial abandon of the call
                if (log.isMember("transfer") && log["transfer"].isMember("state") && log["transfer"].isMember("type") && log["transfer"]["state"] == "abandon" && log["transfer"]["type"] == "initial") {
                    log["user"]["psap"] = trunk->psap;
                }
            }
        }

        if (log.isMember("user") && log["user"].isMember("position")) {
            int position = log["user"]["position"].asInt();
            if (auto psap = config.position(controller, position)) {
                log["user"]["psap"] = psap.value();
            } else {
                log["user"]["psap"] = controller;
            }
        }
    }

    // "call" info
    std::string callid = logs[0]["call"]["callid"].asString();
    boost::optional<std::string> cos;
    boost::optional<int32_t> trunk;
    std::string ani;
    std::string pani;
    std::string utc = logs[0]["utc"].asString();
    bool is_911 = false;

    bool disconnect_valid = true;

    std::map<std::string, Json::Value> transfers;

    // destination -> count
    std::map<std::string, size_t> tids;
    boost::optional<std::string> last_tid;

    boost::optional<std::string> last_ng911_with_id_tid;

    std::set<std::string> tid_simul;

    for (size_t i = 0; i < logs.size(); i++) {
        if (logs[i]["call"]["is_911"].asBool())
            is_911 = true;

        if (logs[i]["ali"].isMember("cos"))
            cos = logs[i]["ali"]["cos"].asString();

        if (logs[i]["call"].isMember("trunk"))
            trunk = logs[i]["call"]["trunk"].asInt();

        ani = logs[i]["call"]["ani"].asString();
        pani = logs[i]["call"]["ani"].asString();

        if (logs[i].isMember("transfer") && !logs[i]["transfer"].isMember("type")) {
            throw std::runtime_error{"transfer needs to have type field"};
        }

        // only add disconnect_utc if an initial transfer has already been found
        if (transfers.count("initial") && logs[i].isMember("code") && logs[i]["code"] == "call/disconnect/") {
            transfers["initial"]["disconnect_utc"] = logs[i]["utc"];
        }

        if (logs[i].isMember("code") && logs[i]["code"] == "call/warning/forced_disconnect/") {
            disconnect_valid = false;
        }

        if (logs[i].isMember("transfer") && logs[i]["transfer"].isMember("type")) {
            const std::string type = logs[i]["transfer"]["type"].asString();
            std::string tid;

            std::string dest = logs[i]["transfer"].isMember("destination") ? logs[i]["transfer"]["destination"].asString() : "";

            if (type == "initial") {
                tid = "initial";
            } else if (logs[i]["transfer"].isMember("id")) {
                tid = logs[i]["transfer"]["id"].asString();
                if (logs[i]["transfer"]["type"].asString() == "ng911") {
                    last_ng911_with_id_tid = tid;
                }
            } else if (logs[i]["transfer"].isMember("_match_with_id")) {
                tid = last_ng911_with_id_tid.value();
            } else if (logs[i]["transfer"]["state"] == "dial") {
                tid = type + "_" + std::to_string(++tids[dest]) + "_" + dest;
            } else {
                if (type == "*") {
                    // if type is unknown (star) use the last tid
                    // will throw exception if there is no last tid
                    tid = last_tid.value();
                } else {
                    tid = type + "_" + std::to_string(tids[dest]) + "_" + dest;
                }
            }

            last_tid = tid;

            if (!transfers.count(tid)) {
                transfers[tid]["abandoned"] = false;
                transfers[tid]["tid"] = tid;
                tid_simul.insert(tid);
            }

            // TODO: warn/error if callee already exists
            if (logs[i]["transfer"]["state"] == "dial") {
                transfers[tid]["utc"] = logs[i]["utc"];
                transfers[tid]["caller"] = logs[i]["user"];
                transfers[tid]["event_count"] = transfers[tid]["event_count"].asInt() + 1;
                if (logs[i]["transfer"]["no_callee_is_abandoned"].asBool()) {
                    transfers[tid]["no_callee_is_abandoned"] = true;
                }
            } else if (!transfers[tid].isMember("callee") && logs[i]["transfer"]["state"] == "join") {
                transfers[tid]["answer_time"] = logs[i]["utc"];
                transfers[tid]["callee"] = logs[i]["user"];
                transfers[tid]["event_count"] = transfers[tid]["event_count"].asInt() + 1;
                transfers[tid]["less_good_utc"] = logs[i]["utc"];
                transfers[tid]["callee_user_positions"] = logs[i]["user_positions"];
                tid_simul.erase(tid);
            } else if (!transfers[tid].isMember("callee") && logs[i]["transfer"]["state"] == "abandon") {
                transfers[tid]["abandoned"] = true;
                transfers[tid]["callee"] = logs[i]["user"];
                transfers[tid]["event_count"] = transfers[tid]["event_count"].asInt() + 1;
                transfers[tid]["less_good_utc"] = logs[i]["utc"];
                tid_simul.erase(tid);
            }

            if (logs[i]["transfer"]["type"].asString() != "*") {
                transfers[tid]["type"] = logs[i]["transfer"]["type"];
            }

            if (logs[i].isMember("transfer") && logs[i]["transfer"].isMember("destination"))
                transfers[tid]["destination"] = logs[i]["transfer"]["destination"];

            if (logs[i]["utc"].asString() > "2017-01-01" && tid_simul.size() > 1) {
                std::string t;
                for (auto x: tid_simul) {
                    t += x + " ";
                }
                //std::cout << logs[i]["utc"].asString() << " " << callid << " has simultaneuous transfers: " << t << std::endl;
            }
        }
    }

    std::vector<CallRecord> ret;

    std::vector<Json::Value> transfer_vector;
    for (auto& x: transfers) {
        if (!x.second.isMember("utc")) {
            x.second["utc"] = x.second["less_good_utc"].asString();
        }
        transfer_vector.push_back(x.second);
    }

    std::sort(transfer_vector.begin(), transfer_vector.end(), [] (const Json::Value& a, const Json::Value& b) {
        return a["utc"] < b["utc"];
    });

    auto enterprise_fixup = [&] (Json::Value& transfer) {
        if (transfer.isMember("destination")) {
            auto dialplan = config.dialplan(controller, transfer["destination"].asString());
            if (dialplan) {
                transfer["callee"]["psap"] = dialplan->psap;
                if (!transfer["abandoned"].asBool() && dialplan->positions.size() == 1) {
                    int position = *dialplan->positions.begin();
                    transfer["callee"]["position"] = position;

                    if (transfer.isMember("callee_user_positions") && transfer["callee_user_positions"].isMember(std::to_string(position))) {
                        transfer["callee"]["name"] = transfer["callee_user_positions"][std::to_string(position)].asString();
                    }
                }
            }
        }
    };

    for (auto& x: transfer_vector) {
        bool may_be_wrong_outbound = false;
        if (x["callee"].isMember("may_be_wrong_outbound"))
            may_be_wrong_outbound = x["callee"]["may_be_wrong_outbound"].asBool();

        if (x["caller"].isMember("psap") && may_be_wrong_outbound) {
            x["callee"].removeMember("psap");
            x["callee"].removeMember("position");
            x["callee"].removeMember("name");
        }

        bool wrong_if_same_position = false;
        if (x["callee"].isMember("wrong_if_same_position")) {
            wrong_if_same_position = x["callee"]["wrong_if_same_position"].asBool();
        }

        if (wrong_if_same_position && x["caller"].isMember("position") && x["callee"].isMember("position") && x["caller"]["position"].asInt() == x["callee"]["position"].asInt()) {
            x["callee"].removeMember("position");
            x["callee"].removeMember("psap");
            x["callee"].removeMember("name");
        }

        if (!x["callee"] && x["no_callee_is_abandoned"].asBool()) {
            x["abandoned"] = true;
        }

        if (!x["callee"].isMember("position") && !x["callee"].isMember("psap")) {
            enterprise_fixup(x);
        }
    }

    int32_t subid = 0;

    for (auto iter = transfer_vector.begin(); iter != transfer_vector.end(); ++iter) {
        const auto& t = *iter;

        if (!t.isMember("type"))
            throw std::runtime_error{"No transfer type in call " + callid};

        if (t.isMember("callee")
            && t.isMember("caller")) {

            Json::Value caller = t["caller"];
            Json::Value callee = t["callee"];

            boost::optional<std::string> caller_psap;
            boost::optional<std::string> callee_psap;

            if (caller.isMember("psap")) {
                caller_psap = caller["psap"].asString();
            }

            if (callee.isMember("psap")) {
                callee_psap = callee["psap"].asString();
            }

            transfer_type t_type = transfer_type_from_string(t["type"].asString());

            {
                // debug sanity test, people can't call themselves
                boost::optional<int> caller_position;
                boost::optional<int> callee_position;

                if (caller.isMember("position")) {
                    caller_position = caller["position"].asInt();
                }

                if (callee.isMember("position")) {
                    callee_position = callee["position"].asInt();
                }

                if (t_type != transfer_type::ng911 && t_type != transfer_type::blind && caller_position && callee_position && caller_position == callee_position) {
                    //throw std::runtime_error{"Caller and callee position can't be equal!"};
                    //std::cout << "Caller and callee position equal: " << callid << " at " << t["utc"] << std::endl;
                }
            }

            if (caller_psap == callee_psap)
                continue;

            if (caller_psap && t_type == transfer_type::initial) {
                // this is an outgoing call for this psap
                CallRecord record{};
                record.incoming = false;

                record.subid = subid++;

                record.callid = callid;
                record.controller = controller;
                record.psap = caller_psap.value();
                record.utc = t["utc"].asString();
                if (t.isMember("answer_time")) {
                    record.answer_utc = t["answer_time"].asString();
                }
                record.abandoned = t["abandoned"].asBool();
                if (caller.isMember("position")) {
                    record.pos = caller["position"].asInt();
                }
                if (caller.isMember("name")) {
                    record.calltaker = caller["name"].asString();
                }
                record.is_911 = is_911;
                record.cos = cos;
                record.trunk = trunk;
                record.transfer_reaction = transfer_type::none;
                record.t_type = t_type;

                ret.push_back(record);
            }

            if (callee_psap) {
                // this is an incoming call for this psap
                CallRecord record{};
                record.incoming = true;

                record.subid = subid++;

                record.callid = callid;
                record.controller = controller;
                record.psap = callee_psap.value();
                record.utc = t["utc"].asString();
                if (t.isMember("answer_time")) {
                    record.answer_utc = t["answer_time"].asString();
                }
                record.abandoned = t["abandoned"].asBool();
                if (callee.isMember("position")) {
                    record.pos = callee["position"].asInt();
                }
                if (callee.isMember("name")) {
                    record.calltaker = callee["name"].asString();
                }
                record.is_911 = is_911;
                record.cos = cos;
                record.trunk = trunk;

                // abandoned calls don't have transfer reactions
                if (!record.abandoned) {
                    // iter is never .end() here, so std::next(iter) is always valid
                    for (auto future = std::next(iter); future != transfer_vector.end(); ++future) {
                        boost::optional<std::string> future_callee_psap;
                        if ((*future)["callee"].isMember("psap")) {
                            future_callee_psap = (*future)["callee"]["psap"].asString();
                        }

                        boost::optional<std::string> future_caller_psap;
                        if ((*future)["caller"].isMember("psap")) {
                            future_caller_psap = (*future)["caller"]["psap"].asString();
                        }

                        if ((*future)["abandoned"] == false && future_caller_psap == callee_psap && future_callee_psap != callee_psap) {
                            record.transfer_reaction = transfer_type_from_string((*future)["type"].asString());
                            break;
                        }
                    }
                }

                record.t_type = t_type;

                if (disconnect_valid && t.isMember("disconnect_utc")) {
                    record.disconnect_utc = t["disconnect_utc"].asString();
                }

                ret.push_back(record);
            }
        }
    }

    // test for some things that should never happen, ever
    for (const auto& x: ret) {
        if (x.abandoned && x.incoming) {
            if (x.pos) {
                throw std::runtime_error{"Abandoned call has position!"};
            }
        }

        if (x.abandoned) {
            if (x.answer_utc) {
                throw std::runtime_error{"Abandoned call has answer time!"};
            }

            if (x.transfer_reaction != transfer_type::none) {
                throw std::runtime_error{"Abandoned/failed call has transfer_reaction!"};
            }
        }

        if (x.calltaker) {
            if (!x.pos) {
                throw std::runtime_error{"Call with calltaker doesn't have position!"};
            }
        }

        if (x.cos) {
            if (!x.is_911) {
                throw std::runtime_error{"non-911 call has COS!"};
            }
        }

        if (x.t_type == transfer_type::none) {
            throw std::runtime_error{"transfer_type can't be none!"};
        }
    }

    return ret;
}
