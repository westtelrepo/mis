#!/bin/bash

echo "#include <cstddef>" > "$2"
echo "extern const char $3[]; const char $3[] = {" >> "$2"
xxd -i < "$1" >> "$2";
echo "}; extern const size_t $3_len; const size_t $3_len = sizeof $3;" >> "$2";
