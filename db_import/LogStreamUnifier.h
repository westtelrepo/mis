#ifndef WESTTEL_MIS_LOGSTREAM_UNIFIER
#define WESTTEL_MIS_LOGSTREAM_UNIFIER

#include <map>
#include <memory>
#include <vector>

#include "LogStream.h"

class LogStreamUnifier {
private:
    struct impl;
    std::unique_ptr<impl, void(*)(impl*)> impl_;
public:
    LogStreamUnifier(std::map<std::string, LogStream>&& logs);

    std::vector<OldLog> process();
};

#endif
