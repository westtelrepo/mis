#ifndef WESTTEL_MIS_ALI
#define WESTTEL_MIS_ALI

#include <memory>

class ali_parser {
private:
    struct impl;
    std::unique_ptr<impl, void(*)(impl*)> impl_;
public:
    ali_parser();

    std::string parse_cos(const std::string& raw) const;
};

#endif
