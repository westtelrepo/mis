#include "parse_config.h"

#include <yaml-cpp/yaml.h>

#include <iostream>

parse_config parse_config::parse(const std::string& file_contents) {
    YAML::Node node = YAML::Load(file_contents);

    parse_config ret;

    for (const auto& controller_entry: node) {
        const std::string controller = controller_entry["controller"].as<std::string>();

        // psap -> position set
        std::map<std::string, std::set<int>> psap_positions;
        // position -> psap
        std::map<int, std::string> position_psap;

        for (const auto& psap: controller_entry["psaps"]) {
            const std::string psap_id = psap["id"].as<std::string>();

            std::set<int> positions;
            for (const auto& position: psap["positions"]) {
                const int pos = position.as<int>();
                positions.insert(pos);
                ret.controller_positions[controller][pos] = psap_id;
                position_psap[pos] = psap_id;
            }

            psap_positions[psap_id] = positions;

            for (const auto& trunk: psap["trunks"]) {
                ret.controller_trunks[controller][trunk.as<int>()].psap = psap_id;
            }

            for (const auto& trunk: psap["911_trunks"]) {
                trunk_config c;
                c.psap = psap_id;
                c.call_type = call_type::is_911;
                ret.controller_trunks[controller][trunk.as<int>()] = c;
            }

            for (const auto& trunk: psap["admin_trunks"]) {
                trunk_config c;
                c.psap = psap_id;
                c.call_type = call_type::admin;
                ret.controller_trunks[controller][trunk.as<int>()] = c;
            }
        }

        for (const auto& dialplan: controller_entry["dialplan"]) {
            const std::string num = dialplan["num"].as<std::string>();
            dialplan_config c;

            for (const auto& position: dialplan["positions"]) {
                const int pos = position.as<int>();
                c.positions.insert(pos);
                c.psap = position_psap[pos];
            }

            if (auto psap = dialplan["psap"]) {
                c.psap = psap.as<std::string>();
                if (c.positions.empty()) {
                    c.positions = psap_positions[c.psap];
                }
            }

            ret.controller_dialplan[controller][num] = c;
        }

    }

    return ret;
}
