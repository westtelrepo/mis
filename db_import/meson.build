project('db_import', 'cpp', default_options: ['cpp_std=c++14'])

if meson.get_compiler('cpp').get_id() == 'gcc'
    add_global_arguments('-fpermissive', language: 'cpp')
elif meson.get_compiler('cpp').get_id() == 'clang'

endif

add_global_arguments(['-fno-strict-aliasing', '-fwrapv'], language: 'cpp')

# put all normal source files here *except* main.cpp
source = [
    'LogStream.cpp',
    'LogStreamUnifier.cpp',
    'ali.cpp',
    'db.cpp',
    'log_converter.cpp',
    'parse_config.cpp',
    'process_call.cpp',
]

# put all test source files here
test_source = [
    'tests/LogStream.test.cpp',
    'tests/LogStreamUnifier.test.cpp',
    'tests/ali.test.cpp',
    'tests/log_converter.test.cpp',
    'tests/logstream.line.cpp',
    'tests/main.test.cpp',
    'tests/parse_angle_form.test.cpp',
    'tests/parse_config.test.cpp',
    'tests/process_call.ali.test.cpp',
    'tests/process_call.basic.enterprise.test.cpp',
    'tests/process_call.basic_outbound.test.cpp',
    'tests/process_call.blind.cpp',
    'tests/process_call.gui_transfer.fail.test.cpp',
    'tests/process_call.gui_transfer.test.cpp',
    'tests/process_call.manualbid.test.cpp',
    'tests/process_call.misc.cpp',
    'tests/process_call.ng911.fail.cpp',
    'tests/process_call.ng911_transfer.cpp',
    'tests/process_call.phone_transfer.fail.test.cpp',
    'tests/process_call.phone_transfer.test.cpp',
    'tests/process_call.test.cpp',
    'tests/process_call.valet.cpp',
]

dependencies = [
    dependency('boost', modules: ['program_options']),
    dependency('jsoncpp'),
    dependency('libpqxx'),
    dependency('threads'),
    dependency('yaml-cpp'),
]

embed = find_program('scripts/embed.sh')

embedded_database = custom_target('embed_database',
    input: [ 'database.pgsql' ],
    output: [ 'database.cpp' ],
    command: [ embed, '@INPUT@', '@OUTPUT@', 'database' ]
)

make_git = find_program('scripts/make_config_file.sh')

git_c = custom_target('git',
    build_always: true,
    output: 'git.cpp',
    command: [make_git, '@OUTPUT@', meson.current_source_dir()])

db_import = executable('db_import', ['main.cpp'] + source, embedded_database, git_c, dependencies: dependencies)

db_import_test = executable('db_import-test', source + test_source, dependencies: dependencies)

test('catch2-test', db_import_test)

run_target('needed', command: ['scripts/needed.sh', db_import])
run_target('todo', command: ['scripts/todo.sh'])