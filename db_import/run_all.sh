#!/bin/bash

pushd /vagrant/tmp_new/
export PSAPS=$(find | cut -b 3-10 | sort | uniq)

echo $PSAPS

time parallel -i bash -c "( time /home/vagrant/db_import/db_import --import --controller={} --parse-config=/vagrant/db_import/enterprise_config.yml --input-file-list0=<(find /vagrant/tmp_new/ -name '{}*' -print0) ) |& ts \"{} %H:%M:%S | \"" -- $PSAPS

popd

psql -c 'VACUUM'
