#include "db.h"

#include <boost/lockfree/spsc_queue.hpp>
#include <json/json.h>
#include <pqxx/pqxx>
#include <thread>

struct db::impl {
    std::string controller;
    boost::lockfree::spsc_queue<boost::optional<CallRecord>, boost::lockfree::capacity<16384>> to_insert;
    std::atomic<bool> die{false};
    std::thread thread;

    void thread_func() {
        pqxx::connection c{"application_name=db_import"};

        c.prepare("insert", R"(
INSERT INTO basic_call_info2(callid, controller, subid, psap, utc, answer, abandoned, pos, calltaker, is_911, cos, trunk, incoming, transfer_reaction, transfer_type, call_length)
VALUES(
    json_array_elements_text($1::json),
    json_array_elements_text($2::json),
    json_array_elements_text($3::json)::int,
    json_array_elements_text($4),
    json_array_elements_text($5)::timestamptz,
    json_array_elements_text($6)::timestamptz - json_array_elements_text($5)::timestamptz,
    json_array_elements_text($7)::boolean,
    json_array_elements_text($8)::int,
    json_array_elements_text($9),
    json_array_elements_text($10)::boolean,
    json_array_elements_text($11),
    json_array_elements_text($12)::int,
    json_array_elements_text($13)::boolean,
    json_array_elements_text($14),
    json_array_elements_text($15),
    json_array_elements_text($16)::timestamptz - json_array_elements_text($6)::timestamptz
)
)");

        pqxx::work w{c};

        // let's not assume this
        w.exec("SET LOCAL TIME ZONE 'UTC'");

        w.parameterized("SELECT pg_advisory_xact_lock(hashtext($1))")
            ("8cb7b9ca-3269-472d-816f-3212875b9ab2 " + controller)
            .exec();
        w.parameterized("DELETE FROM basic_call_info2 WHERE controller = $1")
            (controller)
            .exec();

        Json::FastWriter writer;

        bool done = false;
        while (!done) {
            if (die.load()) {
                return;
            }

            std::array<boost::optional<CallRecord>, 128> buffer;
            size_t len = to_insert.pop(buffer.data(), buffer.size());
            if (len == 0) {
                usleep(1000);
                continue;
            }

            Json::Value callid_arr{Json::arrayValue};
            Json::Value controller_arr{Json::arrayValue};
            Json::Value subid_arr{Json::arrayValue};
            Json::Value psap_arr{Json::arrayValue};
            Json::Value utc_arr{Json::arrayValue};
            Json::Value answer_arr{Json::arrayValue};
            Json::Value abandoned_arr{Json::arrayValue};
            Json::Value pos_arr{Json::arrayValue};
            Json::Value calltaker_arr{Json::arrayValue};
            Json::Value is_911_arr{Json::arrayValue};
            Json::Value cos_arr{Json::arrayValue};
            Json::Value trunk_arr{Json::arrayValue};
            Json::Value incoming_arr{Json::arrayValue};
            Json::Value transfer_reaction_arr{Json::arrayValue};
            Json::Value transfer_type_arr{Json::arrayValue};
            Json::Value call_length_arr{Json::arrayValue};

            for (size_t i = 0; i < len; i++) {
                if (!buffer[i]) {
                    done = true;
                    break;
                }

                callid_arr.append(buffer[i]->callid);
                controller_arr.append(buffer[i]->controller);
                subid_arr.append(buffer[i]->subid);
                if (buffer[i]->psap)
                    psap_arr.append(buffer[i]->psap.value());
                else
                    psap_arr.append(Json::nullValue);
                utc_arr.append(buffer[i]->utc);
                if (buffer[i]->answer_utc)
                    answer_arr.append(buffer[i]->answer_utc.value());
                else
                    answer_arr.append(Json::nullValue);
                abandoned_arr.append(buffer[i]->abandoned);
                if (buffer[i]->pos)
                    pos_arr.append(buffer[i]->pos.value());
                else
                    pos_arr.append(Json::nullValue);
                if (buffer[i]->calltaker)
                    calltaker_arr.append(buffer[i]->calltaker.value());
                else
                    calltaker_arr.append(Json::nullValue);
                is_911_arr.append(buffer[i]->is_911);
                if (buffer[i]->cos)
                    cos_arr.append(buffer[i]->cos.value());
                else
                    cos_arr.append(Json::nullValue);
                if (buffer[i]->trunk)
                    trunk_arr.append(buffer[i]->trunk.value());
                else
                    trunk_arr.append(Json::nullValue);
                incoming_arr.append(buffer[i]->incoming);
                transfer_reaction_arr.append(to_string(buffer[i]->transfer_reaction));
                transfer_type_arr.append(to_string(buffer[i]->t_type));
                if (buffer[i]->disconnect_utc)
                    call_length_arr.append(buffer[i]->disconnect_utc.value());
                else
                    call_length_arr.append(Json::nullValue);
            }

            w.prepared("insert")
                (writer.write(callid_arr))
                (writer.write(controller_arr))
                (writer.write(subid_arr))
                (writer.write(psap_arr))
                (writer.write(utc_arr))
                (writer.write(answer_arr))
                (writer.write(abandoned_arr))
                (writer.write(pos_arr))
                (writer.write(calltaker_arr))
                (writer.write(is_911_arr))
                (writer.write(cos_arr))
                (writer.write(trunk_arr))
                (writer.write(incoming_arr))
                (writer.write(transfer_reaction_arr))
                (writer.write(transfer_type_arr))
                (writer.write(call_length_arr))
                .exec();
        }

        w.commit();
    }
};

db::db(std::string controller): impl_{new impl{}, [] (impl* i) { delete i; }} {
    impl_->controller = controller;

    impl_->thread = std::thread{[this] () { impl_->thread_func(); }};
}

db::~db() {
    if (impl_->thread.joinable()) {
        impl_->die.store(true);
        impl_->thread.join();
    }
}

void db::add_call(CallRecord call) {
    while(!impl_->to_insert.push(call))
        usleep(100);
}

void db::join_and_commit() {
    while(!impl_->to_insert.push(boost::optional<CallRecord>{}))
        usleep(100);
    impl_->thread.join();
}
