#ifndef e0a78d0d_48d7_43ff_bc66_b9262b715582
#define e0a78d0d_48d7_43ff_bc66_b9262b715582

#include <pqxx/pqxx>

// should only be (successfully) called once per session (NOT transaction!)
// i.e. if you call this on a transaction and it commits, these objects
// will be available for the rest of the session
// (though temporary tables will be emptied -- ON COMMIT DELETE ROWS)
inline void init_sql(pqxx::transaction_base& w) {
/*********************** UTILITY FUNCTIONS *********************************/
	// second object has priority
	// TODO: replace, PostgreSQL 9.5 has similar functionality builtin
w.exec(R"pgsql(CREATE FUNCTION pg_temp.wjson_object_merge(a JSON, b JSON) RETURNS json LANGUAGE plv8 IMMUTABLE AS $$
	var ret = {};
	for (var name in a) {
		if (Object.prototype.hasOwnProperty.call(a, name))
			ret[name] = a[name];
	}
	for (var name in b) {
		if (Object.prototype.hasOwnProperty.call(b, name))
			ret[name] = b[name];
	}
	return ret;
$$)pgsql", "FUNC wjson_object_merge");

	// TODO: same
w.exec(R"pgsql(CREATE FUNCTION pg_temp.wjson_del_props(obj_ JSON, vals_ JSON) RETURNS json LANGUAGE plv8 AS $$
	for (var i = 0; i < vals_.length; i++)
		obj_[vals_[i]] = undefined;
	return obj_;
$$ IMMUTABLE)pgsql", "FUNC wjson_del_props");

	// TODO: same
w.exec(R"pgsql(CREATE FUNCTION pg_temp.wjson_add_prop(obj_ json, attr_ TEXT, val_ JSON) RETURNS json LANGUAGE plv8 AS $$
	obj_[attr_] = val_;
	return obj_;
$$ IMMUTABLE STRICT)pgsql", "FUNC wjson_add_prop");

/************************ OLDLOG TO NEWLOG *********************************/
// better idea: only keep track for log w/ position or user information
// maybe even only when it differs from existing?
/*w.exec(R"pgsql(CREATE TEMPORARY TABLE controller_oldlog_min_max (
	controller TEXT PRIMARY KEY,
	min_utc TIMESTAMPTZ NOT NULL,
	max_utc TIMESTAMPTZ NOT NULL
) ON COMMIT DELETE ROWS)pgsql", "TABLE controller_oldlog_min_max");*/

w.exec(R"pgsql(CREATE TEMPORARY TABLE oldlog_callids_to_fix (
	callid UUID PRIMARY KEY
) ON COMMIT DELETE ROWS)pgsql", "TABLE oldlog_callids_to_fix");

w.exec(R"pgsql(CREATE TEMPORARY TABLE oldlog_callids_to_redo (
	callid UUID PRIMARY KEY
) ON COMMIT DELETE ROWS)pgsql", "TABLE oldlog_callids_to_redo");

/*w.exec(R"pgsql(CREATE TEMPORARY TABLE oldlog_temp (LIKE oldlog) ON COMMIT DELETE ROWS)pgsql", "TABLE oldlog_temp");
w.exec("ALTER TABLE oldlog_temp DROP COLUMN id");*/

w.exec(R"(CREATE FUNCTION pg_temp.insert_oldlog(
	controller_ TEXT, utc_ TIMESTAMPTZ, code_ TEXT, callid_ TEXT,
	trunk_ INT, pos_ INT, ani_ TEXT, pani_ TEXT, thread_ TEXT,
	message_ TEXT)
RETURNS void LANGUAGE plpgsql AS $$
DECLARE
	new_id UUID;
	the_new_log JSONB;
BEGIN
	SELECT INTO new_id uuid_generate_v5('cfb7b16f-c134-4af5-8cec-bbb1a62c3881',          
                controller_ || ' | '                                             
                || TO_CHAR(utc_, 'YYYY-MM-DD HH:DD:SS.US') || ' | '              
                || code_ || ' | '                                                
                || COALESCE(callid_, '') || ' | '                                
                || COALESCE(trunk_::text, '') || ' | '                           
                || COALESCE(pos_::text, '') || ' | '                             
                || COALESCE(ani_, '') || ' | '                                   
                || COALESCE(pani_, '') || ' | '                                  
                || COALESCE(thread_, '') || ' | '                                
                || message_);

	IF NOT EXISTS (SELECT 1 FROM oldlog WHERE id = new_id) THEN
		INSERT INTO oldlog(id, controller, utc, code, callid, trunk, pos, ani, pani, thread, message)
		VALUES(new_id, controller_, utc_, code_, callid_, trunk_, pos_, ani_, pani_, thread_, message_);

		-- TODO: replace with upsert (PostgreSQL 9.5)
		-- IF EXISTS (SELECT 1 FROM controller_oldlog_min_max WHERE controller = controller_) THEN
		-- 	UPDATE controller_oldlog_min_max SET
		-- 		min_utc = LEAST(min_utc, utc_),
		-- 		max_utc = GREATEST(max_utc, utc_)
		-- 		WHERE controller = controller_;
		-- ELSE
		-- 	INSERT INTO controller_oldlog_min_max(controller, min_utc, max_utc)
		-- 		VALUES(controller_, utc_, utc_);
		-- END IF;

		DELETE FROM newlog WHERE id = new_id;
		SELECT INTO the_new_log pg_temp.parse_oldlog(new_id, controller_, utc_, code_,
			callid_, trunk_, pos_, ani_, pani_, thread_,
			message_);
		IF the_new_log IS NOT NULL THEN
			INSERT INTO newlog (id, controller, utc, code, orig, data)
				SELECT (the_new_log->>'id')::uuid, the_new_log->>'controller',
				       (the_new_log->>'utc')::timestamptz, the_new_log->>'code',
				       pg_temp.wjson_del_props(the_new_log::json, '["id","controller","utc","code"]')::jsonb,
				       NULL;
		END IF;
	END IF;
END;$$;)", "FUNC insert_oldlog");

w.exec(R"(CREATE FUNCTION pg_temp.redo_oldlog(id_ UUID) RETURNS void LANGUAGE plpgsql AS $$
DECLARE
	-- var_controller TEXT;
	-- var_utc TIMESTAMPTZ;
	the_new_log JSONB;
BEGIN
	-- SELECT controller INTO STRICT var_controller FROM oldlog WHERE id = id_;
	-- SELECT utc INTO STRICT var_utc FROM oldlog WHERE id = id_;
	-- IF EXISTS (SELECT 1 FROM controller_oldlog_min_max WHERE controller = var_controller) THEN
	-- 	UPDATE controller_oldlog_min_max SET
	-- 		min_utc = LEAST(min_utc, var_utc),
	-- 		max_utc = GREATEST(max_utc, var_utc)
	-- 		WHERE controller = var_controller;
	-- ELSE
	-- 	INSERT INTO controller_oldlog_min_max(controller, min_utc, max_utc)
	-- 		VALUES(var_controller, var_utc, var_utc);
	-- END IF;

	DELETE FROM newlog WHERE id = id_;
	SELECT INTO the_new_log pg_temp.parse_oldlog(id, controller, utc, code,
		callid, trunk, pos, ani, pani, thread,
		message) FROM oldlog WHERE id = id_;
	IF the_new_log IS NOT NULL THEN
		INSERT INTO newlog (id, controller, utc, code, orig, data)
			SELECT (the_new_log->>'id')::uuid, the_new_log->>'controller',
			       (the_new_log->>'utc')::timestamptz, the_new_log->>'code',
			       pg_temp.wjson_del_props(the_new_log::json, '["id","controller","utc","code"]')::jsonb,
			       NULL;
	END IF;
	
END;$$)", "FUNC redo_oldlog");

w.exec(R"(CREATE FUNCTION pg_temp.upsert_oldlog_files_processed(
	filename_ TEXT, controller_ TEXT, mtime_ TIMESTAMPTZ, size_ BIGINT)
RETURNS void LANGUAGE plpgsql AS $$
BEGIN
	-- TODO: replace with true upsert (PostgreSQL 9.5)
	DELETE FROM oldlog_files_processed WHERE filename = filename_;
	INSERT INTO oldlog_files_processed(filename, controller, mtime, size) VALUES(filename_, controller_, mtime_, size_);
END;
$$)", "FUNC upsert_oldlog_files_processed");

w.exec(R"pgsql(CREATE FUNCTION pg_temp.parse_oldlog(
	id_ UUID, controller_ TEXT, utc_ TIMESTAMPTZ, code_ TEXT,
	callid_ TEXT, trunk_ INT, pos_ INT, ani_ TEXT, pani_ TEXT,
	thread_ TEXT, message_ TEXT)
RETURNS json LANGUAGE plv8 AS $$
	'use strict';
	var log = {id: id_, controller: controller_, utc: utc_, code: 'unhandled ' + code_};
	if (thread_)
		log.thread = thread_;
	
	if (pos_ !== null && pos_ !== 0) {
		if (log.user === undefined) log.user = {};
		log.user.position = pos_;
	}
	
	if (callid_ !== null || trunk_ !== null || ani_ !== null || pani_ !== null) {
		if (log.call === undefined) log.call = {};
		if (callid_ !== null) {
			const uuid_cache_input_prop = 'MB1WTkfxU2D1pXRFyVxLnFkxvqjswZOv';
			const uuid_cache_output_prop = 'b-j0eIAcv6VXCula9ok1AS0ZOVVHMIcd';
			log.call.callid = plv8.execute(
				"SELECT uuid_generate_v5('fde045fd-a312-41ab-956a-9cd123b336b3', $1::text) AS a",
				[controller_ + ' ' + callid_])[0].a;
			if (log.oldlog === undefined) log.oldlog = {};
			log.oldlog.callid = callid_;
		}
		if (trunk_ !== null) {
			log.call.trunk = trunk_;
		}

		if (ani_ === '9999999999') log.call.is_911 = true;
		else if (ani_ === '1111111111') log.call.is_911 = false;

		if (pani_ === 'p9999999999') log.call.is_911 = true;
		if (pani_ === 'p0000000000') log.call.is_911 = true;

		if (ani_ !== null) log.call.ani = ani_;
		if (pani_ !== null) log.call.pani = pani_;
	}

	const already_inserted_prop = 'vLO6jnEb6UyCDP36dfR9q6ipl_m5YTWx';
	if ('call' in log && 'callid' in log.call && plv8[already_inserted_prop] !== log.call.callid) {
		plv8[already_inserted_prop] = log.call.callid;
		const n = plv8.execute("DELETE FROM oldlog_callids_to_fix WHERE callid = $1", [log.call.callid]);
		if (n === 0 && plv8.execute("SELECT call_id FROM newlog WHERE call_id = $1 AND controller = $2 LIMIT 1", [log.call.callid, controller_]).length > 0)
			plv8.execute('INSERT INTO oldlog_callids_to_redo(callid) VALUES($1)', [log.call.callid]);
		plv8.execute("INSERT INTO oldlog_callids_to_fix(callid) VALUES($1)", [log.call.callid]);
	}

	// WISHLIST: better understandable code. State-machine-ish?
	function parse_angle_form(str) {
		var ret = '';
		var i = 0;

		outer_loop:
		for (; i < str.length; i++) {
			if (str[i] === '<') {
				var code = '';
				i++;
				while(true) {
					if (i >= str.length) {
						ret += '<' + code;
						break outer_loop; // might as well break, string is done
					} else if (str[i] === '<') {
						// retry code processing on new '<'
						i--;
						ret += '<' + code;
						continue outer_loop;
					} else if (str[i] === '>')
						break;
					code += str[i];
					i++;
				}
				if (code === 'STX')
					ret += '\u0002';
				else if (code === 'ETX')
					ret += '\u0003';
				else if (code === 'CR')
					ret += '\r';
				else if (code === 'LF')
					ret += '\n';
				else {
					var ch = Number(code);
					if (ch === 0 || (ch > 127 && ch <= 255))
						ret += String.fromCharCode(0xE000 + ch);
					else if (ch > 0 && ch <= 127)
						String.fromCharCode(ch);
					else {
						ret += '<' + code + '>';
					}
				}
			} else
				ret += str[i];
		}
		return ret;
	}
	
	function no_newlines(str) {
		return str.replace(/(\r|\n)/gm, '');
	}
	
	function ali(str) {
		if (log.ali === undefined) log.ali = {};
		log.ali.raw = parse_angle_form(res[1]);
	}

	function set_error(err) {
		if (log.error === undefined) log.error = [];
		log.error.push(err);
	}

	if (code_ === '222') {
		log.code = 'ali/request/';
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
		if (res = /^Request Sent with RequestKey=(.*); Index=(.*); Trunk=(.*) \(to DB=(.*) PP=(.*)\)$/.exec(no_newlines(message_))) {
			if (log.ali === undefined) log.ali = {};
			log.ali.params = {
				RequestKey: res[1],
				Index: res[2],
				Trunk: res[3],
				DB: res[4],
				PP: res[5]
			};
		} else if (res = /^Request Sent Index=(.*) \(to DB=(.*) PP=(.*); with TransactionID=(.*)\)$/.exec(no_newlines(message_))) {
			if (log.ali === undefined) log.ali = {};
			log.ali.params = {
				Index: res[1],
				DB: res[2],
				PP: res[3],
				TransactionID: res[4]
			};
		} else if (res = /^Request Sent Index=(.*) Trunk=(.*) \(to ALI Service=(.*) \))$/.exec(no_newlines(message_))) {
			if (log.ali === undefined) log.ali = {};
			log.ali.params = {
				Index: res[1],
				Trunk: res[2],
				"ALI Service": res[3]
			};
		} else {
			log.code = 'error';
			set_error('unknown 222 message');
		}
	} else if (code_ === '225') {
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
		log.code = 'ali/nobid/';
	} else if (code_ === '226') {
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
		log.code = 'ali/received/';
		var res = /^(?:Initial )?Record received \(Data Follows\)(.*)$/.exec(no_newlines(message_));
		if (res)
			ali(res[1]);
		else {
			log.code = 'unhandled 226';
			log.parse_error = "Unknown 226 message";
		}
	} else if (code_ === '266') {
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
		log.code = 'ali/received/rebid/';
		var res = /^Rebid Record received \(Data Follows\)(.*)$/.exec(no_newlines(message_));
		if (res)
			ali(res[1]);
		else {
			log.code = 'unhandled 266';
			log.parse_error = "Unknown 266 message";
		}
	} else if (code_ === '267') {
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
		log.code = 'ali/received/manual/';
		var res = /^Manual Bid Record received \(Data Follows\)(.*)$/.exec(no_newlines(message_));
		if (res)
			ali(res[1]);
		else {
			log.code = 'unhandled 267';
			log.parse_error = "Unknown 267 message";
		}
	} else if (code_ === '268') {
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
		log.code = 'ali/received/autorebid/';
		var res = /^Auto Rebid Record received \(Data Follows\)(.*)$/.exec(no_newlines(message_));
		if (res)
			ali(res[1]);
		else {
			log.code = 'unhandled 268';
			log.parse_error = 'Unknown 268 message';
		}
	} else if (code_ === '269') {
		log.code = 'ali/request/';
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
	} else if (code_ === '270') {
		log.code = 'ali/request/';
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
	} else if (code_ === '271') {
		log.code = 'ali/request/';
		if (log.call === undefined) log.call = {};
		log.call.is_911 = true;
	} else if (code_ === '306') {
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'join';
		log.transfer.type = 'initial';

		log.code = 'call/connect/';
		if (log.user === undefined) log.user = {};
		log.user.exten = null; // figure this out later

		if (message_ === 'Connect') {
		} else if (res = /^Connect \(From=(.*)\)$/.exec(no_newlines(message_))) {
			if (log.debug === undefined) log.debug = {};
			log.debug.connect_from = res[1];
		} else {
			log.code = 'unhandled 306';
			set_error('Unknown 306 Connect message');
		}
	} else if (code_ === '307') {
		if (log.user === undefined) log.user = {};
		log.user.exten = null;

		log.code = 'call/disconnect/';
		if (message_ === 'Disconnect') {
		} else {
			log.code = 'unhandled 307';
			log.parse_error = 'Unknown 307 message';
		}
	} else if (code_ === '309') {
		log.code = 'call/join/';
		//TODO: don't ignore these
		//if (log.transfer === undefined) log.transfer = {};
		//log.transfer.state = 'join';
		//if (log.user === undefined) log.user = {};
		if (message_ !== 'Conference Join') {
			log.code = 'unhandled 309';
			log.parse_error = 'Unknown 309 message';
		}
	} else if (code_ === '310') {
		log.code = 'call/onhold/';
		if (message_ !== 'On Hold')
			set_error('Unknown 310 On Hold message');
	} else if (code_ === '311') {
		log.code = 'call/offhold/';
		if (message_ !== 'Off Hold')
			set_error('Unknown 311 Off Hold message');
	} else if (code_ === '312') {
		if (log.transfer === undefined)
			log.transfer = {};
		log.transfer.state = 'abandon';
		log.transfer.type = 'initial';

		// abandoned has 'user' --- that is, intended answerer
		log.user = {}; // destroy user information because position in incorrect and useless
		log.user.controller = controller_;

		log.code = 'call/abandoned/';
		if (message_ === 'Abandoned') {
		} else {
			log.code = 'unhandled 312';
			log.parse_error = 'Unknown 312 message';
		}
	} else if (code_ === '316') {
		if (log.user !== undefined)
			set_error('316 Ringing has position user');

		if (log.user === undefined) log.user = {};
		if (ani_)
			log.user.exten = ani_;
		else if (pani_)
			log.user.exten = pani_;
		else
			log.user.exten = null;
		
		log.code = 'call/ringing/';

		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'dial';
		log.transfer.type = 'initial';
		if (message_ === 'Ringing') {
		} else if (res = /^Ringing \(From=(.*)\)$/.exec(no_newlines(message_))) {
		} else {
			log.code = 'unhandled 316';
			set_error('Unknown 316 message');
		}
	} else if (code_ === '342') {
		if (res = /^Conference Leave ?$/.exec(no_newlines(message_))) {
			log.code = 'call/leave/';
			if (pos_ === 0 || pos_ === null)
				log.parse_error = 'Conference Leave w/o exten with zero or null position';
		} else if (res = /^Conference Leave exten: (.*)$/.exec(no_newlines(message_))) {
			log.code = 'call/leave/';
			if (log.user === undefined)
				log.user = {};
			else
				set_error('342 Conference Leave has both exten and position');
			log.user.exten = res[1];
		} else {
			log.code = 'unhandled 342';
			set_error('Unknown 342 message');
		}
	} else if (code_ === '365') {
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'dial';
		log.transfer.type = 'transfer';
		if (res = /^Conference Transfer from GUI; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/transfer/conference/';
			log.transfer.destination = res[1];
		} else if (res = /^Attended Transfer from GUI; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/transfer/attended/';
			log.transfer.destination = res[1];
		} else if (res = /^Blind Transfer from GUI; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/transfer/blind/';
			log.transfer.destination = res[1];
		} else if (res = /^Tandem Transfer from GUI; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/transfer/tandem/';
			log.transfer.destination = res[1];
		} else if (res = /^NG911 SIP Transfer from GUI; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/transfer/sip/';
			log.transfer.destination = res[1];
		} else if (res = /^Flash Transfer from GUI; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/transfer/flash/';
			log.transfer.destination = res[1];
		} else if (res = /^Transfer from GUI; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/transfer/transfer/';
			log.transfer.destination = res[1];
		} else {
			log.transfer = undefined;
			set_error('Unknown 365 Transfer from GUI message');
		}
	} else if (code_ === '366') {
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'abandon';
		log.transfer.type = 'transfer';
		if (res = /^Transfer from Phone Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
		} else if (res = /^Attended Transfer from Phone Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
		} else if (res = /^Blind Transfer from Phone Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
		} else if (res = /^\{INFO\] Transfer Cancelled; (.*)$/.exec(no_newlines(message_))) {
		} else {
			log.transfer = undefined;
			set_error('Unknown 366 Transfer Cancelled message');
		}
	} else if (code_ === '367') {
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'abandon';
		log.transfer.type = 'transfer';
		if (res = /^Transfer from GUI Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else if (res = /^Transfer from GUI Cancelled; \(Destination=(.*?)\) Reason: (.*)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
			log.transfer.Reason = res[2];
		} else if (res = /^Attended Transfer from GUI Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else if (res = /^Blind Transfer from GUI (?:Cancelled|Failed); \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else if (res = /^Transfer from GUI Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else if (res = /^Tandem Transfer from GUI Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else if (res = /^Conference Transfer from GUI Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else if (res = /^Blind Transfer from GUI Cancelled; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else {
			log.transfer = undefined;
			set_error('Unknown 367 Transfer GUI Cancelled message');
		}
	} else if (code_ === '370') { // outbound transfers?
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'join';
		log.transfer.type = 'transfer';

		if (res = /^Outbound Call from Phone Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			if (log.call === undefined) log.call = {};
			log.call.destination = res[1];
			log.transfer.destination = res[1];
			log.transfer.type = 'initial';
			log.code = 'call/connect/outbound/';
			void function() {
				var dialplan = plv8.execute("SELECT plan::json FROM dialplan WHERE controller = $1 AND num = $2", [controller_, log.call.destination]);
				if (dialplan.length === 1) {
					var plan = dialplan[0].plan;
					if (plan.positions) {
						if (log.user === undefined) log.user = {};
						if (plan.positions.length === 1)
							log.user.position = plan.positions[0];
						else
							log.user.position = plan.positions;
					} else
						log.user.exten = log.call.destination;
				} else if (dialplan.length !== 0) {
					// this should, quite literally, be impossible
					plv8.elog(ERROR, 'got two dialplans for same controller and number');
				}
			}();
		} else if (res = /^Attended Transfer from Phone Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/join/attended/';
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else if (res = /^Unattended Transfer from Phone Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/join/unattended/';
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else if (res = /^Blind Transfer from Phone Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/join/blind/';
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else if (res = /^Outbound Blind Transfer from Phone Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/join/outbound_blind/';
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else if (message_ === 'Reconnect Ringback') {
			log.code = 'call/error/reconnect_ringback/';
			log.transfer = undefined;
		} else {
			log.code = 'unhandled 370';
			log.parse_error = 'Unknown 370 message';
		}	
	} else if (code_ === '372') {
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'dial';
		log.transfer.type = 'initial';

		log.code = 'call/outbound/';
		if (res = /^Outbound Call from Phone Dialed; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			if (log.call === undefined) log.call = {};
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else {
			log.code = 'unhandled 372';
			log.parse_error = 'Unknown 372 message';
		}			
	} else if (code_ === '373') {
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'dial';
		log.transfer.type = 'transfer';

		if (res = /^(?:Attended )?Transfer from Phone Dialed; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else if (res = /^\[INFO\] Transfer\/Conf Dialed; (.*)$/.exec(no_newlines(message_))) {
		} else {
			log.code = 'unhandled 373';
			log.parse_error = 'Unknown 373 message';
		}
	} else if (code_ === '378') {
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'join';
		log.transfer.type = 'transfer';

		if (res = /^Conference Transfer from GUI Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/join/conference/';
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else if (res = /^Blind Transfer from GUI Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/join/blind/';
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else if (res = /^Transfer from GUI Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/join/transfer/';
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else if (res = /^NG911 SIP Transfer from GUI Connected; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.code = 'call/join/ng911/';
			log.call.destination = res[1];
			log.transfer.destination = res[1];
		} else {
			log.code = 'unhandled 378';
			log.parse_error = 'Unknown 378 message';
		}
	} else if (code_ === '382') {
		log.code = 'kbnp 382';
		if (res = /^\[TDD\] Dispatcher (.*?) says: (.*)$/.exec(no_newlines(message_))) {
			if ( !('user' in log) || log.user.position !== Number(res[1]))
				log.parse_error = '382 with mismatching position';
		} else if (res = /^SMS Text From Dispatcher (.*?) : (.*)$/.exec(no_newlines(message_))) {
			if ( !('user' in log) || log.user.position !== Number(res[1]))
				log.parse_error = '382 with mismatching position';
		} else {
			log.code = 'unhandled 382';
			log.parse_error = 'Unknown 382 message';
		}
	} else if (code_ === '392') {
		if (log.transfer === undefined) log.transfer = {};
		log.transfer.state = 'dial';
		log.transfer.type = 'transfer';
		log.code = 'kbnp 392';
		if (res = /^Blind Transfer from Phone Dialed; \(Destination=(.*)\)$/.exec(no_newlines(message_))) {
			log.transfer.destination = res[1];
		} else {
			log.code = 'unhandled 392';
			log.parse_error = 'Unknown 392 message';
		}
	} else if (code_ === '402') {
		if (message_ === 'ALI Record Sent') {
			log.code = 'ali/cad/sent';
		} else {
			log.code = 'unhandled 402';
			log.parse_error = 'Unknown 402 message';
		}
	} else if (code_ === '709') {
		log.code = 'login/login/';
		var res = /^\[INFO\] (?:User (.*) - )?Workstation (.*) At IP: (.*) Now On Line$/.exec(message_);
		if (res) {
			if (log.user === undefined) log.user = {};
			if (res[1]) log.user.name = res[1];
			log.user.known = 'enter';

			if (Number(res[2]) !== pos_)
				log.parse_error = '709 Login Workstation does not match position';
		} else
			log.parse_error = 'Unknown 709 message';
	} else if (code_ === '717') {
		log.code = 'login/logout/';
		var res = /^\[INFO\] (?:User (.*) - )?Workstation (.*) At IP: (.*) Now Off Line$/.exec(message_);
		if (res) {
			if (log.user === undefined) log.user = {};
			if (res[1]) log.user.name = res[1];
			//log.user.known = 'enter'; // TODO: what if this is our only information?

			if (Number(res[2]) !== pos_)
				log.parse_error = '717 Logout Workstation does not match position';
		} else
			log.parse_error = "Unknown 717 message";
	} else if (code_ === '753') {
		if (res = /^\[WARNING\] User (.*?) - (.*)$/.exec(no_newlines(message_))) {
			if ('user' in log && 'position' in log.user) {
				//log.user.known = 'enter';
				log.user.name = res[1];
			}
		} else if (res = /^\[INFO\] User (.*?) - (.*)$/.exec(no_newlines(message_))) {
			if ('user' in log && 'position' in log.user) {
				//log.user.known = 'enter';
				log.user.name = res[1];
			}
		}
	} else if (code_ === '754') {
		log.code = 'kbnp 754';
		if (res = /^\[INFO\] Added (.*) as a valid IP address$/.exec(no_newlines(message_))) {
		} else if (res = /^\[INFO\] Session Change Event \(Session Lock for (.*)\)$/.exec(no_newlines(message_))) {
		} else if (res = /^\[WARNING\] User (.*?) - (.*)$/.exec(no_newlines(message_))) {
			if ('user' in log && 'position' in log.user) {
				//log.user.known = 'enter';
				log.user.name = res[1];
			}
		} else if (res = /^\[WARNING\] Unable to communicate with the 9-1-1 Controller \(no message received for at least 15 seconds\)$/.exec(no_newlines(message_))) {
		} else {
			log.code = 'unhandled 754';
			log.parse_error = 'unknown 754 message';
		}
	} else if (code_ === '755') {
		// TODO: better places for things
		if (res = /^\[WARNING\] Comment: (.*)$/.exec(no_newlines(message_))) {
			log.code = 'workstation/warning/';
			log.comment = parse_angle_form(res[1]);
		} else if (res = /^\[WARNING\] User (.*) - Comment: (.*)$/.exec(no_newlines(message_))) {
			log.code = 'workstation/warning/';
			log.comment = parse_angle_form(res[2]);
			if ('user' in log && 'position' in log.user) {
				//log.user.known = 'enter';
				log.user.name = res[1];
			}
		} else if (res = /^\[INFO\] User (.*) - (.*)$/.exec(no_newlines(message_))) {
			if ('user' in log && 'position' in log.user) {
				//log.user.known = 'enter';
				log.user.name = res[1];
			}
		} else {
			log.code = 'unhandled 755';
			log.parse_error = 'Unknown 755 message';
		}
	} else if (callid_ === null) {
		// if we don't know it, and don't have a callid for it,
		// chuck it out
		return null;
	}
	
	return log;
$$)pgsql", "FUNC parse_oldlog");

/********************************** FIXING OLDLOG ***************************
 * Because of some issues where the logs have incorrect/misleading positions,
 * we have to correct them before we attach usernames to positions.
 * This includes:
 *    *) a bug where calling another position only gives the position
 *       of the callee and not the caller
 *    *) calling inside/outside is not distinguished well from calling
 *       another position
 * etc.
 *
 * Heuristics beware.
 ***************************************************************************/

w.exec(R"pgsql(CREATE FUNCTION pg_temp.fix_call(call_ JSON)
RETURNS void LANGUAGE plv8 AS $$
// A "transfer" is when someone (or no one) "asks"/"dials" to get someone else
// to "join"/"connect" to a call.
// Every call has an (attempted) "transfer"
var main_transfer = {}; // 'join' 'dial' 'tid'

var sub_transfers = Object.create(null); // indexed by destination

var positions = Object.create(null); // pos/exten -> 

var joins = Object.create(null); // pos/exten -> log[]
var leaves = Object.create(null); // pos/exten -> log[]

for (var i = 0; i < call_.length; i++) {
	if (call_[i].transfer !== undefined) {
		if (call_[i].transfer.type === 'initial') {
			if (call_[i].transfer.state === 'dial' && main_transfer.dial === undefined)
				main_transfer.dial = call_[i];
			else if ( (call_[i].transfer.state === 'join' || call_[i].transfer.state === 'abandon')
				&& main_transfer.join === undefined ) {
				main_transfer.join = call_[i];
			}
		} else if (call_[i].transfer.type === 'transfer' && 'destination' in call_[i].transfer) {
			var dest = call_[i].transfer.destination;
			if (sub_transfers[dest] === undefined) sub_transfers[dest] = {};
			switch(call_[i].transfer.state) {
			case 'dial':
				sub_transfers[dest].dial = call_[i];
				break;
			case 'join':
			case 'abandon':
				sub_transfers[dest].join = call_[i];
				break;
			}
		}
	}
}

var connect; var abandoned;
var ringing; var outbound;
if (main_transfer.join !== undefined) {
	if (main_transfer.join.code === 'call/connect/')
		connect = main_transfer.join;
	else if (main_transfer.join.code === 'call/abandoned/')
		abandoned = main_transfer.join;
}
if (main_transfer.dial !== undefined) {
	if (main_transfer.dial.code === 'call/ringing/')
		ringing = main_transfer.dial;
	else if (main_transfer.dial.code === 'call/outbound/')
		outbound = main_transfer.dial;
}

if (ringing !== undefined && connect !== undefined) {
	if (connect.user !== undefined)
		connect.user.exten = undefined;
}

if (outbound !== undefined && connect !== undefined) {
	if (connect.user !== undefined)
		connect.user.position = undefined;
}

if (outbound !== undefined && abandoned !== undefined) {void function(){
	var dialplan = plv8.execute("SELECT plan::json AS plan FROM dialplan WHERE controller = $1 AND num = $2", [outbound.controller, outbound.transfer.destination]);
	if (dialplan.length === 1) {
		abandoned.user.psap = dialplan[0].plan.psap;
	} else {
		abandoned.user.exten = outbound.transfer.destination;
	}
}();}

if (ringing !== undefined && abandoned !== undefined) {void function() {
	var trunk = plv8.execute("SELECT data::json AS data FROM controller_trunks WHERE controller = $1 AND (trunk = $2 OR trunk = 0) ORDER BY trunk DESC", [abandoned.controller, abandoned.call.trunk]);
	if (trunk.length > 0) {
		abandoned.user.psap = trunk[0].data.psap;
	} else {
	}
}();}

if (main_transfer.join !== undefined) {
	main_transfer.tid = plv8.execute("SELECT uuid_generate_v5('bc0b9519-92bc-48d3-87b3-51d0440afa65', $1::uuid::text) AS tid", [main_transfer.join.id])[0].tid;
	main_transfer.join.transfer.tid = main_transfer.tid;
	if (main_transfer.dial !== undefined) {
		if (main_transfer.dial.transfer === undefined) main_transfer.dial.transfer = {};
		main_transfer.dial.transfer.tid = main_transfer.tid;
	}
}

for (var t in sub_transfers) {
	if ('join' in sub_transfers[t]) {
		// TODO: same UUID as up there
		sub_transfers[t].tid = plv8.execute("SELECT uuid_generate_v5('bc0b9519-92bc-48d3-87b3-51d0440afa65', $1::uuid::text) AS tid", [sub_transfers[t].join.id])[0].tid;
		sub_transfers[t].join.transfer.tid = sub_transfers[t].tid;
		if ('dial' in sub_transfers[t]) {
			sub_transfers[t].dial.transfer.tid = sub_transfers[t].tid;
		}
	}
}

// fixed positions by here

for (var i = 0; i < call_.length; i++) {void function() {
	var trunk, position;
	if (call_[i].call !== undefined && call_[i].call.trunk !== undefined) {
		trunk = plv8.execute("SELECT data FROM controller_trunks WHERE controller = $1 AND trunk = $2", [call_[i].controller, call_[i].call.trunk]);
		if (trunk.length === 1) {
			trunk = trunk[0].data;
			if (typeof trunk.is_911 === 'boolean')
				call_[i].call.is_911 = trunk.is_911;
		}
	}

	if (call_[i].user !== undefined && call_[i].user.position !== undefined) {
		var pos_array;
		if (Array.isArray(call_[i].user.position))
			pos_array = call_[i].user.position;
		else
			pos_array = [call_[i].user.position];
		var pos_psap;
		for (var j = 0; j < pos_array.length; j++) {
			var pos = plv8.execute("SELECT data::json AS data FROM controller_positions WHERE controller = $1 AND pos = $2", [call_[i].controller, pos_array[j]]);
			if (pos.length === 1) {
				pos = pos[0].data;
				if (pos.psap !== undefined) {
					if (pos_psap === undefined || pos_psap === pos.psap)
						pos_psap = pos.psap;
					else
						plv8.elog(ERROR, 'TODO FIX MULTIPLE PSAP');
				}
			}
		}
		call_[i].user.psap = pos_psap;
	}

	if (call_[i].user !== undefined && call_[i].user.exten === undefined)
		call_[i].user.controller = call_[i].controller;
	if (call_[i].user !== undefined && call_[i].user.psap === undefined && call_[i].user.controller !== undefined)
		call_[i].user.psap = call_[i].user.controller;
}();}

for (var i = 0; i < call_.length; i++) {
	plv8.execute("UPDATE newlog SET orig = pg_temp.wjson_del_props($1, '[\"id\",\"controller\",\"utc\",\"code\"]')::jsonb WHERE id = $2", [call_[i], call_[i].id]);
}
$$)pgsql", "FUNC fix_call");

/************************** SECOND PASS **************************************
 * This is where it gets really expensive. In order to attach logins to
 * logs that refer only to position, we have to iterate through the logs, in
 * order, keeping track of who is on and adding that to the logs as we go.
 * Turns out to be quite against the grain (but better than other approaches
 * I've tried)
 *
 * This is why I want logins/usernames to be attached to all logs. There isn't
 * a good join for this (especially when done incrementally).
 ****************************************************************************/

	// these three functions describe a loop to get around a memory leak in plv8.cursor
	// i.e., like this:
	// iter = initial(...);
	// for(i = 0; i < n; i++)
	// 	iter = single(iter, x[i]);
	//
	// there are actually some nice mathematical properties to such an approach
	// (something like Haskell's monad if I understand that correctly)
	w.exec(R"pgsql(
CREATE FUNCTION pg_temp.newlog_processed_second_pass(controller_ TEXT, mutc_ TIMESTAMPTZ) RETURNS void LANGUAGE plpgsql AS $$
DECLARE
	login JSON;
	log RECORD;
BEGIN
	SELECT pg_temp.newlog_processed_second_pass_initial(controller_, mutc_) INTO login;
	FOR log IN SELECT id, controller, utc, code, orig::json AS data FROM newlog WHERE controller = controller_ AND utc >= mutc_ AND orig ? 'user' AND orig->'user' ? 'position' ORDER BY utc LOOP
		SELECT pg_temp.newlog_processed_second_pass_single(login, pg_temp.wjson_object_merge(log.data, json_build_object('id', log.id, 'code', log.code, 'utc', log.utc, 'controller', log.controller))) INTO login;
	END LOOP;
END;
$$)pgsql");

	w.exec(R"pgsql(
CREATE FUNCTION pg_temp.newlog_processed_second_pass_initial(controller_ TEXT, mutc_ TIMESTAMPTZ) RETURNS json LANGUAGE plv8 AS $$
	var logins_db = plv8.execute("SELECT DISTINCT ON (orig->'user'->>'position') id, code, orig::json AS data FROM newlog WHERE controller = $1 AND utc < $2 AND orig ? 'user' AND orig->'user' ? 'position' AND orig->'user' ? 'known' ORDER BY orig->'user'->>'position', utc DESC", [controller_, mutc_]);
	var login = Object.create(null);
	var cur;
	for (var i = 0; i < logins_db.length; i++) {
		cur = logins_db[i].data;
		cur.id = logins_db[i].id;
		cur.code = logins_db[i].code;
		login[logins_db[i].data.user.position] = cur;
	}
	return login;
$$)pgsql");

	w.exec(R"pgsql(
CREATE FUNCTION pg_temp.newlog_processed_second_pass_single(login_ JSON, log_ JSON) RETURNS json LANGUAGE plv8 AS $$
	// this is the inside of a loop
	// ``login_'' is the iteration state

	if (login_ === null) plv8.elog(ERROR, 'login_ can not be null');
	if (log_ === null) plv8.elog(ERROR, 'log_ can not be null');

	if (log_.user !== undefined && log_.user.position !== undefined && typeof log_.user.position === 'number') {
		if ('known' in log_.user) login_[log_.user.position] = JSON.parse(JSON.stringify(log_));
		var login_ref = login_[log_.user.position];

		if (login_ref !== undefined && login_ref.user.known === 'enter') {
			log_.user.login_ref = login_ref.id;
			if ('name' in login_ref.user) log_.user.name = login_ref.user.name;
			var id = log_.id;
			log_.id = undefined; log_.thread = undefined; log_.code = undefined; log_.utc = undefined; log_.controller = undefined;
			plv8.execute("UPDATE newlog SET orig = $1::json::jsonb, data = NULL WHERE id = $2", [log_, id]);
			if (log_.call !== undefined && log_.call.callid !== undefined) {
				plv8.execute("DELETE FROM oldlog_callids_to_fix WHERE callid = $1", [log_.call.callid]);
				plv8.execute("INSERT INTO oldlog_callids_to_fix(callid) VALUES($1)", [log_.call.callid]);
			}
		}
	}

	return login_;
$$)pgsql");

/******************************* NEWLOG LATE PARSING (ALI+) *****************/

w.exec(R"pgsql(CREATE FUNCTION pg_temp.newlog_late_parse(log_ JSON)
RETURNS json LANGUAGE plv8 STABLE AS $$

if (log_.ali !== undefined && log_.ali.raw !== undefined) {void function() {
	var regex;
	
	var matched_formats = [];
	
	var known_cos = Object.create(null);
	known_cos.WPH1 = 'WPH1'; known_cos.WPH2 = 'WPH2'; known_cos.WRLS = 'WRLS'; known_cos.MOBL = 'MOBL';
	known_cos.RESD = 'RESD';
	known_cos.BUSN = 'BUSN'; known_cos.BUSX = 'BUSX'; known_cos.BSNX = 'BSNX';
	known_cos.VOIP = 'VOIP'; known_cos.VoIP = 'VOIP';
	known_cos.PBXB = 'PBXB';
	known_cos.CENT = 'CENT';
	known_cos.COIN = 'COIN';
	known_cos['PAY$'] = 'PAY$';
	known_cos.PBXR = 'PBXR';
	known_cos.RES = 'RES'; known_cos.BUS = 'BUS'; known_cos.CTX = 'CTX'; known_cos.PXB = 'PXB';
	known_cos.RESX = 'RESX'; known_cos.CN = 'CN'; known_cos.CNTX = 'CNTX';
	known_cos.PBXb = 'PBXB'; // TODO: very likely an error, happens very commonly though
	known_cos.NRF = 'NRF';
	known_cos['N/A'] = 'Ali Error';
	known_cos.RSDX = 'RSDX'; // TODO: where to put this
	
	function assign_cos(cos) {
		if (known_cos[cos])
			log_.ali.cos = known_cos[cos];
		else {
			log_.ali.cos = 'Unknown';
			log_.ali.parse_error = 'Unknown COS ' + cos;
		}
	}

	function do_cos(cos, format) {
		if (matched_formats.length < 1) {
			assign_cos(cos);
			log_.ali.format = format;
		}
		matched_formats.push(format);
	}
	
	// callid: 245732956890804441 has spurious 0xF8
	if (regex = /^\u0002\d+\r(?: {31}|                     \uE0F8          )\r[\d ]{3}-[\d ]{3}-[\d ]{4}  \d{2}:\d{2}:\d{2} \d{8}\r.{31}\r.{27}(.{4})\r/.exec(log_.ali.raw)) {
		do_cos(regex[1].trim(), 'SPACE_BEFORE');
	}
	
	if (regex = /^\u0002\d{13} NO RECORD FOUND\u0003$/.exec(log_.ali.raw)) {
		do_cos('NRF', 'NRF_FLAT13');
	}
	
	if (regex = /^\u0002\d+\r\d{10} NO RECORD FOUND\u0003$/.exec(log_.ali.raw)) {
		do_cos('NRF', 'NRF_FLAT10');
	}
	
	if (regex = /^\u0002\d+\r\d{3}-\d{3}-\d{4}.{32}(.{4})/.exec(log_.ali.raw)) {
		do_cos(regex[1].trim(), 'HURON');
	}
	
	if (regex = /^\u0002\d+\r\d{3}-\d{3}-\d{4} NO RECORD FOUND\u0003$/.exec(log_.ali.raw)) {
		do_cos('NRF', 'NRF_HYPHEN');
	}

	if (regex = /^\u0002\d{13} RECORD NOT FOUND\n                               \r\u0003$/.exec(log_.ali.raw)) {
		do_cos('NRF', 'NRF_FLAT_LF_CR');
	}

	if (regex = /^\u0002\d+\r\n?\(\d{3}\) \d{3}-\d{4} (.{4}) \d{2}\/\d{2} \d{2}:\d{2}\r/.exec(log_.ali.raw)) {
		do_cos(regex[1].trim(), 'ERIE');
	}

	if (regex = /^\u0002\d+\r\(\d{3}\) \d{3}-\d{4}   \d{2}:\d{2}  \d{2}\/\d{2}\r.{49}\r.{30}\r\r(?:CALLBK=\(\d{3}\)\d{3}-\d{4} {10}|.{30})\r.{24}ESN [ \d]{4}\rCO=.{5} PSAP \d{2} POS# \d{2}   (.{4})\r[^]*\u0003$/.exec(log_.ali.raw)) {
		var regex2;
		if (regex2 = /^\u0002\d+\r\(\d{3}\) \d{3}-\d{4}   \d{2}:\d{2}  \d{2}\/\d{2}\r.{49}\r.{30}\r\r.{30}\r.{24}ESN  {4}\rCO=.{5} PSAP \d{2} POS# \d{2}    {4}\r {20}\r {8}\r {19}\r {6}\rP#\(\d{3}\)\d{3}-\d{4}\r {30}\r[+-]\d{3}\.\d{6} [+-]\d{3}\.\d{6}\u0003$/.exec(log_.ali.raw)) {
			do_cos('WRLS', 'CLAY_WRLS');
		} else 
			do_cos(regex[1].trim(), 'CLAY');
	}

	if (regex = /^\x02\d+\r\r                               \r\d{3}-\d{3}-\d{4}  \d{2}:\d{2}:\d{2} \d{8}\r.{31}\r.{27}(.{4})\r[^]*\x03$/.exec(log_.ali.raw)) {
		do_cos(regex[1], 'UNION');
	}

	if (regex = /^\u0002\d+\r\d{10} NO RESPONSE FROM WIRE STEER\u0003$/.exec(log_.ali.raw)) {
		do_cos('NRF', 'NRF_NO_RESPONSE');
	}

	if (regex = /^\u0002\d+\r\(\d{3}\) \d{3}-\d{4}   \d{2}:\d{2}    \d{2}\/\d{2}\r.{82}.{2} (?:\d{3}|\d{2} ) (.{4})\r/.exec(log_.ali.raw)) {
		do_cos(regex[1], 'HARDIN');
	}

	if (regex = /^\u0002\d+\r\(\d{3}\) \d{3}-\d{4} (.{4}) \d{2}\/\d{2} \d{2}:\d{2} .{28}\r/.exec(log_.ali.raw)) {
		do_cos(regex[1].trim(), 'PUTNAM');
	}

	if (regex = /^\u0002\d+ \d{2}\/\d{2}\/\d{4}  \d{2}:\d{2}:\d{2}\r \(\d{3}\) \d{3}-\d{4}  (.{4})/.exec(log_.ali.raw)) {
		do_cos(regex[1].trim(), 'MONROE');
	}

	if (regex = /^\u0002\d+\r\d{10} NO RESPONSE FROM CELL STEER\u0003$/.exec(log_.ali.raw)) {
		do_cos('NRF', 'NRF_NO_RESPONSE_CELL_STEER');
	}

	if (regex = /^\u0002\d+\r.{31}\r\d{3}-\d{3}-\d{4} \d{2}:\d{2}:\d\d? \d{2}\/\d{2}\/\d{4}\r.{31}\r.{27}(.{4})\r/.exec(log_.ali.raw)) {
		do_cos(regex[1].trim(), 'AUGLAIZE');
	}

	if (matched_formats.length === 0) {
		log_.ali.cos = 'Unknown';
		log_.ali.parse_error = "Unknown ali format";
		plv8.elog(WARNING, 'Unknown ali format: ' + JSON.stringify(log_.ali.raw));
	} else if (matched_formats.length > 1) {
		log_.ali.cos = 'Unknown';
		log_.ali.parse_error = 'Multiple ali formats matched ' + matched_formats.join(', ');
		log_.ali.parse_error_matched_formats = matched_formats;
	}
}();}

return log_;
$$)pgsql", "FUNC newlog_late_parse");

/***************************** FINAL CALL PROCESSING ************************/

w.exec(R"pgsql(CREATE FUNCTION pg_temp.update_call(call_ JSON)
RETURNS void LANGUAGE plv8 AS $$
var call_id = call_[0].call.callid;
var cdata = {controller:call_[0].controller};
var is_911 = false;
var oldlog_callid;

var has_disconnect = false;

var transfers = Object.create(null); // map from UUID to transfer object
for (var i = 0; i < call_.length; i++) {
	if (/^call\/disconnect\//.test(call_[i].code))
		has_disconnect = true;

	if ('oldlog' in call_[i] && 'callid' in call_[i].oldlog)
		oldlog_callid = call_[i].oldlog.callid;

	if (call_[i].call.is_911)
		is_911 = true;

	if (call_[i].transfer && call_[i].transfer.tid) {
		var tid = call_[i].transfer.tid;
		if (transfers[tid] === undefined) transfers[tid] = {abandoned:false};
		if (transfers[tid].tdata === undefined) transfers[tid].tdata = {};

		if (call_[i].transfer.state === 'dial') {
			transfers[tid].utc = call_[i].utc;
			transfers[tid].caller = call_[i].user;
		} else if (call_[i].transfer.state === 'join') {
			transfers[tid].answer_time = call_[i].utc;
			transfers[tid].callee = call_[i].user;
		} else if (call_[i].transfer.state === 'abandon') {
			transfers[tid].abandoned = true;
			transfers[tid].callee = call_[i].user;
		}

		transfers[tid].type = call_[i].transfer.type;
	} else if (call_[i].transfer !== undefined && call_[i].transfer.tid === undefined) {
		if (cdata.warning === undefined) cdata.warning = {};
		if (cdata.warning.no_tid === undefined) cdata.warning.no_tid = 0;
		cdata.warning.no_tid++;
	}

	if (call_[i].ali !== undefined && call_[i].ali.cos !== undefined) {
		//if (!has_disconnect)
			//cdata.cos = call_[i].ali.cos;
		cdata.cos = call_[i].ali.cos;
	}

	if (call_[i].call.trunk !== undefined)
		cdata.trunk = call_[i].call.trunk;
	cdata.ani = call_[i].call.ani;
	cdata.pani = call_[i].call.pani;
	cdata.has_disconnect = has_disconnect;
}

if (oldlog_callid !== undefined)
	cdata.callid = oldlog_callid;

plv8.execute("DELETE FROM call_transfer WHERE call_id = $1", [call_id]);
plv8.execute("DELETE FROM call_info WHERE call_id = $1", [call_id]);
plv8.execute("DELETE FROM basic_call_info2 WHERE callid = $1", [call_id]);

plv8.execute("INSERT INTO call_info(call_id,utc_range,is_911,cdata) VALUES($1,tstzrange($2::timestamptz,$3::timestamptz,'[]'),$4,$5::json::jsonb)",
	[call_id, call_[0].utc, call_[call_.length - 1].utc, is_911, cdata]);

for (var t in transfers) {
	if (transfers[t].utc === undefined) transfers[t].utc = '1800-01-01';
	transfers[t].tdata.controller = call_[0].controller;
	if (transfers[t].type === undefined) {
		transfers[t].tdata.error = 'unknown type';
		transfers[t].type = 'unknown';
	}
	plv8.execute("INSERT INTO call_transfer(transfer_id,call_id,type,utc,answer_time,caller,callee,tdata,abandoned)" +
		" VALUES($1,$2,$3,$4::timestamptz,$5::timestamptz - $4,$6::json::jsonb,$7::json::jsonb,$8::json::jsonb,$9)",
		[t,call_id,transfers[t].type,transfers[t].utc,
		transfers[t].answer_time,transfers[t].caller,
		transfers[t].callee,transfers[t].tdata,transfers[t].abandoned]);

	var bci = {};
	bci.tid = t;
	bci.ani = cdata.ani;
	bci.pani = cdata.pani;
	bci.warning = cdata.warning;
	if (call_[0].oldlog !== undefined)
		bci.callid = call_[0].oldlog.callid;

	if (transfers[t].caller !== undefined && transfers[t].caller.psap !== undefined && transfers[t].callee !== undefined && transfers[t].caller.psap !== transfers[t].callee.psap) {
		var pos = transfers[t].caller.position;
		if (Array.isArray(pos)) pos = null;
		plv8.execute("INSERT INTO basic_call_info2(callid,controller,psap,utc,answer,abandoned,pos,calltaker,is_911,cos,incoming,trunk,internal_transfer,data) VALUES($1,$2,$3,$4,$5::timestamptz-$4,$6,$7,$8,$9,$10,$11,$12,$13,$14::json::jsonb)",
			[call_id,
			call_[0].controller,
			transfers[t].caller.psap,
			transfers[t].utc,
			transfers[t].answer_time,
			transfers[t].abandoned,
			pos,
			transfers[t].caller.name,
			is_911,
			cdata.cos,
			false,
			cdata.trunk,
			transfers[t].type !== 'initial',
			bci]);
	}

	if (transfers[t].callee !== undefined
		&& (transfers[t].callee.psap !== undefined) && (transfers[t].caller !== undefined)
		&& (transfers[t].callee.psap !== transfers[t].caller.psap)) {
		var pos = transfers[t].callee.position;

		if (Array.isArray(pos)) pos = null;
		plv8.execute("INSERT INTO basic_call_info2(callid,controller,psap,utc,answer,abandoned,pos,calltaker,is_911,cos,incoming,trunk,internal_transfer,data) VALUES($1,$2,$3,$4,$5::timestamptz-$4,$6,$7,$8,$9,$10,$11,$12,$13,$14::json::jsonb)",
			[call_id,call_[0].controller,transfers[t].callee.psap,transfers[t].utc,transfers[t].answer_time,transfers[t].abandoned,pos,transfers[t].callee.name,is_911,cdata.cos,true,cdata.trunk,transfers[t].type !== 'initial',bci]);
	}
}

$$)pgsql", "FUNC update_call");

w.exec(R"pgsql(CREATE FUNCTION pg_temp.bci2_fake_psap()
RETURNS void LANGUAGE plv8 AS $$
	plv8.execute("DELETE FROM basic_call_info2 WHERE controller = 'FAKE'");
	var num_calls = 10000 + 1000 * (Math.random() - 0.5);
	for (var i = 0; i < num_calls; i++) {
		
	}
$$)pgsql", "bci2_fake_psap");

}



#endif
