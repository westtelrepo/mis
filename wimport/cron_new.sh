#!/bin/bash

cd "$(dirname "$0")"

(
	flock -n 200 || exit 1

	readonly old_psaps="MT-113 SD-123"

	for p in $old_psaps; do
		\time -p ./wimport --controller $p-1 /mnt/backups/$p/$p-1/home/experient/911/logs/$p-1*.log
		echo
	done

	readonly new_psaps="$(cat cron_config.txt)"

	for p in $new_psaps; do
		\time -p ./wimport --controller $p-1 /mnt/backups/$p/$p-1/datadisk1/ng911/logs/$p-1*.log
		echo
	done
) 200>/var/lock/wimport_cron.5qcgSB0j01sBX5cq.lock &> "$(dirname "$0")/logs/$(date +'%F_%H-%M').log"
