#include "westtel/json.h"
#include "westtel/util.h"
#include "LogStream.h"
#include "sql_functions.h"

#include <algorithm>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <stdexcept>
#include <sys/stat.h>
#include <time.h>
#include <unordered_map>
#include <vector>
#include <thread>

#include <boost/optional.hpp>
#include <boost/lockfree/spsc_queue.hpp>
#include <pqxx/pqxx>

using boost::optional;
using westtel::JSON;
using westtel::JSONType;

namespace pqxx {
	template<class T> struct string_traits<optional<T>> {
		static const char* name() { return std::string("optional<") + pqxx::string_traits<T>::name() + ">"; }
		static bool has_null() { return true; }
		static bool is_null(optional<T> t) { return !t; }
		static optional<T> null() { return optional<T>{}; }
		static void from_string(const char* str, optional<T>& o) {
			T ret{};
			pqxx::string_traits<T>::from_string(str, ret);
			o = optional<T>{ret};
		}
		static std::string to_string(optional<T> o) {
			assert(o);
			return pqxx::string_traits<T>::to_string(*o);
		}
	};

	template<> struct string_traits<JSON> {
		static const char* name() { return "JSON"; }
		static bool has_null() { return false; }
		static bool is_null(const JSON&) { return false; }
		static JSON null() { assert(false); }
		static void from_string(const char*, JSON&) {
			assert(false);
		}
		static std::string to_string(const JSON& j) {
			return j.to_string();
		}
	};
}

template<class T>
JSON to_json_value(const optional<T>& o) {
	JSON ret{};
	if (o) ret = *o;
	return ret;
}

class easy_timer {
private:
	clockid_t clk_id;
	struct timespec begin;
public:
	easy_timer(clockid_t clk = CLOCK_MONOTONIC): clk_id{clk} {
		clock_gettime(clk_id, &begin);
	}

	void start() {
		clock_gettime(clk_id, &begin);
	}

	double seconds() {
		timespec end;
		clock_gettime(clk_id, &end);
		return static_cast<double>(end.tv_sec - begin.tv_sec) + static_cast<double>(end.tv_nsec - begin.tv_nsec) * 1e-9;
	}
};

// TODO: explain below code
int main(int argc, char** argv) {
	easy_timer timer;
	std::set<std::string> filenames;

	bool truncate_all = false;
	bool truncate_newlog = false;
	bool redo_ali_parse_errors = false;
	std::vector<std::string> redo_newlog_psaps;
	bool redo_oldlog_newlog = false;
	std::vector<std::string> redo_all_psaps;
	std::vector<std::string> redo_calls_psaps;
	bool redo_oldlog_parse_errors = false;
	bool no_commit = false;
	bool recursive_descent = false;
	std::set<std::string> only_controllers;
	bool print_explain = false;
	bool set_ndistinct = false;

	bool only_filenames_now = false;

	std::string delete_for_controller_after_controller = "";
	std::string delete_for_controller_after_utc = "";

	for (int i = 1; i < argc; ++i) {
		assert(argv[i]); // something is horribly broken if this ever fires

		if (only_filenames_now) {
		} else if (argv[i] == std::string{"--controller"}) {
			if (i + 1 >= argc) {
				std::cerr << "--controller expects argument" << std::endl;
				return 1;
			}
			only_controllers.emplace(argv[++i]);
			continue;
		} else if (argv[i] == std::string{"--delete-for-controller-after"}) {
			if (i + 2 >= argc) {
				std::cerr << "--delete-for-controller-after expects 2 arguments" << std::endl;
				return 1;
			}
			delete_for_controller_after_controller = argv[++i];
			delete_for_controller_after_utc = argv[++i];
			continue;
		} else if (argv[i] == std::string("--truncate-all")) {
			truncate_all = true;
			continue;
		} else if (argv[i] == std::string{"--truncate-newlog"}) {
			truncate_newlog = true;
			continue;
		} else if (argv[i] == std::string{"--redo-ali-parse-errors"}) {
			redo_ali_parse_errors = true;
			continue;
		} else if (argv[i] == std::string{"--redo-oldlog-parse-errors"}) {
			redo_oldlog_parse_errors = true;
			continue;
		} else if (argv[i] == std::string{"--redo-newlog-psap"}) {
			if (i + 1 == argc) { std::cerr << "bad arguments, needs psap" << std::endl; return 1; }
			redo_newlog_psaps.push_back(argv[++i]);
			continue;
		} else if (argv[i] == std::string{"--redo-oldlog-newlog"}) {
			redo_oldlog_newlog = true;
			continue;
		} else if (argv[i] == std::string{"--redo-all-psap"}) {
			if (i + 1 == argc) { std::cerr << "bad arguments, needs psap" << std::endl; return 1; }
			redo_all_psaps.push_back(argv[++i]);
			continue;
		} else if (argv[i] == std::string{"--redo-calls-psap"}) {
			if (i + 1 == argc) { std::cerr << "bad arguments, needs psap" << std::endl; return 1; }
			redo_calls_psaps.push_back(argv[++i]);
			continue;
		} else if (argv[i] == std::string{"--no-commit"}) {
			no_commit = true;
			continue;
		} else if (argv[i] == std::string{"--recurse"}) {
			recursive_descent = true;
			continue;
		} else if (argv[i] == std::string{"--explain"}) {
			print_explain = true;
			continue;
		} else if (argv[i] == std::string{"--set-ndistinct"}) {
			set_ndistinct = true;
			continue;
		} else if (argv[i] == std::string{"--"}) {
			only_filenames_now = true;
			continue;
		} else if (std::string{argv[i]}.substr(0, 2) == "--") {
			std::cerr << "unknown argument " << argv[i] << std::endl;
			exit(1);
		}
		
		filenames.emplace(argv[i]);

		std::string f = westtel::util::str_basename(argv[i]);
		LogStream reader{std::unique_ptr<std::ifstream>{
			new std::ifstream(argv[i])}, f};
		reader.set_preserve_newlines();
	}

	// TODO: recurse into directories
	/*{
		std::set<std::string> new_filenames;
		auto add_f = [&](const std::string& f) {
			struct stat st;
			int error = stat(f.c_str(), &st);
			assert(error == 0); // TODO: better error handling
			if (S_ISREG(st.st_mode)) {
				new_filenames.emplace(f);
			} else if (S_ISDIR(st.st_mode)) {
				if (recursive_descent) {
					assert(false);
				} else {
					assert(false);
				}
			} else {
				assert(false); // TODO
			}
		};
		for (const auto& f: filenames) {
			add_f(f);
		}
	}*/

	std::cout << "opening connection & starting transaction..." << std::flush;
	pqxx::connection c{"application_name=wimport"};
	pqxx::transaction<pqxx::repeatable_read, pqxx::read_write> w{c};

	// TODO: config and/or commandline argument?
	w.exec("SET LOCAL work_mem TO '1GB'");
	w.exec("SET LOCAL temp_buffers TO '384MB'");

	// puts on advisory lock on the controller. Will always lock with same
	// controller, may conflict with other locks
	// TODO: find/make 64-bit version of hashtext
	auto lock_controller = [&](const std::string& controller) {
		w.exec("SELECT pg_advisory_xact_lock(hashtext("
			+ w.quote("8cb7b9ca-3269-472d-816f-3212875b9ab2 "
			+ controller) + "))");

	};
	auto try_lock_controller = [&](const std::string& controller) {
		return w.exec("SELECT pg_try_advisory_xact_lock(hashtext("
			+ w.quote("8cb7b9ca-3269-472d-816f-3212875b9ab2 "
			+ controller) + "))").at(0).at(0).as<bool>();
	};
	std::cout << " started" << std::endl;

	std::cout << "Initializing SQL functions..." << std::flush;
	init_sql(w);
	std::cout << " done!" << std::endl;
	
	std::cout << "taking out controller locks..." << std::flush;
	for (const auto& s: only_controllers) {
		if (!try_lock_controller(s)) {
			std::cerr << " Could not lock " << s << ", exiting..." << std::endl;
			exit(1);
		}
	}
	std::cout << " done!" << std::endl;

	/*if (redo_ali_parse_errors) {
		easy_timer tim;
		std::cout << "nulling ali parse errors..." << std::flush;
		w.exec("UPDATE newlog SET data = NULL WHERE data ? 'ali' AND data->'ali' ? 'parse_error'");
		std::cout << " " << tim.seconds() << "s" << std::endl;
	}

	for (const auto& s: redo_newlog_psaps) {
		easy_timer tim;
		std::cout << "redoing newlog processing for " << s << std::endl;
		std::cout << "\tnulling newlog.data..." << std::flush;
		w.exec("UPDATE newlog SET data = NULL WHERE controller = " + w.quote(s));
		std::cout << " " << tim.seconds() << "s" << std::endl;

		tim.start();
		std::cout << "\tdeleting from basic_call_info2..." << std::flush;
		w.exec("DELETE FROM basic_call_info2 WHERE controller = " + w.quote(s));
		std::cout << " " << tim.seconds() << "s" << std::endl;
	}*/

	for (const auto& s: redo_all_psaps) {
		std::cout << "Deleting all for " << s << std::endl;
		std::cout << "\tLocking controller " + s << "..." << std::flush;
		lock_controller(s);
		std::cout << " done!" << std::endl;

		easy_timer tim;

		std::cout << "\tdeleting oldlog..." << std::flush;
		auto affected = w.exec("DELETE FROM oldlog WHERE controller = " + w.quote(s)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;
		
		tim.start();
		std::cout << "\tdeleting newlog..." << std::flush;
		affected = w.exec("DELETE FROM newlog WHERE controller = " + w.quote(s)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;

		tim.start();
		std::cout << "\tdeleting from basic_call_info2..." << std::flush;
		affected = w.exec("DELETE FROM basic_call_info2 WHERE controller =  " + w.quote(s)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;

		tim.start();
		std::cout << "\tdeleting from oldlog_files_processed..." << std::flush;
		affected = w.exec("DELETE FROM oldlog_files_processed WHERE controller = " + w.quote(s)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;

		tim.start();
		std::cout << "\tdeleting from call_transfer..." << std::flush;
		affected = w.exec("DELETE FROM call_transfer WHERE tdata @> json_build_object('controller'," + w.quote(s) + ")::jsonb").affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;

		tim.start();
		std::cout << "\tdeleting from call_info..." << std::flush;
		affected = w.exec("DELETE FROM call_info WHERE cdata @> json_build_object('controller'," + w.quote(s) + ")::jsonb").affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;
	}

	if (delete_for_controller_after_controller != "") {
		std::cout << "Deleting all for " << delete_for_controller_after_controller << " after " << delete_for_controller_after_utc << std::endl;

		std::cout << "\tlocking controller " << delete_for_controller_after_controller << "..." << std::flush;
		lock_controller(delete_for_controller_after_controller);
		std::cout << " done!" << std::endl;

		easy_timer tim;

		const auto& s = delete_for_controller_after_controller;
		const auto& utc = delete_for_controller_after_utc;

		std::cout << "\tdeleting oldlog..." << std::flush;
		auto affected = w.exec("DELETE FROM oldlog WHERE controller = " + w.quote(s) + " AND utc >= " + w.quote(utc)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;
		
		tim.start();
		std::cout << "\tdeleting newlog..." << std::flush;
		affected = w.exec("DELETE FROM newlog WHERE controller = " + w.quote(s) + " AND utc >= " + w.quote(utc)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;

		tim.start();
		std::cout << "\tdeleting from basic_call_info2..." << std::flush;
		affected = w.exec("DELETE FROM basic_call_info2 WHERE controller =  " + w.quote(s) + " AND utc >= " + w.quote(utc)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;

		// for oldlog_files_processed we are assuming that the mtime is also the last utc of the last log in the file (or close)
		// for the next iteration, we should keep a tstzrange of the logs in the file for situations
		// when we need to delete/redo logs for a certain time period
		tim.start();
		std::cout << "\tdeleting from oldlog_files_processed..." << std::flush;
		affected = w.exec("DELETE FROM oldlog_files_processed WHERE controller = " + w.quote(s) + " AND mtime >= " + w.quote(utc)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;

		tim.start();
		std::cout << "\tdeleting from call_transfer..." << std::flush;
		affected = w.exec("DELETE FROM call_transfer WHERE tdata @> json_build_object('controller'," + w.quote(s) + ")::jsonb AND utc >= " + w.quote(utc)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;

		tim.start();
		std::cout << "\tdeleting from call_info..." << std::flush;
		affected = w.exec("DELETE FROM call_info WHERE cdata @> json_build_object('controller'," + w.quote(s) + ")::jsonb AND lower(utc_range) >= " + w.quote(utc)).affected_rows();
		std::cout << " " << tim.seconds() << "s (" << affected << " rows)" << std::endl;
	}

	if (truncate_all) {
		std::cout << "TRUNCATING EVERYTHING" << std::endl;
		w.exec("TRUNCATE oldlog, oldlog_files_processed, newlog, basic_call_info2, call_info, call_transfer");
	} else if (truncate_newlog) {
		std::cout << "TRUNCATING NEWLOG (processed) AND UP" << std::endl;
		w.exec("UPDATE newlog SET data = NULL");
		w.exec("TRUNCATE basic_call_info2, call_info, call_transfer");
	}

	/*if (redo_oldlog_newlog) {
		w.exec(R"pgsql(
UPDATE newlog SET
	controller = f->>'controller',
	utc = (f->>'utc')::timestamptz,
	code = f->>'code',
	orig = wjson_del_props(f, '["id","controller","utc","code"]')::jsonb,
	data = NULL
FROM oldlog, convert_oldlog(oldlog.id, oldlog.controller, oldlog.utc, oldlog.code, callid, trunk, pos, ani, pani, thread, message) f
WHERE newlog.id = oldlog.id)pgsql");
	}

	if (redo_oldlog_parse_errors) {
		std::cout << "redoing oldlog-newlog conversion for data with parse errors" << std::endl;
		w.exec(R"pgsql(
UPDATE newlog SET
	controller = f->>'controller',
	utc = (f->>'utc')::timestamptz,
	code = f->>'code',
	orig = wjson_del_props(f, '["id","controller","utc","code"]')::jsonb,
	data = NULL
FROM oldlog, convert_oldlog(oldlog.id, oldlog.controller, oldlog.utc, oldlog.code, callid, trunk, pos, ani, pani, thread, message) f
WHERE newlog.id = oldlog.id AND data ? 'parse_error')pgsql");
	}*/


	easy_timer all_files_time;
	for (auto& full_file: filenames) {
		easy_timer this_file_time;

		std::string bname = westtel::util::str_basename(full_file);

		std::cout << bname << ": " << std::flush;
		boost::lockfree::spsc_queue<boost::optional<OldLog>, boost::lockfree::capacity<16384>> to_insert;

		struct stat stat_struct;
		const int error = stat(full_file.c_str(), &stat_struct);
		if (error != 0) {
			switch(error) {
			case ENOENT:
				std::cerr << "file " << full_file << " does not exist" << std::endl;
				break;
			default:
				std::cerr << "could not stat file " << full_file << std::endl;
				assert(false);
				break;
			}
			exit(1);
		}

		struct tm* utc = gmtime(&stat_struct.st_mtime);
		char utc_str[50];
		strftime(utc_str, sizeof utc_str, "%F %T", utc);

		if (w.exec("SELECT 1 FROM oldlog_files_processed WHERE filename = " + w.quote(bname)
			+ " AND mtime = " + w.quote(utc_str) + "::timestamptz AND size = " + w.quote(stat_struct.st_size)).size() == 1) {
			std::cout << "skipped" << std::endl;
			continue;
		} else {
			std::cout << "processing... " << stat_struct.st_size / (1024 * 1024.0) << "MiB" << std::endl;
		}


		const boost::regex rbname{R"((..-...-.)[-_].*\.log)"};
		boost::smatch mbname;
		std::string current_controller = "";
		if (boost::regex_match(bname, mbname, rbname)) {
			current_controller = mbname[1].str();
		} else {
			std::cout << "unknown filename '" << bname << "', skipping " << std::endl;
			continue;
		}
		assert(current_controller != "");

		// THIS IS A BIG ASSUMPTION/ASSERTION HERE
		// WE CAN NOT CALL WIMPORT ON THE SAME CONTROLLER MULTIPLE TIMES AT ONCE
		// THE REST OF THE CODE ASSUMES THAT, AND IT'S PRETTY MUCH IMPOSSIBLE
		// TO ASSUME OTHERWISE IN THE GENERAL CASE (POPULATING LOGINS AND SUCH)
		std::cout << "\tGetting advisory lock on " << current_controller << "..." << std::flush;
		lock_controller(current_controller);
		only_controllers.emplace(current_controller);
		std::cout << " done!" << std::endl;
		assert(only_controllers.size() == 1);

		// will take control of database connection for life of thread
		// i.e., w & c are owned by thread now
		std::thread write_db{[&w, &to_insert]() {
			bool done = false;
			std::array<boost::optional<OldLog>, 128> buffer;
			std::string query;
			while (!done) {
				size_t len = to_insert.pop(buffer.data(), buffer.size());
				if (len == 0) { usleep(1000); continue; }

				query.clear();
				for (size_t i = 0; i < len; i++) {
					if (!buffer[i]) { done = true; break; }
					query += "(" + w.quote(buffer[i]->psap)
					      + "," + w.quote(buffer[i]->utc())
					      + "," + w.quote(buffer[i]->code)
					      + "," + w.quote(buffer[i]->callid)
					      + "," + w.quote(buffer[i]->trunk)
					      + "," + w.quote(buffer[i]->position)
					      + "," + w.quote(buffer[i]->ani)
					      + "," + w.quote(buffer[i]->pani)
					      + "," + w.quote(buffer[i]->thread)
					      + "," + w.quote(buffer[i]->message)
					      + "),";
				}

				// remove last comma
				if (query.size() == 0) continue;
				query.resize(query.size() - 1, ';');

				//w.exec("INSERT INTO oldlog_temp VALUES" + query);
				w.exec("SELECT pg_temp.insert_oldlog(psap,utc::timestamptz,code,callid,trunk::int,position::int,ani,pani,thread,message)FROM(VALUES"
					+ query + ")AS t(psap,utc,code,callid,trunk,position,ani,pani,thread,message)");
			}
		}};

		boost::optional<OldLog> log;
		LogStream reader{std::unique_ptr<std::ifstream>{
			new std::ifstream{full_file}}, bname};
		reader.set_preserve_newlines();

		do {
			try {
				log = reader.next();
				// TODO: skip log if controller doesn't match
				if (log) {
					assert(log->psap == current_controller);
					if (!log->callid && log->code != 717 && log->code != 709) 
						continue;
				}
			} catch(std::exception& e) {
				std::cout << "corruption, skipping: " << e.what() << std::endl;
				continue;
			} catch(...) {
				std::cout << "corruption, skipping" << std::endl;
				continue;
			}
			while (!to_insert.push(log))
				usleep(100);
		} while(log);

		write_db.join(); // we have w and c back

		w.exec("SELECT pg_temp.upsert_oldlog_files_processed("
			+ w.quote(bname) + ","
			+ w.quote(current_controller) + ","
			+ w.quote(utc_str) + "::timestamptz,"
			+ w.quote(stat_struct.st_size) + ")");

		struct stat stat_new;
		const int err = stat(full_file.c_str(), &stat_new);
		assert(err == 0);
		if (stat_new.st_mtime != stat_struct.st_mtime || stat_new.st_size != stat_struct.st_size) {
			// TODO: subtransaction, retry
			std::cerr << "file changed while reading, aborting..." << std::endl;
			exit(1);
		}

		const double time = this_file_time.seconds();
		std::cout << "\t" << time << "s" << std::endl;
		std::cout << "\t" << stat_struct.st_size / (1024 * time) << " KiB/s" << std::endl;
	}
	std::cout << "all files: " << all_files_time.seconds() << "s" << std::endl;
	
	const std::string controller_any_string = [&]() {
		if (only_controllers.size() == 1)
			return w.quote(*only_controllers.begin());
		else {
			assert(only_controllers.size() == 0);
			return std::string{"ANY(ARRAY[]::text[])"};
		}
	}();
	std::cout << "controller_any_string: " << controller_any_string << std::endl;

	/*timer.start();
	std::cout << "start log processing..." << std::flush;
	w.exec("ANALYZE oldlog_temp");
	w.exec("SELECT pg_temp.insert_oldlog(controller,utc,code,callid,trunk,pos,ani,pani,thread,message) FROM oldlog_temp");
	w.exec("DROP TABLE oldlog_temp");
	std::cout << " " << timer.seconds() << "s" << std::endl;*/

	timer.start();
	std::cout << "analyzing oldlog_callids_to_fix..." << std::flush;
	w.exec("ANALYZE oldlog_callids_to_fix");
	std::cout << " " << timer.seconds() << "s (~"
		<< w.exec("SELECT COUNT(*) FROM oldlog_callids_to_fix").at(0).at(0).as<intmax_t>()
		<< " calls)" << std::endl;

	timer.start();
	std::cout << "analyzing oldlog_callids_to_redo..." << std::flush;
	w.exec("ANALYZE oldlog_callids_to_redo");
	std::cout << " " << timer.seconds() << "s (~"
		<< w.exec("SELECT COUNT(*) FROM oldlog_callids_to_redo").at(0).at(0).as<intmax_t>()
		<< " calls)" << std::endl;

	{
		timer.start();
		std::cout << "redo some log processing..." << std::flush;
		const std::string query =
R"pgsql(SELECT pg_temp.redo_oldlog(newlog.id)
FROM oldlog_callids_to_redo
JOIN newlog
ON oldlog_callids_to_redo.callid = newlog.call_id
WHERE newlog.controller =)pgsql" + controller_any_string;
		if (print_explain)
			std::cout << "\nEXPLAIN: "
				  << w.exec("EXPLAIN (FORMAT JSON) " + query).at(0).at(0).as<std::string>()
				  << std::endl;
		w.exec(query, "redo log processing");
		std::cout << " " << timer.seconds() << "s" << std::endl;
	}

	easy_timer oldlog_fix_timer;
	std::cout << "fixing oldlog calls..." << std::flush;
	if (only_controllers.size() > 0) {
		const std::string query =
R"pgsql(SELECT pg_temp.fix_call(
	json_agg(pg_temp.wjson_object_merge(orig::json,
		json_build_object('id',id,'controller',controller,'utc',utc,'code',code))
		ORDER BY utc))
FROM (newlog JOIN oldlog_callids_to_fix ON (newlog.call_id) = oldlog_callids_to_fix.callid
	AND newlog.controller = )pgsql" + controller_any_string + R"pgsql()
GROUP BY newlog.call_id)pgsql";
		if (print_explain)
			std::cout << "\nEXPLAIN: "
			          << w.exec("EXPLAIN (FORMAT JSON) " + query).at(0).at(0).as<std::string>()
			          << std::endl;
	       	w.exec(query, "fixing oldlog calls");
	}
	std::cout << " " << oldlog_fix_timer.seconds() << "s" << std::endl;

	std::string mutc = "";
	{
		easy_timer newlog_enhancement_timer2;
		std::cout << "figuring out newlog enhancement..." << std::flush;
		const std::string query = R"(SELECT min(utc) AS mutc FROM newlog WHERE data IS NULL AND controller = )" + controller_any_string;
		if (print_explain)
			std::cout << "\nEXPLAIN: "
			          << w.exec("EXPLAIN (FORMAT JSON) " + query).at(0).at(0).as<std::string>()
			          << std::endl;
		const pqxx::result result2 = w.exec(query);
		std::cout << " " << newlog_enhancement_timer2.seconds() << "s" << std::endl;

		assert(result2.size() <= 1);
		if (result2.size() == 1 && !result2.at(0).at(0).is_null()) {
			const auto& i = result2.at(0);
			easy_timer tim;
			mutc = i.at(result2.column_number("mutc")).as<std::string>();
			const std::string controller = controller_any_string;
			std::cout << "\tsecond pass..." << std::flush;
			w.exec("SELECT pg_temp.newlog_processed_second_pass(" + controller + "," + w.quote(mutc) + ")");
			std::cout << " " << tim.seconds() << "s" << std::endl;
		}
		std::cout << "mutc: " << mutc << std::endl;
	}

	timer.start();
	std::cout << "analyzing oldlog_callids_to_fix..." << std::flush;
	w.exec("ANALYZE oldlog_callids_to_fix");
	std::cout << " " << timer.seconds() << "s (~"
		<< w.exec("SELECT COUNT(*) FROM oldlog_callids_to_fix").at(0).at(0).as<intmax_t>()
		<< " calls)" << std::endl;

	if (mutc != "") {
		timer.start();
		std::cout << "ALI and other late parsing..." << std::flush;
		w.exec(R"pgsql(UPDATE newlog SET data = pg_temp.wjson_del_props(pg_temp.newlog_late_parse(orig::json),'["id","controller","utc","code"]')::jsonb WHERE utc >= )pgsql" + w.quote(mutc) + "::timestamptz AND controller = " + controller_any_string, "EXEC newlog_late_parse");
		std::cout << " " << timer.seconds() << "s" << std::endl;
	}

	if (redo_ali_parse_errors) {
		timer.start();
		std::cout << "Redoing ALI parse errors..." << std::flush;
		auto rows = w.exec(R"pgsql(DELETE FROM oldlog_callids_to_fix USING newlog WHERE newlog.call_id = callid AND data ? 'ali' AND data->'ali' ? 'parse_error' AND controller = )pgsql" + controller_any_string).affected_rows();
		auto inserted = w.exec(R"pgsql(INSERT INTO oldlog_callids_to_fix SELECT DISTINCT call_id FROM newlog WHERE data ? 'ali' AND data->'ali' ? 'parse_error' AND controller = )pgsql" + controller_any_string).affected_rows();
		std::cout << " (" << inserted << " calls)..." << std::flush;
		w.exec(R"pgsql(UPDATE newlog SET data = pg_temp.wjson_del_props(pg_temp.newlog_late_parse(orig::json),'["id","controller","utc","code"]')::jsonb WHERE data ? 'ali' AND data->'ali' ? 'parse_error' AND controller = )pgsql" + controller_any_string, "EXEC fix parse_error");
		std::cout << " " << timer.seconds() << "s" << std::endl;
	}
	
	timer.start();
	std::cout << "Collecting call info..." << std::flush;
	if (only_controllers.size() > 0) {
		std::string query = 
R"pgsql(SELECT pg_temp.update_call(
	json_agg(pg_temp.wjson_object_merge(data::json,
		json_build_object('id',id,'controller',controller,'utc',utc,'code',code))
		ORDER BY utc))
	FROM (newlog JOIN oldlog_callids_to_fix ON newlog.call_id = oldlog_callids_to_fix.callid
		AND newlog.controller = )pgsql" + controller_any_string + R"pgsql()
GROUP BY newlog.call_id)pgsql";
		
		if (print_explain)
			std::cout << "\n\tEXPLAIN: " << w.exec("EXPLAIN (FORMAT JSON) " + query).at(0).at(0).as<std::string>() << std::endl;
		w.exec(query, "call updating");
	}
	std::cout << " " << timer.seconds() << "s" << std::endl;

	if (set_ndistinct) {
		// n_distinct is needed on the call_id column because PostgreSQL
		// thinks that the number of distinct call_id in newlog is NOT
		// proportional to the size of newlog, and so its estimate
		// is *very* low.
		timer.start();
		std::cout << "Getting distinct counts..." << std::flush;
		// TODO: use TABLESAMPLE or similar (PostgreSQL 9.5)
		auto res = w.exec("SELECT COUNT(DISTINCT call_id) AS dis, COUNT(*) AS cnt FROM newlog");
		std::cout << " " << timer.seconds() << "s" << std::endl;

		auto dis = res.at(0).at(res.column_number("dis")).as<intmax_t>();
		auto cnt = res.at(0).at(res.column_number("cnt")).as<intmax_t>();

		double n_distinct = static_cast<double>(dis) / cnt;
		assert(n_distinct > 0 && n_distinct <= 1.0); // its a proportion
		assert(std::isfinite(n_distinct));

		std::array<char, 128> str; // we need ~13 characters, use an order of magnitude more just in case
		snprintf(str.data(), str.size(), "-%.10f", static_cast<double>(n_distinct));
		// n_distinct is negative to signal PostgreSQL to use it as a proportion and not an absolute number of distinct elements

		std::cout << "\tn_distinct: " << dis << " / " << cnt << " ~= "
		          << str.data() << std::endl;

		// we DON'T use w.quote() here because this PostgreSQL command is different and doesn't accept normal expressions
		w.exec("ALTER TABLE newlog ALTER COLUMN call_id SET (n_distinct = " + std::string{str.data()} + ")");

		// we analyze newlog so new n_distinct will take effect
		// PostgreSQL ANALYZE uses a TABLESAMPLE equivalent, so should take constant time
		timer.start();
		std::cout << "\tanalyzing newlog..." << std::flush;
		w.exec("ANALYZE newlog");
		std::cout << " " << timer.seconds() << "s" << std::endl;
	}

	timer.start();
	if (no_commit)
		std::cout << "DID NOT COMMIT" << std::endl;
	else {
		std::cout << "commiting..." << std::flush;
		w.commit();
		std::cout << " " << timer.seconds() << "s" << std::endl;
	}
	return 0;
}
