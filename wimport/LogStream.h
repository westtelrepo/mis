#ifndef WESTTEL_LOGSTREAM_HEADER
#define WESTTEL_LOGSTREAM_HEADER

#include "log_util.h"
#include <boost/optional.hpp>
#include <istream>
#include <memory>

class LogStream {
private:
	struct Line {
		std::string psap;
		std::string utc;
		int32_t code; // TODO: should probably become a string
		std::string callid;
		std::string trunk;
		std::string position;
		std::string ani;
		std::string pani;
		std::string thread;
		std::string direction;
		std::string message;

		Line(const std::string& line);
	};	

	std::unique_ptr<std::istream> in_;
	const std::string filename_;

	bool peeking_{false};
	boost::optional<OldLog> the_peek_{};
	
	int64_t line_counter_{0};
	int64_t entry_counter_{0};

	const std::set<int32_t> newline_codes{361};

	boost::optional<OldLog> next_{};

	// internal line-stream for peeking
	// TODO: refactor out
	bool line_peeking_{false};
	boost::optional<std::string> line_peek_{};

	boost::optional<std::string> read_line();
	boost::optional<std::string> peek_line();

	bool preserve_newlines_ = false; // TODO: refactor to always true
public:
	LogStream(std::unique_ptr<std::istream> in, std::string filename);

	void set_preserve_newlines() { preserve_newlines_ = true; }

	// return nullptr on EOF
	boost::optional<OldLog> peek();
	boost::optional<OldLog> next();
};
#endif
