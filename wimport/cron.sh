pushd ~/westtel_reports/wimport

PSAPS=`cat cron_config.txt`
for p in ${PSAPS}
do
	time ./wimport --controller $p ~/PSAP/$p/incoming/logs/911/$p*.log
done
popd
