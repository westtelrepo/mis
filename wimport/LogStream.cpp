#include "LogStream.h"
#include "westtel/json.h"
#include "westtel/util.h"
#include <algorithm>
#include <boost/regex.hpp>

LogStream::LogStream(std::unique_ptr<std::istream> in, std::string filename)
: in_{std::move(in)}, filename_{std::move(filename)} {

}

boost::optional<OldLog> LogStream::peek() {
	if (peeking_) return the_peek_;
	else {
		the_peek_ = next();
		peeking_ = true;
		return the_peek_;
	}
}

boost::optional<OldLog> LogStream::next() {
	if (peeking_) {
		peeking_ = false;
		return the_peek_;
	}
	if ( !(*in_) )
		return boost::none;

	boost::optional<std::string> line_raw = read_line();

	if (!line_raw)
		return boost::none;
	assert(line_raw);

	OldLog ret;
	const Line first{*line_raw};

	auto follows = [](const Line& f, const Line& s) {
		return f.psap == s.psap && f.utc == s.utc && f.code == s.code
		    && s.callid == "" && s.trunk == "" && s.position == ""
		    && s.ani == "" && s.pani == "" && s.thread == ""
		    && s.direction == "";
	};

	ret.psap = first.psap;
	ret.set_utc(first.utc);
	ret.code = first.code;
	ret.callid = first.callid == ""
		? boost::optional<int64_t>{} 
		: westtel::string_to_int<int64_t>(first.callid);

	if (first.trunk == "")
		ret.trunk = boost::none;
	else if (first.trunk[0] == 'T')
		ret.trunk = westtel::string_to_int<int32_t>(first.trunk.substr(1));
	else
		throw std::runtime_error{"invalid trunk format: " + first.trunk};

	if (first.position == "")
		ret.position = boost::none;
	else if (first.position[0] == 'P')
		ret.position = westtel::string_to_int<int32_t>(first.position.substr(1));
	else
		throw std::runtime_error{"invalid position format: " + first.position};

	ret.ani = westtel::emptystr_null(first.ani);
	ret.pani = westtel::emptystr_null(first.pani);
	ret.thread = westtel::emptystr_null(first.thread);
	ret.direction = westtel::emptystr_null(first.direction);
	ret.message = first.message;

	ret.filename = filename_;
	ret.entry = entry_counter_++;
	ret.linenum = line_counter_;

	while(true) {
		auto line = peek_line();
		if (!line) break;
		if (std::count(line->begin(), line->end(), '|') == 0) {
			if (preserve_newlines_)
				ret.message += "\n";
			ret.message += *line;
			read_line();
			continue;
		}
		Line n{*line};
		if (!follows(first, n)) break;
		if (preserve_newlines_ || newline_codes.count(n.code))
			ret.message += "\n";
		ret.message += n.message;
		read_line();
	}

	return ret;
}

boost::optional<std::string> LogStream::peek_line() {
	if (line_peeking_) {
		return line_peek_;
	}
	else {
		line_peek_ = read_line();
		line_peeking_ = true;
		return line_peek_;
	}
}

boost::optional<std::string> LogStream::read_line() {
	if (line_peeking_) {
		line_peeking_ = false;
		return line_peek_;
	} else if ( !(*in_) ) {
		return boost::optional<std::string>{};
	}

	static std::string ret;
	std::getline(*in_, ret);
	line_counter_++;
	if (ret[ret.size() - 1] == '\r')
		ret.resize(ret.size() - 1);

	if (ret == "" && !(*in_) ) {
		return boost::none;
	}

	for (auto ch: ret)
		if (static_cast<unsigned char>(ch) > 127 || ch == 0)
			throw std::runtime_error{
			      "Invalid non-ascii character in file "
			      + filename_};
	return ret;
}

#if 0
LogStream::Line::Line(const std::string& line) {
	std::vector<std::string> params
		= westtel::split(line, '|', 12);
	if (params.size() != 12) {
		throw std::runtime_error{"Line doesn't have 12 fields: "
			+ line};
	}

	psap = westtel::trim(params[0]);
	
	utc = westtel::trim(params[1]);
	boost::regex r{R"(([0-9]{4}[-/][0-9]{2}[-/][0-9]{2} )"
		R"([0-9]{2}:[0-9]{2}:[0-9]{2}\.[0-9]{3})(?: UTC)?)"};
	boost::smatch m;
	if (boost::regex_match(utc, m, r)) {
		assert(m.size() == 2);
		utc = m[1].str();
		// bring it closer to ISO format
		std::replace(utc.begin(), utc.end(), '/', '-');
	} else {
		throw std::runtime_error{"Invalid utc(" + utc + ") in line "
			+ line};
	}

	code = westtel::string_to_int<int32_t>(westtel::trim(params[3]));
	callid = westtel::trim(params[4]);
	trunk = westtel::trim(params[5]);
	position = westtel::trim(params[6]);
	ani = westtel::trim(params[7]);
	pani = westtel::trim(params[8]);
	thread = westtel::trim(params[9]);
	direction = westtel::trim(params[10]);
	message = params[11].substr(1);
}
#endif

static const boost::regex whole_line{
	R"((\u{2}-\d{3}-1) \| )" // PSAP name
	R"((\d{4}[-/]\d{2}[-/]\d{2} \d{2}:\d{2}:\d{2}.\d{3})(?: UTC)? \| )" // UTC
	R"(\w* \d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2} \w* \| )" // local time (unused)
	R"((\d+) \| )" // code
	R"(( *| *\d+ *) \| )" // callid
	R"((T\d*| *) \| )" // trunk number
	R"((P\d*| *) \| )" // position number
	R"(( *| *[^|]+ *) \| )" // ani
	R"(( *| *p\d+ *) \| )" // pani
	R"(([^|]*) \| )" // thread
	R"(([^|]*) \| )" // direction
	R"((.*))" // comment
};

LogStream::Line::Line(const std::string& line) {
	boost::smatch m;
	if (boost::regex_match(line, m, whole_line)) {
		assert(m.size() == 12);
	} else {
		throw std::runtime_error{"Invalid line: " + line};
	}

	psap = westtel::trim(m[1].str());
	utc = westtel::trim(m[2].str());
	std::replace(utc.begin(), utc.end(), '/', '-');

	code = westtel::string_to_int<int32_t>(westtel::trim(m[3].str()));
	callid = westtel::trim(m[4].str());
	trunk = westtel::trim(m[5].str());
	position = westtel::trim(m[6].str());
	ani = westtel::trim(m[7].str());
	pani = westtel::trim(m[8].str());
	thread = westtel::trim(m[9].str());
	direction = westtel::trim(m[10].str());
	message = m[11].str();
}
